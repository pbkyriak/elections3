#!/bin/bash
start_time=`date +%s`

rm -rf web/uploads/elections/*
app/console doctrine:database:drop --force
app/console doctrine:database:create
app/console doctrine:schema:create
app/console doctrine:fixtures:load
rm app/cache/dev/elections/*
app/console elections:import:mappoints web/uploads/initial/dimoi-fill-points.csv steraia-ellada2
app/console elections:import:syndiasmos-colors perifereiakes_2014 web/uploads/initial/syndiasmoi-colors.csv

# import dimotikon
app/console elections:import dimotikes_2014 eyrytania  web/uploads/initial/dimotikes/eyrytania/pste_dimot_evrytania_2014.txt --show-bar
app/console elections:import dimotikes_2014 fokida   web/uploads/initial/dimotikes/fokida/pste_dimot_foki_2014.txt --show-bar
app/console elections:import dimotikes_2014 eyvoia  web/uploads/initial/dimotikes/eyvoia/pste_dimot_evboia_2014.txt --show-bar
app/console elections:import dimotikes_2014 voiotia  web/uploads/initial/dimotikes/viotia/pste_dimot_viotia_2014.txt --show-bar
app/console elections:import dimotikes_2014 fthiotida  web/uploads/initial/dimotikes/fthiotida/dimot_2014_pste_export.txt --show-bar

# import periferiakon
app/console elections:import perifereiakes_2014 eyrytania web/uploads/initial/perifereiakes/eyrytania/pste_perif_evrytania_2014.txt --show-bar
app/console elections:import perifereiakes_2014 fokida   web/uploads/initial/perifereiakes/fokida/pste_perif_foki_2014.txt --show-bar
app/console elections:import perifereiakes_2014 eyvoia  web/uploads/initial/perifereiakes/eyvoia/pste_perif_evboia_2014.txt --show-bar
app/console elections:import perifereiakes_2014 voiotia  web/uploads/initial/perifereiakes/viotia/pste_perif_viotia_2014.txt --show-bar
app/console elections:import perifereiakes_2014 fthiotida  web/uploads/initial/perifereiakes/fthiotida/perif_2014_pste_export.txt --show-bar

end_time=`date +%s`
echo Total execution time was `expr $end_time - $start_time` s.