<?php

namespace Slx\MediaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Οργάνωση")
 * @Breadcrumb("media.edit.media", route="slx_media")
 * @Security("has_role('ROLE_ADMIN')")
 * @CurrentMenuItem("slx_media")
 */
class MediaController extends Controller
{

    public function indexAction()
    {
        $form = $this->getUploadForm();
        $repo = new \Slx\MediaBundle\Lib\MediaRepository();
        $images = $repo->getImageList();
        return $this->render('SlxMediaBundle:Media:index.html.twig',
                array(
                'form' => $form->createView(),
                'images' => $images
                )
        );
    }

    public function uploadAction()
    {
        $form = $this->getUploadForm();
        $msg = null;
        $request = $this->getRequest();
        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                $img = $form['mediafile']->getData();
                if ($img instanceof UploadedFile) {

                    $media = new \Slx\MediaBundle\Lib\MediaImage();
                    if ($media->isMediaValid($img)) {
                        $media->moveToUploadedMedia($img);
                        $msg = 'success';
                    } else {
                        $msg = $media->getError();
                    }
                } else {
                    $msg = 'media.edit.noFileUploaded';
                }
            }
        }
        return $this->render('SlxMediaBundle:Media:upload.html.twig',
                array('form' => $form->createView(), 'msg' => $msg));
    }

    private function getUploadForm()
    {
        $filterData = array();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData,
                array('method' => 'GET', 'csrf_protection' => false))
            ->add(
                'mediafile', 'file',
                array('required' => true, 'label' => 'File', 'attr' => array('class' => 'default'))
            )
            ->getForm();
        return $form;
    }

    public function deleteAction($name)
    {
        $repo = new \Slx\MediaBundle\Lib\MediaRepository();

        if( $repo->deleteMedia($name) ) {
            $this->get('session')->getFlashBag()->add('info',
                'media.edit.record_deleted');
        }
        else {
            $this->get('session')->getFlashBag()->add('notice',
                'media.edit.record_not_deleted');
        }
        return $this->redirect($this->generateUrl('slx_media'));
    }

}
