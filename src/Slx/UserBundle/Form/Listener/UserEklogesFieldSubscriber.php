<?php

namespace Slx\UserBundle\Form\Listener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Description of UserEklogesFieldSubscriber
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class UserEklogesFieldSubscriber implements EventSubscriberInterface {

    private $electionWidget;
    private $regionWidget;
    
    public function __construct($electionWidget='welection', $regionWidget='wregion') {
        $this->electionWidget = $electionWidget;
        $this->regionWidget = $regionWidget;
    }
    
    public static function getSubscribedEvents() {
        return array(
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit'
        );
    }

    public function preSetData(FormEvent $event) {
        $data = $event->getData();
        $form = $event->getForm();

        if (null === $data) {
            return;
        }

        $accessor = PropertyAccess::createPropertyAccessor();

        $election = $accessor->getValue($data, $this->electionWidget);
        $electionId = ($election) ? $election->getId() : null;

        $this->addRegionForm($form, $electionId);
    }

    public function preSubmit(FormEvent $event) {
        $data = $event->getData();
        $form = $event->getForm();

        $electionId = array_key_exists($this->electionWidget, $data) ? $data[$this->electionWidget] : null;

        $this->addRegionForm($form, $electionId);
    }

    private function addRegionForm($form, $electionId) {
        $form
            ->add($this->regionWidget, 'entity',
                array(
                'class' => 'GnosisElectionsBaseBundle:RegionNomos',
                'property' => 'title',
                'label' => 'Νομός εργασίας',
                'required' => false,
                'empty_value' => '(Ολοι)',
                'query_builder' => function ($repository) use ($electionId) {
                                $qb = $repository->createQueryBuilder('r')
                                    ->innerJoin('r.election', 'e')
                                    ->where('e.id = :eid')
                                    ->setParameter('eid', $electionId)
                                ;

                                return $qb;
                            }

                ));
    }
}
