<?php

namespace Slx\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Slx\UserBundle\Form\Listener\UserEklogesFieldSubscriber;

/**
 * Description of ChangeContextType
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ChangeContextType  extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            ->add('welection', 'entity',
                array(
                'class' => 'GnosisElectionsBaseBundle:Ekloges',
                'property' => 'title',
                'label' => 'Τρέχουσες Εκλογές εργασίας',
                'required' => false,
                'empty_value' => '(Ολες)'
                )
            )
            ->add('wregion', 'entity',
                array(
                'class' => 'GnosisElectionsBaseBundle:RegionNomos',
                'property' => 'title',
                'label' => 'Τρέχον Νομός εργασίας',
                'required' => false,
                'empty_value' => '(Ολοι)'
                ))
            ->add('welection_new', 'entity',
                array(
                'class' => 'GnosisElectionsBaseBundle:Ekloges',
                'property' => 'title',
                'label' => 'Νέες Εκλογές εργασίας',
                'required' => false,
                'empty_value' => '(Ολες)'
                )
            )
            ->add('wregion_new', 'entity',
                array(
                'class' => 'GnosisElectionsBaseBundle:RegionNomos',
                'property' => 'title',
                'label' => 'Νέος Νομός εργασίας',
                'required' => false,
                'empty_value' => '(Ολοι)'
                ))
            ->add('save', 'submit', array('label'=>'users.user.change','attr'=>array('class'=>'btn blue')))
            ->addEventSubscriber(new UserEklogesFieldSubscriber())
            ->addEventSubscriber(new UserEklogesFieldSubscriber('welection_new', 'wregion_new'))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Slx\UserBundle\Model\ChangeContext'
        ));
    }

    public function getName()
    {
        return 'changecontext';
    }
}
