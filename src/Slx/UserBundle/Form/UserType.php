<?php

namespace Slx\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityManager;
use Slx\UserBundle\Form\Listener\UserEklogesFieldSubscriber;

class UserType extends AbstractType
{
    
    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $genders = array(
            'male' => 'Αρρεν',
            'female' => 'Θήλυ',
        );

        $builder
            ->add('username')
            ->add('email')
            ->add('fullname', null, array('label'=> 'users.user.fullname'))
            ->add('fathername', null, array('label'=> 'users.user.fathername', 'required'=>false))
            ->add('klados', null, array('label'=> 'users.user.klados', 'required'=>false))
            ->add('bathmos', null, array('label'=> 'users.user.bathmos', 'required'=>false))
            ->add('gender', 'choice', array('label'=>'users.user.gender','choices'=>$genders,'required'=>false,))
            ->add('isActive', null, array('required'=>false))
            ->add('userType', 'choice', array('label'=>'users.user.userType', 'choices'=>array(0=>'Χρήστης', 1=>'Διαχειριστής', 2=>'Maintenance')))
            ->add('roles', null, array('label'=>'users.user.roles', 'multiple'=>true))
            ->add('welection', 'entity',
                array(
                'class' => 'GnosisElectionsBaseBundle:Ekloges',
                'property' => 'title',
                'label' => 'Εκλογές εργασίας',
                'required' => false,
                'empty_value' => '(Ολες)'
                )
            )
            ->add('wregion', 'entity',
                array(
                'class' => 'GnosisElectionsBaseBundle:RegionNomos',
                'property' => 'title',
                'label' => 'Νομός εργασίας',
                'required' => false,
                'empty_value' => '(Ολοι)'
                ))
            ->add('save', 'submit', array('label'=>'gsprod.general.save','attr'=>array('class'=>'btn blue')))
            ->add('saveAndAdd', 'submit', array('label'=>'gsprod.general.save_and_add','attr'=>array('class'=>'btn blue')))
            ->addEventSubscriber(new UserEklogesFieldSubscriber())
        ;
//        $builder->get('welection')->getOption('as');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Slx\UserBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'slx_userbundle_usertype';
    }
}
