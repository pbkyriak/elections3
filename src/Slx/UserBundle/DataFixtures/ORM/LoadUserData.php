<?php

namespace Slx\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Slx\UserBundle\Entity\User;
use Slx\UserBundle\Entity\Role;
use Slx\UserBundle\Security\User\SlxUser;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{
   /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $role1 = new Role();
        $role1->setName('Super Admin')->setRole('ROLE_SUPER_ADMIN');
        $manager->persist($role1);
        $role2 = new Role();
        $role2->setName('Χρήστης')->setRole('ROLE_USER');
        $manager->persist($role2);
        $role3 = new Role();
        $role3->setName('Admin')->setRole('ROLE_ADMIN');
        $manager->persist($role3);
        $manager->flush();
        
        $factory = $this->container->get('security.encoder_factory');
        $user = new User();
        $user->setUsername('panos');
        $user->setEmail('panos@salix.gr');
        $encoder = $factory->getEncoder(new SlxUser('', '', '', array()));
        $password = $encoder->encodePassword('panos', $user->getSalt());
        $user->setPassword($password);
        $user->setIsActive(true)->setUserType(2);
        $user->addRole($role1);
        $manager->persist($user);
        $manager->flush();
    }

}
