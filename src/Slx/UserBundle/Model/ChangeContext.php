<?php
namespace Slx\UserBundle\Model;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Description of ChangeContext
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ChangeContext {

    /**
     *
     * @Assert\NotBlank
     */
    private $welection=0;
    
    /**
     *
     * @Assert\NotBlank
     */
    private $wregion=0;

    /**
     *
     * @Assert\NotBlank
     */
    private $welection_new=0;
    
    /**
     *
     * @Assert\NotBlank
     */
    private $wregion_new=0;

    function getWelection() {
        return $this->welection;
    }

    function getWregion() {
        return $this->wregion;
    }

    function getWelectionNew() {
        return $this->welection_new;
    }

    function getWregionNew() {
        return $this->wregion_new;
    }

    function setWelection($welection) {
        $this->welection = $welection;
        return $this;
    }

    function setWregion($wregion) {
        $this->wregion = $wregion;
        return $this;
    }

    function setWelectionNew($welection_new) {
        $this->welection_new = $welection_new;
        return $this;
    }

    function setWregionNew($wregion_new) {
        $this->wregion_new = $wregion_new;
        return $this;
    }


}
