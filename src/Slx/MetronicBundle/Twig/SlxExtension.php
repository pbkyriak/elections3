<?php

namespace Slx\MetronicBundle\Twig;

/**
 * Description of SlxExtension
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class SlxExtension extends \Twig_Extension
{

    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('paginator_header_class',
                array($this, 'paginatorHeaderClassFilter')),
            new \Twig_SimpleFilter('boolToText',
                array($this, 'boolToText')),
            new \Twig_SimpleFilter('percTo',
                array($this, 'percTo')),
            new \Twig_SimpleFilter('repeat',
                array($this, 'repeat')),
        );
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('get_class',
                array($this, 'twGetClass')),
            new \Twig_SimpleFunction('file_exists',
                array($this, 'twFileExists')),
        );
    }
    
    public function paginatorHeaderClassFilter($pagination, $fieldName)
    {
        $class = 'sorting';
        $params = $pagination->getParams();
        $direction = '';
        if (in_array('asc', $params))
            $direction = 'asc';
        if (in_array('desc', $params))
            $direction = 'desc';
        if ($pagination->isSorted($fieldName))
            $class = 'sorting_' . $direction;
        return $class;
    }

    public function boolToText($bool) {
        if ($bool)
            return 'Ναι';
        else
            return 'Όχι';
    }
    
    public function percTo($part, $whole) {
        return round(($part/$whole*100),1);
    }
    
    public function repeat($str, $count) {
        if( $count<0 )
            $count=0;
        return str_repeat($str, $count);
    }
    
    public function twGetClass($obj) {
        return get_class($obj);
    }

    public function twFileExists($fn) {
        return file_exists($fn);
    }

    public function getName()
    {
        return 'slx_extension';
    }

}

