var Slx = function() {
  var handleModalConfirm = function() {
    $('.modal-confirm').each(function() {
      var msg = $(this).attr('confirm-message');
      var ref = $(this).attr('href');
      $(this).click(function() {
        //jsLinkConfirm.show(msg, ref);
        BootstrapDialog.confirm(msg, function(result) {
          if (result) {
            window.location = ref;
          }
        });
        return false;
      });

    });
  };

  var handleInputMasks = function() {
    $.extend($.inputmask.defaults, {
      'autounmask': true
    });

    $(".hasDatepicker").inputmask("d/m/y", {autoUnmask: true});  //direct mask   
    $('.colorpicker').colorpicker();
  };

  var handleBatchLists = function() {
    $('.batch-list-container').each(function() {
      var list = $(this);

      $('.group-checkable', list).change(function() {
        var set = $(this).attr("data-set");
        var checked = $(this).is(":checked");
        $(set).each(function() {
          if (checked) {
            $(this).attr("checked", true);
          } else {
            $(this).attr("checked", false);
          }
        });
        jQuery.uniform.update(set);
      });

      $('.batch_actions', list).each(function() {
        var actionsSelect = $(this);
        actionsSelect.change(function() {
          var actionForm = $('.batch_list_form', list);

          if ($(this).val() === 'please') {
            return;
          }

          var postForm = function() {
            actionForm.submit();
          };

          var batchCancel = function() {
            actionsSelect.val('please');
          };

          var msg = $(this).find('option:selected').attr('data-msg');
          var chk = $('.checkboxess', list);
          var checked = 0;
          chk.each(function() {
            if ($(this).attr("checked")) {
              checked++;
            }
          });

          if (checked === 0) {
            $(this).val('please');
            return;
          }
          //jsModalConfirm.init(postForm, batchCancel, msg);
          BootstrapDialog.confirm(msg, function(result) {
            if (result) {
              postForm();
            }
            else {
              batchCancel();
            }
          });

        });
      });
    });
  };

  var handleModalAjax = function() {
    // general settings
    $.fn.modal.defaults.spinner =
            $.fn.modalmanager.defaults.spinner =
            '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar" style="width: 100%;"></div>' +
            '</div>' +
            '</div>';

    $.fn.modalmanager.defaults.resize = true;

    $('[data-toggle="modal"]').on('click', function(e) {
      e.preventDefault();
      var url = $(this).attr('href');
      if (url.indexOf('#') == 0) {
        $(url).modal('open');
      }
      else {
        var $modal = null;
        $('body').modalmanager('loading');
        $.get(url, function(data) {
          $modal = $('<div class="modal fade">' + data + '</div>')
                  .modal()
                  .on('hidden', function() {
                    $('.modal-backdrop.in').each(function(i) {
                      $(this).remove();
                    });
                  });

          $modal.on('click', '.update', function() {
            form = $modal.find('form');
            form.ajaxForm({
              beforeSend: function() {
                $modal.modal('loading');
              },
              complete: function(xhr) {
                $modal.html(xhr.responseText).modal();
              }
            });
            $modal.find('form').submit();
          });
        });
      }
    });

  };
 
  var handleNumberFields = function() {
      $(document).find("[data-validate='positive-number']").on('change',
              function( ev ) {
                  var input = $(ev.target);
                  var v = parseInt(input.val());
                  if(v>=0) {
                    ev.target.setCustomValidity('');
                    input.removeClass("has-error");
                  } else {
                      input.val('');
                      input.addClass("has-error");
                      ev.target.setCustomValidity("Αριθμό μόνο παρακαλώ");
                  }
              }
              );
  }
  
  return {
    init: function() {
      handleModalConfirm();
      handleInputMasks();
      handleModalAjax();
      handleBatchLists();
      handleNumberFields();
    }
  };
}();


    $(document).ready(function(){
        $('#downloads-toggle').click(function(event){
            $('#downloads-content').toggle();
            event.preventDefault();
        });
    });
    