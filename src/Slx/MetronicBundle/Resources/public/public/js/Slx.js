var Slx = function() {
  var handleModalConfirm = function() {
    $('.modal-confirm').each(function() {
      var msg = $(this).attr('confirm-message');
      var ref = $(this).attr('href');
      $(this).click(function() {
        //jsLinkConfirm.show(msg, ref);
        BootstrapDialog.confirm(msg, function(result) {
          if (result) {
            window.location = ref;
          }
        });
        return false;
      });

    });
  };

  var handleCustomSelect = function() {
        $(".custom-select").each(function(){
            $(this).wrap("<span class='select-wrapper'></span>");
            $(this).after("<span class='holder'></span>");
        });
        $(".custom-select").change(function(){
            var selectedOption = $(this).find(":selected").text();
            $(this).next(".holder").text(selectedOption);
        }).trigger('change');
  };

  var handleSubRegionSelector = function() {
    $("#subRegionSelector").change(function(){
      var newLocation = $(this).val();
      if( newLocation!=='no' ) {
		window.location.href=newLocation;
      }
    });
  }
  return {
    init: function() {
      handleModalConfirm();
      handleCustomSelect();
      handleSubRegionSelector();
    }
  };
}();


    $(document).ready(function(){
        $('#downloads-content').hide();
        $('#downloads-toggle').click(function(event){
            var content = $('#downloads-content');
            content.toggle();
            event.preventDefault();
        });
    });
