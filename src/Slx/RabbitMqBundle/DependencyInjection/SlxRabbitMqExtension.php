<?php

namespace Slx\RabbitMqBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;
/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class SlxRabbitMqExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $container->setParameter('slx_rabbit_mq.p_key', RabbitMqConstants::PSIFOI_TRACK_QUEUE);
        $container->setParameter('slx_rabbit_mq.s_key', RabbitMqConstants::STAUROI_LANE_QUEUE);
        $container->setParameter('slx_rabbit_mq.e_key', RabbitMqConstants::ET_LANE_QUEUE);
        $container->setParameter('slx_rabbit_mq.n_key', RabbitMqConstants::NUMBERING_QUEUE);
        $container->setParameter('slx_rabbit_mq.j_key', RabbitMqConstants::JOBS_QUEUE);
        $container->setParameter('slx_rabbit_mq.p_name', $config['psifoi']);
        $container->setParameter('slx_rabbit_mq.s_name', $config['stauroi']);
        $container->setParameter('slx_rabbit_mq.e_name', $config['et']);
        $container->setParameter('slx_rabbit_mq.n_name', $config['numbering']);
        $container->setParameter('slx_rabbit_mq.j_name', $config['jobs']);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }
}
