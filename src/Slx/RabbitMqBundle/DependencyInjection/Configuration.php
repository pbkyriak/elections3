<?php

namespace Slx\RabbitMqBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('slx_rabbit_mq');
        $rootNode->children()
                ->scalarNode('psifoi')->cannotBeEmpty()->defaultValue('psifoi_track')->end()
                ->scalarNode('stauroi')->cannotBeEmpty()->defaultValue('stauroi_track')->end()
                ->scalarNode('et')->cannotBeEmpty()->defaultValue('et_track')->end()
                ->scalarNode('numbering')->cannotBeEmpty()->defaultValue('numbering_track')->end()
                ->scalarNode('jobs')->cannotBeEmpty()->defaultValue('jobs_queue')->end()
            ->end();
        return $treeBuilder;
    }
}
