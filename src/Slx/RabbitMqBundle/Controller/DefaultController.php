<?php

namespace Slx\RabbitMqBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SlxRabbitMqBundle:Default:index.html.twig', array('name' => $name));
    }
}
