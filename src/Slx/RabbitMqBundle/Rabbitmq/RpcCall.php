<?php

namespace Slx\RabbitMqBundle\Rabbitmq;

use PhpAmqpLib\Message\AMQPMessage;
use Slx\RabbitMqBundle\Rabbitmq\Connection as MqConnection;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;

/**
 * Description of RpcCall
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class RpcCall {
    /** @var \Slx\RabbitMqBundle\Rabbitmq\Connection */
    private $mq;
    private $response=null;
    private $callbackQueue;
    private $correlationId;
    
    /**
     * 
     * @param type $mq
     */
    public function __construct(MqConnection $mq) {
        $this->mq = $mq;
        $this->callbackQueue = $this->mq->declareAnonymousQueue();
        $this->mq->setConsumer($this->callbackQueue, array($this, 'onResponse'));
    }
    
    public function onResponse(AMQPMessage $msg) {
        if( $msg->get('correlation_id')==$this->correlationId ) {
            $this->response = $msg->body;
            $this->mq->messageAck($msg);
        }
    }
    
    public function call($body) {
        $this->response = null;
        $this->correlationId = uniqid();

        $msg = $this->mq->createMessage(
                RabbitMqConstants::MSG_PREFIX_RPC.$body,
                array(
                    'correlation_id' => $this->correlationId,
                    'reply_to' => $this->callbackQueue,
                    )
                );
        $this->mq->publishMessage($msg, RabbitMqConstants::NUMBERING_QUEUE);
        while(!$this->response) {
            $this->mq->wait();
        }
        return $this->response;
    }
}
