<?php

namespace Slx\RabbitMqBundle\Rabbitmq;

/**
 * Description of RabbitMqConstants
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class RabbitMqConstants {

    /* rabbitmq queue keys */
    const PSIFOI_TRACK_QUEUE = 'psifoi';
    const STAUROI_LANE_QUEUE = 'stauroi';
    const ET_LANE_QUEUE = 'et';
    const NUMBERING_QUEUE = 'numbering';
    const JOBS_QUEUE = 'jobs';
    
    const MSG_PREFIX_RPC = 'rpc:';
        
}

