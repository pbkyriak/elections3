<?php

namespace Gnosis\ProtokoloBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of ProtokoloEntryType
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ProtokoloEntryType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add('remarks', null, array('label' => 'Παρατηρήσεις', 'required'=>false))
            ->add('save', 'submit', array('label' => 'Αποθήκευση', 'attr' => array('class' => 'btn blue')))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Gnosis\ProtokoloBundle\Entity\ProtokoloEntry'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'gnosis_protokolo_entry';
    }

}
