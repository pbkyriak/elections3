<?php

namespace Gnosis\ProtokoloBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Description of EtSelectType
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class EtSelectType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $etypes = array(
            'psifoi' => 'Ψήφοι',
            'stauroi' => 'Σταυροί'
        );
        
        $builder
                ->add(
                        'et_number', 
                        'text', 
                        array(
                            'required' => true, 
                            'label' => 'elections.psifoi.et',
                            'help' => 'elections.protokolo.et_help'
                            )
                        )
                ->add(
                    'entry_type', 'choice', array(
                    'label' => 'elections.protokolo.entry_type',
                    'choices' => $etypes,
                    'required' => true,
                    )
                )
                ->add('save', 'submit', array('label' => 'Αναζήτηση', 'attr' => array('class' => 'btn blue')))
        ;
    }

    public function getName() {
        return 'et_select_form';
    }

}
