<?php
namespace Gnosis\ProtokoloBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Gnosis\ProtokoloBundle\Form\EtSelectType;
use Gnosis\ProtokoloBundle\Form\ProtokoloEntryType;
use Gnosis\ProtokoloBundle\Entity\ProtokoloEntry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Καταχώρηση")
 * @Breadcrumb("Πρωτόκολλο", route="election_admin_protokolo_etsearch")
 * @CurrentMenuItem("election_admin_protokolo_etsearch")
 * @Security("has_role('ROLE_PROTOK')")
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ProtokoloController extends Controller {
    private $eklogesId;
    private $nomosId;
    private $election;
    private $nomos;
    private $region;
    private $entryExists = false;
    
    public function etSelectAction(Request $request) {
        $form = $this->createForm(new EtSelectType());

        $viewParams = array(
            'form' => $form->createView(),
            'printEt' => false,
        );
        if( $request->get('print', null) ) {
            $viewParams['printEt'] = $request->get('print');
        }
        return $this->render('GnosisProtokoloBundle:Protokolo:etSelect.html.twig', $viewParams);
    }
    
    /**
     * an yparxei to tmima redirect sto enterResults allios redirect piso sto etSelect
     */
    public function etSearchAction(Request $request) {
        $form = $this->createForm(new EtSelectType());
        $form->handleRequest($this->get('request'));
        $data = $form->getData();
        $et = $data['et_number'];
        $entryType = $data['entry_type'];
        $this->entryExists = false;
        if( !$this->canUpdateEt($entryType, $et) ) {
            if($this->entryExists) {
                return $this->redirect($this->generateUrl('gnosis_elections_protokolo_show', array('etId'=>$this->region->getId(), 'entryType'=>$entryType)));
            }
            else {
                return $this->redirect($this->generateUrl('election_admin_protokolo_etsearch'));
            }
        }
        else {
            $key = uniqid();
            $this->get('session')->set('entry_key', $key);
            return $this->forward('GnosisProtokoloBundle:Protokolo:enter', array('etId'=>$this->region->getId(), 'entryType'=>$entryType, 'key'=>$key));
        }
    }

    /**
     * 
     */
    public function enterAction($etId, $entryType, $key, Request $request) {
        if( !$this->isEntryKeyValid($key) ) {
            return $this->redirect($this->generateUrl('election_admin_protokolo_etsearch'));            
        }
        $em = $this->getDoctrine()->getManager();
        $this->loadRequired(null, $etId);
        
        $entity = $this->getEtRecord($this->election, $this->region, $entryType);
        
        $form = $this->createForm(new ProtokoloEntryType(), $entity);
        return $this->render('GnosisProtokoloBundle:Protokolo:entry.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }
    
    public function updateAction($etId, $entryType, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $this->loadRequired(null, $etId);
        $entity = $this->getEtRecord($this->election, $this->region, $entryType);
        $form = $this->createForm(new ProtokoloEntryType(), $entity);
        $form->submit($request);

        if ($form->isValid()) {
            if( $this->updateEtEntry($entity) ) {
                $this->get('session')->getFlashBag()->add('info','Αποθηκεύτηκε πρωτόκολο του τμήματος '.$this->region->getTitle());
                return $this->redirect($this->generateUrl('election_admin_protokolo_etsearch'));
            }
        }
        else {
            $this->get('session')->getFlashBag()->add('notice','gsprod.general.record_errors');
        }
        return $this->render('GnosisProtokoloBundle:Protokolo:entry.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));

    }
    
    public function showAction($etId, $entryType) {
        $this->loadRequired(null, $etId);
        $entity = $this->getEtRecord($this->election, $this->region, $entryType);
        return $this->render('GnosisProtokoloBundle:Protokolo:show.html.twig',
                array(
                'nomos' => $this->nomos,
                'entity' => $entity,
        ));
        
    }
    
    public function deleteAction($etId, $entryType) {
        $this->loadRequired(null, $etId);
        $entry = $this->getDoctrine()
                ->getRepository('GnosisProtokoloBundle:ProtokoloEntry')
                ->findOneBy(array('region'=>$this->region, 'entry_type'=>$entryType));
        if( $entry ) {
            $this->getDoctrine()->getManager()->remove($entry);
            $this->getDoctrine()->getManager()->flush();
            $this->get('session')->getFlashBag()->add('info', $this->get('translator')->trans('elections.protokolo.record_deleted'));
        }
        else {
            $this->get('session')->getFlashBag()->add('notice', $this->get('translator')->trans('elections.protokolo.record_not_found', array('%et%' => $etId)));
        }
        return $this->redirect($this->generateUrl('election_admin_protokolo_etsearch'));
    }
    
    public function printAllAction() {
        $this->loadRequired();
        $rows = $this->getDoctrine()->getRepository('GnosisProtokoloBundle:ProtokoloEntry')->getNomosEntries($this->nomos);
        return $this->render('GnosisProtokoloBundle:Protokolo:printall.html.twig',
                array(
                'nomos' => $this->nomos,
                'ekloges' => $this->election,
                'rows' => $rows,
        ));        
    }
    
    private function loadRequired($et=null, $etId=null) {
        $em = $this->getDoctrine()->getManager();
        $this->eklogesId = $this->get('session')->get('election_id');
        $this->nomosId = $this->get('session')->get('nomos_id');
        $this->election = $em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($this->eklogesId);
        $this->nomos = $em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->find($this->nomosId);
        $this->region = null;
        if($et) {
            $this->region = $em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($this->election, $this->nomos, $et);
        }
        elseif($etId) {
            $this->region = $em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->find($etId);
        }
    }
    
    private function canUpdateEt($entryType, $et=null, $etId=null) {
        $this->loadRequired($et, $etId);
        if( !$this->election->getActive() ) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('elections.psifoi.elections_not_active'));
            return false;
        }
        if(!$this->region) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('elections.psifoi.et_not_found', array('%et%' => ($et? $et : $etId))));
            return false;
        }
        if( $this->getDoctrine()->getRepository('GnosisProtokoloBundle:ProtokoloEntry')->hasEtProtokolo($this->region->getId(),$entryType) ) {
            $this->get('session')->getFlashBag()->add('notice', $this->get('translator')->trans('elections.protokolo.has_entry'));
            $this->entryExists = true;
            return false;
        }
                
        return true;
    }
    
    private function isEntryKeyValid($entryKey) {
        $out = false;
        if($this->get('session')->has('entry_key')) {
            $sessKey = $this->get('session')->get('entry_key');
            if(!empty($sessKey) && !empty($entryKey) && $sessKey==$entryKey) {
                $out = true;
            }
            $this->get('session')->remove('entry_key');
        }
        return $out;
    }

    private function getEtRecord($election, $region, $entryType) {
        $entry = $this->getDoctrine()
                ->getRepository('GnosisProtokoloBundle:ProtokoloEntry')
                ->findOneBy(array('region'=>$region, 'entry_type'=>$entryType));
        if(!$entry) {
            $entry = new ProtokoloEntry();
            $entry->setElection($election)
                    ->setRegion($region)
                    ->setEntryType($entryType)
                    ->setRemarks('');
                    
        }
        return $entry;
    }
    
    private function updateEtEntry(ProtokoloEntry $entity) {
        if( !$entity->getId() ) {   // new record! get PaktikoId from service
            $rpc = $this->get('slx_rabbit_mq.rpccall');
            $data = array('key'=>sprintf("protokolo_%s", $this->nomosId));
            $prId = $rpc->call(json_encode($data));
            $entity->setEntryId($prId);
            $entity->setUpdatedAt(new \DateTime());
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        return true;
    }
    
    public function report01Action($rptId, Request $request) {
        $this->loadRequired();
        $pgTitle = 'Ολα';
        switch ($rptId) {
            case 1:
                $rows = $this->getDoctrine()->getRepository('GnosisProtokoloBundle:ProtokoloEntry')->getEtReport01Data($this->nomos);
                $pgTitle = 'Ολα';
                break;
            case 2:
                $rows = $this->getDoctrine()->getRepository('GnosisProtokoloBundle:ProtokoloEntry')->getEtReport02Data($this->nomos);
                $pgTitle = 'ΕΤ πλήρη απο ψήφους';
                break;
            case 3:
                $rows = $this->getDoctrine()->getRepository('GnosisProtokoloBundle:ProtokoloEntry')->getEtReport03Data($this->nomos);
                $pgTitle = 'ΕΤ κάποια έλλειψη απο ψήφους';
                break;
            case 4:
                $rows = $this->getDoctrine()->getRepository('GnosisProtokoloBundle:ProtokoloEntry')->getEtReport04Data($this->nomos);
                $pgTitle = 'ΕΤ πλήρη από σταυρούς'; 
                break;
            case 5:
                $rows = $this->getDoctrine()->getRepository('GnosisProtokoloBundle:ProtokoloEntry')->getEtReport05Data($this->nomos);
                $pgTitle = 'ΕΤ κάποια έλλειψη απο σταυρούς';
                break;
            case 6:
                $rows = $this->getDoctrine()->getRepository('GnosisProtokoloBundle:ProtokoloEntry')->getEtReport06Data($this->nomos);
                $pgTitle = 'ΕΤ που δεν έχουν τίποτα';
                break;
            case 7:
                $rows = $this->getDoctrine()->getRepository('GnosisProtokoloBundle:ProtokoloEntry')->getEtReport07Data($this->nomos);
                $pgTitle = 'ΕΤ πληρη';
                break;
            default:
                $rows = $this->getDoctrine()->getRepository('GnosisProtokoloBundle:ProtokoloEntry')->getEtReport01Data($this->nomos);
        }
        return $this->render('GnosisProtokoloBundle:Protokolo:printReport.html.twig',
                array(
                'nomos' => $this->nomos,
                'ekloges' => $this->election,
                'rows' => $rows,
                'rptTitle' => $pgTitle,
        ));        
    }
    
}
