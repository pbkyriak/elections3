<?php
namespace Gnosis\ProtokoloBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
/**
 * Description 
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ProtokoloEntryRepository extends EntityRepository {
    
    public function hasEtProtokolo($etId, $entryType) {
        $out = false;
        $stmt = $this->getEntityManager()
                ->getConnection()
                ->executeQuery("SELECT count(*) FROM protokolo_entry WHERE region_id=:etid and entry_type=:etype", 
                        array('etid'=>$etId, 'etype'=>$entryType )
                );
        $cnt = $stmt->fetchColumn(0);
        if( $cnt ) {
            if($cnt>0) {
                $out = true;
            }
        }
        return $out;
    }
    
    public function getNomosEntries($nomos) {
        $qb = $this->createQueryBuilder('pr')
                ->select('pr.entry_id, pr.entry_type, pr.updated_at, r.title, pr.remarks')
                ->leftJoin('pr.region', 'r')
                ->where('r.id_path like :idp')
                ->orderBy('pr.entry_id')
                ->setParameter('idp', sprintf("%s,%%", $nomos->getIdPath()))
                ;
        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }
    
    public function getEtReport01Data($nomos) {
        return $this->getReportDataCore($nomos);
    }

    /**
     * ayta poy exoyn kataxorimenoys psifoys
     * @param type $nomos
     * @return type
     */
    public function getEtReport02Data($nomos) {
        $where = array();
        $where[] = 'not isnull(ep.updated_at)';
        return $this->getReportDataCore($nomos, $where);
    }
    
    /**
     * ΕΤ κάποια έλλειψη απο ψηφους
     */
    public function getEtReport03Data($nomos) {
        $where = array();
        $where[] = '(isnull(ep.updated_at) or isnull(pp.updated_at))';
        return $this->getReportDataCore($nomos, $where);
    }

    /**
     * ΕΤ πλήρη από σταυρους
     */
    public function getEtReport04Data($nomos) {
        $where = array();
        $where[] = '(not isnull(ep.updated_at))';
        $where[] = '(not isnull(pp.updated_at))';
        return $this->getReportDataCore($nomos, $where);
    }

    /**
     * ΕΤ κάποια έλλειψη απο σταυρους
     */
    public function getEtReport05Data($nomos) {
        $where = array();
        $where[] = '(isnull(es.updated_at) or isnull(ps.updated_at))';
        return $this->getReportDataCore($nomos, $where);
    }

    /**
     * ΕΤ που δεν έχουν τίποτα
     */
    public function getEtReport06Data($nomos) {
        $where = array();
        $where[] = '(isnull(ep.updated_at))';
        $where[] = '(isnull(pp.updated_at))';
        $where[] = '(isnull(es.updated_at))';
        $where[] = '(isnull(ps.updated_at))';
        return $this->getReportDataCore($nomos, $where);
    }

    /**
     * ΕΤ πληρη - τα παντα ολα
     */
    public function getEtReport07Data($nomos) {
        $where = array();
        $where[] = '(not isnull(ep.updated_at))';
        $where[] = '(not isnull(pp.updated_at))';
        $where[] = '(not isnull(es.updated_at))';
        $where[] = '(not isnull(ps.updated_at))';
        return $this->getReportDataCore($nomos, $where);
    }

    private function getReportDataCore($nomos, $where=array()) {
        $sql = "select r.id, r.title, pp.updated_at as protokP, ps.updated_at as protokS, ep.updated_at entryP, es.updated_at entryS
                from region r
                left join protokolo_entry pp on (pp.region_id=r.id and pp.entry_type='psifoi')
                left join protokolo_entry ps on (ps.region_id=r.id and ps.entry_type='stauroi')
                left join psifoi_entry ep on (ep.region_id=r.id )
                left join stauroi_entry es on (es.region_id=r.id )
                where r.region='eklTmima' and r.id_path like :idp
                ";
        $whereClause = implode(' AND ',$where);
        if($whereClause) {
            $sql .= ' AND '. $whereClause;
        }
        $params = array(
            'idp'=>sprintf("%s,%%", $nomos->getIdPath())
        );
        $stmt = $this->getEntityManager()->getConnection()->executeQuery($sql, $params);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);        
    }
    
}
