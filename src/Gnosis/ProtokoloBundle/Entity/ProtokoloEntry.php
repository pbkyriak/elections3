<?php

namespace Gnosis\ProtokoloBundle\Entity;

/**
 * ProtokoloEntry
 */
class ProtokoloEntry
{
    use \Gnosis\ElectionsEntryBaseBundle\Entity\LogUserActionsTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $entry_id;

    /**
     * @var string
     */
    private $entry_type;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima
     */
    private $region;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\Ekloges
     */
    private $election;

    private $remarks;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entryId
     *
     * @param integer $entryId
     *
     * @return ProtokoloEntry
     */
    public function setEntryId($entryId)
    {
        $this->entry_id = $entryId;

        return $this;
    }

    /**
     * Get entryId
     *
     * @return integer
     */
    public function getEntryId()
    {
        return $this->entry_id;
    }

    /**
     * Set entryType
     *
     * @param string $entryType
     *
     * @return ProtokoloEntry
     */
    public function setEntryType($entryType)
    {
        $this->entry_type = $entryType;

        return $this;
    }

    /**
     * Get entryType
     *
     * @return string
     */
    public function getEntryType()
    {
        return $this->entry_type;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ProtokoloEntry
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set region
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima $region
     *
     * @return ProtokoloEntry
     */
    public function setRegion(\Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set election
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Ekloges $election
     *
     * @return ProtokoloEntry
     */
    public function setElection(\Gnosis\ElectionsBaseBundle\Entity\Ekloges $election = null)
    {
        $this->election = $election;

        return $this;
    }

    /**
     * Get election
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Ekloges
     */
    public function getElection()
    {
        return $this->election;
    }
    
    public function getRemarks() {
        return $this->remarks;
    }
    
    public function setRemarks($v) {
        $this->remarks = $v;
        return $this;
    }
    
    public function __toString() {
        return sprintf("Πρωτόκολο: %s - %s - %s", $this->getElection()->getTitle(),$this->getEntryType(), $this->getEntryId());
    }
}

