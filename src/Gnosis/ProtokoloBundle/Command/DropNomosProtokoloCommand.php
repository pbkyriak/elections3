<?php

namespace Gnosis\ProtokoloBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;

/**
 * Description of DropNomosProtokoloCommand
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class DropNomosProtokoloCommand extends ContainerAwareCommand
{
    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    
    protected function configure()
    {
        $this
            ->setName('elections:drop:protokolo')
            ->setDescription("Drops protokolo records for nomos")
            ->addArgument('eklogesSlug',
                InputArgument::REQUIRED,
                'ekloges to import?')
            ->addArgument('nomosSlug',
                InputArgument::REQUIRED,
                'nomos to import?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $t1 = time();
    
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $eklogesSlug = $input->getArgument('eklogesSlug');
        $nomosSlug = $input->getArgument('nomosSlug');
        
        $output->writeln("<info>Ekloges slug=</info>".$eklogesSlug);
        $output->writeln("<info>Nomos slug=</info>".$nomosSlug);
        
        $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug'=>$eklogesSlug));
        if( !$ekloges ) {
            $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlug));
            return;
        }
        
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('election'=>$ekloges, 'slug'=>$nomosSlug));
        if( !$nomos ) {
            $output->writeln(sprintf("<error>No nomos found with slug %s</error>", $nomosSlug));
            return;
        }
        
        // drop protokolo records for nomos
        $rCnt = $this->em->getConnection()->executeUpdate("DELETE a FROM protokolo_entry a, region r where (a.region_id=r.id) and r.id_path like :idp", array('idp'=> sprintf("%s,%%",$nomos->getIdPath())));
        $output->writeln("<info>Records Deleted=</info>".$rCnt);
        // reset sequence
        $sCnt = $this->em->getConnection()->delete('sequence', array('sqkey'=>sprintf("protokolo_%s", $nomos->getId())));
        $output->writeln("<info>Sequence Deleted=</info>".$sCnt);
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
    }   
    
}
