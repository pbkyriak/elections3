<?php

namespace Gnosis\ElectionsEntryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Gnosis\ElectionsEntryBaseBundle\Entity\EntryLock;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Description
 *
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Αναφορές", route="elections_admin_reports")
 * @CurrentMenuItem("elections_admin_reports")
 * @Security("has_role('ROLE_RADMIN')")
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class EtReportsController extends Controller {
    private $eklogesId;
    private $nomosId;
    private $election;
    private $nomos;

    private function loadRequired() {
        $em = $this->getDoctrine()->getManager();
        $this->eklogesId = $this->get('session')->get('election_id');
        $this->nomosId = $this->get('session')->get('nomos_id');
        $this->election = $em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($this->eklogesId);
        $this->nomos = $em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->find($this->nomosId);
        
    }

    /**
     * @Breadcrumb("Λείπουν ψήφοι")
     * @CurrentMenuItem("elections_admin_reports")
     */
    public function etReportMissingPsifoiAction() {
        $this->loadRequired();
        $em = $this->getDoctrine()->getManager();
        $results = $em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEtWithMissingPsifoi($this->nomos);
        //$results = $this->getDoctrine()->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEtWithMissingStauroi($this->nomos);
        $viewParams = array(
            'title' => 'elections.etreports.missing_psifoi',
            'results' => $results,
        );
        return $this->render('GnosisElectionsEntryBundle:EtReports:missingPsifoi.html.twig', $viewParams);
    }

    /**
     * @Breadcrumb("Λείπουν σταυροί")
     * @CurrentMenuItem("elections_admin_reports")
     */
    public function etReportMissingStauroiAction() {
        $this->loadRequired();
        $results = $this->getDoctrine()->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEtWithMissingStauroi($this->nomos);
        $viewParams = array(
            'title' => 'elections.etreports.missing_stauroi',
            'results' => $results,
        );
        return $this->render('GnosisElectionsEntryBundle:EtReports:missingPsifoi.html.twig', $viewParams);
    }

    /**
     * @Breadcrumb("Λείπουν όλα")
     * @CurrentMenuItem("elections_admin_reports")
     */
    public function etReportMissingBothAction() {
        $this->loadRequired();
        $results = $this->getDoctrine()->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEtWithMissingBoth($this->nomos);
        $viewParams = array(
            'title' => 'elections.etreports.missing_both',
            'results' => $results,
        );
        return $this->render('GnosisElectionsEntryBundle:EtReports:missingPsifoi.html.twig', $viewParams);
    }

    /**
     * @Breadcrumb("Λείπει κάτι")
     * @CurrentMenuItem("elections_admin_reports")
     */
    public function etReportMissingAnyAction() {
        $this->loadRequired();
        $results = $this->getDoctrine()->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEtWithMissingAny($this->nomos);
        $viewParams = array(
            'title' => 'elections.etreports.missing_any',
            'results' => $results,
        );
        return $this->render('GnosisElectionsEntryBundle:EtReports:missingAny.html.twig', $viewParams);
    }

    /**
     * @Breadcrumb("Ωρες καταχώρησης")
     * @CurrentMenuItem("elections_admin_reports")
     */
    public function etReportEntryTimesAction() {
        $this->loadRequired();
        $results = $this->getDoctrine()->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEtEntryTimes($this->nomos);
        $viewParams = array(
            'title' => 'elections.etreports.entry_times',
            'results' => $results,
        );
        return $this->render('GnosisElectionsEntryBundle:EtReports:entryTimes.html.twig', $viewParams);
    }
    
    /**
     * @Breadcrumb("Καταγραφή αποστολών σε ΥΠΕΣ")
     * @CurrentMenuItem("elections_admin_reports")
     */
    public function etReportYpesAction() {
        $this->loadRequired();
        $results = $this->getDoctrine()->getRepository('GnosisElectionsExportsBundle:YpesToExport')->getNomosLog($this->nomos);
        $viewParams = array(
            'title' => 'elections.etreports.ypes',
            'results' => $results,
        );
        return $this->render('GnosisElectionsEntryBundle:EtReports:ypes.html.twig', $viewParams);
    }

    /**
     * @Breadcrumb("Καταγραφή τηλεφωνικών αποστολών σε ΥΠΕΣ")
     * @CurrentMenuItem("elections_admin_reports")
     */
    public function etReportPhoneYpesAction() {
        $this->loadRequired();
        $results = $this->getDoctrine()->getRepository('GnosisElectionsPhoneBundle:YpesToPhone')->getNomosLog($this->nomos);
        $viewParams = array(
            'title' => 'elections.etreports.phoneypes',
            'results' => $results,
        );
        return $this->render('GnosisElectionsEntryBundle:EtReports:phoneypes.html.twig', $viewParams);
    }
}
