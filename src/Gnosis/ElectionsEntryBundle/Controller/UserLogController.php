<?php

namespace Gnosis\ElectionsEntryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Gnosis\ElectionsEntryBaseBundle\Entity\UserLog;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Description
 *
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Εργαλεία", route="gnosis_elections_entry_stauroi")
 * @Breadcrumb("User Log")
 * @CurrentMenuItem("userlog")
 * @Security("has_role('ROLE_RADMIN')")
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class UserLogController extends Controller
{

    /**
     * Lists all UserLog entities.
     *
     */
    public function indexAction()
    {
        $form = $this->getFilterForm();
        $query = $this->makeListQuery($form);
        $request = $this->get('request');
        if ($request->get('out') == 'csv') {
            $entities = $query->getResult();
            $response = $this->render('GnosisElectionsEntryBundle:UserLog:index.csv.twig',
                array(
                'entities' => $entities
            ));
            $response->headers->set('Content-Type', 'text/csv');
            $date = new \DateTime();
            $response->headers->set('Content-Disposition',
                'attachment; filename="' . $date->format("U") . '-machines.csv"');
            return $response;
        }
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1) /* page number */,
            $this->get('request')->query->get('limit', 15)
        );

        return $this->render('GnosisElectionsEntryBundle:UserLog:index.html.twig',
                array(
                'pagination' => $pagination,
                'filterForm' => $form->createView()
        ));
    }

    /**
     * Create list query
     */
    private function makeListQuery($form)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT a, b FROM GnosisElectionsEntryBaseBundle:UserLog a JOIN a.user b ";
        $form->handleRequest($this->get('request'));
        $filterQryParams = array();
        $where = array();
        $this->get('request')->setLocale('el');
        if ($form->isValid()) {
            $filterData = $form->getData();
            if ($filterData['class']) {
                $where[] = " a.class like :class ";
                $filterQryParams['class'] = str_replace('*', '%',str_replace('\\','\\\\',$filterData['class']));
            }
            if ($filterData['action']) {
                $where[] = " a.action like :action ";
                $filterQryParams['action'] = str_replace('*', '%',$filterData['action']);
            }
            if ($filterData['title']) {
                $where[] = " a.title like :title ";
                $filterQryParams['title'] = str_replace('*', '%',$filterData['title']);
            }
            if ($filterData['object_id']) {
                $where[] = " a.object_id =:object_id ";
                $filterQryParams['object_id'] = $filterData['object_id'];
            }
            if ($filterData['username']) {
                $where[] = " b.username =:uname ";
                $filterQryParams['uname'] = $filterData['username'];
            }
        }
        if ($where) {
            $dql .= ' where ' . implode(' and ', $where);
        }
        $dql .= ' ORDER BY a.created_at desc';
        $query = $em->createQuery($dql);
        $query->setParameters($filterQryParams);
        return $query;
    }

    /**
     * Creates filter form
     *
     */
    private function getFilterForm()
    {
        $em = $this->getDoctrine()->getManager();
        $list = $em->getRepository('GnosisElectionsEntryBaseBundle:UserLog')->getDistinctLoggedClasses();
        $choices = [];
        foreach($list as $item) {
            $iitem = str_replace('\\', '', $item['class']);
            $choices[$item['class']] = 'elections.userlog.'.$iitem;
        }
        $filterData = array();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData,
                array('method' => 'GET', 'csrf_protection' => false))
            ->add(
                'class', 'choice',
                array('required' => false, 'label' => 'elections.userlog.class', 'choices'=>$choices)
            )
            ->add(
                'object_id', 'text',
                array('required' => false, 'label' => 'elections.userlog.object_id')
            )
            ->add(
                'action', 'text',
                array('required' => false, 'label' => 'elections.userlog.action')
            )
            ->add(
                'title', 'text',
                array('required' => false, 'label' => 'elections.userlog.title')
            )
            ->add(
                'username', 'text',
                array('required' => false, 'label' => 'elections.userlog.user')
            )
            ->getForm();
        return $form;
    }

    /**
     * Finds and displays a UserLog entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GnosisElectionsEntryBaseBundle:UserLog')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserLog entity.');
        }

        $user = null;
        if( $entity->getUserId() ) {
            $user = $em->getRepository('SlxUserBundle:User')->find($entity->getUserId());
        }
        return $this->render('GnosisElectionsEntryBundle:UserLog:show.html.twig',
                array(
                'entity' => $entity,
                'user' => $user,
        ));
    }

}
