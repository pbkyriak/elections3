<?php

namespace Gnosis\ElectionsEntryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Gnosis\ElectionsEntryBaseBundle\Entity\EntryLock;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * Description
 *
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Εργαλεία")
 * @Breadcrumb("Entry Locks")
 * @CurrentMenuItem("gnosis_elections_entrylock")
 * @Security("has_role('ROLE_RADMIN')")
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class EntryLockController extends Controller
{

    /**
     * Lists all Entry lock entities.
     *
     */
    public function indexAction()
    {
        $form = $this->getFilterForm();
        $query = $this->makeListQuery($form);
        $request = $this->get('request');
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1) /* page number */,
            $this->get('request')->query->get('limit', 15)
        );

        return $this->render('GnosisElectionsEntryBundle:EntryLock:index.html.twig',
                array(
                'pagination' => $pagination,
                'filterForm' => $form->createView()
        ));
    }

    /**
     * Create list query
     */
    private function makeListQuery($form)
    {
        $nomosId = $this->get('session')->get('nomos_id');
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT a, b, r FROM GnosisElectionsEntryBaseBundle:EntryLock a JOIN a.user b JOIN a.region r ";
        $form->handleRequest($this->get('request'));
        $filterQryParams = array();
        $where = array();
        $this->get('request')->setLocale('el');
        if ($form->isValid()) {
            $filterData = $form->getData();
            if ($filterData['entry_type']) {
                $where[] = " a.entry_type like :entry_type ";
                $filterQryParams['entry_type'] = str_replace('*', '%',$filterData['entry_type']);
            }
        }
        $where[] = " r.id_path like :idpath ";
        $filterQryParams['idpath'] = sprintf("%%,%s,%%", $nomosId);
        
        if ($where) {
            $dql .= ' where ' . implode(' and ', $where);
        }
        $dql .= ' ORDER BY a.lock_timestamp';
        $query = $em->createQuery($dql);
        $query->setParameters($filterQryParams);
        return $query;
    }

    /**
     * Creates filter form
     *
     */
    private function getFilterForm()
    {
        $filterData = array();

        $choises = [
            'psifoi' => 'Ψήφοι',
            'stauroi'=>'Σταυροί',
            'praktika'=>'Πρακτικά',
            'phone'=>'Τηλεφωνικό'
        ];

        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData,
                array('method' => 'GET', 'csrf_protection' => false))
            ->add(
                'entry_type', 'choice',
                array('required' => false, 'label' => 'elections.entrylock.type', 'choices'=>$choises)
            )
            ->getForm();
        return $form;
    }

    public function unlockAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('GnosisElectionsEntryBaseBundle:EntryLock')->find($id);
        if($entity) {
            $em->remove($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info', 'ET unlocked');
        }
        return $this->redirect($this->generateUrl('gnosis_elections_entrylock'));            
    }
}
