<?php

namespace Gnosis\ElectionsEntryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Gnosis\ElectionsEntryBundle\Form\EtSelectType;
use Symfony\Component\HttpFoundation\Request;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Gnosis\ElectionsEntryBundle\Form\PsifoiEntryType;
use Gnosis\ElectionsEntryBaseBundle\Annotation\KeepEntryLock;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Description of PsifoiController
 *
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Καταχώρηση", route="gnosis_elections_entry_psifoi")
 * @Breadcrumb("Ψήφων", route="gnosis_elections_entry_psifoi")
 * @CurrentMenuItem("gnosis_elections_entry_psifoi")
 * @Security("has_role('ROLE_USER')")
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class PsifoiController extends Controller {
    private $eklogesId;
    private $nomosId;
    private $election;
    private $nomos;
    private $region;

    public function etSelectAction(Request $request) {
        $form = $this->createForm(new EtSelectType());

        $viewParams = array(
            'form' => $form->createView(),
            'printEt' => false,
        );
        if( $request->get('print', null) ) {
            $viewParams['printEt'] = $request->get('print');
        }
        return $this->render('GnosisElectionsEntryBundle:Psifoi:etSelect.html.twig', $viewParams);
    }

    private function loadRequired($et=null, $etId=null) {
        $em = $this->getDoctrine()->getManager();
        $this->eklogesId = $this->get('session')->get('election_id');
        $this->nomosId = $this->get('session')->get('nomos_id');
        $this->election = $em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($this->eklogesId);
        $this->nomos = $em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->find($this->nomosId);
        $this->region = null;
        if($et) {
            $this->region = $em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($this->election, $this->nomos, $et);
        }
        elseif($etId) {
            $this->region = $em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->find($etId);
        }
    }
    
    /**
     * an yparxei to tmima redirect sto enterResults allios redirect piso sto etSelect
     * @KeepEntryLock("psifoi")
     */
    public function etSearchAction(Request $request) {
        $form = $this->createForm(new EtSelectType());
        $form->handleRequest($this->get('request'));
        $data = $form->getData();
        $et = $data['et_number'];
        if( !$this->canUpdateEt($et) ) {
            return $this->redirect($this->generateUrl('gnosis_elections_entry_psifoi'));            
        }
        else {
            $key = uniqid();
            $this->get('session')->set('entry_key', $key);
            //return $this->redirect($this->generateUrl('gnosis_elections_entry_psifoi_enterresults', array('etId'=>$this->region->getId(), 'key'=>$key)));
            return $this->forward('GnosisElectionsEntryBundle:Psifoi:enterResults', array('etId'=>$this->region->getId(), 'key'=>$key));
        }
    }
    
    private function canUpdateEt($et=null, $etId=null) {
        $this->loadRequired($et, $etId);
        $em = $this->getDoctrine()->getManager();
        if( !$this->election->getActive() ) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('elections.psifoi.elections_not_active'));
            return false;
        }
        if(!$this->region) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('elections.psifoi.et_not_found', array('%et%' => ($et? $et : $etId))));
            return false;
        }
        if( $this->nomos->getHasProtokolo() ) {
            if( !$em->getRepository('GnosisProtokoloBundle:ProtokoloEntry')->hasEtProtokolo($this->region->getId(), 'psifoi')) {
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('elections.protokolo.et_has_no_protokolo', array('%et%' => ($et? $et : $etId))));
                return false;
            }
        }
        $lockmgr = $this->container->get('gnosis_elections_entry_base.lock_manager');
        $out = $lockmgr->lockForPsifoi($this->region,$this->getUser()->getId());
        if($out) {
            $this->get('session')->set('et_lock', array('type'=>'psifoi', 'et'=>$this->region->getId(), 'user_id'=>$this->getUser()->getId()));
        }
        else {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('elections.psifoi.et_is_locked'));
            return false;
        }
        $mgr = $this->container->get('gnosis_elections_entry_base.stauroi_manager');
        if( $mgr->hasEtStauroiEntry($this->region->getId()) ) {
            $this->get('session')->getFlashBag()->add('notice', $this->get('translator')->trans('elections.psifoi.has_stauroi'));
            return true;
        }
        return true;
    }
    
    /**
     * @KeepEntryLock("psifoi")
     */
    public function enterResultsAction($etId, $key, Request $request) {

        if( !$this->isEntryKeyValid($key) ) {
            return $this->redirect($this->generateUrl('gnosis_elections_entry_psifoi'));            
        }
        $em = $this->getDoctrine()->getManager();
        $this->loadRequired(null, $etId);
        $mgr = $this->container->get('gnosis_elections_entry_base.psifoi_manager');
        $entity = $this->getEtRecord($mgr, $this->election, $this->region);
        $form = $this->createForm(new PsifoiEntryType(), $entity);
        return $this->render('GnosisElectionsEntryBundle:Psifoi:etPsifoi.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }

    private function isEntryKeyValid($entryKey) {
        $out = false;
        if($this->get('session')->has('entry_key')) {
            $sessKey = $this->get('session')->get('entry_key');
            if(!empty($sessKey) && !empty($entryKey) && $sessKey==$entryKey) {
                $out = true;
            }
            $this->get('session')->remove('entry_key');
        }
        return $out;
    }
    
    private function getEtRecord($mgr, $election, $region, $mute=false) {
        $entity = $mgr->fetchEtEntry($election, $region);
        if( $entity ) {
            if( !$mute ) {
                $this->get('session')->getFlashBag()->add('notice','elections.psifoi.modifing_record');
            }
        }
        else {
            $entity = $mgr->createEntryForEt($election, $region);
        }
        return $entity;
    }
    /**
     * 
     * @Breadcrumb("Τμήματος", route="gnosis_elections_entry_psifoi")
     */    
    public function updateResultsAction($etId,Request $request) {
        $em = $this->getDoctrine()->getManager();
        $this->loadRequired(null, $etId);
        $mgr = $this->container->get('gnosis_elections_entry_base.psifoi_manager');
        $entity = $this->getEtRecord($mgr, $this->election, $this->region, true);
        $form = $this->createForm(new PsifoiEntryType(), $entity);
        $form->submit($request);

        if ($form->isValid()) {
            if( $mgr->updateEtEntry($entity) ) {
                $this->get('session')->getFlashBag()->add('info','Αποθηκεύτηκαν οι ψήφοι του τμήματος '.$this->region->getTitle());
                $this->crossCheckStauroi();
                return $this->redirect($this->generateUrl('gnosis_elections_entry_psifoi', array('print'=> $this->region->getId())));
            }
            else {
                $this->get('session')->getFlashBag()->add('notice','gsprod.general.record_errors');
                $this->get('session')->getFlashBag()->add('notice',$mgr->getMessage());
            }
        }
        else {
            $this->get('session')->getFlashBag()->add('notice','gsprod.general.record_errors');
        }
        return $this->render('GnosisElectionsEntryBundle:Psifoi:etPsifoi.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }
    
    private function crossCheckStauroi() {
        $smgr = $this->container->get('gnosis_elections_entry_base.stauroi_manager');
        if($smgr->hasEtStauroiEntry($this->region->getId())) {
            $sentity = $smgr->fetchEtEntry($this->election, $this->region);
            $errors = $this->get('validator')->validate($sentity);
            if(count($errors)) {
                $msgs=array();
                foreach($errors as $error) {
                    $msgs[] = $error->getMessage();
                }
                $err = implode('<br />', $msgs);
                $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('elections.psifoi.caused_stauroi_error', array('%err%' => $err)));
                //return $this->redirect($this->generateUrl('gnosis_elections_entry_stauroi_enterresults', array('etId'=>$this->region->getId())));
            }
        }        
    }

    public function printAction($etId) {
        $em = $this->getDoctrine()->getManager();
        $this->loadRequired(null, $etId);
        $mgr = $this->container->get('gnosis_elections_entry_base.psifoi_manager');
        $entity = $this->getEtRecord($mgr, $this->election, $this->region, true);
        if ($this->container->has('profiler')) {
            $this->container->get('profiler')->disable();
        }

        return $this->render('GnosisElectionsEntryBundle:Psifoi:print.html.twig',
                array(
                'nomos' => $this->nomos,
                'entity' => $entity,
        ));
        
    }
}
