<?php

namespace Gnosis\ElectionsEntryBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gnosis\ElectionsEntryBundle\Form\DataTransformer\VisSyndiasmosToNumberTransformer;
use Gnosis\ElectionsEntryBundle\Form\ViewTransformer\SyndiasmosVisViewTransformer;
/**
 * Description of SyndiasmosVisType
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class SyndiasmosVisType extends AbstractType {

    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $transformer = new VisSyndiasmosToNumberTransformer($this->om);
        $vtransformer = new SyndiasmosVisViewTransformer($this->om);
        $builder->addModelTransformer($transformer);
        $builder->addViewTransformer($vtransformer);

    }

    public function setDefaultOptions(\Symfony\Component\OptionsResolver\OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'widget' => 'syndiasmosvis',
            'compound' => false,
            //'data_class' => 'Gnosis\ElectionsBaseBundle\Entity\Syndiasmos'
        ));
    }

    public function getName() {
        return 'syndiasmosvis';
    }

}
