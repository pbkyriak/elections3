<?php

namespace Gnosis\ElectionsEntryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Description of EtSelectType
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class EtSelectType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add(
                        'et_number', 
                        'number', 
                        array(
                            'required'=>true, 
                            'label'=>'elections.psifoi.et',
                            'help' => 'elections.psifoi.et_help',
                            'attr' => array('step'=>'0',
                                'data-validate' => 'positive-number',
                                'min'=>'0',
                                'max'=>'99999')
                            )
                        )
                ->add('save', 'submit', array('label'=>'Αναζήτηση','attr'=>array('class'=>'btn blue')))
                ;
    }
    
    public function getName() {
        return 'et_select_form';
    }
}
