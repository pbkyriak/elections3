<?php

namespace Gnosis\ElectionsEntryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Gnosis\ElectionsEntryBundle\Form\StauroiEntryRowType;

/**
 * Description of StauroiEntryType
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class StauroiEntryType  extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('stauroiEntryRows', 'collection', array(
                    'type' => new StauroiEntryRowType(),
                    'allow_add' => false,
                    'by_reference' => false,
                    'allow_delete' => false,
                    'label' => 'elections.psifoi.results')
                )
                ->add('save', 'submit', array('label' => 'Αποθήκευση', 'attr' => array('class' => 'btn blue')))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Gnosis\ElectionsEntryBaseBundle\Entity\StauroiEntry'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'stauroi_entry_form';
    }

}
