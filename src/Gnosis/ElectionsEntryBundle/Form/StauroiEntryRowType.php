<?php

namespace Gnosis\ElectionsEntryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of StauroiEntryRowType
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class StauroiEntryRowType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ypopsifios', 'ypopsifiosvis', array('label'=>'elections.psifoi.syndiasmos'))    
            ->add(
                    'amount', 
                    null, 
                    array(
                        'label'=>'elections.stauroi.stauroi', 
                        'attr'=>array(
                            'class'=>'stauroi-amount form-control',
                            'min'=>'0',
                            'max'=>'99999',
                            'data-validate' => 'positive-number',
                            )
                        )
                    )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gnosis\ElectionsEntryBaseBundle\Entity\StauroiEntryRow'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'stauroi_row';
    }
}
