<?php
namespace Gnosis\ElectionsEntryBundle\Form\ViewTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Description of SyndiasmosVisViewTransformer
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class SyndiasmosVisViewTransformer implements DataTransformerInterface  {
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object to a string (number).
     *
     * @param  Region|null $issue
     * @return string
     */
    public function reverseTransform($number)
    {
        //printf("%s <br />",__METHOD__);
        if (!$number) {
            return null;
        }

        $object = $this->om
            ->getRepository('GnosisElectionsBaseBundle:Syndiasmos')
            ->findOneBy(array('id' => $number))
        ;

        if (null === $object) {
            throw new TransformationFailedException(sprintf(
                'Synsdiasmos with id "%s" does not exist!',
                $number
            ));
        }

        return $object;
    }

    /**
     * Transforms a string (number) to an object (Ekloges).
     *
     * @param  string $number
     *
     * @return Ekloges|null
     *
     * @throws TransformationFailedException if object (Ekloges) is not found.
     */
    public function transform($object)
    {
        //printf("%s <br />",__METHOD__);
        return $object;
        if (null === $object) {
            return "";
        }
        $out = array(
            'id' => $object->getId(),
            'title' => $object->getTitle(),
            'slug' => $object->getSlug(),
            );
        return $out;

    }
}
