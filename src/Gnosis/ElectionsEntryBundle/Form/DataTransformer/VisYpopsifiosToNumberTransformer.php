<?php

namespace Gnosis\ElectionsEntryBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Description of VisYpopsifiosToNumberTransformer
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class VisYpopsifiosToNumberTransformer implements DataTransformerInterface {
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object to a string (number).
     *
     * @param  Region|null $issue
     * @return string
     */
    public function transform($object)
    {
        //echo __METHOD__;echo '<br/>';
        if (null === $object) {
            return "";
        }
        $out = array(
            'id' => $object->getId(),
            'title' => $object->getTitle(),
            'slug' => $object->getSlug(),
            'syndiasmos_id' => $object->getKomma()->getId(),
            );
        return $out;
    }

    /**
     * Transforms a string (number) to an object (Ekloges).
     *
     * @param  string $number
     *
     * @return Ekloges|null
     *
     * @throws TransformationFailedException if object (Ekloges) is not found.
     */
    public function reverseTransform($number)
    {
        //echo __METHOD__;echo '<br/>';
        if (!$number) {
            return null;
        }

        $object = $this->om
            ->getRepository('GnosisElectionsBaseBundle:Ypopsifios')
            ->findOneBy(array('id' => $number))
        ;

        if (null === $object) {
            throw new TransformationFailedException(sprintf(
                'Ypopsifios with id "%s" does not exist!',
                $number
            ));
        }

        return $object;
    }
    
}
