<?php

namespace Gnosis\ElectionsEntryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Gnosis\ElectionsEntryBundle\Form\PsifoiEntryRowType;
/**
 * Description of PsifoiEntryType
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class PsifoiEntryType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $isReadonly = $options['readonly'];
        $attrs = ['class'=>'form-control header-data'];
        if($isReadonly) {
            $attrs['readonly']='readonly';
        }
        $builder
                ->add('eggegramenoi', null, array('label'=>'elections.psifoi.eggegrammenoi', 'attr'=>$attrs))
                ->add('psifisan', null, array('label'=>'elections.psifoi.psifisan', 'attr'=>$attrs))
                ->add('leyka', null, array('label'=>'elections.psifoi.leyka', 'attr'=>$attrs))
                ->add('akyra', null, array('label'=>'elections.psifoi.akyra', 'attr'=>$attrs))
                ->add('egkyra', null, array('label'=>'elections.psifoi.egkyra', 'attr'=>array('readonly'=>'readonly')))
                ->add('psifoiEntryRows', 'collection', array(
                    'type' => new PsifoiEntryRowType(),
                    'allow_add' => false,
                    'by_reference' => false,
                    'allow_delete' => false,
                    'label' => 'elections.psifoi.results',
                    'options' => [
                        'readonly' => $isReadonly
                    ]
                    )
                )
                ->add('save', 'submit', array('label' => 'Αποθήκευση', 'attr' => array('class' => 'btn blue')))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntry',
            'readonly' => false,
        ));
        $resolver->setOptional(['readonly']);
    }

    /**
     * @return string
     */
    public function getName() {
        return 'psifoi_entry_form';
    }

}
