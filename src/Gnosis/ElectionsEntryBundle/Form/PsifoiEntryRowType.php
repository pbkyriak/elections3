<?php

namespace Gnosis\ElectionsEntryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of PsifoiEntryRowType
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class PsifoiEntryRowType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $attrs = array(
                            'class'=>'psifoi-amount form-control',
                            'min'=>'0',
                            'max'=>'99999',
                            'data-validate' => 'positive-number',
                            );
        if($options['readonly']) {
            $attrs['readonly']='readonly';
        }

        $builder
            ->add('syndiasmos', 'syndiasmosvis', array('label'=>'elections.psifoi.syndiasmos'))    
            ->add(
                    'amount', 
                    null, 
                    array(
                        'label'=>'elections.psifoi.psifoi', 
                        'attr'=>$attrs,
                        )
                    )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntryRow',
            'readonly' => false,
        ));
        $resolver->setOptional(['readonly']);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'psifoi_row';
    }
}
