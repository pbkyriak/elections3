<?php

namespace Gnosis\ElectionsBaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Gnosis\ElectionsBaseBundle\Form\EventListener\SyndiasmosRegionSubscriber;

/**
 * Description of SyndiasmosType
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Dec 2, 2016
 */
class SyndiasmosType  extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, array('label' => 'Τίτλος',))
            ->add(
                'region', 'region_selector',
                array(
                'required' => true,
                'label' => 'Ανήκει',
                'list_route' => 'elections_admin_regions_json_list',
                'list_route_params' => array('level' => $options['parentLevel']),
                'item_route' => 'elections_admin_regions_json_getItem',
                'entity' => 'region',
                'attr' => array(
                    'class' => 'select2obj col-md-12 select2 form-control',
                    'data-inited' => 0,
                    ),
                )
            )
            //->add('slug')
            ->add('color',null, array(
                'required' => true,
                'label' => 'Χρώμα',
                'attr' => array(
                    'class' => 'colorpicker form-control'
                )
            ))
            ->add('logo', 'file', [
                'required' => false,
                'label' => 'Λογότυπο',
                'help' => 'elections.syndyasmos.logo_help_text',
                'attr' => ['class'=>'default']
            ])
            ->add('prev_id', null, 
                    array(
                        'label'=>'elections.elections.prev_id',
                        'help' => 'elections.syndyasmos.prev_id_help_text',
                        )
                    )
            ->add('save', 'submit', array('label'=>'Αποθήκευση','attr'=>array('class'=>'btn blue')))
            ->add('saveAndAdd', 'submit', array('label'=>'gsprod.general.save_and_add','attr'=>array('class'=>'btn blue')))
        ;
        $builder->addEventSubscriber(new SyndiasmosRegionSubscriber());
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gnosis\ElectionsBaseBundle\Entity\Syndiasmos'
        ));
        $resolver->setRequired(array(
            'parentLevel',
        ));

    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gnosis_electionsbasebundle_syndiasmos';
    }
}
