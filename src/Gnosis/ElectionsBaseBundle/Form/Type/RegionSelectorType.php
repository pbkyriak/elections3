<?php

namespace Gnosis\ElectionsBaseBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Gnosis\ElectionsBaseBundle\Form\DataTransformer\RegionToNumberTransformer;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of Select2Type
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class RegionSelectorType extends AbstractType
{

    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if( $options['entity']=='region' ) {
            $transformer = new RegionToNumberTransformer($this->om);
        }
        $builder->addModelTransformer($transformer);
    }

    public function buildView(FormView $view, FormInterface $form,
        array $options)
    {
        parent::buildView($view, $form, $options);
        $view->vars = array_replace($view->vars, array(
            'list_route'        => $options['list_route'],
            'list_route_params' => isset($options['list_route_params']) ? $options['list_route_params'] : array(),
            'item_route'        => $options['item_route'],
            'item_route_params' => isset($options['item_route_params']) ? $options['item_route_params'] : array(),
            'row_type'          => isset($options['row_type']) ? $options['row_type'] : 'hidden',
            'add_clear_btn'     => isset($options['add_clear_btn']) ? $options['add_clear_btn'] : false,
        ));
    }

    public function getDefaultOptions(array $options)
    {
        return array_replace($options, array(
            'invalid_message' => 'Selected article does not exist',
            'list_route_params' => array(),
            'item_route_params' => array(),
        ));
    }

    public function getParent()
    {
        return 'text';
    }

    public function getName()
    {
        return 'region_selector';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setRequired(array('list_route', 'item_route', 'entity'));
        $resolver->setOptional(array('list_route_params', 'item_route_params', 'row_type', 'add_clear_btn'));
    }

}

