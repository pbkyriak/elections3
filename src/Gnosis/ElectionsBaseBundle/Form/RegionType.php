<?php

namespace Gnosis\ElectionsBaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Gnosis\ElectionsBaseBundle\Form\EventListener\RegionParentSubscriber;

class RegionType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('seira')
            ->add(
                'parent', 'region_selector',
                array(
                'required' => false,
                'label' => 'Ανήκει',
                'list_route' => 'elections_admin_regions_json_list',
                'list_route_params' => array('level' => $options['parentLevel']),
                'item_route' => 'elections_admin_regions_json_getItem',
                'entity' => 'region',
                'attr' => array(
                    'class' => 'select2obj col-md-12 select2',
                    'data-inited' => 0,
                    ),
                )
            )
            ->add('prev_id')
            ->add('save', 'submit', array('label'=>'Αποθήκευση','attr'=>array('class'=>'btn blue')))
            ->add('saveAndAdd', 'submit', array('label'=>'gsprod.general.save_and_add','attr'=>array('class'=>'btn blue')))
        ;
        $builder->addEventSubscriber(new RegionParentSubscriber());
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gnosis\ElectionsBaseBundle\Entity\Region'
        ));
        $resolver->setRequired(array(
            'parentLevel',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gnosis_electionsbasebundle_region';
    }
}
