<?php

namespace Gnosis\ElectionsBaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of StaticBlockType
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Sep 13, 2016
 */
class StaticBlockType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $choices = array(
            'footer_left' => 'Footer left',
            'footer_center' => 'Footer center',
            'footer_right' => 'Footer right',
        );
        $builder
                ->add(
                        'block_code', 
                        'choice', 
                        array(
                            'label' => 'elections.staticblock.block_code',
                            'choices' => $choices,
                            'choices_as_values' => false,
                            'help' => 'elections.staticblock.block_code_help',
                            )
                    )
                ->add(
                        'content', 
                        null, 
                        array(
                            'label' => 'elections.staticblock.content',
                            )
                        )
                ->add('save', 'submit', array('label' => 'Αποθήκευση', 'attr' => array('class' => 'btn blue')))
                ->add('saveAndAdd', 'submit', array('label' => 'gsprod.general.save_and_add', 'attr' => array('class' => 'btn blue')))

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Gnosis\ElectionsBaseBundle\Entity\StaticBlock'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'gnosis_electionsbasebundle_staticblock';
    }

}
