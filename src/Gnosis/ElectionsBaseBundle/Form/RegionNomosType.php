<?php

namespace Gnosis\ElectionsBaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RegionNomosType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                    'title', 
                    null, 
                    array(
                        'label'=>'elections.regions.title', 
                        'help'=>'elections.regions.title_help',
                        )
                    )
            ->add('seira',
                    null,
                    array(
                        'label'=> 'elections.regions.seira',
                        'help' => 'elections.regions.seira_help'
                        )
                    )
            ->add('prev_id',
                    null,
                    array(
                        'label'=> 'elections.regions.prev_id',
                        'help' => 'elections.regions.prev_id_help',
                        )
                    )
            ->add('download_url')
            ->add('import_file')
            ->add('stauroi_per_ballot',
                    null,
                    array(
                        'label'=> 'elections.regions.stauroi_per_ballot',
                        'help' => 'elections.regions.stauroi_per_ballot_help',
                        )
                    )
            ->add(
                    'capital_name', 
                    null, 
                    array(
                        'label'=>'elections.regions.capital_name',
                        'help'=>'elections.regions.capital_name_help',
                        )
                    )
            ->add(
                    'has_protokolo', 
                    null, 
                    array(
                        'label'=>'elections.regions.has_protokolo', 
                        'help'=>'elections.regions.has_protokolo_help', 
                        'required'=>false
                        )
                    )
            ->add(
                    'ypes_ota_id', 
                    null, 
                    array(
                        'required'=>false, 
                        'label'=>'elections.regions.ypes_ota_id',
                        'help'=>'elections.regions.ypes_ota_id_help',
                        )
                    )
            ->add('save', 'submit', array('label'=>'Αποθήκευση','attr'=>array('class'=>'btn blue')))
            ->add('saveAndAdd', 'submit', array('label'=>'gsprod.general.save_and_add','attr'=>array('class'=>'btn blue')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gnosis\ElectionsBaseBundle\Entity\RegionNomos'
        ));
        $resolver->setRequired(array(
            'parentLevel',
        ));

    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gnosis_electionsbasebundle_regionnomos';
    }
}
