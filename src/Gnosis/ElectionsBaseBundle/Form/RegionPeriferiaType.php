<?php

namespace Gnosis\ElectionsBaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RegionPeriferiaType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           ->add(
                   'title',
                   null,
                   array(
                       'label' => 'elections.regions.title',
                       'help' => 'elections.regions.title_help',
                   )
                )
            ->add(
                    'seira',
                    null,
                    array(
                        'label'=> 'elections.regions.seira',
                        'help' => 'elections.regions.seira_help'
                        )
                    )
            ->add(
                    'ypes_ota_id', 
                    null, 
                    array(
                        'required'=>false, 
                        'label'=>'elections.regions.ypes_ota_id',
                        'help'=>'elections.regions.ypes_ota_id_help',
                        )
                    )
            ->add(
                    'prev_id',
                    null,
                    array(
                        'label'=> 'elections.regions.prev_id',
                        'help' => 'elections.regions.prev_id_help',
                        )
                    )
            ->add('save', 'submit', array('label'=>'Αποθήκευση','attr'=>array('class'=>'btn blue')))
            ->add('saveAndAdd', 'submit', array('label'=>'gsprod.general.save_and_add','attr'=>array('class'=>'btn blue')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gnosis\ElectionsBaseBundle\Entity\RegionPeriferia'
        ));
        $resolver->setRequired(array(
            'parentLevel',
        ));

    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gnosis_electionsbasebundle_regionnomos';
    }
}
