<?php

namespace Gnosis\ElectionsBaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Gnosis\ElectionsBaseBundle\Util\EklogesConsts;

class EklogesType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $etypes = array(
            EklogesConsts::ELECTION_TYPE_DIMO => 'Δημοτικές',
            EklogesConsts::ELECTION_TYPE_PERI => 'Περιφερειακές',
            EklogesConsts::ELECTION_TYPE_BOYL => 'Εθνικές',
            EklogesConsts::ELECTION_TYPE_EURO => 'Ευρωεκλογές',
        );
        
        $builder
            ->add(
                    'title', 
                    null, 
                    array(
                        'label'=>'elections.ekloges.title',
                        'help' => 'elections.ekloges.title_help'
                        )
                    )
            ->add(
                    'long_title', 
                    null, 
                    array(
                        'label'=>'elections.ekloges.long_title',
                        'help' => 'elections.ekloges.long_title_help'
                        )
                    )
            ->add(
                    'active', 
                    null, 
                    array(
                        'label'=>'Ενεργές', 
                        'required'=>false,
                        'help' => 'elections.ekloges.active_help'
                        )
                    )
            ->add(
                    'published', 
                    null, 
                    array(
                        'label'=>'Δημόσιες', 
                        'required'=>false,
                        'help'=> 'elections.ekloges.public_help',
                        )
                    )
            ->add(
                'etype', 
                'choice', 
                array(
                    'label'=>'Κατηγορία',
                    'choices'=>$etypes,
                    )
                )
            ->add(
                    'hasStaurous', 
                    null, 
                    array(
                        'label'=>'elections.ekloges.hasStaurous', 
                        'required'=>false,
                        'help'=> 'elections.ekloges.hasStaurous_help'
                        )
                    )
            ->add(
                    'prev_id', 
                    null, 
                    array(
                        'label'=>'elections.ekloges.prev_id',
                        'help'=> 'elections.ekloges.prev_id_help'
                        )
                    )
            ->add(
                    'seira', 
                    null, 
                    array(
                        'label'=>'elections.ekloges.seira',
                        'help'=> 'elections.ekloges.seira_help'
                        )
                    )
            ->add(
                    'ypes_type', 
                    null, 
                    array(
                        'label'=>'elections.ekloges.ypes_type', 
                        'required'=>false,
                        'help'=> 'elections.ekloges.ypes_type_help'
                        )
                    )
            ->add('save', 'submit', array('label'=>'Αποθήκευση','attr'=>array('class'=>'btn blue')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gnosis\ElectionsBaseBundle\Entity\Ekloges'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gnosis_electionsbasebundle_ekloges';
    }
}
