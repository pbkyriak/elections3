<?php

namespace Gnosis\ElectionsBaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SyndiasmosColorType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                    'title', 
                    null, 
                    array(
                        'label'=>'elections.syndColors.title', 
                        'help'=>'elections.syndColors.title_help',
                        )
                    )
            ->add('color', 
                    null, 
                    array(
                        'label'=>'elections.syndColors.color', 
                        'help'=>'elections.syndColors.color_help',
                        )
                    )
            ->add('save', 'submit', array('label'=>'Αποθήκευση','attr'=>array('class'=>'btn blue')))
            ->add('saveAndAdd', 'submit', array('label'=>'gsprod.general.save_and_add','attr'=>array('class'=>'btn blue')))

        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gnosis\ElectionsBaseBundle\Entity\SyndiasmosColor'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gnosis_electionsbasebundle_syndiasmoscolor';
    }
}
