<?php

namespace Gnosis\ElectionsBaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RegionDimosType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('seira')
            ->add('ypes_ota_id', null, array('required'=>false, 'label'=>'elections.regions.ypes_ota_id'))
            ->add('prev_id')
            ->add('save', 'submit', array('label'=>'Αποθήκευση','attr'=>array('class'=>'btn blue')))
            ->add('saveAndAdd', 'submit', array('label'=>'gsprod.general.save_and_add','attr'=>array('class'=>'btn blue')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Gnosis\ElectionsBaseBundle\Entity\RegionDimos'
        ));
        $resolver->setRequired(array(
            'parentLevel',
        ));

    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'gnosis_electionsbasebundle_regionnomos';
    }
}
