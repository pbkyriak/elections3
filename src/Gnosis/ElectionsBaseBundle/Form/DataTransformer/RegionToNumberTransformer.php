<?php
namespace Gnosis\ElectionsBaseBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use Gnosis\ElectionsBaseBundle\Entity\Region;

/**
 * Description of RegionToNumberTransformer
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class RegionToNumberTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (Region) to a string (number).
     *
     * @param  Region|null $issue
     * @return string
     */
    public function transform($region)
    {
        if (null === $region) {
            return "";
        }

        return $region->getId();
    }

    /**
     * Transforms a string (number) to an object (Region).
     *
     * @param  string $number
     *
     * @return Article|null
     *
     * @throws TransformationFailedException if object (Region) is not found.
     */
    public function reverseTransform($number)
    {
        if (!$number) {
            return null;
        }

        $region = $this->om
            ->getRepository('GnosisElectionsBaseBundle:Region')
            ->findOneBy(array('id' => $number))
        ;

        if (null === $region) {
            throw new TransformationFailedException(sprintf(
                'A region with id "%s" does not exist!',
                $number
            ));
        }

        return $region;
    }
    
}

