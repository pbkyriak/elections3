<?php
namespace Gnosis\ElectionsBaseBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;

/**
 * Description of RegionToNumberTransformer
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class EklogesToNumberTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (Ekloges) to a string (number).
     *
     * @param  Region|null $issue
     * @return string
     */
    public function reverseTransform($ekloges)
    {
        if (null === $ekloges) {
            return "";
        }

        return $ekloges->getId();
    }

    /**
     * Transforms a string (number) to an object (Ekloges).
     *
     * @param  string $number
     *
     * @return Ekloges|null
     *
     * @throws TransformationFailedException if object (Ekloges) is not found.
     */
    public function transform($number)
    {
        if (!$number) {
            return null;
        }

        $ekloges = $this->om
            ->getRepository('GnosisElectionsBaseBundle:Ekloges')
            ->findOneBy(array('id' => $number))
        ;

        if (null === $ekloges) {
            throw new TransformationFailedException(sprintf(
                'Ekloges with id "%s" does not exist!',
                $number
            ));
        }

        return $ekloges;
    }
    
}

