<?php

namespace Gnosis\ElectionsBaseBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

/**
 * Description of RegionParentSubscriber
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Dec 5, 2016
 */
class RegionParentSubscriber implements EventSubscriberInterface {

    public static function getSubscribedEvents() {
        // Tells the dispatcher that you want to listen on the form.pre_set_data
        // event and that the preSetData method should be called.
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    public function preSetData(FormEvent $event) {
        $region = $event->getData();
        $form = $event->getForm();

        if( $region || $region->getId()!==null ) {
            $form->remove('parent');
        }
        
    }

}
