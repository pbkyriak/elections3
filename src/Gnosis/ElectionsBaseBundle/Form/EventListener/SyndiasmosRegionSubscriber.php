<?php

namespace Gnosis\ElectionsBaseBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Description of SyndiasmosRegionSubscriber
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Dec 10, 2016
 */
class SyndiasmosRegionSubscriber implements EventSubscriberInterface {

    public static function getSubscribedEvents() {
        // Tells the dispatcher that you want to listen on the form.pre_set_data
        // event and that the preSetData method should be called.
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    public function preSetData(FormEvent $event) {
        $entity = $event->getData();
        $form = $event->getForm();

        if( $entity && $entity->getId()!==null ) {
            $form->remove('region');
        }
        
    }

}
