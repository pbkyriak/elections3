<?php

namespace Gnosis\ElectionsBaseBundle\Util;

/**
 * Creates and deletes region psifoi rows for a syndiasmo
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Dec 10, 2016
 */
class RegionRowsPsifoi {
    
    private $em;
    private $nomos;
    private $syndiasmos;
    
    public function __construct($em) {
        $this->em = $em;
    }
        
    public function create($syndiasmos, $nomos) {
        $this->syndiasmos = $syndiasmos;
        $this->nomos = $nomos;
        
        $this->insertPeriferiaRow();    // perifereia
        if( $syndiasmos->getElection()->getEtype()== EklogesConsts::ELECTION_TYPE_DIMO) {
            $this->insertRRRow($this->syndiasmos->getId(), $this->nomos->getId());
            $this->insertForRegionSubRegions($this->nomos);
        }
        else {
            $this->insertPeriferiaRow();
            $this->insertForAllNomoi();
        }
    }
    
    protected function insertForAllNomoi() {
        $nomoi = $this->nomos->getParent()->getSubRegions();
        foreach($nomoi as $nomos) {
            $this->insertRRRow($this->syndiasmos->getId(), $nomos->getId());
            $this->insertForRegionSubRegions($nomos);
        }
    }
    
    protected function insertForRegionSubRegions($region) {
        $subRegions = $region->getSubRegions();
        foreach($subRegions as $subRegion) {
            $this->insertRRRow($this->syndiasmos->getId(), $subRegion->getId());
            if( $subRegion->getSubRegions() ) {
                $this->insertForRegionSubRegions($subRegion);
            }
        }
    }
        
    protected function insertPeriferiaRow() {
        $row = $this->em
            ->getRepository('GnosisElectionsBaseBundle:RegionPsifoi')
            ->findOneBy(array('syndiasmos'=>$this->syndiasmos, 'region'=>$this->nomos->getParent()));
        if( !$row ) {
            $this->insertRRRow($this->syndiasmos->getId(), $this->nomos->getParent()->getId());    // periferia
        }
    }
    
    protected function insertRRRow($syndId, $regId) {
        $data = array(
            'region_id' => $regId,
            'syndiasmos_id' => $syndId,
            'amount' => 0,
        );
        $this->em->getConnection()->insert('region_psifoi', $data);
    }
}
