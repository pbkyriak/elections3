<?php

namespace Gnosis\ElectionsBaseBundle\Util;

/**
 * Description of RegionRowsStauroi
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Dec 10, 2016
 */
class RegionRowsStauroi {
    private $em;
    private $nomos;
    private $ypopsifios;
    
    public function __construct($em) {
        $this->em = $em;
    }
        
    public function create($ypopsifios, $nomos) {
        $this->ypopsifios = $ypopsifios;
        $this->nomos = $nomos;
        
        $this->insertPeriferiaRow();    // perifereia
        if( $ypopsifios->getElection()->getEtype()== EklogesConsts::ELECTION_TYPE_EURO) {
            $this->insertPeriferiaRow();
            $this->insertForAllNomoi();
        }
        else {
            $this->insertRRRow($this->ypopsifios->getId(), $this->nomos->getId());
            $this->insertForRegionSubRegions($this->nomos);
        }
    }
    
    protected function insertForAllNomoi() {
        $nomoi = $this->nomos->getParent()->getSubRegions();
        foreach($nomoi as $nomos) {
            $this->insertRRRow($this->ypopsifios->getId(), $nomos->getId());
            $this->insertForRegionSubRegions($nomos);
        }
    }
    
    protected function insertForRegionSubRegions($region) {
        $subRegions = $region->getSubRegions();
        foreach($subRegions as $subRegion) {
            $this->insertRRRow($this->ypopsifios->getId(), $subRegion->getId());
            if( $subRegion->getSubRegions() ) {
                $this->insertForRegionSubRegions($subRegion);
            }
        }
    }
    
    protected function insertPeriferiaRow() {
        //printf("a %s \n", $ypoId);
        $row = $this->em
            ->getRepository('GnosisElectionsBaseBundle:RegionStauroi')
            ->findOneBy(array('ypopsifios'=>$this->ypopsifios, 'region'=>$this->nomos->getParent()));
        if( !$row ) {
            $this->insertRRRow($this->ypopsifios->getId(), $this->nomos->getParent()->getId());    // periferia
        }
    }
    
    protected function insertRRRow($ypoId, $regId) {
        $data = array(
            'region_id' => $regId,
            'ypopsifios_id' => $ypoId,
            'amount' => 0,
        );
        $this->em->getConnection()->insert('region_stauroi', $data);
    }
}
