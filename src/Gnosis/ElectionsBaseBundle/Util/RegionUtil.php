<?php

namespace Gnosis\ElectionsBaseBundle\Util;

/**
 * Description of RegionUtil
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 9 Απρ 2014  
 */
class RegionUtil
{
    /**
     * Return next level's region key.
     * 
     * @param string $regKey
     * @return string|boolean
     */
    public static function getNextRegKey($regKey) {
        switch ($regKey) {
            case 'd':
                return 'ep';
            case 'ep':
                return 'de';
            case 'de':
                return 'et';
            default:
                return false;
        }
        
    }   
    
    public static function getPrevRegKey($regKey) {
        switch ($regKey) {
            case 'd':
                return '';
            case 'ep':
                return 'd';
            case 'de':
                return 'ep';
            case 'et':
                return 'de';
            default:
                return false;
        }
    }   
    
    public static function regKeyToDiscriminator($regKey) {
        switch ($regKey) {
            case 'd':
                return 'dimos';
            case 'ep':
                return 'eklPeriferia';
            case 'de':
                return 'dimEnotita';
            case 'et':
                return 'eklTmima';
        }
    }
    
    public static function discriminatorToRegKey($regKey) {
        switch ($regKey) {
            case 'dimos':
                return 'd';
            case 'eklPeriferia':
                return 'ep';
            case 'dimEnotita':
                return 'de';
            case 'eklTmima':
                return 'et';
        }
    }
    
    public static function getNextDiscriminator($discr) {
        switch($discr) {
            case 'periferia':
                return 'nomos';
            case 'nomos':
                return 'dimos';
            case 'dimos':
                return 'eklPeriferia';
            case 'eklPeriferia':
                return 'dimEnotita';
            case 'dimEnotita':
                return 'eklTmima';
            default:
                return null;
        }
    }
    
    public static function getNextClassTypeFromClass($clazz) {
        $clazz = lcfirst(str_replace('Gnosis\\ElectionsBaseBundle\\Entity\\Region', '', $clazz));
        return RegionUtil::getNextDiscriminator($clazz);
    }
    
    public static function getRegionTextFromClass($clazz) {
    	$clazz = lcfirst(str_replace('Gnosis\\ElectionsBaseBundle\\Entity\\Region', '', $clazz));
        switch($clazz) {
            case 'periferia':
                return 'elections.regions.periferia';
            case 'nomos':
                return 'elections.regions.nomos';
            case 'dimos':
                return 'elections.regions.dimos';
            case 'eklPeriferia':
                return 'eklPeriferia';
            case 'dimEnotita':
                return 'elections.regions.dimEnotita';
            case 'eklTmima':
                return 'elections.regions.eklTmima';
            default:
                return 'elections.regions.undefined';
        }
    }
}
