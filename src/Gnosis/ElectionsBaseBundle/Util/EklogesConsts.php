<?php

namespace Gnosis\ElectionsBaseBundle\Util;

/**
 * Description of EklogesConsts
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 26 Απρ 2014
 */
class EklogesConsts
{
    const ELECTION_TYPE_DIMO = 'dimotikes';
    const ELECTION_TYPE_PERI = 'periferiakes';
    const ELECTION_TYPE_EURO = 'euro';
    const ELECTION_TYPE_BOYL = 'bouleytikes';
}
