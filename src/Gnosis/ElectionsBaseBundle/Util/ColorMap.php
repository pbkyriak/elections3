<?php

namespace Gnosis\ElectionsBaseBundle\Util;

/**
 * Description of ColorMap
 *
 * @author panos
 */
class ColorMap
{

    protected $resultImage = null;

    public function __construct($data, $mapFile)
    {
        $pallete = array();
        $hotPoints = array();
        foreach($data as $row) {
            $pallete[$row['color']] = $this->hexToRgb($row['color']);
            $hotPoints[] = array($row['color'], $row['fillX'], $row['fillY']);
        }
        
        $this->mapChart2($mapFile, $hotPoints, $pallete);
    }

    public function saveImage($outFilename)
    {
        imagepng($this->resultImage, $outFilename);
    }

    private function mapChart2($mapFile, $regionHotPoints, $regionColors)
    {

        $srcMap = $this->loadImageIcon($mapFile);
        if ($srcMap != false) {
            $imgW = imagesx($srcMap);
            $imgH = imagesy($srcMap);
            $img = imageCreateTrueColor($imgW, $imgH);
            $black = imagecolorallocate($img, 0, 0, 0);
            $white = imagecolorAllocate($img, 255, 255, 255);
            $red = imagecolorAllocate($img, 255, 0, 0);

            imagefilledrectangle($img, 0, 0, $imgW, $imgH, $white);
            imagecopy($img, $srcMap, 0, 0, 0, 0, $imgW, $imgH);
            $colors = array();
            foreach ($regionColors as $key => $colorRGB) {
                //$key = sprintf("%02x%02x%02x", $colorRGB[0], $colorRGB[1],
                //    $colorRGB[2]);
                $colors[$key] = imagecolorallocate($img, intval($colorRGB[0]),
                    intval($colorRGB[1]), intval($colorRGB[2]));
            }
            // fill regions
            $pointCount = count($regionHotPoints);
            for ($i = 0; $i < $pointCount; $i++) {
                if (array_key_exists($regionHotPoints[$i][0], $regionColors))
                    $color = $colors[$regionHotPoints[$i][0]];
                else
                    $color = $white;
                imagefill($img, $regionHotPoints[$i][1],
                    $regionHotPoints[$i][2], $color); //
            }
            imagecolortransparent($img, $white);
            //imagefilter($img, IMG_FILTER_GAUSSIAN_BLUR);
            // save image
            $this->resultImage = $img;
        } else {
            $this->quitErrImgMessage("map file not found!");
            return;
        }
    }

    private function loadImageIcon($iconFile)
    {
        $ext = "";
        $pos = strRPos($iconFile, ".");

        if ($pos != false) {
            $ext = strtolower(substr($iconFile, $pos + 1,
                    strlen($iconFile) - $pos + 1));
            if ($ext != false) {
                if (strcmp($ext, "jpg") == 0) {
                    $im = @imagecreatefromjpeg($iconFile); //Attempt to open
                } elseif (strcmp($ext, "png") == 0) {
                    $im = imagecreatefrompng($iconFile);
                } elseif (strcmp($ext, "bmp") == 0) {
                    $im = @imagecreatefromwbmp($iconFile);
                } elseif (strcmp($ext, "xpm") == 0) {
                    $im = @imagecreatefromxpm($iconFile);
                } elseif (strcmp($ext, "gif") == 0) {
                    $im = @imagecreatefromgif($iconFile);
                } else
                    $im = false;
            } else
                $im = false;
        } else
            $im = false;

        if (!$im) { /* See if it failed */
            $im = imagecreate(30, 30); /* Create a blank image */
            $bgc = imagecolorallocate($im, 255, 255, 255);
            $tc = imagecolorallocate($im, 255, 0, 0);
            imagefilledrectangle($im, 0, 0, 30, 30, $bgc);
            imageline($im, 5, 5, 25, 25, $tc);
            imageline($im, 25, 5, 5, 25, $tc);
            imagestring($im, 4, 5, 5, $ext, $tc);
        }
        return $im;
    }

    private function quitErrImgMessage($msg)
    {
        //header("Content-type: image/png");
        $im = imagecreate(300, 300); /* Create a blank image */
        $bgc = imagecolorallocate($im, 255, 255, 255);
        $tc = imagecolorallocate($im, 255, 0, 0);
        imagefilledrectangle($im, 0, 0, imgW, imgH, $bgc);
        imagestring($im, 4, 5, 5, $msg, $tc);
        $this->resultImage = $im;
        exit;
    }

    private function hexToRgb($hexColor) {
       $colorVal = hexdec($hexColor);
        $rgbArray[] = 0xFF & ($colorVal >> 0x10);
        $rgbArray[] = 0xFF & ($colorVal >> 0x8);
        $rgbArray[] = 0xFF & $colorVal;
        return $rgbArray;
    }
}
