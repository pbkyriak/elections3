<?php
namespace Gnosis\ElectionsBaseBundle\Validator;

use Symfony\Component\Validator\Constraint;
/**
 * Description of AcceptableRegionEklTmimaEntry
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Dec 16, 2016
 */
class AcceptableRegionEklTmimaCode extends Constraint
{
    
    public $message = 'elections.validation.ekltm_code_exists';
    
    public function validatedBy() {
        return 'ekltm_code_validator';
    }
    
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}