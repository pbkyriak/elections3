<?php

namespace Gnosis\ElectionsBaseBundle\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\NonUniqueResultException;

/**
 * Description of AcceptableRegionEklTmimaCodeValidator
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Dec 16, 2016
 */
class AcceptableRegionEklTmimaCodeValidator extends ConstraintValidator {

    private $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    /**
     * 
     * @param PsifoiEntry $entry
     * @param Constraint $constraint
     */
    public function validate($entry, Constraint $constraint) {
        $violate = false;
        $idp = sprintf("%s,%%",implode(',',array_slice(explode(',',$entry->getIdPath()),0,2)));
        $qb = $this->em->createQueryBuilder()
                ->select('e')
                ->from('GnosisElectionsBaseBundle:RegionEklTmima', 'e')
                ->where('e.ekltm_code=:ec')
                ->andWhere('e.id_path like :idp')
                ->setParameter('ec', $entry->getEkltmCode())
                ->setParameter('idp', $idp);
        if( $entry->getId() ) {
            $qb->andWhere('e.id!=:eid')->setParameter('eid', $entry->getId());
        }
        try {
            $et = $qb->getQuery()->getOneOrNullResult();
            if( $et ) {
                $violate = true;
            }
                
        }
        catch(NonUniqueResultException $ex) {
            $violate = true;
        }
        if ($violate) {
            $this->context->addViolationAt('ekltm_code', $constraint->message, array('{{value}}'=>$entry->getEkltmCode()), null);
        }
    }

}
