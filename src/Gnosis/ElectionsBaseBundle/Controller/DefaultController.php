<?php

namespace Gnosis\ElectionsBaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('GnosisElectionsBaseBundle:Default:index.html.twig', array('name' => $name));
    }
}
