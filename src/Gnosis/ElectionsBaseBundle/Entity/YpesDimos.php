<?php

namespace Gnosis\ElectionsBaseBundle\Entity;

/**
 * YpesDimos
 */
class YpesDimos
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $ota_code;

    /**
     * @var string
     */
    private $ota_title;

    /**
     * @var string
     */
    private $pe_code;

    /**
     * @var string
     */
    private $pe_title;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set otaCode
     *
     * @param string $otaCode
     *
     * @return YpesDimos
     */
    public function setOtaCode($otaCode)
    {
        $this->ota_code = $otaCode;

        return $this;
    }

    /**
     * Get otaCode
     *
     * @return string
     */
    public function getOtaCode()
    {
        return $this->ota_code;
    }

    /**
     * Set otaTitle
     *
     * @param string $otaTitle
     *
     * @return YpesDimos
     */
    public function setOtaTitle($otaTitle)
    {
        $this->ota_title = $otaTitle;

        return $this;
    }

    /**
     * Get otaTitle
     *
     * @return string
     */
    public function getOtaTitle()
    {
        return $this->ota_title;
    }

    /**
     * Set peCode
     *
     * @param string $peCode
     *
     * @return YpesDimos
     */
    public function setPeCode($peCode)
    {
        $this->pe_code = $peCode;

        return $this;
    }

    /**
     * Get peCode
     *
     * @return string
     */
    public function getPeCode()
    {
        return $this->pe_code;
    }

    /**
     * Set peTitle
     *
     * @param string $peTitle
     *
     * @return YpesDimos
     */
    public function setPeTitle($peTitle)
    {
        $this->pe_title = $peTitle;

        return $this;
    }

    /**
     * Get peTitle
     *
     * @return string
     */
    public function getPeTitle()
    {
        return $this->pe_title;
    }
}

