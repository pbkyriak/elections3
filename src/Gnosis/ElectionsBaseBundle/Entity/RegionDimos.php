<?php

namespace Gnosis\ElectionsBaseBundle\Entity;

/**
 * Description of RegionNomos
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class RegionDimos extends Region {
    private $ypes_ota_id;
    
    public function getYpesOtaId() {
        return $this->ypes_ota_id;
    }
    
    public function setYpesOtaId($v) {
        $this->ypes_ota_id = $v;
        return $this;
    }
}
