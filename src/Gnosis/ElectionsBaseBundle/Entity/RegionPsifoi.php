<?php

namespace Gnosis\ElectionsBaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RegionPsifoi hold psifous for a syndiasmos in a region.
 * 
 */
class RegionPsifoi
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $psifoi;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\Region
     */
    private $region;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\Syndiasmos
     */
    private $syndiasmos;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set psifoi
     *
     * @param integer $psifoi
     * @return RegionPsifoi
     */
    public function setPsifoi($psifoi)
    {
        $this->psifoi = $psifoi;

        return $this;
    }

    /**
     * Get psifoi
     *
     * @return integer 
     */
    public function getPsifoi()
    {
        return $this->psifoi;
    }

    /**
     * Set region
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Region $region
     * @return RegionPsifoi
     */
    public function setRegion(\Gnosis\ElectionsBaseBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Region 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set syndiasmos
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Syndiasmos $syndiasmos
     * @return RegionPsifoi
     */
    public function setSyndiasmos(\Gnosis\ElectionsBaseBundle\Entity\Syndiasmos $syndiasmos = null)
    {
        $this->syndiasmos = $syndiasmos;

        return $this;
    }

    /**
     * Get syndiasmos
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Syndiasmos 
     */
    public function getSyndiasmos()
    {
        return $this->syndiasmos;
    }
    /**
     * @var integer
     */
    private $amount;


    /**
     * Set amount
     *
     * @param integer $amount
     * @return RegionPsifoi
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }
}
