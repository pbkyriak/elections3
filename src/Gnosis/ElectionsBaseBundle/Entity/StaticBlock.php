<?php

namespace Gnosis\ElectionsBaseBundle\Entity;

/**
 * Description of StaticBlock
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Sep 13, 2016
 */
class StaticBlock {

    use \Gnosis\ElectionsEntryBaseBundle\Entity\LogUserActionsTrait;
    
    private $id;
    private $block_code;
    private $content;
    
    
    function getId() {
        return $this->id;
    }

    function getBlockCode() {
        return $this->block_code;
    }

    function getContent() {
        return $this->content;
    }

    function setId($id) {
        $this->id = $id;
        return $this;
    }

    function setBlockCode($block_code) {
        $this->block_code = $block_code;
        return $this;
    }

    function setContent($content) {
        $this->content = $content;
        return $this;
    }

    public function __toString() {
        return sprintf("Static block");
    }
    
}
