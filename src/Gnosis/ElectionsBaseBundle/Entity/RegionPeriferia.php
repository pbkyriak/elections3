<?php

namespace Gnosis\ElectionsBaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of RegionPeriferia
 * @ORM\Entity @ORM\HasLifecycleCallbacks 
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class RegionPeriferia extends Region
{

    private $ypes_ota_id;
    
    public function getYpesOtaId() {
        return $this->ypes_ota_id;
    }
    
    public function setYpesOtaId($v) {
        $this->ypes_ota_id = $v;
        return $this;
    }

    public function getUploadsDir() {
        return sprintf("%s/%s",
            $this->getElection()->getUploadsDir(),
            $this->getSlug()
            );
    }

    /** @ORM\PostPersist */
    public function onPostPersist()
    {
        $udir = $this->getUploadsDir();
        if( !file_exists($udir) ) {
            mkdir($udir, 0777);
        }
    }

}
