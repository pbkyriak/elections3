<?php

namespace Gnosis\ElectionsBaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Syndiasmos
 */
class Syndiasmos
{
    use \Gnosis\ElectionsEntryBaseBundle\Entity\LogUserActionsTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $ypopsifioi;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\Ekloges
     */
    private $election;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\Region
     */
    private $region;

    private $slug;
    
    private $prev_id;
    private $color = '#ffffff';
    
    private $logo;
    private $logoRelPath;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ypopsifioi = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Syndiasmos
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add ypopsifioi
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Ypopsifios $ypopsifioi
     * @return Syndiasmos
     */
    public function addYpopsifioi(\Gnosis\ElectionsBaseBundle\Entity\Ypopsifios $ypopsifioi)
    {
        $this->ypopsifioi[] = $ypopsifioi;

        return $this;
    }

    /**
     * Remove ypopsifioi
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Ypopsifios $ypopsifioi
     */
    public function removeYpopsifioi(\Gnosis\ElectionsBaseBundle\Entity\Ypopsifios $ypopsifioi)
    {
        $this->ypopsifioi->removeElement($ypopsifioi);
    }

    /**
     * Get ypopsifioi
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getYpopsifioi()
    {
        return $this->ypopsifioi;
    }

    /**
     * Set election
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Ekloges $election
     * @return Syndiasmos
     */
    public function setElection(\Gnosis\ElectionsBaseBundle\Entity\Ekloges $election = null)
    {
        $this->election = $election;

        return $this;
    }

    /**
     * Get election
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Ekloges 
     */
    public function getElection()
    {
        return $this->election;
    }

    /**
     * Set region
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Region $region
     * @return Syndiasmos
     */
    public function setRegion(\Gnosis\ElectionsBaseBundle\Entity\Region $dimos = null)
    {
        $this->region = $dimos;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Region
     */
    public function getRegion()
    {
        return $this->region;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $psifoi;


    /**
     * Add psifoi
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\RegionPsifoi $psifoi
     * @return Syndiasmos
     */
    public function addPsifoi(\Gnosis\ElectionsBaseBundle\Entity\RegionPsifoi $psifoi)
    {
        $this->psifoi[] = $psifoi;

        return $this;
    }

    /**
     * Remove psifoi
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\RegionPsifoi $psifoi
     */
    public function removePsifoi(\Gnosis\ElectionsBaseBundle\Entity\RegionPsifoi $psifoi)
    {
        $this->psifoi->removeElement($psifoi);
    }

    /**
     * Get psifoi
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPsifoi()
    {
        return $this->psifoi;
    }
    
    /**
     * Set slug
     *
     * @param string $slug
     * @return Region
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    public function setPrevId($v) {
        $this->prev_id=$v;
        return $this;
    }
    
    public function getPrevId() {
        return $this->prev_id;
    }

    public function getColor() {
        return $this->color;
    }
    
    public function setColor($v) {
        $this->color = $v;
        return $this;
    }
    
    public function getLogo() {
        return $this->logo;
    }
    
    public function setLogo($v) {
        $this->logo = $v;
        return $this;
    }

    public function setLogoRelPath($v) {
        $this->logoRelPath = $v;
        return $this;
    }
    
    public function getLogoRelPath() {
        if(file_exists($this->getLogo())) {
            return $this->logoRelPath;
        }
        else {
            return '';
        }
    }
    
    public function __toString() {
        return sprintf("Συνδυασμός: %s", $this->getTitle());
    }

}
