<?php

namespace Gnosis\ElectionsBaseBundle\Entity;

use Slx\MetronicBundle\Lib\GreekText;

/**
 * Region is the super class for all regions in elections.
 * Periferia, Nomos, dimos, eklPeriferia, dimEnotita and eklTmima subclass this one.
 * Single table inheritance is employed to store all data to a single table.
 * Population data are also embended here. Eggegramenoi, psifisan, leyka, akyra, egkyra.
 * 
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class Region
{
    
    use \Gnosis\ElectionsEntryBaseBundle\Entity\LogUserActionsTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\Ekloges
     */
    private $election;

    private $parent;

    private $slug;
    
    /**
     * @var integer
     */
    private $eggegramenoi=0;

    /**
     * @var integer
     */
    private $psifisan=0;

    /**
     * @var integer
     */
    private $leyka=0;

    /**
     * @var integer
     */
    private $akyra=0;

    /**
     * @var integer
     */
    private $egkyra=0;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $subRegions;
    private $seira=0;
    private $id_path='';
    private $level =0;
    private $et_count =0;
    private $et_counted=0;
    private $prev_id=null;
    private $egg_counted=0;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->subRegions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Region
     */
    public function setTitle($title)
    {
        $this->title = $title;
        $this->setSlug($this->slugify('', $title));
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set election
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Ekloges $election
     * @return Region
     */
    public function setElection(\Gnosis\ElectionsBaseBundle\Entity\Ekloges $election = null)
    {
        $this->election = $election;

        return $this;
    }

    /**
     * Get election
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Ekloges 
     */
    public function getElection()
    {
        return $this->election;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Region
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set eggegramenoi
     *
     * @param integer $eggegramenoi
     * @return Region
     */
    public function setEggegramenoi($eggegramenoi)
    {
        $this->eggegramenoi = $eggegramenoi;

        return $this;
    }

    /**
     * Get eggegramenoi
     *
     * @return integer 
     */
    public function getEggegramenoi()
    {
        return $this->eggegramenoi;
    }

    /**
     * Set psifisan
     *
     * @param integer $psifisan
     * @return Region
     */
    public function setPsifisan($psifisan)
    {
        $this->psifisan = $psifisan;

        return $this;
    }

    /**
     * Get psifisan
     *
     * @return integer 
     */
    public function getPsifisan()
    {
        return $this->psifisan;
    }

    /**
     * Set leyka
     *
     * @param integer $leyka
     * @return Region
     */
    public function setLeyka($leyka)
    {
        $this->leyka = $leyka;

        return $this;
    }

    /**
     * Get leyka
     *
     * @return integer 
     */
    public function getLeyka()
    {
        return $this->leyka;
    }

    /**
     * Set akyra
     *
     * @param integer $akyra
     * @return Region
     */
    public function setAkyra($akyra)
    {
        $this->akyra = $akyra;

        return $this;
    }

    /**
     * Get akyra
     *
     * @return integer 
     */
    public function getAkyra()
    {
        return $this->akyra;
    }

    /**
     * Set egkyra
     *
     * @param integer $egkyra
     * @return Region
     */
    public function setEgkyra($egkyra)
    {
        $this->egkyra = $egkyra;

        return $this;
    }

    /**
     * Get egkyra
     *
     * @return integer 
     */
    public function getEgkyra()
    {
        return $this->egkyra;
    }

    /**
     * Add subRegions
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Region $subRegions
     * @return Region
     */
    public function addSubRegion(\Gnosis\ElectionsBaseBundle\Entity\Region $subRegions)
    {
        $this->subRegions[] = $subRegions;

        return $this;
    }

    /**
     * Remove subRegions
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Region $subRegions
     */
    public function removeSubRegion(\Gnosis\ElectionsBaseBundle\Entity\Region $subRegions)
    {
        $this->subRegions->removeElement($subRegions);
    }

    /**
     * Get subRegions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSubRegions()
    {
        return $this->subRegions;
    }

    /**
     * Set parent
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Region $parent
     * @return Region
     */
    public function setParent(\Gnosis\ElectionsBaseBundle\Entity\Region $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Region 
     */
    public function getParent()
    {
        return $this->parent;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $psifoi;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $stauroi;


    /**
     * Add psifoi
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\RegionPsifoi $psifoi
     * @return Region
     */
    public function addPsifoi(\Gnosis\ElectionsBaseBundle\Entity\RegionPsifoi $psifoi)
    {
        $this->psifoi[] = $psifoi;

        return $this;
    }

    /**
     * Remove psifoi
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\RegionPsifoi $psifoi
     */
    public function removePsifoi(\Gnosis\ElectionsBaseBundle\Entity\RegionPsifoi $psifoi)
    {
        $this->psifoi->removeElement($psifoi);
    }

    /**
     * Get psifoi
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPsifoi()
    {
        return $this->psifoi;
    }

    /**
     * Add stauroi
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\RegionStauroi $stauroi
     * @return Region
     */
    public function addStauroi(\Gnosis\ElectionsBaseBundle\Entity\RegionStauroi $stauroi)
    {
        $this->stauroi[] = $stauroi;

        return $this;
    }

    /**
     * Remove stauroi
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\RegionStauroi $stauroi
     */
    public function removeStauroi(\Gnosis\ElectionsBaseBundle\Entity\RegionStauroi $stauroi)
    {
        $this->stauroi->removeElement($stauroi);
    }

    /**
     * Get stauroi
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getStauroi()
    {
        return $this->stauroi;
    }
    
    public function getSeira() {
        return $this->seira;
    }
    
    public function setSeira($v) {
        $this->seira = $v;
        return $this;
    }

    public function getIdPath() {
        return $this->id_path;
    }
    
    public function setIdPath($v) {
        $this->id_path = $v;
        $this->setLevel(substr_count($v,',')+1);
        return $this;
    }

    public function slugify($nomos) {
        if( trim($nomos) ) {
            $slug = trim($nomos.' '.$this->title);
        }
        else {
            $slug = trim($this->title);
        }
        $slug = GreekText::slugify($slug);
        return $slug;
    }
    
    /**
     * Do this after entity is flushed (has its own id)
     */
    public function updateMyIdPath() {
        if( $this->getParent() ) {
            $ids = explode(',',$this->getParent()->getIdPath());
        }
        else {
            $ids = array();
        }
        $ids[] = $this->getId();
        $this->setIdPath(implode(',',$ids));
    }
    
    public function getEtCount() {
        return $this->et_count;
    }
    
    public function setEtCount($v) {
        $this->et_count = $v;
        return $this;
    }

    public function getEtCounted() {
        return $this->et_counted;
    }
    
    public function setEtCounted($v) {
        $this->et_counted = $v;
        return $this;
    }

    public function setPrevId($v) {
        $this->prev_id=$v;
        return $this;
    }
    
    public function getPrevId() {
        return $this->prev_id;
    }

    public function setLevel($v) {
        $this->level=$v;
        return $this;
    }
    
    public function getLevel() {
        return $this->level;
    }

    public function getNextLevelsNameG() {
        switch($this->level) {
            case 1:
                return 'Νομό';
            case 2:
                return 'Δήμο';
            case 3:
                return 'Εκλ. Περιφέρεια';
            case 4:
                return 'Δ/Τ Ενότητα';
            case 5:
                return 'Εκλ. Τμήμα';
        }
        return null;
    }

    public function getEggCounted() {
        return $this->egg_counted;
    }
    
    public function setEggCounted($v) {
        $this->egg_counted = $v;
        return $this;
    }
    
    public function __toString() {
        return sprintf("Region %s %s (%s)", $this->getLevel(), $this->getTitle(), $this->getId());
    }
}
