<?php

namespace Gnosis\ElectionsBaseBundle\Entity;

use Slx\MetronicBundle\Lib\GreekText;

/**
 * Description of SyndiasmosColor
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 9 Μαϊ 2014
 */
class SyndiasmosColor
{
    private $id;
    private $ekloges_id;
    private $title;
    private $slug;
    private $color;
    
    public function getId() {
        return $this->id;
    }
    
    public function setId($v) {
        $this->id=$v;
        return $this;
    }
    
    public function getEklogesId()
    {
        return $this->ekloges_id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setEklogesId($ekloges_id)
    {
        $this->ekloges_id = $ekloges_id;
        return $this;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        $this->setSlug(GreekText::slugify(trim($title)));
        return $this;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }


}
