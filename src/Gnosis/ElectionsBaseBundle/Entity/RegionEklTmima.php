<?php

namespace Gnosis\ElectionsBaseBundle\Entity;

/**
 * Description of RegionEklPeriferia
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class RegionEklTmima extends Region {

    private $is_eterodim=false;
    private $ekltm_code = 0;
    
    public function getEtNumber() {
        $out = '';
        if( preg_match("/^[0-9]{1,3}/", $this->getTitle(), $matches) ) {
            $out = $matches[0];
        }
        return $out;
    }

    public function getIsEterodim() {
        return $this->is_eterodim;
    }
    
    public function setIsEterodim($v) {
        $this->is_eterodim = $v;
        return $this;
    }
    
    public function getEkltmCode() {
        return $this->ekltm_code;
    }
    
    public function setEkltmCode($v) {
        $this->ekltm_code = $v;
        return $this;
    }
}
