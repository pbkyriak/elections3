<?php
namespace Gnosis\ElectionsBaseBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima;

/**
 * Description of RegionNomosRepository
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 9 Απρ 2014
 */
class RegionNomosRepository extends EntityRepository
{
    public function getEtIds() {
        
    }
    
    public function getNomosFromEt(RegionEklTmima $et) {
        $pIds = explode(',', $et->getIdPath());
        $nId = $pIds[1];
        return $this->find($nId);
    }
}
