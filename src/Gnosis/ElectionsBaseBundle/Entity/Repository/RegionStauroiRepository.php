<?php
namespace Gnosis\ElectionsBaseBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Gnosis\ElectionsBaseBundle\Util\EklogesConsts;
/**
 * Description of RegionStauroiRepository
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 23 Απρ 2014
 */
class RegionStauroiRepository extends EntityRepository
{

    public function hasYpopsifiosRegionRows($ypoId, $regionId) {
        $out = false;
        $res = $this->createQueryBuilder('rs')
            ->select('count(rs.id) as cnt')
            ->where('rs.region=:r and rs.ypopsifios=:s')
            ->setParameter('r', $regionId)
            ->setParameter('s', $ypoId)
            ->getQuery()
            ->getSingleScalarResult();
        if( $res ) {
            $out = true;
        }
        return $out;
    }

    public function getRegionSyndiasmosStauroi($region, $syndiasmos) {
        $ekloges = $region->getElection();
        $ypoRoot = $this->getYpoLocalityRoot($ekloges, $region->getIdPath());
        $ypoMaxIdPath = $this->getYpoLocalityMaxPath($ekloges, $region->getIdPath());
        $syRoot =  $this->getSyndLocalityRoot($ekloges, $region->getIdPath());
        $qb = $this->createQueryBuilder('rs')
            ->select('yp.title, yp.slug,ypr.title YPRtitle, ypr.slug YPRslug, rs.amount, r.egkyra egkyra')
            ->leftJoin('rs.region', 'r')
            ->leftJoin('rs.ypopsifios', 'yp')
            ->leftJoin('yp.komma', 's')
            ->leftJoin('yp.region', 'ypr')
            ->where('rs.region=:rg and yp.komma=:sy and s.region=:sreg ');  // περιοχή που παίρνουμε αποτελεσματα AND του συνδυασμού που μας ενδιαφέρει AND η περιοχή του συνδυασμού να είναι η περιοχή εμβέλειας του
            
        if( $ekloges->getEtype()==EklogesConsts::ELECTION_TYPE_DIMO) {
            $qb->andWhere('ypr.id_path=:ypoRoot or ypr.id_path like :ypoRoot2')
                ->setParameter('ypoRoot', $ypoRoot)
                ->setParameter('ypoRoot2', $ypoMaxIdPath.'%');
        }
        $qb ->orderBy('ypr.level')
            ->addOrderBy('ypr.title')
            ->addOrderBy('rs.amount', 'desc')
            ->setParameter('rg', $region->getId())
            ->setParameter('sy', $syndiasmos->getId())
            ->setParameter('sreg', $syRoot);
        
        $res = $qb->getQuery()
            ->execute();
        return $res;
    }

    public function getRegionAllStauroi($region) {
        $ekloges = $region->getElection();
        $ypoRoot = $this->getYpoLocalityRoot($ekloges, $region->getIdPath());
        $ypoMaxIdPath = $this->getYpoLocalityMaxPath($ekloges, $region->getIdPath());
        $syRoot =  $this->getSyndLocalityRoot($ekloges, $region->getIdPath());
        $qb = $this->createQueryBuilder('rs')
            ->select('yp.title, yp.slug,ypr.title YPRtitle, ypr.slug YPRslug, rs.amount, r.egkyra egkyra, s.title synTitle')
            ->leftJoin('rs.region', 'r')
            ->leftJoin('rs.ypopsifios', 'yp')
            ->leftJoin('yp.komma', 's')
            ->leftJoin('yp.region', 'ypr')
            ->where('rs.region=:rg and s.region=:sreg ');  // περιοχή που παίρνουμε αποτελεσματα AND η περιοχή να είναι η περιοχή εμβέλειας tων συνδυασμών του τύπου εκλογών
            
        if( $ekloges->getEtype()==EklogesConsts::ELECTION_TYPE_DIMO) {
            $qb->andWhere('ypr.id_path=:ypoRoot or ypr.id_path like :ypoRoot2')
                ->setParameter('ypoRoot', $ypoRoot)
                ->setParameter('ypoRoot2', $ypoMaxIdPath.'%');
        }
        $qb ->orderBy('ypr.level')
            ->addOrderBy('ypr.title')
            ->addOrderBy('rs.amount', 'desc')
            ->setParameter('rg', $region->getId())
            ->setParameter('sreg', $syRoot);
        
        $res = $qb->getQuery()
            ->execute();
        return $res;
    }

    private function getYpoLocalityRoot($ekloges, $regionPath) {
        $out ='';
        $path = explode(',', $regionPath);
        if( $ekloges->getEtype()==EklogesConsts::ELECTION_TYPE_DIMO) {
            if( isset($path[2]) ) {
                $out = array_slice($path, 0,3);
            }
        }
        elseif( $ekloges->getEtype()==EklogesConsts::ELECTION_TYPE_PERI || $ekloges->getEtype()==EklogesConsts::ELECTION_TYPE_BOYL ) {
            if( isset($path[1]) ) {
                $out = array_slice($path, 0,2);
            }            
        } 
        elseif( $ekloges->getEtype()==EklogesConsts::ELECTION_TYPE_EURO) {
            if( isset($path[0]) ) {
                $out = array_slice($path, 0,1);
            }
        }
        if(is_array($out) ) {
            $out = implode(',',$out);
        }
        
        return $out;
    }
    
    private function getYpoLocalityMaxPath($ekloges, $regionPath) {
        $out ='';
        $path = explode(',', $regionPath);
        if( $ekloges->getEtype()==EklogesConsts::ELECTION_TYPE_DIMO) {
            $idx = min(array(5, count($path)));
            if( isset($path[$idx-1]) ) {
                $out = array_slice($path, 0,$idx);
            }
        }
        else {
            $out = $this->getYpoLocalityRoot($ekloges, $regionPath);
        } 
        if(is_array($out) ) {
            $out = implode(',',$out);
        }
        return $out;
    }
    
    private function getSyndLocalityRoot($ekloges, $regionPath) {
        $out ='';
        $path = explode(',', $regionPath);
        if( $ekloges->getEtype()==EklogesConsts::ELECTION_TYPE_DIMO) {
            if( isset($path[2]) ) {
                $out = $path[2];
            }
        }
        else {
            if( isset($path[0]) ) {
                $out = $path[0];
            }
        }
        return $out;
    }
}
