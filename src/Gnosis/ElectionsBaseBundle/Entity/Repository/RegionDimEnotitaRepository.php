<?php

namespace Gnosis\ElectionsBaseBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Gnosis\ElectionsBaseBundle\Entity\RegionDimos;
/**
 * Description of RegionDimEnotitaRepository
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Dec 10, 2016
 */
class RegionDimEnotitaRepository extends EntityRepository {
    
    public function getDimosDimEnotites(RegionDimos $dimos) {
        $idp = sprintf("%s,%%",$dimos->getIdPath());
        $qb = $this->createQueryBuilder('de')
                ->where('de.id_path like :idp')
                ->orderBy("de.title")
                ->setParameter('idp', $idp);
        $res = $qb->getQuery()
                ->execute();
        return $res;

    }

}
