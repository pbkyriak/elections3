<?php
namespace Gnosis\ElectionsBaseBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
/**
 * Description of SyndiasmosRepository
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 23 Απρ 2014
 */
class SyndiasmosRepository extends EntityRepository
{
    public function getRegionSyndiasmoi($regionId) {
        $res = $this->createQueryBuilder('s')
            ->select('s.id, s.slug, rg.id rid')
            ->join('GnosisElectionsBaseBundle:Region', 'rg')
            ->where('s.region=:r')
            ->setParameter('r', $regionId)
            ->getQuery()
            ->getArrayResult();
        return $res;
    }
    
    public function getRegionSyndiasmos($ekloges, $region, $slug) {
        $qb = $this->createQueryBuilder('s')
                ->select()
                ->where('s.election=:eid and s.slug=:slug and s.region in (:idp)')
                ->setParameters(array(
                    'eid'=>$ekloges->getId(),
                    'slug'=>$slug,
                    'idp' => explode(',', $region->getIdPath())
                    ));
        try {
            $res = $qb->getQuery()->getSingleResult();
        }
        catch(NoResultException $ex ) {
            $res = null;
        }
        return $res;
    }
    
    public function getSyndYpopsForStauroiEntry($election) {
        $qb = $this->createQueryBuilder('s')
                ->select()
                ->leftJoin('s.ypopsifioi', 'y')
                ->addSelect('y')
                ->where('s.election=:ekl')
                ->setParameter('ekl', $election);
        return $qb->getQuery()->getResult();
    }
    
}
