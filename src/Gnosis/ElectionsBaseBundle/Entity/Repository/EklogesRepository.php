<?php

namespace Gnosis\ElectionsBaseBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\Criteria;
/**
 * Description of EklogesRepository
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 29 Απρ 2014
 */
class EklogesRepository extends EntityRepository
{
   /**
    * Returns published ekloges filtered by etype or not.
    * 
    * @param string $etype
    * @return mixed
    */
    public function getPublishedEkloges($etype=null) {
        $qb = $this->createQueryBuilder('e')
            ->where("e.published=1");
        if( $etype ) {
            $qb->andWhere("e.etype=:et")
                ->setParameter('et', $etype);
        }
        return $qb->orderBy('e.etype, e.seira')
            ->getQuery()
            ->execute();
    }

    /**
     * a mashup for the home page
     */
    public function getActiveEklogesMashup() {
        $out = null;
        $ekloges = $this->createQueryBuilder('e')
            ->leftJoin('e.regions', 'r')
            ->where('e.active=1 and e.published=1')
            ->orderBy('e.seira', 'desc')
            ->getQuery()
            ->execute();
        if( $ekloges ) {
            foreach($ekloges as $e) {
                $criteria = Criteria::create()
                    ->where(Criteria::expr()->eq("level", 2))
                    ->orderBy(array("seira" => Criteria::ASC))
                    ->setFirstResult(0)
                    ->setMaxResults(20)
                ;
                $regCollection = $e->getRegions();
                $eRegions = $regCollection->matching($criteria);
                $out[] = array('e'=>$e, 'r'=>$eRegions);
            }
        }
        return $out;
    }
    
    public function getNewestEkloges() {
        return $this->createQueryBuilder('e')->orderBy('e.id')->setMaxResults(1)->getQuery()->getSingleResult();
    }
}
