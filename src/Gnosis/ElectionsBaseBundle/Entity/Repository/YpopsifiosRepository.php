<?php
namespace Gnosis\ElectionsBaseBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of SyndiasmosRepository
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 23 Απρ 2014
 */
class YpopsifiosRepository extends EntityRepository
{
    public function getRegionYpopsifioi($regionId) {
        $res = $this->createQueryBuilder('y')
            ->select('y.id, y.slug, rg.id rid, sy.id as kid, sy.slug kslug')
            ->leftJoin('y.region', 'rg')
            ->leftJoin('y.komma', 'sy')
            ->where('y.region=:r')
            ->setParameter('r', $regionId)
            ->getQuery()
            ->getArrayResult();
        return $res;
    }
    
    /**
     * Returns ypopsifoi organized in regions
     * First level is the region and each region has the ypopsifoi for it
     * 
     */
    public function getSyndiasmosYpopsifioiPerRegion($election, $syndiasmos) {
        $qb = $this->createQueryBuilder('y')
                ->select()
                ->leftJoin('y.komma', 's')
                ->leftJoin('y.region', 'r')
                ->orderBy('r.level')
                ->where('s.election=:ekl')
                ->andWhere('s.id=:sid')
                ->setParameter('ekl', $election)
                ->setParameter('sid', $syndiasmos->getId());
        $results = $qb->getQuery()->getResult();
        $out = array();
        if($results) {
            foreach($results as $result) {
                $rid = $result->getRegion()->getId();
                if( !isset($out[$rid])) {
                    $out[$rid] = array('region'=>$result->getRegion(), 'ypopsifioi'=>[]);
                }
                $out[$rid]['ypopsifioi'][$result->getId()] = $result;
            }
        }
        return $out;
    }

}
