<?php
namespace Gnosis\ElectionsBaseBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Gnosis\ElectionsBaseBundle\Util\EklogesConsts;
/**
 * Description of RegionPsifoiRepository
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 23 Απρ 2014
 */
class RegionPsifoiRepository extends EntityRepository
{

    public function hasSyndiasmosRegionRows($syndId, $regionId) {
        $out = false;
        $res = $this->createQueryBuilder('rp')
            ->select('count(rp.id) as cnt')
            ->where('rp.region=:r and rp.syndiasmos=:s')
            ->setParameter('r', $regionId)
            ->setParameter('s', $syndId)
            ->getQuery()
            ->getSingleScalarResult();
        if( $res ) {
            $out = true;
        }
        return $out;
    }
    
    public function getRegionPsifoi($region) {
        $ekloges = $region->getElection();
        
        $qb = $this->createQueryBuilder('p')
            ->select('s.id, s.title, s.slug, s.logo, s.color, p.amount, r.egkyra egkyra')
            ->leftJoin('p.region', 'r')
            ->leftJoin('p.syndiasmos', 's')
            ->where('p.region=:rg');
        if( $ekloges->getEtype()==EklogesConsts::ELECTION_TYPE_DIMO) {
            $qb->andWhere('s.region in ('.$region->getIdPath().')');
        }
        $qb ->orderBy('p.amount', 'desc')->addOrderBy('s.id')
            ->setParameter('rg', $region->getId());
        $res = $qb->getQuery()
            ->execute();
        return $res;
    }

    public function getRegionPsifoiAndPrev($region) {
        $ekloges = $region->getElection();
        
        $qb = $this->createQueryBuilder('rp')
            ->select('s.id, s.title, s.slug, s.logo, s.color, rp.amount, r.egkyra, rp2.amount pamount, r2.egkyra pegkyra')
            ->leftJoin('rp.region', 'r')
            ->leftJoin('rp.syndiasmos', 's')
            ->leftJoin('GnosisElectionsBaseBundle:Syndiasmos', 's2', \Doctrine\ORM\Query\Expr\Join::WITH, 's.prev_id=s2.id')
            ->leftJoin('GnosisElectionsBaseBundle:RegionPsifoi', 'rp2', \Doctrine\ORM\Query\Expr\Join::WITH, 'rp2.syndiasmos=s2.id and rp2.region=r.prev_id')
            ->leftJoin('GnosisElectionsBaseBundle:Region', 'r2', \Doctrine\ORM\Query\Expr\Join::WITH, 'rp2.region=r2.id')
            ->where('rp.region=:rg');
        if( $ekloges->getEtype()==EklogesConsts::ELECTION_TYPE_DIMO) {
            $qb->andWhere('s.region in ('.$region->getIdPath().')');
        }        
        $qb ->orderBy('rp.amount', 'desc')->addOrderBy('s.id')
            ->setParameter('rg', $region->getId());
        $res = $qb ->getQuery()
            ->execute();
        return $res;
    }
   
    /**
     * επιστρέφει τα slug των δήμων kai to slug του συνδυασμού που είναι πρώτος σε καθέναν από αυτούς
     * 
     * @param integer $eklogesId
     * @return array
     */
    public function getWhosAheadPerDimoSlugs($eklogesId) {
        $sql = "select rpm.slug as rslug, s.slug as sslug, s.color
                from region_psifoi as rp 
                    right join (
                        select r.id, r.slug, max(rp.amount) as lamount
                            from region r 
                                left join region_psifoi rp on rp.region_id=r.id
                            where r.ekloges_id=:eklId and r.region='dimos' and rp.amount!=0
                            group by r.slug
                            order by r.seira
                        ) rpm on rp.region_id = rpm.id and rp.amount = rpm.lamount
                    left join syndiasmos as s on rp.syndiasmos_id=s.id";
        $results = $this->getEntityManager()
            ->getConnection()
            ->executeQuery($sql, array('eklId'=>$eklogesId))
            ->fetchAll(\PDO::FETCH_ASSOC);
        return $results;
    }
    
}
