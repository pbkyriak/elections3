<?php

namespace Gnosis\ElectionsBaseBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;
use Gnosis\ElectionsBaseBundle\Entity\RegionNomos;
use Doctrine\ORM\Query;
/**
 * Description of RegionEklTmimaRepository
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class RegionEklTmimaRepository extends EntityRepository {
    
    public function getEklTmimaByNum(Ekloges $ekloges, RegionNomos $nomos, $etNum) {
        //$slug = sprintf("%so%%", $etNum);
        $slug = $etNum;
        $idp = sprintf("%s,%%",$nomos->getIdPath());
        $qb = $this->getEntityManager()
                ->createQueryBuilder()
                ->select('e')
                ->from('GnosisElectionsBaseBundle:RegionEklTmima', 'e')
                ->where('e.election=:ekl and e.ekltm_code=:sl and e.id_path like :idp')
                ->setParameter('ekl', $ekloges)
                ->setParameter('sl', $slug)
                ->setParameter('idp', $idp);
        $res = $qb->getQuery()
            ->getOneOrNullResult();
        return $res;

    }
    

    public function getEtWithMissingPsifoi($nomos) {
        $qb = $this->createQueryBuilder('et')
                ->select('et.title, ps.updated_at')
                ->leftJoin('GnosisElectionsEntryBaseBundle:PsifoiEntry', 'ps','WITH', 'et.id=ps.region')
                ->where('ps.updated_at is null')
                ->andWhere('et.id_path like :idp')
                ->orderBy('et.id')
                ->setParameter('idp', sprintf("%s,%%", $nomos->getIdPath()))
                ;
        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }
    
    public function getEtWithMissingStauroi($nomos) {
        $qb = $this->createQueryBuilder('et')
                ->select('et.title, ps.updated_at')
                ->leftJoin('GnosisElectionsEntryBaseBundle:StauroiEntry', 'ps','WITH', 'et.id=ps.region')
                ->where('ps.updated_at is null')
                ->andWhere('et.id_path like :idp')
                ->orderBy('et.id')
                ->setParameter('idp', sprintf("%s,%%", $nomos->getIdPath()))
                ;
        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function getEtWithMissingBoth($nomos) {
        $qb = $this->createQueryBuilder('et')
                ->select('et.title')
                ->leftJoin('GnosisElectionsEntryBaseBundle:PsifoiEntry', 'ps','WITH', 'et.id=ps.region')
                ->leftJoin('GnosisElectionsEntryBaseBundle:StauroiEntry', 'st','WITH', 'et.id=st.region')
                ->where('ps.updated_at is null and st.updated_at is null')
                ->andWhere('et.id_path like :idp')
                ->orderBy('et.id')
                ->setParameter('idp', sprintf("%s,%%", $nomos->getIdPath()))
                ;
        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function getEtWithMissingAny($nomos) {
        $qb = $this->createQueryBuilder('et')
                ->select('et.title, ps.updated_at pstime, st.updated_at sttime')
                ->leftJoin('GnosisElectionsEntryBaseBundle:PsifoiEntry', 'ps','WITH', 'et.id=ps.region')
                ->leftJoin('GnosisElectionsEntryBaseBundle:StauroiEntry', 'st','WITH', 'et.id=st.region')
                ->where('ps.updated_at is null or st.updated_at is null')
                ->andWhere('et.id_path like :idp')
                ->orderBy('et.id')
                ->setParameter('idp', sprintf("%s,%%", $nomos->getIdPath()))
                ;
        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function getEtEntryTimes($nomos) {
        $qb = $this->createQueryBuilder('et')
                ->select('et.title, ps.updated_at pstime, st.updated_at sttime, ppe.updated_at ppetime, pse.updated_at psetime, pre.updated_at pretime')
                ->leftJoin('GnosisElectionsEntryBaseBundle:PsifoiEntry', 'ps','WITH', 'et.id=ps.region')
                ->leftJoin('GnosisElectionsEntryBaseBundle:StauroiEntry', 'st','WITH', 'et.id=st.region')
                ->leftJoin('GnosisProtokoloBundle:ProtokoloEntry', 'ppe','WITH', 'et.id=ppe.region and ppe.entry_type=:ppetype1')
                ->leftJoin('GnosisProtokoloBundle:ProtokoloEntry', 'pse','WITH', 'et.id=pse.region and pse.entry_type=:ppetype2')
                ->leftJoin('GnosisPraktikaBundle:PraktikoEntry', 'pre','WITH', 'et.id=pre.region')
                ->where('et.id_path like :idp')
                ->orderBy('et.id')
                ->setParameter('idp', sprintf("%s,%%", $nomos->getIdPath()))
                ->setParameter('ppetype1', 'psifoi')
                ->setParameter('ppetype2', 'stauroi')
                ;
        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }
   
}
