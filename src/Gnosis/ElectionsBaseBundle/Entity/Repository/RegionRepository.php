<?php
namespace Gnosis\ElectionsBaseBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of RegionRepository
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 24 Απρ 2014
 */
class RegionRepository extends EntityRepository
{

    public function getSubRegion($ekloges, $slug, $parentPath=null) {
        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('s')
            ->from('GnosisElectionsBaseBundle:Region', 's')
            ->where('s.election=:ekl and s.slug=:slug')
            ->setParameter('ekl', $ekloges)
            ->setParameter('slug', $slug)
            ;
        if( $parentPath ) {
            $qb->andWhere('s.id_path like :idp')
                ->setParameter('idp', $parentPath.'%')
                ;
        }
        $res = $qb->getQuery()
            ->getOneOrNullResult();
        return $res;
    }
    
    public function getSubRegions($ekloges, $parentPath=null) {
        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('s')
            ->from('GnosisElectionsBaseBundle:Region', 's')
            ->where('s.election=:ekl and s.level=:lv')
            ->setParameter('ekl', $ekloges)
            ->setParameter('lv', substr_count($parentPath, ',')+2)
            ;
        if( $parentPath ) {
            $qb->andWhere('s.id_path like :idp')
                ->setParameter('idp', $parentPath.',%')
                ;
        }
        $res = $qb->getQuery()
            ->execute();
        return $res;
    }
    
    public function getRegionsFromPath($path) {
        $qb = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('s')
            ->from('GnosisElectionsBaseBundle:Region', 's')
            ->where('s.id in (:idp)')
            ->orderBy('s.level')
            ->setParameter('idp', explode(',',$path))
            ;
        $res = $qb->getQuery()
            ->execute();
        return $res;        
    }
    
    public function fixIdPaths($oldIdPath) {
        $em = $this->getEntityManager();
        $qb = $em
            ->createQueryBuilder()
            ->select('r')
            ->from('GnosisElectionsBaseBundle:Region', 'r')
            ->where('r.id_path like :idp')
            ->orderBy('r.level')
            ->setParameter('idp', sprintf("%s,%%",$oldIdPath));
        $res = $qb->getQuery()
            ->execute();
        if($res) {
            foreach($res as $re) {
                $re->updateMyIdPath();
                $em->persist($re);
                $em->flush();
            }
        }
    }
}
