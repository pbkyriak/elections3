<?php

namespace Gnosis\ElectionsBaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of RegionNomos
 * @ORM\Entity @ORM\HasLifecycleCallbacks 
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class RegionNomos extends Region
{
 
    /**
     * @var string
     */
    private $download_url;

    /**
     * @var string
     */
    private $import_file;

    private $capital_name;
    
    private $has_protokolo = false;
    
    private $ypes_ota_id;
    
    /**
     * Set download_url
     *
     * @param string $downloadUrl
     * @return RegionNomos
     */
    public function setDownloadUrl($downloadUrl)
    {
        $this->download_url = $downloadUrl;

        return $this;
    }

    /**
     * Get download_url
     *
     * @return string 
     */
    public function getDownloadUrl()
    {
        return $this->download_url;
    }

    /**
     * Set import_file
     *
     * @param string $importFile
     * @return RegionNomos
     */
    public function setImportFile($importFile)
    {
        $this->import_file = $importFile;

        return $this;
    }

    /**
     * Get import_file
     *
     * @return string 
     */
    public function getImportFile()
    {
        return $this->import_file;
    }
    
    public function getUploadsDir() {
        return sprintf("%s/%s",
            $this->getParent()->getUploadsDir(),
            $this->getSlug()
            );
    }

    /** @ORM\PostPersist */
    public function onPostPersist()
    {
        $udir = $this->getUploadsDir();
        if( !file_exists($udir) ) {
            @mkdir($udir, 0777);
        }
    }

    /**
     * @var integer
     */
    private $stauroi_per_ballot=0;


    /**
     * Set stauroiPerBallot
     *
     * @param integer $stauroiPerBallot
     *
     * @return RegionNomos
     */
    public function setStauroiPerBallot($stauroiPerBallot)
    {
        $this->stauroi_per_ballot = $stauroiPerBallot;

        return $this;
    }

    /**
     * Get stauroiPerBallot
     *
     * @return integer
     */
    public function getStauroiPerBallot()
    {
        return $this->stauroi_per_ballot;
    }
    
    public function getCapitalName() {
        return $this->capital_name;
    }
    public function setCapitalName($v) {
        $this->capital_name = $v;
        return $this;
    }
    
    public function getHasProtokolo() {
        return $this->has_protokolo;
    }
    
    public function setHasProtokolo($v) {
        $this->has_protokolo = $v;
        return $this;
    }
    
    public function getYpesOtaId() {
        return $this->ypes_ota_id;
    }
    
    public function setYpesOtaId($v) {
        $this->ypes_ota_id = $v;
        return $this;
    }

}
