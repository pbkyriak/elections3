<?php

namespace Gnosis\ElectionsBaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Slx\MetronicBundle\Lib\GreekText;

/**
 * Ekloges is the ekloges definition. 
 * @ORM\Entity @ORM\HasLifecycleCallbacks 
 */
class Ekloges
{
    
    use \Gnosis\ElectionsEntryBaseBundle\Entity\LogUserActionsTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var boolean
     */
    private $active;

    /**
     *
     * @var boolean
     */
    private $published;
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $regions;

    /**
     *
     * @var string
     */
    private $etype;
    
    private $prev_id=0;
    
    private $hasStaurous=true;
    
    private $seira=0;
    
    private $long_title;
    
    private $ypes_type;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->regions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Ekloges
     */
    public function setTitle($title)
    {
        $this->title = $title;
        $this->setSlug($this->slugify($title));
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Ekloges
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add regions
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Region $regions
     * @return Ekloges
     */
    public function addRegion(\Gnosis\ElectionsBaseBundle\Entity\Region $regions)
    {
        $this->regions[] = $regions;

        return $this;
    }

    /**
     * Remove regions
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Region $regions
     */
    public function removeRegion(\Gnosis\ElectionsBaseBundle\Entity\Region $regions)
    {
        $this->regions->removeElement($regions);
    }

    /**
     * Get regions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRegions()
    {
        return $this->regions;
    }
    
    /**
     * Set slug
     *
     * @param string $slug
     * @return Region
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    public function slugify() {
        $slug = trim($this->title);
        $slug = GreekText::slugify($slug);
        return $slug;
    }

    /**
     * Set etype
     *
     * @param string $v
     * @return Region
     */
    public function setEtype($v)
    {
        $this->etype = $v;

        return $this;
    }

    /**
     * Get etype
     *
     * @return string 
     */
    public function getEtype()
    {
        return $this->etype;
    }

    public function setPrevId($v) {
        $this->prev_id=$v;
        return $this;
    }
    
    public function getPrevId() {
        return $this->prev_id;
    }

    public function setHasStaurous($v) {
        $this->hasStaurous=$v;
        return $this;
    }
    
    public function getHasStaurous() {
        return $this->hasStaurous;
    }

    /**
     * Set published
     *
     * @param boolean $v
     * @return Ekloges
     */
    public function setPublished($v)
    {
        $this->published = $v;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean 
     */
    public function getPublished()
    {
        return $this->published;
    }

    public function setSeira($v) {
        $this->seira = $v;
        return $this;
    }
    
    public function getSeira() {
        return $this->seira;
    }

    public function getUploadsDir($relative = false) {
        $root = "/var/www/pste-site-elections-2014/web/uploads/elections";
        if( $relative ) {
            $root = 'uploads/elections';
        }
        return sprintf("%s/%s", $root, $this->getSlug());
    }
    
    /** @ORM\PostPersist */
    public function onPostPersist()
    {
        $udir = $this->getUploadsDir();
        if( !file_exists($udir) ) {
            mkdir($udir, 0777);
        }
    }

    public function getLongTitle() {
        return $this->long_title;
    }
    
    public function setLongTitle($v) {
        $this->long_title=$v;
        return $this;
    }
    
    public function getYpesType() {
        return $this->ypes_type;
    }
    
    public function setYpesType($v) {
        $this->ypes_type = $v;
        return $this;
    }
    
    public function __toString() {
        return sprintf("Εκλογές: %s", $this->getTitle());
    }
}
