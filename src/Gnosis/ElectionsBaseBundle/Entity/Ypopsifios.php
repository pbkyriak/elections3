<?php

namespace Gnosis\ElectionsBaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ypopsifios
 */
class Ypopsifios
{
    use \Gnosis\ElectionsEntryBaseBundle\Entity\LogUserActionsTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\Ekloges
     */
    private $election;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\Region
     */
    private $region;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\Syndiasmos
     */
    private $komma;

    private $slug;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Ypopsifios
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set election
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Ekloges $election
     * @return Ypopsifios
     */
    public function setElection(\Gnosis\ElectionsBaseBundle\Entity\Ekloges $election = null)
    {
        $this->election = $election;

        return $this;
    }

    /**
     * Get election
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Ekloges 
     */
    public function getElection()
    {
        return $this->election;
    }

    /**
     * Set dimos
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Region $region
     * @return Ypopsifios
     */
    public function setLocality(\Gnosis\ElectionsBaseBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get dimos
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Region
     */
    public function getLocality()
    {
        return $this->region;
    }

    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set komma
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Syndiasmos $komma
     * @return Ypopsifios
     */
    public function setKomma(\Gnosis\ElectionsBaseBundle\Entity\Syndiasmos $komma = null)
    {
        $this->komma = $komma;

        return $this;
    }

    /**
     * Get komma
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Syndiasmos 
     */
    public function getKomma()
    {
        return $this->komma;
    }
    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\RegionStauroi
     */
    private $stauroi;


    /**
     * Set stauroi
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\RegionStauroi $stauroi
     * @return Ypopsifios
     */
    public function setStauroi(\Gnosis\ElectionsBaseBundle\Entity\RegionStauroi $stauroi = null)
    {
        $this->stauroi = $stauroi;

        return $this;
    }

    /**
     * Get stauroi
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\RegionStauroi 
     */
    public function getStauroi()
    {
        return $this->stauroi;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->stauroi = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add stauroi
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\RegionStauroi $stauroi
     * @return Ypopsifios
     */
    public function addStauroi(\Gnosis\ElectionsBaseBundle\Entity\RegionStauroi $stauroi)
    {
        $this->stauroi[] = $stauroi;

        return $this;
    }

    /**
     * Remove stauroi
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\RegionStauroi $stauroi
     */
    public function removeStauroi(\Gnosis\ElectionsBaseBundle\Entity\RegionStauroi $stauroi)
    {
        $this->stauroi->removeElement($stauroi);
    }
    
    /**
     * Set slug
     *
     * @param string $slug
     * @return Region
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    public function __toString() {
        return sprintf("Υποψήφιος: %s", $this->getTitle());
    }

}
