<?php

namespace Gnosis\ElectionsBaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RegionStauroi holds staurous for a given ypopsifios in a region
 * 
 */
class RegionStauroi
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $psifoi;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\Region
     */
    private $region;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\Ypopsifios
     */
    private $syndiasmos;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set psifoi
     *
     * @param integer $psifoi
     * @return RegionStauroi
     */
    public function setPsifoi($psifoi)
    {
        $this->psifoi = $psifoi;

        return $this;
    }

    /**
     * Get psifoi
     *
     * @return integer 
     */
    public function getPsifoi()
    {
        return $this->psifoi;
    }

    /**
     * Set region
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Region $region
     * @return RegionStauroi
     */
    public function setRegion(\Gnosis\ElectionsBaseBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Region 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set syndiasmos
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Ypopsifios $syndiasmos
     * @return RegionStauroi
     */
    public function setSyndiasmos(\Gnosis\ElectionsBaseBundle\Entity\Ypopsifios $syndiasmos = null)
    {
        $this->syndiasmos = $syndiasmos;

        return $this;
    }

    /**
     * Get syndiasmos
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Ypopsifios 
     */
    public function getSyndiasmos()
    {
        return $this->syndiasmos;
    }
    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\Ypopsifios
     */
    private $ypopsifios;


    /**
     * Set ypopsifios
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Ypopsifios $ypopsifios
     * @return RegionStauroi
     */
    public function setYpopsifios(\Gnosis\ElectionsBaseBundle\Entity\Ypopsifios $ypopsifios = null)
    {
        $this->ypopsifios = $ypopsifios;

        return $this;
    }

    /**
     * Get ypopsifios
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Ypopsifios 
     */
    public function getYpopsifios()
    {
        return $this->ypopsifios;
    }
    /**
     * @var integer
     */
    private $amount;


    /**
     * Set amount
     *
     * @param integer $amount
     * @return RegionStauroi
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }
}
