<?php

namespace Gnosis\ElectionsBaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Slx\MetronicBundle\Lib\GreekText;
/**
 * Description of RegionMapPoint
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 3 Μαϊ 2014
 */
class RegionMapPoint
{
    private $id;
    private $mapfile;
    private $title;
    private $slug;
    private $fill_x;
    private $fill_y;
    
    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getFillX()
    {
        return $this->fill_x;
    }

    public function getFillY()
    {
        return $this->fill_y;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        $this->setSlug(GreekText::slugify(trim($title)));
        return $this;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    public function setFillX($fill_x)
    {
        $this->fill_x = $fill_x;
        return $this;
    }

    public function setFillY($fill_y)
    {
        $this->fill_y = $fill_y;
        return $this;
    }

    public function getMapFile() {
        return $this->mapfile;
    }
 
    public function setMapFile($v) {
        $this->mapfile = $v;
        return $this;
    }
}
