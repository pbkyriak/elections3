<?php

namespace Gnosis\ElectionsBaseBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;
use Gnosis\ElectionsBaseBundle\Entity\RegionPeriferia;
use Gnosis\ElectionsBaseBundle\Entity\RegionNomos;
use Slx\UserBundle\Entity\User;
use Slx\UserBundle\Security\User\SlxUser;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadEklogesData implements FixtureInterface, ContainerAwareInterface
{
    
    private $manager;
    
    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $this->add2015sep();
        
    }

    private function add2015sep() {
        $this->fixRoles2015sep();
        $ekloges = new Ekloges();
        $ekloges->setTitle('Εθνικές 2015 Σεπ');
        $ekloges->setActive(1);
        $ekloges->setPublished(1);
        $ekloges->setEtype('bouleytikes');
        $ekloges->setHasStaurous(false);
        $ekloges->setSeira(80);
        $ekloges->setLongTitle('ΕΘΝΙΚΕΣ ΕΚΛΟΓΕΣ ΣΕΠΤΕΜΒΡΙΟΥ 2015');
        $this->manager->persist($ekloges);
        $this->manager->flush();
        $this->addEthnikes2015sepData($ekloges, 'Στερεάς Ελλάδας'); 
        $this->addUsers2015sep($ekloges);
    }
    
    private function fixRoles2015sep() {
        $this->manager->getConnection()->update('slx_role', array('name'=>'Καταχωρητής Νομού'),array('id'=>2));
        $this->manager->getConnection()->update('slx_role', array('name'=>'Διαχειριστής'),array('id'=>3));
        $this->manager->getConnection()->insert('slx_role', array('name'=>'Διαχειριστής Νομού', 'role'=>'ROLE_RADMIN'));
        $this->manager->getConnection()->insert('slx_role', array('name'=>'Πρακτικά', 'role'=>'ROLE_PRAKT'));
        $this->manager->getConnection()->insert('slx_role', array('name'=>'Πρωτόκολλο', 'role'=>'ROLE_PROTOK'));
    }
    
    private function addEthnikes2015sepData($ekloges, $title) {
        $perif = new RegionPeriferia();
        $perif->setTitle($title)->setSlug($perif->slugify(''))->setEggegramenoi(0);
        $perif->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0)->setElection($ekloges);
        $perif->setPrevId(0);
        $this->manager->persist($perif);
        $this->manager->flush();
        $perif->updateMyIdPath();
        $this->manager->persist($perif);
        
        $nomos1 = new RegionNomos();
        $nomos1->setTitle('Φθιώτιδα')->setParent($perif)->setElection($ekloges);
        $nomos1->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0);
        $nomos1->setImportFile('none')->setDownloadUrl('none')->setPrevId(0)->setCapitalName('Λαμία')->setYpesOtaId(51);
        $this->manager->persist($nomos1);
        $this->manager->flush();
        $nomos1->updateMyIdPath();
        $this->manager->persist($nomos1);
        
        $nomos2 = new RegionNomos();
        $nomos2->setTitle('Ευβοια')->setParent($perif)->setElection($ekloges);
        $nomos2->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0);
        $nomos2->setImportFile('none')->setDownloadUrl('none')->setCapitalName('Χαλκίδα')->setYpesOtaId(16);
        $nomos2->setPrevId(0);
        $this->manager->persist($nomos2);
        $this->manager->flush();
        $nomos2->updateMyIdPath();
        $this->manager->persist($nomos2);

        $nomos3 = new RegionNomos();
        $nomos3->setTitle('Φωκίδα')->setParent($perif)->setElection($ekloges);
        $nomos3->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0);
        $nomos3->setImportFile('none')->setDownloadUrl('none')->setCapitalName('Αμφισσα')->setYpesOtaId(53);
        $nomos3->setPrevId(0);
        $this->manager->persist($nomos3);
        $this->manager->flush();
        $nomos3->updateMyIdPath();
        $this->manager->persist($nomos3);

        $nomos4 = new RegionNomos();
        $nomos4->setTitle('Ευρυτανία')->setParent($perif)->setElection($ekloges);
        $nomos4->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0);
        $nomos4->setImportFile('none')->setDownloadUrl('none')->setCapitalName('Καρπενήσι')->setYpesOtaId(17);
        $nomos4->setPrevId(0);
        $this->manager->persist($nomos4);
        $this->manager->flush();
        $nomos4->updateMyIdPath();
        $this->manager->persist($nomos4);
        
        $nomos5 = new RegionNomos();
        $nomos5->setTitle('Βοιωτία')->setParent($perif)->setElection($ekloges);
        $nomos5->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0);
        $nomos5->setImportFile('none')->setDownloadUrl('none')->setCapitalName('Θήβα')->setYpesOtaId(11);
        $nomos5->setPrevId(0);
        $this->manager->persist($nomos5);
        $this->manager->flush();
        $nomos5->updateMyIdPath();
        $this->manager->persist($nomos5);

        $this->manager->flush();
        
    }
    
    private function addUsers2015sep($ekloges) {
        $nomoi = $this->manager->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findBy(array('election'=>$ekloges));
        $roleIns = $this->manager->getRepository("SlxUserBundle:Role")->findOneBy(array('role'=>'ROLE_USER'));
        $roleAdm = $this->manager->getRepository("SlxUserBundle:Role")->findOneBy(array('role'=>'ROLE_RADMIN'));
        $rolePro = $this->manager->getRepository("SlxUserBundle:Role")->findOneBy(array('role'=>'ROLE_PROTOK'));
        $rolePra = $this->manager->getRepository("SlxUserBundle:Role")->findOneBy(array('role'=>'ROLE_PRAKT'));
        $i=1;
        foreach($nomoi as $nomos) {
            $this->addUser(sprintf('entry%s', $i), $roleIns, $ekloges, $nomos,0);
            $this->addUser(sprintf('protok%s', $i), $rolePro, $ekloges, $nomos,0);
            $this->addUser(sprintf('prakt%s', $i), $rolePra, $ekloges, $nomos,0);
            $this->addUser(sprintf('radmin%s', $i), $roleAdm, $ekloges, $nomos,1);
            $i++;
        }
        $this->manager->flush();
    }
    
    private function addUser($username, $role, $ekloges, $nomos, $utype) {

        $userIns1 = new User();
        $userIns1->setUsername($username)->setIsActive(true)->setBathmos('--')->setEmail('info@sterea-ekloges.gr')->setFathername('--')
                ->setFullname($username)->setGender('male')->setKlados('--')->setUserType($utype)->setWelection($ekloges)->setWregion($nomos);
        $userIns1->addRole($role);
        $factory = $this->container->get('security.encoder_factory');
        $encoder = $factory->getEncoder(new SlxUser('', '', '', array(),0,0));
        $encodedPassword = $encoder->encodePassword($username, $userIns1->getSalt());
        $userIns1->setPassword($encodedPassword);
        $this->manager->persist($userIns1);        
    }
    
    private function add2014() {
        $ekloges = new Ekloges();
        $ekloges->setTitle('Περιφερειακές 2014 Β');
        $ekloges->setActive(1);
        $ekloges->setPublished(1);
        $ekloges->setEtype('periferiakes');
        $ekloges->setHasStaurous(false);
        $ekloges->setSeira(30);
        $manager->persist($ekloges);
        $this->manager->flush();
        $this->addPerifereiakesData($ekloges, 'Στερεάς Ελλάδας');

        $ekloges = new Ekloges();
        $ekloges->setTitle('Δημοτικές 2014 Β');
        $ekloges->setActive(1);
        $ekloges->setPublished(1);
        $ekloges->setEtype('dimotikes');
        $ekloges->setHasStaurous(false);
        $ekloges->setSeira(40);
        $manager->persist($ekloges);
        $this->manager->flush();
        $this->addDimotikesData($ekloges, 'Στερεάς Ελλάδας');

        $ekloges = new Ekloges();
        $ekloges->setTitle('Ευρωεκλογές 2014');
        $ekloges->setActive(1);
        $ekloges->setPublished(1);
        $ekloges->setSeira(50);
        $ekloges->setEtype('euro');
        $manager->persist($ekloges);
        $this->manager->flush();
        $this->addEuroData($ekloges, 'Στερεάς Ελλάδας');        
    }
    
    private function addDimotikesData($ekloges, $title) {
        $perif = new RegionPeriferia();
        $perif->setTitle($title)
            ->setSlug($perif->slugify(''))
            ->setEggegramenoi(0);
        $perif->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0)->setElection($ekloges);
        $perif->setPrevId(0);
        $this->manager->persist($perif);
        $this->manager->flush();
        $perif->updateMyIdPath();
        $this->manager->persist($perif);
        
        $nomos1 = new RegionNomos();
        $nomos1->setTitle('Φθιώτιδα')->setParent($perif)->setElection($ekloges);
        $nomos1->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0);
        $nomos1->setImportFile('dimot_2014_pste_export_BK.txt')
            ->setDownloadUrl('/var/www/fthiotida/downloads/pste/dimot_2014_pste_export_BK.txt.zip');
        $nomos1->setPrevId(0);
        $this->manager->persist($nomos1);
        $this->manager->flush();
        $nomos1->updateMyIdPath();
        $this->manager->persist($nomos1);
        
        $nomos2 = new RegionNomos();
        $nomos2->setTitle('Ευβοια')->setParent($perif)->setElection($ekloges);
        $nomos2->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0);
        $nomos2->setImportFile('pste_dimot_evboia_2014_BK.txt')
            ->setDownloadUrl('http://www.naevias.gr/ekloges2014/dhmo_2014b/pste_dimot_evboia_2014_BK.zip');
        $nomos2->setPrevId(0);
        $this->manager->persist($nomos2);
        $this->manager->flush();
        $nomos2->updateMyIdPath();
        $this->manager->persist($nomos2);
        
        $nomos3 = new RegionNomos();
        $nomos3->setTitle('Φωκίδα')->setParent($perif)->setElection($ekloges);
        $nomos3->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0);
        $nomos3->setImportFile('pste_dimot_foki_2014_BK.txt')
            ->setDownloadUrl('http://www.fokida.gr/site/system/icons/Arxeia/CSV+komma+dimoiB.csp?viewMode=normal');
        $nomos3->setPrevId(0);
        $this->manager->persist($nomos3);
        $this->manager->flush();
        $nomos3->updateMyIdPath();
        $this->manager->persist($nomos3);
        
        $nomos5 = new RegionNomos();
        $nomos5->setTitle('Βοιωτία')->setParent($perif)->setElection($ekloges);
        $nomos5->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0);
        $nomos5->setImportFile('pste_dimot_viotia_2014_BK.txt')
            ->setDownloadUrl('http://www.stereahellas.gr/ekloges/evrytania/ekloges2014d/na/eyrit/Round1/pste_dimot.zip');
        $nomos5->setPrevId(0);
        $this->manager->persist($nomos5);
        $this->manager->flush();
        $nomos5->updateMyIdPath();
        $this->manager->persist($nomos5);

        $this->manager->flush();
    }
    
    private function addPerifereiakesData($ekloges, $title) {
        $perif = new RegionPeriferia();
        $perif->setTitle($title)
            ->setSlug($perif->slugify(''))
            ->setEggegramenoi(0);
        $perif->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0)->setElection($ekloges);
        $perif->setPrevId(0);
        $this->manager->persist($perif);
        $this->manager->flush();
        $perif->updateMyIdPath();
        $this->manager->persist($perif);
        
        $nomos1 = new RegionNomos();
        $nomos1->setTitle('Φθιώτιδα')->setParent($perif)->setElection($ekloges);
        $nomos1->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0);
        $nomos1->setImportFile('perif_2014_pste_export_BK.txt')
            ->setDownloadUrl('/var/www/fthiotida/downloads/pste/perif_2014_pste_export_BK.txt.zip');
        $nomos1->setPrevId(0);
        $this->manager->persist($nomos1);
        $this->manager->flush();
        $nomos1->updateMyIdPath();
        $this->manager->persist($nomos1);
        
        $nomos2 = new RegionNomos();
        $nomos2->setTitle('Ευβοια')->setParent($perif)->setElection($ekloges);
        $nomos2->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0);
        $nomos2->setImportFile('pste_perif_evboia_2014_BK.txt')
            ->setDownloadUrl('http://www.naevias.gr/ekloges2014/peri_2014b/pste_perif_evboia_2014_BK.zip');
        $nomos2->setPrevId(0);
        $this->manager->persist($nomos2);
        $this->manager->flush();
        $nomos2->updateMyIdPath();
        $this->manager->persist($nomos2);
        
        $nomos3 = new RegionNomos();
        $nomos3->setTitle('Φωκίδα')->setParent($perif)->setElection($ekloges);
        $nomos3->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0);
        $nomos3->setImportFile('pste_perif_foki_2014_BK.txt')
            ->setDownloadUrl('http://www.fokida.gr/site/system/icons/Arxeia/CSV+komma+perifB.csp?viewMode=normal');
        $nomos3->setPrevId(0);
        $this->manager->persist($nomos3);
        $this->manager->flush();
        $nomos3->updateMyIdPath();
        $this->manager->persist($nomos3);
        
        $nomos4 = new RegionNomos();
        $nomos4->setTitle('Ευρυτανία')->setParent($perif)->setElection($ekloges);
        $nomos4->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0);
        $nomos4->setImportFile('pste_perif_evrytania_2014_BK.txt')
            ->setDownloadUrl('http://www.stereahellas.gr/ekloges/viotia/ekloges2014d/na/viot/Round2/pste_perif.zip');
        $nomos4->setPrevId(0);
        $this->manager->persist($nomos4);
        $this->manager->flush();
        $nomos4->updateMyIdPath();
        $this->manager->persist($nomos4);

        $nomos5 = new RegionNomos();
        $nomos5->setTitle('Βοιωτία')->setParent($perif)->setElection($ekloges);
        $nomos5->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0);
        $nomos5->setImportFile('pste_perif_viotia_2014_BK.txt')
            ->setDownloadUrl('http://www.stereahellas.gr/ekloges/evrytania/ekloges2014d/na/eyrit/Round2/pste_perif.zip');
        $nomos5->setPrevId(0);
        $this->manager->persist($nomos5);
        $this->manager->flush();
        $nomos5->updateMyIdPath();
        $this->manager->persist($nomos5);

        $this->manager->flush();
    }
    
    private function addEuroData($ekloges, $title) {
        $perif = new RegionPeriferia();
        $perif->setTitle($title)
            ->setSlug($perif->slugify(''))
            ->setEggegramenoi(0);
        $perif->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0)->setElection($ekloges);
        $perif->setPrevId(0);
        $this->manager->persist($perif);
        $this->manager->flush();
        $perif->updateMyIdPath();
        $this->manager->persist($perif);
        
        $nomos1 = new RegionNomos();
        $nomos1->setTitle('Φθιώτιδα')->setParent($perif)->setElection($ekloges);
        $nomos1->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0);
        $nomos1->setImportFile('euro_2014_pste_export.txt')
            ->setDownloadUrl('/var/www/fthiotida/downloads/pste/euro_2014_pste_export.txt.zip');
        $nomos1->setPrevId(0);
        $this->manager->persist($nomos1);
        $this->manager->flush();
        $nomos1->updateMyIdPath();
        $this->manager->persist($nomos1);
        
        $nomos2 = new RegionNomos();
        $nomos2->setTitle('Ευβοια')->setParent($perif)->setElection($ekloges);
        $nomos2->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0);
        $nomos2->setImportFile('pste_euro_evboia_2014.txt')
            ->setDownloadUrl('http://www.naevias.gr/ekloges2014/euro_2014/pste_euro_evboia_2014.zip');
        $nomos2->setPrevId(0);
        $this->manager->persist($nomos2);
        $this->manager->flush();
        $nomos2->updateMyIdPath();
        $this->manager->persist($nomos2);
        
        $nomos3 = new RegionNomos();
        $nomos3->setTitle('Φωκίδα')->setParent($perif)->setElection($ekloges);
        $nomos3->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0);
        $nomos3->setImportFile('pste_euro_foki_2014.txt')
            ->setDownloadUrl('http://www.fokida.gr/site/system/icons/Arxeia/CSV+euroekloges.csp?viewMode=normal');
        $nomos3->setPrevId(0);
        $this->manager->persist($nomos3);
        $this->manager->flush();
        $nomos3->updateMyIdPath();
        $this->manager->persist($nomos3);
        
        $nomos4 = new RegionNomos();
        $nomos4->setTitle('Ευρυτανία')->setParent($perif)->setElection($ekloges);
        $nomos4->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0);
        $nomos4->setImportFile('pste_euro_evrytania_2014_BK.txt')
            ->setDownloadUrl('http://www.stereahellas.gr/ekloges/evrytania/ekloges2014d/na/eyrit/Round1/pste_euro.zip');
        $nomos4->setPrevId(0);
        $this->manager->persist($nomos4);
        $this->manager->flush();
        $nomos4->updateMyIdPath();
        $this->manager->persist($nomos4);

        $nomos5 = new RegionNomos();
        $nomos5->setTitle('Βοιωτία')->setParent($perif)->setElection($ekloges);
        $nomos5->setEggegramenoi(0)->setPsifisan(0)->setAkyra(0)->setLeyka(0)->setEgkyra(0);
        $nomos5->setImportFile('pste_euro_viotia_2014_BK.txt')
            ->setDownloadUrl('http://www.stereahellas.gr/ekloges/viotia/ekloges2014d/na/viot/Round1/pste_euro.zip');
        $nomos5->setPrevId(0);
        $this->manager->persist($nomos5);
        $this->manager->flush();
        $nomos5->updateMyIdPath();
        $this->manager->persist($nomos5);

        $this->manager->flush();
    }
}
