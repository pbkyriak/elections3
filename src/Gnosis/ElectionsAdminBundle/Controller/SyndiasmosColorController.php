<?php

namespace Gnosis\ElectionsAdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Gnosis\ElectionsBaseBundle\Entity\SyndiasmosColor;
use Gnosis\ElectionsBaseBundle\Form\SyndiasmosColorType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * SyndiasmosColor controller.
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Οργάνωση")
 * @Breadcrumb("Χρώματα", route="elections_admin_syndiasmoscolor")
 * @Security("has_role('ROLE_ADMIN')")
 * 
 * @CurrentMenuItem("elections_admin_syndiasmoscolor")

 */
class SyndiasmosColorController extends Controller
{

    /**
     * Lists all SyndiasmosColor entities.
     *
     */
    public function indexAction()
    {
        $form = $this->getFilterForm();
        $query = $this->makeListQuery($form);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1) /* page number */,
            $this->get('request')->query->get('limit', 15)
        );

        return $this->render('GnosisElectionsAdminBundle:SyndiasmosColor:index.html.twig',
                array(
                'pagination' => $pagination,
                'filterForm' => $form->createView()
        ));
    }

    /**
     * Create list query
     */
    private function makeListQuery($form)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT a FROM GnosisElectionsBaseBundle:SyndiasmosColor a";
        $form->handleRequest($this->get('request'));
        $filterQryParams = array();
        $where = array();
        $where[] = " a.ekloges_id=:ekl ";
        $filterQryParams['ekl'] = $this->get('session')->get('election_id');
        $this->get('request')->setLocale('el');
        if ($form->isValid()) {
            $filterData = $form->getData();
            if ($filterData['title']) {
                $where[] = " a.title like :atitle ";
                $filterQryParams['atitle'] = str_replace('*', '%',
                    $filterData['title']);
            }
        }
        if ($where) {
            $dql .= ' where ' . implode(' and ', $where);
        }

        $query = $em->createQuery($dql);
        $query->setParameters($filterQryParams);
        return $query;
    }

    /**
     * Creates filter form
     *
     */
    private function getFilterForm()
    {
        $filterData = array();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData,
                array('method' => 'GET', 'csrf_protection' => false))
            ->add(
                'title', 'text',
                array('required' => false, 'label' => 'elections.syndColors.title')
            )
            ->getForm();
        return $form;
    }

    /**
     * Creates a new SyndiasmosColor entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new SyndiasmosColor();
        $form = $this->createForm(new SyndiasmosColorType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $entity->setEklogesId($this->get('session')->get('election_id'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info',
                'gsprod.general.record_saved');
            if ($form->get('saveAndAdd')->isClicked())
                return $this->redirect($this->generateUrl('elections_admin_syndiasmoscolor_new'));
            else
                return $this->redirect($this->generateUrl('elections_admin_syndiasmoscolor_show',
                            array('id' => $entity->getId())));
        }
        else {
            $this->get('session')->getFlashBag()->add('notice',
                'gsprod.general.record_errors');
        }

        return $this->render('GnosisElectionsAdminBundle:SyndiasmosColor:new.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Region entity.
     * @Breadcrumb("Προσθήκη")
     */
    public function newAction()
    {
        $entity = new SyndiasmosColor();
        $form = $this->createForm(new SyndiasmosColorType(), $entity);

        return $this->render('GnosisElectionsAdminBundle:SyndiasmosColor:new.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }

    /**
     * @Breadcrumb("Προβολή")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GnosisElectionsBaseBundle:SyndiasmosColor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SyndiasmosColor entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GnosisElectionsAdminBundle:SyndiasmosColor:show.html.twig',
                array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),));
    }

    /**
     * Displays a form to edit an existing region entity.
     * @Breadcrumb("Μεταβολή")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GnosisElectionsBaseBundle:SyndiasmosColor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SyndiasmosColor entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GnosisElectionsAdminBundle:SyndiasmosColor:edit.html.twig',
                array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a SyndiasmosColor entity.
     *
     * @param SyndiasmosColor $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(SyndiasmosColor $entity)
    {
        $form = $this->createForm(new SyndiasmosColorType(), $entity,
            array(
            'action' => $this->generateUrl('elections_admin_syndiasmoscolor_update',
                array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }

    /**
     * Edits an existing SyndiasmosColor entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GnosisElectionsBaseBundle:SyndiasmosColor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SyndiasmosColor entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new SyndiasmosColorType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info',
                'gsprod.general.record_saved');
            if ($editForm->get('saveAndAdd')->isClicked())
                return $this->redirect($this->generateUrl('elections_admin_syndiasmoscolor_new'));
            else
                return $this->redirect($this->generateUrl('elections_admin_syndiasmoscolor_edit',
                            array('id' => $id)));
        } else {
            $this->get('session')->getFlashBag()->add('notice',
                'gsprod.general.record_errors');
        }


        return $this->render('GnosisElectionsAdminBundle:SyndiasmosColor:edit.html.twig',
                array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a SyndiasmosColor entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('GnosisElectionsBaseBundle:SyndiasmosColor')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SyndiasmosColor entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('elections_admin_syndiasmoscolor'));
    }

    /**
     * Creates a form to delete a SyndiasmosColor entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
                ->setAction($this->generateUrl('elections_admin_syndiasmoscolor_delete',
                        array('id' => $id)))
                ->setMethod('DELETE')
                ->add('submit', 'submit', array('label' => 'Delete'))
                ->getForm()
        ;
    }

}
