<?php

namespace Gnosis\ElectionsAdminBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Gnosis\ElectionsBaseBundle\Entity\StaticBlock;
use Gnosis\ElectionsBaseBundle\Form\StaticBlockType;

/**
 * Description of StaticBlockController
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Οργάνωση")
 * @Breadcrumb("elections.staticblock.static_blocks", route="elections_admin_staticblocks")
 * @Security("has_role('ROLE_ADMIN')")
 * @CurrentMenuItem("elections_admin_staticblocks")
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Sep 13, 2016
 */
class StaticBlockController extends Controller {
  /**
     * Lists all entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT a FROM GnosisElectionsBaseBundle:StaticBlock a";
        $query = $em->createQuery($dql);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1) /* page number */,
            $this->get('request')->query->get('limit', 15)
        );

        return $this->render('GnosisElectionsAdminBundle:StaticBlock:index.html.twig',
                array(
                'pagination' => $pagination,
        ));
    }

    /**
     * Creates a new SyndiasmosColor entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new StaticBlock();
        $form = $this->createForm(new StaticBlockType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info', 'gsprod.general.record_saved');
            if ($form->get('saveAndAdd')->isClicked())
                return $this->redirect($this->generateUrl('elections_admin_staticblocks_new'));
            else
                return $this->redirect($this->generateUrl('elections_admin_staticblocks'));
        }
        else {
            $this->get('session')->getFlashBag()->add('notice', 'gsprod.general.record_errors');
        }

        return $this->render('GnosisElectionsAdminBundle:StaticBlock:new.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Region entity.
     * @Breadcrumb("Προσθήκη")
     */
    public function newAction()
    {
        $entity = new StaticBlock();
        $form = $this->createForm(new StaticBlockType(), $entity);

        return $this->render('GnosisElectionsAdminBundle:StaticBlock:new.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing region entity.
     * @Breadcrumb("Μεταβολή")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GnosisElectionsBaseBundle:StaticBlock')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StaticBlock entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('GnosisElectionsAdminBundle:StaticBlock:edit.html.twig',
                array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a SyndiasmosColor entity.
     *
     * @param SyndiasmosColor $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(StaticBlock $entity)
    {
        $form = $this->createForm(new StaticBlockType(), $entity,
            array(
            'action' => $this->generateUrl('elections_admin_syndiasmoscolor_update',
                array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        return $form;
    }

    /**
     * Edits an existing SyndiasmosColor entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GnosisElectionsBaseBundle:StaticBlock')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StaticBlock entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new StaticBlockType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info',
                'gsprod.general.record_saved');
            if ($editForm->get('saveAndAdd')->isClicked())
                return $this->redirect($this->generateUrl('elections_admin_staticblocks_new'));
            else
                return $this->redirect($this->generateUrl('elections_admin_staticblocks_edit',
                            array('id' => $id)));
        } else {
            $this->get('session')->getFlashBag()->add('notice',
                'gsprod.general.record_errors');
        }


        return $this->render('GnosisElectionsAdminBundle:StaticBlock:edit.html.twig',
                array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a SyndiasmosColor entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('GnosisElectionsBaseBundle:StaticBlock')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find StaticBlock entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('elections_admin_staticblocks'));
    }

    /**
     * Creates a form to delete a SyndiasmosColor entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
                ->setAction($this->generateUrl('elections_admin_staticblocks_delete',
                        array('id' => $id)))
                ->setMethod('DELETE')
                ->add('submit', 'submit', array('label' => 'Delete'))
                ->getForm()
        ;
    }
}
