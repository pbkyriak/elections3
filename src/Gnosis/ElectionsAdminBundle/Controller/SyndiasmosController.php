<?php

namespace Gnosis\ElectionsAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Gnosis\ElectionsBaseBundle\Form\SyndiasmosType;
use Gnosis\ElectionsBaseBundle\Entity\Syndiasmos;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;
use Gnosis\ElectionsBaseBundle\Util\EklogesConsts;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Slx\MetronicBundle\Lib\GreekText;
use Gnosis\ElectionsBaseBundle\Util\RegionRowsPsifoi;

/**
 * Description of SyndiasmosController
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Οργάνωση")
 * @Breadcrumb("Συνδυασμοί", route="elections_admin_syndyasmoi")
 * @CurrentMenuItem("elections_admin_syndyasmoi")
 * @Security("has_role('ROLE_ADMIN')")
 * 
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Dec 2, 2016
 */
class SyndiasmosController extends Controller
{

    private $election;
    private $nomos;
    private $em;
    
    private function actionPre(Request $request) {
        $this->em = $this->getDoctrine()->getManager();
        $this->election = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($this->get('session')->get('election_id'));    
        $this->nomos = $this->em->getRepository("GnosisElectionsBaseBundle:RegionNomos")->find($this->get('session')->get('nomos_id'));
    }
    
    /**
     * Lists all Article entities.
     */
    public function indexAction(Request $request)
    {
        $this->actionPre($request);
        $form = $this->getFilterForm();
        $query = $this->makeListQuery($request, $form);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1) /* page number */,
            $this->get('request')->query->get('limit', 15)
        );
        return $this->render('GnosisElectionsAdminBundle:Syndiasmos:index.html.twig',
                array(
                'pagination' => $pagination,
                'filterForm' => $form->createView()
        ));
    }

    /**
     * Create list query
     */
    private function makeListQuery(Request $request, $form) {
        
        $dql = "SELECT a FROM GnosisElectionsBaseBundle:Syndiasmos a LEFT JOIN a.region r";
        $form->handleRequest($request);
        $filterQryParams = array();
        $where = array();
        $where[] = " a.election=:ekl ";
        $filterQryParams['ekl'] = $this->election->getId();
        if($this->election->getEType()== EklogesConsts::ELECTION_TYPE_DIMO) {
            $where[] = " r.id_path  like :nom ";
            $filterQryParams['nom'] = sprintf("%s,%%",$this->nomos->getIdPath());
        }
        $request->setLocale('el');
        if ($form->isValid()) {
            $filterData = $form->getData();
            if ($filterData['title']) {
                $where[] = " a.title like :title ";
                $filterQryParams['title'] = str_replace('*', '%',
                    $filterData['title']);
            }
        }
        if ($where) {
            $dql .= ' where ' . implode(' and ', $where);
        }

        $query = $this->em->createQuery($dql);
        $query->setParameters($filterQryParams);
        return $query;
    }

    /**
     * Creates filter form
     *
     */
    private function getFilterForm()
    {
        $filterData = array();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData,
                array('method' => 'GET', 'csrf_protection' => false))
            ->add(
                'title', 'text',
                array('required' => false, 'label' => 'Τίτλος')
            )
            ->getForm();
        return $form;
    }

    /**
     * Creates a new Project entity.
     * @Breadcrumb("Προβολή")
     */
    public function showAction(Request $request, $id) {
        $this->actionPre($request);
        $entity = $this->em->getRepository('GnosisElectionsBaseBundle:Syndiasmos')->find($id);
        $ypos = $this->em->getRepository('GnosisElectionsBaseBundle:Ypopsifios')->getSyndiasmosYpopsifioiPerRegion($this->election, $entity);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Syndiasmos entity.');
        }

        // the regions that ypopsifioi can be added
        $subRegions = $this->getRegionsForYpopsEntry($entity);
        // 
        return $this->render('GnosisElectionsAdminBundle:Syndiasmos:show.html.twig',
                array(
                'entity' => $entity,
                'ypopsifioi' => $ypos,
                'subRegions' => $subRegions,
                )
            );

    }
    
    private function getRegionsForYpopsEntry($entity) {
        $subRegions = array();
        if( $this->election->getEType()== EklogesConsts::ELECTION_TYPE_DIMO) {  // dimoi kai dim enotites
            $subRegions[] = [
                'rid' => $entity->getRegion()->getId(),
                'title' => $entity->getRegion()->getTitle(),
                'rtype' => 'd',
            ];
            $srs = $this->em->getRepository('GnosisElectionsBaseBundle:RegionDimEnotita')->getDimosDimEnotites($entity->getRegion());
            foreach($srs as $r) {
                $subRegions[] = [
                    'rid' => $r->getId(),
                    'title' => $r->getTitle(),
                    'rtype' => 'de',
                ];
                
            }
        }
        else if( $this->election->getEType()== EklogesConsts::ELECTION_TYPE_PERI || $this->election->getEType()== EklogesConsts::ELECTION_TYPE_BOYL) { // nomos
            $subRegions[] = [
                'rid' => $this->nomos->getId(),
                'title' => $this->nomos->getTitle(),
                'rtype' => 'n',
            ];
        }
        else if( $this->election->getEType()== EklogesConsts::ELECTION_TYPE_EURO) { // perifereia
            $subRegions[] = [
                'rid' => $this->nomos->getParent()->getId(),
                'title' => $this->nomos->getParent()->getTitle(),
                'rtype' => 'p',
            ];            
        }
        return $subRegions;
    }
    /**
     * Creates a new Project entity.
     * @Breadcrumb("Προσθήκη")
     */
    public function createAction(Request $request)
    {
        $this->actionPre($request);
        $entity = new Syndiasmos();
        $form = $this->createNewForm($entity);
        $form->bind($request);

        if ($form->isValid()) {
            $entity->setSlug(GreekText::slugify($entity->getTitle()));
            $this->em->persist($entity);
            $this->em->flush();
            $hasRegionRows = $this->em->getRepository('GnosisElectionsBaseBundle:RegionPsifoi')->hasSyndiasmosRegionRows($entity->getId(),$this->nomos->getId());
            if(!$hasRegionRows) {
                $rrpsifoi = new RegionRowsPsifoi($this->em);
                $rrpsifoi->create($entity, $this->nomos);
            }            
            $this->get('session')->getFlashBag()->add('info',
                'gsprod.general.record_saved');
            return $this->redirect($this->generateUrl('elections_admin_syndyasmos_show',
                        array('id' => $entity->getId())));
        } else {
            $this->get('session')->getFlashBag()->add('notice',
                'gsprod.general.record_errors');
        }

        return $this->render('GnosisElectionsAdminBundle:Syndiasmos:new.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Project entity.
     * @Breadcrumb("Προσθήκη")
     */
    public function newAction(Request $request)
    {
        $this->actionPre($request);
        $entity = new Syndiasmos();
        $form = $this->createNewForm($entity);
        return $this->render('GnosisElectionsAdminBundle:Syndiasmos:new.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }

    private function createNewForm($entity) {
        $entity->setElection($this->election);
        $parentLevel = 1;
        if( $this->election->getEType()== EklogesConsts::ELECTION_TYPE_DIMO ) {
            $parentLevel = 3;
        }
        $form = $this->createForm(new SyndiasmosType(), $entity, array('parentLevel' => $parentLevel));
        return $form;
    }
    
    /**
     * Creates a form to edit a Project entity.
     *
     * @param Ekloges $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Syndiasmos $entity) {
        $parentLevel = 1;
        if( $this->election->getEType()== EklogesConsts::ELECTION_TYPE_DIMO ) {
            $parentLevel = 3;
        }
        $form = $this->createForm(new SyndiasmosType(), $entity,
            array(
            'action' => $this->generateUrl('elections_admin_syndyasmos_update',
                array('id' => $entity->getId())),
            'method' => 'PUT',
            'parentLevel' => $parentLevel,
        ));

        return $form;
    }
    
    /**
     * Displays a form to edit an existing Project entity.
     * @Breadcrumb("Μεταβολή")
     */
    public function editAction(Request $request, $id) {
        $this->actionPre($request);
        
        $entity = $this->em->getRepository('GnosisElectionsBaseBundle:Syndiasmos')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ekloges entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('GnosisElectionsAdminBundle:Syndiasmos:edit.html.twig',
                array(
                'entity' => $entity,
                'form' => $editForm->createView(),
        ));
    }

    /**
     * Edits an existing Project entity.
     * @Breadcrumb("Μεταβολή")
     */
    public function updateAction(Request $request, $id)
    {
        $this->actionPre($request);

        $entity = $this->em->getRepository('GnosisElectionsBaseBundle:Syndiasmos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Syndiasmos entity.');
        }
        $originalLogo = $entity->getLogo();
        $editForm = $this->createEditForm($entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $files = $request->files;
            //var_dump($files->get('gnosis_electionsbasebundle_syndiasmos')['logo']);die(1);
            //$uploadedFile = $files->get('logo')['file'];
            if(!$files->get('gnosis_electionsbasebundle_syndiasmos')['logo'] instanceof \Symfony\Component\HttpFoundation\File\UploadedFile) {
                $entity->setLogo($originalLogo);
            }
                
            $entity->setSlug(GreekText::slugify($entity->getTitle()));
            $this->em->persist($entity);
            $this->em->flush();
            $this->get('session')->getFlashBag()->add('info','gsprod.general.record_saved');
            return $this->redirect($this->generateUrl('elections_admin_syndyasmos_show', array('id' => $id)));
        } else {
            $this->get('session')->getFlashBag()->add('notice','gsprod.general.record_errors');
        }

        return $this->render('GnosisElectionsAdminBundle:Syndiasmos:edit.html.twig',
                array(
                'entity' => $entity,
                'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a Project entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('GnosisElectionsBaseBundle:Syndiasmos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Syndiasmos entity.');
        }
        $em->remove($entity);
        $em->flush();
        $this->get('session')->getFlashBag()->add('info',
            'gsprod.general.record_deleted');

        return $this->redirect($this->generateUrl('elections_admin_syndyasmoi'));
    }

    public function removeLogoAction(Request $request, $id) {
        $this->actionPre($request);
        $entity = $this->em->getRepository('GnosisElectionsBaseBundle:Syndiasmos')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Syndiasmos entity.');
        }

        $fileName = $entity->getLogo();
        if( $fileName ) {
            if(file_exists($fileName)) {
                @unlink($fileName);
                $entity->setLogo(null);
                $this->em->persist($entity);
                $this->em->flush();
                $this->get('session')->getFlashBag()->add('info',
                    'elections.syndyasmos.logo_deleted');
            }
        }
        return $this->redirect($this->generateUrl('elections_admin_syndyasmos_show', array('id' => $id)));
    }
}
