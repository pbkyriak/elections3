<?php

namespace Gnosis\ElectionsAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Gnosis\ElectionsBaseBundle\Entity\Syndiasmos;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;
use Gnosis\ElectionsBaseBundle\Util\EklogesConsts;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Slx\MetronicBundle\Lib\GreekText;
use Gnosis\ElectionsBaseBundle\Entity\Ypopsifios;
use Gnosis\ElectionsBaseBundle\Form\YpopsifiosType;
use Gnosis\ElectionsBaseBundle\Util\RegionRowsStauroi;
/**
 * Description of YpopsifiosController
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Οργάνωση")
 * @Breadcrumb("Συνδυασμοί", route="elections_admin_syndyasmoi")
 * @Breadcrumb("Υποψήφιος")
 * @CurrentMenuItem("elections_admin_syndyasmoi")
 * @Security("has_role('ROLE_ADMIN')")
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Dec 10, 2016
 */
class YpopsifiosController extends Controller {
    private $election;
    private $nomos;
    private $em;
    private $syndiasmos;
    private $region;
    
    private function actionPre(Request $request) {
        $this->em = $this->getDoctrine()->getManager();
        $this->election = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($this->get('session')->get('election_id'));    
        $this->nomos = $this->em->getRepository("GnosisElectionsBaseBundle:RegionNomos")->find($this->get('session')->get('nomos_id'));
        $this->syndiasmos = $this->em->getRepository("GnosisElectionsBaseBundle:Syndiasmos")->find($request->get('sid'));
        $this->region = $this->em->getRepository("GnosisElectionsBaseBundle:Region")->find($request->get('rid'));
    }
    
    public function newAction(Request $request, $sid, $rid) {
        $this->actionPre($request);
        $entity = new Ypopsifios();
        $entity->setElection($this->election)
                ->setKomma($this->syndiasmos)
                ->setLocality($this->region);
        $form = $this->createForm(new YpopsifiosType(), $entity);
        if( $request->isMethod('POST') ) {
            $form->bind($request);

            if ($form->isValid()) {
                $entity->setSlug(GreekText::slugify($entity->getTitle()));
                $this->em->persist($entity);
                $this->em->flush();
                $hasRegionRows = $this->em->getRepository('GnosisElectionsBaseBundle:RegionStauroi')->hasYpopsifiosRegionRows($entity->getId(),$this->nomos->getId());
                if(!$hasRegionRows) {
                    $rrpsifoi = new RegionRowsStauroi($this->em);
                    $rrpsifoi->create($entity, $this->nomos);
                }
                $this->get('session')->getFlashBag()->add('info','gsprod.general.record_saved');
                if( $form->get('saveAndAdd')->isClicked() ) {
                    return $this->redirect($this->generateUrl('elections_admin_ypopsifios_new',array('sid' => $sid, 'rid'=>$rid)));
                }
                return $this->redirect($this->generateUrl('elections_admin_syndyasmos_show',array('id' => $sid)));
            } else {
                $this->get('session')->getFlashBag()->add('notice','gsprod.general.record_errors');
            }
        }
        return $this->render('GnosisElectionsAdminBundle:Ypopsifios:new.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
                'sid' => $sid,
                'rid' => $rid,
        ));
    }
   
    public function editAction(Request $request, $sid, $rid, $id) {
        $this->actionPre($request);
        $entity = $this->em->getRepository('GnosisElectionsBaseBundle:Ypopsifios')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ypopsifios entity.');
        }
        $form = $this->createForm(new YpopsifiosType(), $entity);
        if( $request->isMethod('POST') ) {
            $form->bind($request);

            if ($form->isValid()) {
                $entity->setSlug(GreekText::slugify($entity->getTitle()));
                $this->em->persist($entity);
                $this->em->flush();
                $this->get('session')->getFlashBag()->add('info','gsprod.general.record_saved');
                if( $form->get('saveAndAdd')->isClicked() ) {
                    return $this->redirect($this->generateUrl('elections_admin_ypopsifios_new',array('sid' => $sid, 'rid'=>$rid)));
                }
                return $this->redirect($this->generateUrl('elections_admin_syndyasmos_show',array('id' => $sid)));
            } else {
                $this->get('session')->getFlashBag()->add('notice','gsprod.general.record_errors');
            }
        }
        return $this->render('GnosisElectionsAdminBundle:Ypopsifios:edit.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
                'sid' => $sid,
                'rid' => $rid,
        ));
    }
    
    public function deleteAction(Request $request, $sid, $rid, $id) {
        $this->actionPre($request);
        $entity = $this->em->getRepository('GnosisElectionsBaseBundle:Ypopsifios')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ypopsifios entity.');
        }
        $this->em->remove($entity);
        $this->em->flush();
        $this->get('session')->getFlashBag()->add('info','gsprod.general.record_deleted');

        return $this->redirect($this->generateUrl('elections_admin_syndyasmos_show',array('id' => $sid)));

    }
}
