<?php

namespace Gnosis\ElectionsAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class DefaultController extends Controller {

    /**
     * 
     * @Breadcrumb("elections.elections.home", route="metronic_admin_homepage")
     * @CurrentMenuItem("metronic_admin_homepage")
     */
    public function indexAction() {
        return $this->render('GnosisElectionsAdminBundle:Default:index.html.twig');
    }

    public function electionSelectorAction() {
        $securityContext = $this->get('security.context');
        if( $securityContext->isGranted(array('ROLE_SUPER_ADMIN', 'ROLE_ADMIN'))) {
            $ekloges = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('GnosisElectionsBaseBundle:Ekloges')
                    ->findAll();
        }
        else {
            $ekloges = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('GnosisElectionsBaseBundle:Ekloges')
                    ->findById($this->getUser()->getWelection());
        }
        $cEklogesTitle = $this->get('session')->get('election_title');
        return $this->render(
                'GnosisElectionsAdminBundle:Default:_electionSelector.html.twig', 
                array(
                    'ekloges' => $ekloges,
                    'cEklogesTitle' => $cEklogesTitle,
                )
        );
    }

    public function selectWorkEklogesAction($id) {
        $em = $this->getDoctrine()->getManager();
        $ekloges = $em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($id);
        $session = $this->get('session');
        $session->set('election_id', $ekloges->getId());
        $session->set('election_title', $ekloges->getTitle());
        $nomos = $em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('election' => $ekloges));
        $session->set('nomos_id', $nomos ? $nomos->getId(): 0);
        $session->set('nomos_title', $nomos ? $nomos->getTitle() : '');

        return $this->redirect($this->generateUrl('metronic_admin_homepage'));
    }

    public function nomosSelectorAction() {
        $securityContext = $this->get('security.context');
        $election = $this->getDoctrine()
                ->getManager()
                ->getRepository('GnosisElectionsBaseBundle:Ekloges')
                ->find($this->get('session')->get('election_id'));
        if( $securityContext->isGranted(array('ROLE_SUPER_ADMIN', 'ROLE_ADMIN'))) {
            $nomoi = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('GnosisElectionsBaseBundle:RegionNomos')
                    ->findBy(array('election' => $election));
        }
        else {
            $nomoi = $this->getDoctrine()
                    ->getManager()
                    ->getRepository('GnosisElectionsBaseBundle:RegionNomos')
                    ->findById($this->getUser()->getWregion());
        }
        $cEklogesTitle = $this->get('session')->get('nomos_title');
        return $this->render(
                'GnosisElectionsAdminBundle:Default:_nomosSelector.html.twig', 
                array(
                    'nomoi' => $nomoi,
                    'cEklogesTitle' => $cEklogesTitle,
                )
        );
    }

    public function selectWorkNomosAction($id) {
        $em = $this->getDoctrine()->getManager();
        $session = $this->get('session');
        $electionId = $session->get('election_id');
        $ekloges = $em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($electionId);
        $nomos = $em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('id' => $id, 'election' => $ekloges));
        if ($nomos) {
            $session->set('nomos_id', $nomos->getId());
            $session->set('nomos_title', $nomos->getTitle());
        }
        return $this->redirect($this->generateUrl('metronic_admin_homepage'));
    }

    /**
     * @Breadcrumb("Home", route="metronic_admin_homepage")
     * @Breadcrumb("elections.elections.reports")
     * @CurrentMenuItem("elections_admin_reports")
     * @Security("has_role('ROLE_RADMIN')")
     */
    public function reportsAction() {
        return $this->render('GnosisElectionsAdminBundle:Default:reports.html.twig');
        
    }
}
