<?php

namespace Gnosis\ElectionsAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;

use Gnosis\ElectionsBaseBundle\Entity\Ekloges;
use Gnosis\ElectionsBaseBundle\Form\EklogesType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Description of ElectionsController
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Οργάνωση")
 * @Breadcrumb("Εκλογές", route="elections_admin_elections")
 * @CurrentMenuItem("elections_admin_elections")
 * @Security("has_role('ROLE_ADMIN')")
 * 
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 10 Μαϊ 2014
 */
class ElectionsController extends Controller
{
    
    /**
     * Lists all Article entities.
     */
    public function indexAction()
    {
        $form = $this->getFilterForm();
        $query = $this->makeListQuery($form);
        $request = $this->get('request');
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1) /* page number */,
            $this->get('request')->query->get('limit', 15)
        );
        return $this->render('GnosisElectionsAdminBundle:Elections:index.html.twig',
                array(
                'pagination' => $pagination,
                'filterForm' => $form->createView()
        ));
    }

    /**
     * Create list query
     */
    private function makeListQuery($form)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT a FROM GnosisElectionsBaseBundle:Ekloges a";
        $form->handleRequest($this->get('request'));
        $filterQryParams = array();
        $where = array();
        $this->get('request')->setLocale('el');
        if ($form->isValid()) {
            $filterData = $form->getData();
            if ($filterData['title']) {
                $where[] = " a.title like :title ";
                $filterQryParams['title'] = str_replace('*', '%',
                    $filterData['title']);
            }
        }
        if ($where) {
            $dql .= ' where ' . implode(' and ', $where);
        }

        $query = $em->createQuery($dql);
        $query->setParameters($filterQryParams);
        return $query;
    }

    /**
     * Creates filter form
     *
     */
    private function getFilterForm()
    {
        $filterData = array();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData,
                array('method' => 'GET', 'csrf_protection' => false))
            ->add(
                'title', 'text',
                array('required' => false, 'label' => 'Τίτλος')
            )
            ->getForm();
        return $form;
    }

    /**
     * Creates a new Project entity.
     * @Breadcrumb("Προβολή")
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Article entity.');
        }

        return $this->render('GnosisElectionsAdminBundle:Elections:show.html.twig',
                array(
                'entity' => $entity,
                )
            );

    }
    
    /**
     * Creates a new Project entity.
     * @Breadcrumb("Προσθήκη")
     */
    public function createAction(Request $request)
    {
        $entity = new Ekloges();
        $form = $this->createForm(new EklogesType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info',
                'gsprod.general.record_saved');
            return $this->redirect($this->generateUrl('elections_admin_elections_show',
                        array('id' => $entity->getId())));
        } else {
            $this->get('session')->getFlashBag()->add('notice',
                'gsprod.general.record_errors');
        }

        return $this->render('GnosisElectionsAdminBundle:Elections:new.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Project entity.
     * @Breadcrumb("Προσθήκη")
     */
    public function newAction()
    {
        $entity = new Ekloges();
        $form = $this->createForm(new EklogesType(), $entity);

        return $this->render('GnosisElectionsAdminBundle:Elections:new.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Project entity.
     * @Breadcrumb("Μεταβολή")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ekloges entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('GnosisElectionsAdminBundle:Elections:edit.html.twig',
                array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Project entity.
     *
     * @param Ekloges $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Ekloges $entity)
    {
        $form = $this->createForm(new EklogesType(), $entity,
            array(
            'action' => $this->generateUrl('elections_admin_elections_update',
                array('id' => $entity->getId())),
            'method' => 'PUT',
        ));


        return $form;
    }

    /**
     * Edits an existing Project entity.
     * @Breadcrumb("Μεταβολή")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ekloges entity.');
        }

        $editForm = $this->createForm(new EklogesType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info',
                'gsprod.general.record_saved');
            return $this->redirect($this->generateUrl('elections_admin_elections_show',
                        array('id' => $id)));
        } else {
            $this->get('session')->getFlashBag()->add('notice',
                'gsprod.general.record_errors');
        }


        return $this->render('GnosisElectionsAdminBundle:Elections:edit.html.twig',
                array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a Project entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ekloges entity.');
        }
        $em->getConnection()->executeQuery("delete from ekloges where id=:idp", array('idp'=> $entity->getId()));
        //$em->remove($entity);
        //$em->flush();
        $this->get('session')->getFlashBag()->add('info',
            'gsprod.general.record_deleted');

        return $this->redirect($this->generateUrl('elections_admin_elections'));
    }

}
