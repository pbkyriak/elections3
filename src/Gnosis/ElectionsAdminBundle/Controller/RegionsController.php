<?php

namespace Gnosis\ElectionsAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Gnosis\ElectionsBaseBundle\Entity\Region;
use Gnosis\ElectionsBaseBundle\Entity\RegionPeriferia;
use Gnosis\ElectionsBaseBundle\Entity\RegionNomos;
use Gnosis\ElectionsBaseBundle\Entity\RegionEklPeriferia;
use Gnosis\ElectionsBaseBundle\Entity\RegionDimos;
use Gnosis\ElectionsBaseBundle\Entity\RegionDimEnotita;
use Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima;
use Gnosis\ElectionsBaseBundle\Util\RegionUtil;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * Description of regionsController
 *
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Οργάνωση")
 * @Breadcrumb("Γεωγραφία", route="elections_admin_regions")
 * @CurrentMenuItem("elections_admin_regions")
 * @Security("has_role('ROLE_ADMIN')")
 * 
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 10 Μαϊ 2014
 */
class RegionsController  extends Controller
{
    
    public function indexAction() {
        $form = $this->getFilterForm();
        $query = $this->makeListQuery($form);
        $request = $this->get('request');
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1) /* page number */,
            $this->get('request')->query->get('limit', 15)
        );
        return $this->render('GnosisElectionsAdminBundle:Regions:index.html.twig',
                array(
                'pagination' => $pagination,
                'filterForm' => $form->createView()
        ));
    }
    
    /**
     * Create list query
     */
    private function makeListQuery($form)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT a FROM GnosisElectionsBaseBundle:Region a";
        $form->handleRequest($this->get('request'));
        $filterQryParams = array();
        $where = array();
        $this->get('request')->setLocale('el');
        $where[] = " a.election=:ekl ";
        $filterQryParams['ekl'] = $this->get('session')->get('election_id');
        if ($form->isValid()) {
            $filterData = $form->getData();
            if ($filterData['title']) {
                $where[] = " a.title like :title ";
                $filterQryParams['title'] = str_replace('*', '%', $filterData['title']);
            }
            if ($filterData['slug']) {
                $where[] = " a.slug like :slug ";
                $filterQryParams['slug'] = str_replace('*', '%', $filterData['slug']);
            }
            if ($filterData['idpath']) {
                $where[] = " a.id_path like :idpath ";
                $filterQryParams['idpath'] = str_replace('*', '%', $filterData['idpath']);
            }
            if ($filterData['flevel']) {
                $where[] = " a.level >= :flevel ";
                $filterQryParams['flevel'] = $filterData['flevel'];
            }
            if ($filterData['tlevel']) {
                $where[] = " a.level <= :tlevel ";
                $filterQryParams['tlevel'] = $filterData['tlevel'];
            }
        }
        if ($where) {
            $dql .= ' where ' . implode(' and ', $where);
        }
        $dql .= ' order by a.id_path';
        $query = $em->createQuery($dql);
        $query->setParameters($filterQryParams);
        return $query;
    }

    /**
     * Creates filter form
     *
     */
    private function getFilterForm()
    {
        $levelChoises = array(
            1 => 'Περιφέρεια',
            2 => 'Νομός',
            3 => 'Δήμος',
            4 => 'Εκλ. Περιφέρεια',
            5 => 'Δημ. Ενότητα',
            6 => 'Εκλ. Τμήμα',
        );
        
        $filterData = array();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData,
                array('method' => 'GET', 'csrf_protection' => false))
            ->add(
                'title', 'text',
                array('required' => false, 'label' => 'Τίτλος')
            )
            ->add(
                'slug', 'text',
                array('required' => false, 'label' => 'Slug')
            )
            ->add(
                'idpath', 'text',
                array('required' => false, 'label' => 'IdPath')
            )
            ->add(
                'flevel', 'choice',
                array('required'=>false, 'label'=>'Από επίπεδο', 'choices'=>$levelChoises)
            )
            ->add(
                'tlevel', 'choice',
                array('required'=>false, 'label'=>'Έως επίπεδο', 'choices'=>$levelChoises)
            )
            ->getForm();
        return $form;
    }
    
    /**
     * @Breadcrumb("Προβολή")
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GnosisElectionsBaseBundle:Region')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Region entity.');
        }

        $nextLevel = RegionUtil::getNextClassTypeFromClass(get_class($entity));
        return $this->render('GnosisElectionsAdminBundle:Regions:show.html.twig',
                array(
                'entity' => $entity,
                'nextLevel' => $nextLevel,
                )
            );
    }
    
    /**
     * Displays a form to create a new Region entity.
     * @Breadcrumb("Προσθήκη")
     */
    public function newAction($newType, $parentId)
    {
        $entity = $this->getNewEntity($newType, $parentId);
        $regionType = $this->getNewRegionType($newType);
        $form = $this->createForm($regionType, $entity, array('parentLevel' => $entity->getLevel()-1));

        return $this->render('GnosisElectionsAdminBundle:Regions:new.html.twig',
                array(
                'entity' => $entity,
                'newType' => $newType,
                'parentId' => $parentId,
                'form' => $form->createView(),
                'regionText' => RegionUtil::getRegionTextFromClass(get_class($entity))
        ));
    }

    /**
     * Creates a new Region entity.
     * @Breadcrumb("Προσθήκη")
     */
    public function createAction(Request $request, $newType, $parentId)
    {
        $entity = $this->getNewEntity($newType, $parentId);
        $regionType = $this->getNewRegionType($newType);
        $form = $this->createForm($regionType, $entity, array('parentLevel' => $entity->getLevel()-1,));
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $idPath = '';
            if( $entity->getParent() ) {
                $idPath = $entity->getParent()->getIdPath();
                $idPath .= ',';
            }
            $idPath .= $entity->getId();
            $entity->setIdPath($idPath);
            $entity->setLevel(substr_count($idPath, ',')+1);
            $em->persist($entity);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('info',
                'gsprod.general.record_saved');
            if ($form->get('saveAndAdd')->isClicked()) {
                return $this->redirect($this->generateUrl('elections_admin_regions_new', array('newType'=>$newType, 'parentId'=>$parentId)));
            }
            else {
                return $this->redirect($this->generateUrl('elections_admin_regions_show',
                            array('id' => $entity->getId())));
            }
        } else {
            $this->get('session')->getFlashBag()->add('notice',
                'gsprod.general.record_errors');
        }

        return $this->render('GnosisElectionsAdminBundle:Regions:new.html.twig',
                array(
                'entity' => $entity,
                'newType' => $newType,
                'parentId' => $parentId,
                'form' => $form->createView(),
        ));
    }

    private function getNewEntity($newType, $parentId) {
        $em = $this->getDoctrine()->getManager();
        $parent = $em->getRepository('GnosisElectionsBaseBundle:Region')->find($parentId);
        $election = $em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($this->get('session')->get('election_id'));
        $clazz = 'Gnosis\ElectionsBaseBundle\Entity\Region'.ucfirst($newType);
        $entity = new $clazz();
        $entity->setParent($parent);
        $entity->setElection($election);
        $entity->setLevel($parent ? $parent->getLevel()+1 : 1);
        return $entity;
    }
    
    private function getNewRegionType($newType) {
        if( $newType=='nomos' ) {
            return new \Gnosis\ElectionsBaseBundle\Form\RegionNomosType();
        }
        elseif( $newType=='dimos' ) {
            return new \Gnosis\ElectionsBaseBundle\Form\RegionDimosType();
        }
        elseif( $newType=='periferia' ) {
            return new \Gnosis\ElectionsBaseBundle\Form\RegionPeriferiaType();
        }
        elseif( $newType=='eklTmima' ) {
            return new \Gnosis\ElectionsBaseBundle\Form\RegionEktTmimaType();
        }
        else {
            return new \Gnosis\ElectionsBaseBundle\Form\RegionType();
        }
    }
    
    /**
     * Displays a form to edit an existing region entity.
     * @Breadcrumb("Μεταβολή")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GnosisElectionsBaseBundle:Region')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Region entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('GnosisElectionsAdminBundle:Regions:edit.html.twig',
                array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Region entity.
     *
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm($entity)
    {
        if( $entity instanceof \Gnosis\ElectionsBaseBundle\Entity\RegionNomos ) {
            $fType = new \Gnosis\ElectionsBaseBundle\Form\RegionNomosType();
        }
        elseif( $entity instanceof \Gnosis\ElectionsBaseBundle\Entity\RegionDimos ) {
            $fType = new \Gnosis\ElectionsBaseBundle\Form\RegionDimosType();
        }
        elseif( $entity instanceof \Gnosis\ElectionsBaseBundle\Entity\RegionPeriferia ) {
            $fType = new \Gnosis\ElectionsBaseBundle\Form\RegionPeriferiaType();
        }
        elseif( $entity instanceof \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima ) {
            $fType = new \Gnosis\ElectionsBaseBundle\Form\RegionEktTmimaType();
        }
        else {
            $fType = new \Gnosis\ElectionsBaseBundle\Form\RegionType();
        }
        $form = $this->createForm($fType, $entity,
            array(
            'action' => $this->generateUrl('elections_admin_elections_update',array('id' => $entity->getId())),
            'method' => 'PUT',
            'parentLevel' => $entity->getLevel()-1,
        ));


        return $form;
    }

    /**
     * Edits an existing Project entity.
     * @Breadcrumb("Μεταβολή")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GnosisElectionsBaseBundle:Region')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Region entity.');
        }
        $oldIdPath = $entity->getIdPath();
        //$fType = $this->getEditRegionType($entity);
        $editForm = $this->createEditForm($entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
            //
            if( $entity->getLevel()>1 ) {
                $entity->updateMyIdPath();
                $newIdPath = $entity->getIdPath();
                if( $newIdPath!=$oldIdPath ) {
                    $em->persist($entity);
                    $em->flush();
                    $em->getRepository('GnosisElectionsBaseBundle:Region')->fixIdPaths($oldIdPath);
                }
            }
            //
            $this->get('session')->getFlashBag()->add('info',
                'gsprod.general.record_saved');
            if ($editForm->get('saveAndAdd')->isClicked()) {
                $newType = lcfirst(str_replace('Gnosis\\ElectionsBaseBundle\\Entity\\Region','',get_class($entity)));
                $parentId = $entity->getParent() ? $entity->getParent()->getId() : 0;
                return $this->redirect($this->generateUrl('elections_admin_regions_new', array('newType'=>$newType, 'parentId'=>$parentId)));
            }
            else {
                return $this->redirect($this->generateUrl('elections_admin_regions_show',
                        array('id' => $id)));
            }
        } else {
            $this->get('session')->getFlashBag()->add('notice',
                'gsprod.general.record_errors');
        }


        return $this->render('GnosisElectionsAdminBundle:Regions:edit.html.twig',
                array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
        ));
    }
    
    /**
     * Deletes a Region entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('GnosisElectionsBaseBundle:Region')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Region entity.');
        }
        $em->remove($entity);
        $em->flush();
        $this->get('session')->getFlashBag()->add('info',
            'gsprod.general.record_deleted');

        return $this->redirect($this->generateUrl('elections_admin_regions'));
    }
    
    /**
     * @Security("has_role('ROLE_USER','ROLE_PROTOK','ROLE_PRAKT')")
     * @param Request $request
     * @param type $level
     * @return Response
     */
    public function jsonListAction(Request $request, $level)
    {
        $q = str_replace('*', '%', $request->get('q'));

        $callback = $request->get('callback');
        $em = $this->getDoctrine()->getManager();
        $nomos = $em->getRepository("GnosisElectionsBaseBundle:RegionNomos")->find($this->get('session')->get('nomos_id'));
        $qb = $em->getRepository('GnosisElectionsBaseBundle:Region')
            ->createQueryBuilder('a')
            ->where('a.title like :code or a.slug like :code')
            ->andWhere('a.election=:ekl');
        if( $level==6 || $level==3 ) {
            $qb->andWhere('a.id_path like :nom');
            $qb->setParameter('nom', sprintf("%s,%%", $nomos->getIdPath()));
        }
        if ($level) {
            $qb->andWhere('a.level=:level')
                ->setParameter('level', $level);
        }
               
        $qb
            ->setParameter('code', $q)
            ->setParameter('ekl', $this->get('session')->get('election_id'));
        $entities = $qb
            ->getQuery()
            ->getResult();
        $results = array();
        if ($entities) {
            foreach ($entities as $entity) {
                $results[] = array(
                    'id' => $entity->getId(), 
                    'title' => $entity->getTitle()
                    );
            }
        }
        $r = array('total' => count($results), 'movies' => $results);
        $response = new Response(sprintf("%s(%s)", $callback, json_encode($r)));
        $response->headers->set('Content-Type', 'text/json');
        return $response;
    }

    /**
     * @Security("has_role('ROLE_USER','ROLE_PROTOK','ROLE_PRAKT')")
     */
    public function jsonGetItemAction(Request $request)
    {
        $q = $request->get('q');
        $callback = $request->get('callback');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('GnosisElectionsBaseBundle:Region')->find($q);
        $results = array();
        if ($entity) {
            $results = array('id' => $entity->getId(), 'title' => $entity->getTitle());
        }
        $r = array('total' => count($results), 'movie' => $results);
        $response = new Response(sprintf("%s(%s)", $callback,
                json_encode($results)));
        $response->headers->set('Content-Type', 'text/json');
        return $response;
    }

    /**
     * @Security("has_role('ROLE_USER','ROLE_PROTOK','ROLE_PRAKT')")
     * @param Request $request
     * @return JsonResponse
     */
    public function jsonSelectByEklogesNomosAction(Request $request) {
        $electionId = $request->request->get('election_id');
        $em = $this->getDoctrine()->getManager();
        $election = $em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($electionId);
        $nomoi = $em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findByElection($election);
        $out = array();
        foreach($nomoi as $nomos) {
            $out[] = array('id'=>$nomos->getId(), 'title'=>$nomos->getTitle());
        }
        return new JsonResponse($out);
    }
}
