<?php

namespace Gnosis\ElectionsAdminBundle\Listener;

use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine; 
use Symfony\Component\HttpFoundation\Session\Session;
use Gnosis\ElectionsEntryBaseBundle\Entity\UserLog;
/**
 * Description of LoginListener
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 10 Μαϊ 2014
 */
class LoginListener
{
    /** @var \Symfony\Component\HttpFoundation\Session\Session */
    private $session;
	/** @var \Doctrine\ORM\EntityManager */
    private $em;
    
    public function __construct(Session $session, Doctrine $doctrine) {
        $this->session = $session;
        $this->em = $doctrine->getManager();
    }
    
    public function onInteractiveLogin(InteractiveLoginEvent $event) {
        $user = $event->getAuthenticationToken()->getUser();
        if( $user->getWelection() ) {
            $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($user->getWelection());
        }
        else {
            $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->getNewestEkloges();
        }
        $this->session->set('election_id', $ekloges->getId());
        $this->session->set('election_title', $ekloges->getTitle());
        $this->session->set('election_has_staurous', $ekloges->getHasStaurous());
        if( $user->getWregion() ) {
            $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->find($user->getWregion());
        }
        else {
            $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('election'=>$ekloges));
        }
        $this->session->set('nomos_id', $nomos->getId());
        $this->session->set('nomos_title', $nomos->getTitle());

        // remove any locks for that user
        $this->em->getConnection()->delete('entry_lock', array('user_id'=>$user->getId()));
        
        // log his login
        $euser = $this->em->getReference('Slx\UserBundle\Entity\User', $user->getId());
        $logObj = new UserLog();
        $logObj->setUser($euser)
            ->setClass('Login')
            ->setObjectId(0)
            ->setTitle('Login')
            ->setAction('Login')
            ->setCreatedAt(new \DateTime())
            ;
        $this->em->persist($logObj);
        $this->em->flush();

    }
}
