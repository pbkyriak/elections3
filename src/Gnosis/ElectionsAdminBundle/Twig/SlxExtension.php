<?php

namespace Gnosis\ElectionsAdminBundle\Twig;
use Gnosis\ElectionsBaseBundle\Util\RegionUtil;
/**
 * Description of SlxExtension
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class SlxExtension extends \Twig_Extension {

    private $em;

    public function __construct($em) {
        $this->em = $em;
    }

    public function getFunctions() {
        return array(
            new \Twig_SimpleFunction('renderStaticBlock', array($this, 'renderStaticBlock')),
            new \Twig_SimpleFunction('getNextRegionLevelFromEntity', array($this, 'getNextRegionLevelFromEntity')),
        );
    }

    public function renderStaticBlock($blockCode) {
        $out = 'hello';
        $block = $this->em->getRepository("GnosisElectionsBaseBundle:StaticBlock")->findOneBy(array('block_code'=>$blockCode));
        if( $block ) {
            $out = $block->getContent();
        }
        return $out;
    }

    public function getNextRegionLevelFromEntity($entity) {
        return RegionUtil::getNextClassTypeFromClass(get_class($entity));
    }
    
    public function getName() {
        return 'slx_block_extension';
    }

}
