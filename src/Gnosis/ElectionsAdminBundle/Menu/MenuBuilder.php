<?php

// src/Acme/MainBundle/Menu/MenuBuilder.php

namespace Gnosis\ElectionsAdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;
use Slx\MetronicBundle\Menu\AbstractMenuBuilder;

class MenuBuilder extends AbstractMenuBuilder
{

    /** @var ContainerInterface */
    private $container;

    /** @var Router */
    private $router;

    /**
     * @var SecurityContext      
     */
    private $securityContext;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->router = $container->get('router');
        $this->securityContext = $container->get('security.context');
        $this->currentItemRouteName = 'metronic_admin_homepage';
    }

    public function createMainMenu(FactoryInterface $factory)
    {
        $isRAdmin = $this->securityContext->isGranted(array('ROLE_RADMIN'));
        $isAdmin = $this->securityContext->isGranted(array('ROLE_ADMIN'));
        $isSuperAdmin = $this->securityContext->isGranted(array('ROLE_SUPER_ADMIN'));
        
        $menu = $factory->createItem('root');
        $menu->addChild('Home', array('route' => 'metronic_admin_homepage'))->setAttribute('icon', 'fa-home');
        if( $this->securityContext->isGranted(array('ROLE_USER', 'ROLE_PROTOK', 'ROLE_PRAKT', 'ROLE_PHONE')) ) {
            $ent = $menu->addChild('Καταχώρηση')->setAttribute('icon','fa-stack-overflow');
            if( $this->securityContext->isGranted(array('ROLE_USER')) ) {
                $ent->addChild('Ψήφων', array('route'=>'gnosis_elections_entry_psifoi'));
                $ent->addChild('Σταυρών', array('route'=>'gnosis_elections_entry_stauroi'));
            }
            if( $this->securityContext->isGranted(array('ROLE_PRAKT')) ) {
                $ent->addChild('Πρακτικών', array('route'=>'election_admin_praktika_etsearch'));
            }
            if( $this->securityContext->isGranted(array('ROLE_PROTOK')) ) {
                $ent->addChild('Πρωτόκολλο', array('route'=>'election_admin_protokolo_etsearch'));
            }
            if( $this->securityContext->isGranted(array('ROLE_PHONE')) ) {
                $ent->addChild('Τηλεφωνική αποστολή', array('route'=>'gnosis_elections_phone_index'));
            }
        }
        if( $isRAdmin || $isAdmin || $isSuperAdmin ) {
            $tools = $menu->addChild('Εργαλεία')->setAttribute('icon','fa-gears');
            $tools->addChild('User log', array('route'=>'userlog'));
            $tools->addChild('Entry locks', array('route'=>'gnosis_elections_entrylock'));
        
            $rpt = $menu->addChild('Αναφορές')->setAttribute('icon','fa-book');
            $rpt->addChild('Αναφορές', array('route'=>'elections_admin_reports'));
        }
        
        if( $isSuperAdmin || $isAdmin ) {
            $org = $menu->addChild('Οργάνωση')->setAttribute('icon','fa-sitemap');
            $org->addChild('Εκλογές', array('route'=>'elections_admin_elections'));
            $org->addChild('Γεωγραφία', array('route'=>'elections_admin_regions'));
            $org->addChild('Συνδυασμοί', array('route'=>'elections_admin_syndyasmoi'));
            $org->addChild('Αρχικοποίηση', array('route'=>'gnosis_elections_imp_admin_homepage'));
            $org->addChild('Δικαστικοί', array('route'=>'elections_admin_lawyers'));
            //$org->addChild('Χρώματα', array('route'=>'elections_admin_syndiasmoscolor'));
            $org->addChild('Στατικό περιεχόμενο', array('route'=>'elections_admin_staticblocks'));
            $org->addChild('Media', array('route' => 'slx_media'));
        }
        
        if( $isSuperAdmin || $isAdmin ) {
            $users = $menu->addChild('Χρήστες')->setAttribute('icon', 'fa-user');
            if( $isSuperAdmin ) {
                $users->addChild('Roles', array('route' => 'roles'));
            }
            if( $isSuperAdmin || $isAdmin ) {
                $users->addChild('Users', array('route' => 'user'));
                $users->addChild('Αλλαγή πλαισίου', array('route' => 'user_changeContext'));
            }
        }
        if($this->currentItemRouteName ) {
            $this->setCurrentMenuItem($menu, $this->currentItemRouteName);
        }
        return $menu;
    }

}