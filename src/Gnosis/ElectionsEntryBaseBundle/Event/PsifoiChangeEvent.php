<?php
namespace Gnosis\ElectionsEntryBaseBundle\Event;

use Symfony\Component\EventDispatcher\Event;
/**
 * Event fired when PsifoiEntry is created/updated/deleted
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class PsifoiChangeEvent extends Event {
    private $etId;
    private $changePData;
    private $changeRData;
    
    public function __construct($etId, $changeRData, $changePData) {
        $this->etId = $etId;
        $this->changeRData = $changeRData;
        $this->changePData = $changePData;
    }
    
    public function getEtId() {
        return $this->etId;
    }
    
    public function getChangeRData() {
        return $this->changeRData;
    }
    
    public function getChangePData() {
        return $this->changePData;
    }

}
