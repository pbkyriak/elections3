<?php

namespace Gnosis\ElectionsEntryBaseBundle\Event\Transformer;

use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;
/**
 * Description of MqMessageTransformerFactory
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqMessageTransformerFactory {

    /**
     * 
     * @param string $queue
     * @return Gnosis\ElectionsEntryBaseBundle\Event\Transformer\MqMessageToChangeEventInterface
     */
    public static function getTransformer($queue) {
        switch ($queue) {
            case RabbitMqConstants::PSIFOI_TRACK_QUEUE:
                return new MqMessageToPsifoiChangeEvent();
            case RabbitMqConstants::ET_LANE_QUEUE:
                return new MqMessageToEtChangeEvent();
            case RabbitMqConstants::STAUROI_LANE_QUEUE:
                return new MqMessageToStauroiChangeEvent();
            case RabbitMqConstants::NUMBERING_QUEUE:
                return new MqMessageToNumberingEvent();
            case RabbitMqConstants::JOBS_QUEUE:
                return new \Gnosis\ElectionsImpAdminBundle\Event\Transformer\MqMessageToJobInsertEvent();
            default:
                return null;
        }
    }
}
