<?php

namespace Gnosis\ElectionsEntryBaseBundle\Event\Transformer;

use PhpAmqpLib\Message\AMQPMessage;
/**
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
interface MqMessageToChangeEventInterface {

    public function transform(AMQPMessage $msg);
    public function getEventName();
    public function getEvent();

}
