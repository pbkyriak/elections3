<?php

namespace Gnosis\ElectionsEntryBaseBundle\Event\Transformer;

use PhpAmqpLib\Message\AMQPMessage;
use Gnosis\ElectionsEntryBaseBundle\Event\Transformer\MqMessageToChangeEventInterface;
use Gnosis\ElectionsEntryBaseBundle\Event\ElectionsEvents;
use Gnosis\ElectionsEntryBaseBundle\Event\MqPsifoiChangeEvent;
/**
 * Description of MqMessageToPsifoiChangeEvent
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqMessageToPsifoiChangeEvent implements MqMessageToChangeEventInterface {
    private $eventName;
    private $event;
    
    public function transform(AMQPMessage $msg) {
        $this->eventName = ElectionsEvents::MQ_PSIFOI_CHANGE;
        $eventData = json_decode($msg->body,true);
        $this->event = new MqPsifoiChangeEvent($eventData);
    }
        
    public function getEventName() {
        return $this->eventName;
    }
    
    public function getEvent() {
        return $this->event;
    }

}
