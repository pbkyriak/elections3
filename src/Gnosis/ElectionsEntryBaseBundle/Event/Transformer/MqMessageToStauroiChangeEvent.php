<?php

namespace Gnosis\ElectionsEntryBaseBundle\Event\Transformer;

use PhpAmqpLib\Message\AMQPMessage;
use Gnosis\ElectionsEntryBaseBundle\Event\Transformer\MqMessageToChangeEventInterface;
use Gnosis\ElectionsEntryBaseBundle\Event\ElectionsEvents;
use Gnosis\ElectionsEntryBaseBundle\Event\MqStauroiChangeEvent;

/**
 * Description of MqMessageToStauroiChangeEvent
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqMessageToStauroiChangeEvent implements MqMessageToChangeEventInterface {
    private $eventName;
    private $event;
    
    public function transform(AMQPMessage $msg) {
        $this->eventName = ElectionsEvents::MQ_STAUROI_CHANGE;
        $eventData = json_decode($msg->body, true);
        $this->event = new MqStauroiChangeEvent($eventData);
    }
        
    public function getEventName() {
        return $this->eventName;
    }
    
    public function getEvent() {
        return $this->event;
    }
}
