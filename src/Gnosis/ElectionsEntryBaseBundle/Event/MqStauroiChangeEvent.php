<?php

namespace Gnosis\ElectionsEntryBaseBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Description of MqStauroiChangeEvent
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqStauroiChangeEvent  extends Event {
    private $data;
    
    public function __construct($data) {
        $this->data = $data;
    }
    
    public function getData() {
        return $this->data;
    }
    
    public function isRPC() {
        return false;
    }

}
