<?php

namespace Gnosis\ElectionsEntryBaseBundle\Event;

/**
 * Description of ElectionsEvents
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
final class ElectionsEvents {
    
    // events that are fired from frontend
    const PSIFOI_INSERT = 'elections.psifoi_insert';
    const PSIFOI_UPDATE = 'elections.psifoi_update';
    const PSIFOI_DELETE = 'elections.psifoi_delete';

    const STAUROI_INSERT = 'elections.stauroi_insert';
    const STAUROI_UPDATE = 'elections.stauroi_update';
    const STAUROI_DELETE = 'elections.stauroi_delete';

    const JOB_INSERT = 'elections.job_insert';
    
    // events fired from rabbitmq event listener
    const MQ_PSIFOI_CHANGE = 'elections.mq.psifoi_change';
    const MQ_ET_CHANGE = 'elections.mq.et_change';
    const MQ_STAUROI_CHANGE = 'elections.mq.stauroi_change';
    
    const MQ_NUMBERING = 'elections.mq.numbering';
    
    const MQ_JOB_INSERT = 'elections.mq.job_insert';
}
