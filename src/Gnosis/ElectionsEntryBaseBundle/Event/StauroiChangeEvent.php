<?php

namespace Gnosis\ElectionsEntryBaseBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Description of StauroiChangeEvent
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class StauroiChangeEvent  extends Event {
    private $etId;
    private $changeData;
    
    public function __construct($etId, $changeData) {
        $this->etId = $etId;
        $this->changeData = $changeData;
    }
    
    public function getEtId() {
        return $this->etId;
    }
    
    public function getChangeData() {
        return $this->changeData;
    }
}
