<?php

namespace Gnosis\ElectionsEntryBaseBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntry;
use Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntryRow;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Description of DropElectionResultsCommand
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class DropElectionResultsCommand extends ContainerAwareCommand {

    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    
    protected function configure() {
        $this
                ->setName('elections:drop:results')
                ->setDescription("Drops elections results")
                ->addArgument('eklogesSlug', InputArgument::REQUIRED, 'ekloges to export?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $t1 = time();
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $eklogesSlug = $input->getArgument('eklogesSlug');
        
        $output->writeln("<info>Ekloges slug=</info>".$eklogesSlug);
        
        $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug'=>$eklogesSlug));
        if( !$ekloges ) {
            $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlug));
            return;
        }
        /*
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Continue with this action? (y/n)', false);
        if (!$helper->ask($input, $output, $question)) {
            $output->writeln('Operation rejected.');
            return;
        }
        */
        $this->dropEtResults($ekloges);
        $this->dropPsifoiResults($ekloges);
        $this->dropStauroiResults($ekloges);
        //$this->dropYpesToExport($ekloges);
        
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
    }
    
    private function dropEtResults($ekloges) {
        $data = array(
            'psifisan' => 0,
            'leyka' => 0,
            'akyra' => 0,
            'egkyra' => 0,
            'et_counted' => 0,
            'egg_counted' => 0,
        );
        $this->em->getConnection()->update('region', $data, array('ekloges_id'=>$ekloges->getId()));
    }
    
    private function dropPsifoiResults($ekloges) {
        $sql = sprintf("update region_psifoi p, region r set amount=0 where p.region_id=r.id and r.ekloges_id=%s", $ekloges->getId());
        $this->em->getConnection()->exec($sql);
    }
    
    private function dropStauroiResults($ekloges) {
        $sql = sprintf("update region_stauroi p, region r set amount=0 where p.region_id=r.id and r.ekloges_id=%s", $ekloges->getId());
        $this->em->getConnection()->exec($sql);        
    }

    private function dropYpesToExport($ekloges) {
        $sql = sprintf("delete * from ypes_to_export where ekloges_id=%s", $ekloges->getId());
        $this->em->getConnection()->exec($sql); 
    }

}
