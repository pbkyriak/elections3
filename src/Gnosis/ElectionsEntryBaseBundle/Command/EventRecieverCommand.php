<?php

namespace Gnosis\ElectionsEntryBaseBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Slx\RabbitMqBundle\Rabbitmq\Connection;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;
use PhpAmqpLib\Message\AMQPMessage;
use Gnosis\ElectionsEntryBaseBundle\Event\Transformer\MqMessageTransformerFactory;

/**
 * Description of EventRecieverCommand
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class EventRecieverCommand extends ContainerAwareCommand {
    /** @var \Symfony\Component\Console\Output\OutputInterface */
    private $output;
    /** @var \Slx\RabbitMqBundle\Rabbitmq\Connection */
    private $mq;
    /** @var \Doctrine\ORM\EntityManager */
    private $em;
    private $queue;
    private $trans;
    
    protected function configure()
    {
        $this
            ->setName('elections:rabbitmq:eventreciever')
            ->setDescription("Recieves event messages from rabbitmq")
            ->addArgument('queue', InputArgument::REQUIRED, 'Queue name to listen' )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $this->mq = $this->getContainer()->get('slx_rabbit_mq.rabbitmq');

        $queue = $input->getArgument('queue');
        $validQueueNames = $this->mq->getQueueKeys();
        if( !in_array($queue, $validQueueNames)) {
            $output->writeln(sprintf('Queue name can be %s', implode(' or ', $validQueueNames)));
            return false;
        }
        
        $this->queue = $queue;
        $this->trans = MqMessageTransformerFactory::getTransformer($this->queue);
        if(!$this->trans) {
            $output->writeln(sprintf('Message to Event transformer for queue %s is not defined!', $this->queue));
            return false;
        }
        $this->mq->setConsumer($queue, array($this, 'onMessage'));
        $this->mq->waitForMessages();
        $this->mq->close();
    }
    
    public function onMessage(AMQPMessage $msg) {
        $data = $msg->body;
        $isRpc = $this->checkIfRPC($data);
        $msg->body = $data;
        $this->trans->transform($msg);
        $event = $this->trans->getEvent();
        if( $isRpc ) {
            $event->setRPC($msg->get('reply_to'), $msg->get('correlation_id'));
        }    
        $eventDispatcher = $this->getContainer()->get('event_dispatcher');
        $this->em->getConnection()->query("SET autocommit=0;"); 
        $eventDispatcher->dispatch($this->trans->getEventName(), $event);
        $this->em->getConnection()->query("COMMIT;");
        $this->mq->messageAck($msg);
    }

    /**
     * Checks if message data has the 'rpc:' prefix. if so, return true and removes the prefix from data
     * 
     * @param string $data
     * @return boolean
     */
    private function checkIfRPC(&$data) {
        $out = false;
        if(strpos($data, RabbitMqConstants::MSG_PREFIX_RPC)!==false) { // it is rpc
            $data = substr($data, strlen(RabbitMqConstants::MSG_PREFIX_RPC));   // remove rpc: prefix
            $out = true;
        }
        return $out;
    }

}
