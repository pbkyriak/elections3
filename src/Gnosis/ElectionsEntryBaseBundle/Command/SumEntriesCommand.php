<?php

namespace Gnosis\ElectionsEntryBaseBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntry;
use Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntryRow;
use Gnosis\ElectionsEntryBaseBundle\Entity\StauroiEntry;
use Gnosis\ElectionsEntryBaseBundle\Entity\StauroiEntryRow;

/**
 * Description of SumEntriesCommand
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class SumEntriesCommand extends ContainerAwareCommand {

    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    
    protected function configure() {
        $this
                ->setName('elections:sum:entries')
                ->setDescription("Update region data from entries")
                ->addArgument('eklogesSlug', InputArgument::REQUIRED, 'ekloges to export?')
                ->addArgument('nomosSlug', InputArgument::REQUIRED, 'nomos to export?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $t1 = time();
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $eklogesSlug = $input->getArgument('eklogesSlug');
        $nomosSlug = $input->getArgument('nomosSlug');
        
        $output->writeln("<info>Ekloges slug=</info>".$eklogesSlug);
        $output->writeln("<info>Nomos slug=</info>".$nomosSlug);
        
        $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug'=>$eklogesSlug));
        if( !$ekloges ) {
            $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlug));
            return;
        }
        
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('election'=>$ekloges, 'slug'=>$nomosSlug));
        if( !$nomos ) {
            $output->writeln(sprintf("<error>No nomos found with slug %s</error>", $nomosSlug));
            return;
        }
        $this->processPsifoi($ekloges, $nomos);
        $this->processStauroi($ekloges, $nomos);
        
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
    }
    
    protected function processPsifoi($ekloges, $nomos) {
        $entries = $this->em->createQueryBuilder()
                ->select('e')
                ->addSelect('er')
                ->addSelect('r')
                ->from('GnosisElectionsEntryBaseBundle:PsifoiEntry', 'e')
                ->leftJoin('e.psifoiEntryRows', 'er')
                ->leftJoin('e.region', 'r')
                ->where('e.election=:ekl')
                ->andWhere('r.id_path like :idp')
                ->getQuery()
                ->setParameter('ekl', $ekloges)
                ->setParameter('idp', sprintf("%s,%%",$nomos->getIdPath()))
                ->getResult();
        if( $entries) {
            foreach($entries as $entry) {
                $this->updateEt($entry);
                $this->output->write("\n -");
                foreach($entry->getPsifoiEntryRows() as $entryRow) {
                    $this->updatePsifoi($entry, $entryRow);
                    $this->output->write('|');
                }
            }
        }
    }
    
    private function updateEt(PsifoiEntry $entry) {
        $params = array(
            'egg' => (int) $entry->getEggegramenoi(),
            'psi' => (int) $entry->getPsifisan(),
            'ley' => (int) $entry->getLeyka(),
            'aky' => (int) $entry->getAkyra(),
            'egy' => (int) $entry->getEgkyra(),
            'etcd' => 1,
            'eggc' => $entry->getEggegramenoi(),
        );
        $sql = "UPDATE region SET 
                eggegramenoi=eggegramenoi+(:egg), 
                psifisan=psifisan+(:psi), 
                leyka=leyka+(:ley), 
                akyra=akyra+(:aky), 
                egkyra=egkyra+(:egy), 
                et_counted=et_counted+(:etcd), 
                egg_counted=egg_counted+(:eggc)
                WHERE id in (%s)
                ";
        // den einai kalo etsi...
        $sql = sprintf($sql, $entry->getRegion()->getIdPath());
        $this->em->getConnection()->executeQuery($sql, $params);
    }
    
    private function updatePsifoi(PsifoiEntry $entry,PsifoiEntryRow $entryRow) {
        $sql = "UPDATE region_psifoi SET amount=amount+(%s) WHERE region_id in (%s) and syndiasmos_id=%s";
        // den einai kalo etsi...
        $sql = sprintf($sql, $entryRow->getAmount(), $entry->getRegion()->getIdPath(), $entryRow->getSyndiasmos()->getId());
        $this->em->getConnection()->query($sql);
    }
    
    
    protected function processStauroi($ekloges, $nomos) {
        $entries = $this->em->createQueryBuilder()
                ->select('e')
                ->addSelect('er')
                ->addSelect('r')
                ->from('GnosisElectionsEntryBaseBundle:StauroiEntry', 'e')
                ->leftJoin('e.stauroiEntryRows', 'er')
                ->leftJoin('e.region', 'r')
                ->where('e.election=:ekl')
                ->andWhere('r.id_path like :idp')
                ->getQuery()
                ->setParameter('ekl', $ekloges)
                ->setParameter('idp', sprintf("%s,%%",$nomos->getIdPath()))
                ->getResult();
        if( $entries) {
            foreach($entries as $entry) {
                $this->output->write("\n -");
                foreach($entry->getStauroiEntryRows() as $entryRow) {
                    $this->updateStauroi($entry, $entryRow);
                    $this->output->write('|');
                }
            }
        }
    }

    private function updateStauroi(StauroiEntry $entry,StauroiEntryRow $entryRow) {
        $sql = "UPDATE region_stauroi SET amount=amount+(%s) WHERE region_id in (%s) and ypopsifios_id=%s";
        // den einai kalo etsi...
        $sql = sprintf($sql, $entryRow->getAmount(), $entry->getRegion()->getIdPath(), $entryRow->getYpopsifios()->getId());
        $this->em->getConnection()->query($sql);
    }
 
}
