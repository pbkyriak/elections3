<?php

namespace Gnosis\ElectionsEntryBaseBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Description of DropElectionEntriesCommand
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class DropElectionEntriesCommand extends ContainerAwareCommand {

    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    
    protected function configure() {
        $this
                ->setName('elections:drop:entries')
                ->setDescription("Drops election entries")
                ->addArgument('eklogesSlug', InputArgument::REQUIRED, 'ekloges to clean?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $t1 = time();
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $eklogesSlug = $input->getArgument('eklogesSlug');
        
        $output->writeln("<info>Ekloges slug=</info>".$eklogesSlug);
        
        $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug'=>$eklogesSlug));
        if( !$ekloges ) {
            $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlug));
            return;
        }
        /*
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Continue with this action? (y/n)', false);
        if (!$helper->ask($input, $output, $question)) {
            $output->writeln('Operation rejected.');
            return;
        }
        */
        $this->em->getConnection()->delete('psifoi_entry', array('ekloges_id'=>$ekloges->getId()));
        $this->em->getConnection()->delete('stauroi_entry', array('ekloges_id'=>$ekloges->getId()));
        $this->em->getConnection()->delete('ypes_to_export', array('ekloges_id'=>$ekloges->getId()));
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
    }
}
