<?php
namespace Gnosis\ElectionsEntryBaseBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * Constraint for PsifoiEntry
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class AcceptablePsifoiEntry extends Constraint
{
    
    public $messageTotH = 'elections.validation.total_egg';
    public $messageEP = 'elections.validation.egg_psif';
    public $messageTotPE = 'elections.validation.psifoi_egkyra';
    
    public function validatedBy() {
        return 'psifoi_validator';
    }
    
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}

