<?php

namespace Gnosis\ElectionsEntryBaseBundle\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManager;

/**
 * Description of AcceptableStauroiEntryValidator
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class AcceptableStauroiEntryValidator  extends ConstraintValidator
{

    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Validation:
     *  - if stauroi per syndiasmos are <= psifoi per syndiasmos * nomos.stauroi_per_ballot
     *      psifoi per syndiasmos come from et psifoi entry
     * It is a heavy validation because it has to fetch psifoiEntry for the ET 
     * and fetch RegionNomos entity of the ET
     * 
     * @param StauroiEntry $entry
     * @param Constraint $constraint
     */
    public function validate($entry, Constraint $constraint) {
        $totals = $this->getStauroiPerSyndiasmos($entry);
        $totalsP = $this->getMaxStauroiFromNomos(
                $entry, 
                $this->getPsifoiPerSyndiasmos($entry)
                );
        if(!$this->isAmountValid($totals, $totalsP) ) {
            $this->context->addViolation($constraint->messageTotPS);
        }
    }
    
    /**
     * collect total stauroi per syndiasmos from stauroiEntry
     */
    private function getStauroiPerSyndiasmos($entry) {
        $totals = array();
        foreach($entry->getStauroiEntryRows() as $row) {
            if( !isset($totals[$row->getSyndiasmos()->getId()]) ) {
                $totals[$row->getSyndiasmos()->getId()] = 0;
            }
            $totals[$row->getSyndiasmos()->getId()] += $row->getAmount();
        }
        return $totals;
    }
    
    /**
     * fetch psifoiEntry and collect psifoi per syndiasmos
     */
    private function getPsifoiPerSyndiasmos($entry) {
        $psifoi = $this->em
                ->getRepository('GnosisElectionsEntryBaseBundle:PsifoiEntry')
                ->getEntryForEt($entry->getElection(),$entry->getRegion());
        $totalsP = array();
        if($psifoi) {
            foreach($psifoi->getPsifoiEntryRows() as $row) {
                $totalsP[$row->getSyndiasmos()->getId()] = $row->getAmount();
            }
        }
        return $totalsP;
    }
    
    /**
     * get RegionNomos for ET  and multiply psifoi with stauroi per person
     * defined in nomos.
     * 
     */
    private function getMaxStauroiFromNomos($entry, $totalsP) {
        $nomos = $this->em
                ->getRepository('GnosisElectionsBaseBundle:RegionNomos')
                ->getNomosFromEt($entry->getRegion());
        if($nomos) {
            // multiply number of psifoi * stauroi per person
            array_walk(
                    $totalsP, 
                    function(&$itm, $key, $spp){ $itm=$itm*$spp; }, 
                    $nomos->getStauroiPerBallot() 
                    );
        }
        return $totalsP;
    }
    
    /**
     * check if any syndiasmos has more stauroi than psifoi*stauroiPerBallot
     */
    private function isAmountValid($totals, $totalsP) {
        $valid = true;
        foreach($totals as $k=>$st) {
            if(array_key_exists($k, $totalsP)) {
                if( $totals[$k]>$totalsP[$k] ) {
                    $valid= false;
                    break;
                }
            }
        }
        return $valid;
    }
}
