<?php

namespace Gnosis\ElectionsEntryBaseBundle\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManager;

/**
 * Description of AcceptableStauroiRowValidator
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Jun 16, 2017
 */
class AcceptableStauroiRowValidator extends ConstraintValidator {

    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Validation:
     *  - if stauroi per syndiasmos are <= psifoi per syndiasmos * nomos.stauroi_per_ballot
     *      psifoi per syndiasmos come from et psifoi entry
     * It is a heavy validation because it has to fetch psifoiEntry for the ET
     * and fetch RegionNomos entity of the ET
     *
     * @param StauroiEntry $entry
     * @param Constraint $constraint
     */
    public function validate($row, Constraint $constraint) {
        $entry = $row->getStauroiEntry();
        if($entry) {
            $psifoi = $this->getPsifoiForSyndiasmos($entry, $row);
            if($row->getAmount()>$psifoi) {
                $this->context->addViolation($constraint->messageTotPS, ['%synd%'=>$row->getSyndiasmos()->getTitle(), '%ypo%'=>$row->getYpopsifios()->getTitle()]);
            }
        }
        else {
            $this->context->addViolation('no entry related');
        }
        
    }


    /**
     * fetch psifoiEntry and collect psifoi per syndiasmos
     */
    private function getPsifoiForSyndiasmos($entry, $trow) {
        $psifoi = $this->em
                ->getRepository('GnosisElectionsEntryBaseBundle:PsifoiEntry')
                ->getEntryForEt($entry->getElection(),$entry->getRegion());
        $amount = 0;
        if($psifoi) {
            foreach($psifoi->getPsifoiEntryRows() as $row) {
                if( $row->getSyndiasmos()->getId() == $trow->getSyndiasmos()->getId()) {
                    $amount = $row->getAmount();
                    break;
                }
            }
        }
        return $amount;
    }

}
