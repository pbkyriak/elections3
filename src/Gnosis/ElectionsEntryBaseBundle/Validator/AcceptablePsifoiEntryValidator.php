<?php
namespace Gnosis\ElectionsEntryBaseBundle\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntry;
/**
 * Description of AcceptablePsifoiEntryValidator
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class AcceptablePsifoiEntryValidator extends ConstraintValidator
{

    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * 
     * @param PsifoiEntry $entry
     * @param Constraint $constraint
     */
    public function validate($entry, Constraint $constraint) {
        
        if( $entry->getPsifisan()>$entry->getEggegramenoi()) {
            $this->context->addViolationAt('eggegramenoi', $constraint->messageEP, array(), null);            
        }
        if( $entry->getPsifisan()!=$entry->getEgkyra()+$entry->getAkyra()+$entry->getLeyka() ) {
            $this->context->addViolationAt('psifisan', $constraint->messageTotH, array(), null);            
        }
        $total = 0;
        foreach($entry->getPsifoiEntryRows() as $row) {
            $total += $row->getAmount();
        }
        if( $total!=$entry->getEgkyra() ) {
            $this->context->addViolationAt('egkyra', $constraint->messageTotPE, array(), null);            
        }
    }
}
