<?php

namespace Gnosis\ElectionsEntryBaseBundle\Validator;

use Symfony\Component\Validator\Constraint;
/**
 * Description of AcceptableStauroiRow
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Jun 16, 2017
 */
class AcceptableStauroiRow extends Constraint
{

    public $messageTotPS = 'elections.validation.row_psifoi_stauroi_not_match';

    public function validatedBy() {
        return 'stauroi_row_validator';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
