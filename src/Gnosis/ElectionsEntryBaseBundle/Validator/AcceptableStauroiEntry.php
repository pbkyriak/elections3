<?php

namespace Gnosis\ElectionsEntryBaseBundle\Validator;

use Symfony\Component\Validator\Constraint;
/**
 * Description of AcceptableStauroiEntry
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class AcceptableStauroiEntry  extends Constraint
{
    
    public $messageTotPS = 'elections.validation.total_psifoi_stauroi_not_match';
    
    public function validatedBy() {
        return 'stauroi_validator';
    }
    
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
