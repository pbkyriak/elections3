<?php

namespace Gnosis\ElectionsEntryBaseBundle\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Gnosis\ElectionsEntryBaseBundle\Entity\UserLog;
/**
 * Description of LogUserInEntityListener
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class LogUserInEntityListener
{

    /**
     * @var callable
     */
    private $userCallable;

    /**
     * 
     * @param \Slx\GsprodBundle\Listener\callable $userCallable
     */
    public function __construct(callable $userCallable)
    {
        $this->userCallable = $userCallable;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->logUserActivity($args, 'INSERT');
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->logUserActivity($args, 'UPDATE');
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $this->logUserActivity($args, 'DELETE');
    }

    private function logUserActivity(LifecycleEventArgs $args, $action)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
        if ($entity instanceof Gnosis\ElectionsEntryBaseBundle\Entity\UserLog) {
            return;
        }

        if (
            method_exists($entity, 'isLogUserActionEnabled') && method_exists($entity,
                'getId') && method_exists($entity, '__toString')
        ) {
            $callable = $this->userCallable;
            $userId = $callable();
            if ($userId) {
                $user = $em->getReference('Slx\UserBundle\Entity\User', $userId);
                $class = get_class($entity);
                $objectId = $entity->getId();
                $title = (string) $entity;
                $logObj = new UserLog();
                $logObj->setUser($user)
                    ->setClass($class)
                    ->setObjectId($objectId)
                    ->setTitle($title)
                    ->setAction($action)
                    ->setCreatedAt(new \DateTime())
                    ;
                $em->persist($logObj);
                $em->flush();
            }
        }
    }
}

