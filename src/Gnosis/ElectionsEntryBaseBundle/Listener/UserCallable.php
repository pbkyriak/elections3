<?php

namespace Gnosis\ElectionsEntryBaseBundle\Listener;

use Symfony\Component\DependencyInjection\Container;

/**
 * Description of UserCallable
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class UserCallable
{

    /**
     * @var Container
     */
    private $container;

    /**
     * @param callable
     * @param string $userEntity
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function __invoke()
    {
        $userId = 0;
        $token = $this->container->get('security.context')->getToken();
        if (null !== $token) {
            if ($token->isAuthenticated()) {
                $userId = $token->getUser()->getId();
            }
        }
        return $userId;
    }

}

