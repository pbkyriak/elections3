<?php

namespace Gnosis\ElectionsEntryBaseBundle\Listener;

use Doctrine\Common\Annotations\Reader;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Gnosis\ElectionsEntryBaseBundle\Annotation\KeepEntryLock;
use Gnosis\ElectionsEntryBaseBundle\Manager\EntryLockManager;
use Symfony\Component\HttpFoundation\Session\Session;
/**
 * Description of EntryLockListener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class EntryLockListener {

    /**
     * @var Reader An Reader instance
     */
    protected $reader;

    /**
     * @var Gnosis\ElectionsEntryBaseBundle\Manager\EntryLockManager
     */
    protected $lockMgr;
    /**
     *
     * @var Symfony\Component\HttpFoundation\Session\Session
     */
    private $session;
    private $logger;
    /**
     * Constructor.
     *
     * @param Reader $reader An Reader instance
     * @param MenuBuilderInterface $menuBuilder A menu instance
     */
    public function __construct(Reader $reader, EntryLockManager $lockMgr, $session, $logger) {
        $this->reader = $reader;
        $this->lockMgr = $lockMgr;
        $this->session = $session;
        $this->logger = $logger;
    }

    /**
     * @param FilterControllerEvent $event A FilterControllerEvent instance
     */
    public function onKernelController(FilterControllerEvent $event) {
        if (!is_array($controller = $event->getController())) {
            return;
        }

        $controllerClassName = get_class($controller[0]);
        if(strpos($controllerClassName, 'Symfony')!==false) {
            return;
        }
        // Annotations from class
        $class = new \ReflectionClass($controller[0]);

        // Manage JMSSecurityExtraBundle proxy class
        if (false !== $className = $this->getRealClass($class->getName())) {
            $class = new \ReflectionClass($className);
        }

        if ($class->isAbstract()) {
            throw new \InvalidArgumentException(sprintf('Annotations from class "%s" cannot be read as it is abstract.', $class));
        }
        if( $event->isMasterRequest() ) {
            // Annotations from method
            $method = $class->getMethod($controller[1]);
            $keepLockType = $this->getKeepLockTypeFromAnnotations($this->reader->getMethodAnnotations($method));
            $this->doUnlocks($keepLockType);
        }
    }

    private function getKeepLockTypeFromAnnotations(array $annotations) {
        $keepLockType = false;
        // requirements (@KeepEntryLock)
        foreach ($annotations as $annotation) {
            if ($annotation instanceof KeepEntryLock) {
                $keepLockType = $annotation->getType();
            }
        }
        return $keepLockType;
    }

    private function getRealClass($className) {
        if (false === $pos = strrpos($className, '\\__CG__\\')) {
            return false;
        }

        return substr($className, $pos + 8);
    }

    private function doUnlocks($keepLockType) {
        if( !$this->session->has('et_lock') ) {
            return;
        }
        $lock = $this->session->get('et_lock');
        if( $lock['type']=='psifoi' && $lock['type']!=$keepLockType ) {
            $this->lockMgr->unlockForPsifoiByEtId($lock['et'], $lock['user_id']);
            $this->session->remove('et_lock');
        }
        if( $lock['type']=='stauroi' && $lock['type']!=$keepLockType ) {
            $this->lockMgr->unlockForStauroiByEtId($lock['et'], $lock['user_id']);
            $this->session->remove('et_lock');
        }        
        if( $lock['type']=='praktika' && $lock['type']!=$keepLockType ) {
            $this->lockMgr->unlockForPraktikaByEtId($lock['et'], $lock['user_id']);
            $this->session->remove('et_lock');
        }        
        if( $lock['type']=='phone' && $lock['type']!=$keepLockType ) {
            $this->lockMgr->unlockForPhoneByEtId($lock['et'], $lock['user_id']);
            $this->session->remove('et_lock');
        }
    }
}
