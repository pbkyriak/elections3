<?php

namespace Gnosis\ElectionsEntryBaseBundle\Listener;

use Gnosis\ElectionsEntryBaseBundle\Listener\AbstractMqEventListener;
use Gnosis\ElectionsEntryBaseBundle\Entity\Sequence;
/**
 * Description of MqNumberingListener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqNumberingListener extends AbstractMqEventListener {
    
    protected function processEvent($event) {
        $data = $event->getData();
        $key = $data['key'];
        printf("numbering event key=%s\n", $key);
        $sq = $this->em->getRepository("GnosisElectionsEntryBaseBundle:Sequence")->findOneBy(array('sqkey'=>$key));
        if(!$sq) {
            $sq = new Sequence();
            $sq->setSqkey($key)->setSq(1);
        }
        else {
            $sq->setSq($sq->getSq()+1);
        }
        $newSq = $sq->getSq();
        $this->em->persist($sq);
        $this->em->flush();        
        return $newSq;
    }

}
