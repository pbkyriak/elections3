<?php

namespace Gnosis\ElectionsEntryBaseBundle\Listener;

use Gnosis\ElectionsEntryBaseBundle\Listener\AbstractMqEventListener;

/**
 * Description of MqEtChangeListener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqEtChangeListener extends AbstractMqEventListener {

    protected function processEvent($event) {
        $data = $event->getData();
        printf("event for et=%s\n",$data['et']);
        $info = $this->getRegionsFromEtId($data['et']);
        if( $info ) {
            $this->persistEt($info[1], $info[0], $data);
        }

    }

    private function getRegionsFromEtId($etId) {
        $out = null;
        $et = $this->em->getRepository("GnosisElectionsBaseBundle:RegionEklTmima")->find($etId);
        if ($et) {
            $out = array($et->getEggegramenoi(), $et->getIdPath());
        }
        return $out;
    }

    protected function persistEt($etPIds, $etEggegr, $data) {
        $sign = 0;
        $eggCnt = $data['eggegramenoi'];
        if($data['a']=='i') {
            $sign = 1;
            $eggCnt = $etEggegr+($data['eggegramenoi']);
        }
        elseif($data['a']=='d') {
            $sign = -1;
            $eggCnt = -($data['eggegramenoi']);
        }
        $params = array(
            'egg' => (int) $data['eggegramenoi'],
            'psi' => (int) $data['psifisan'],
            'ley' => (int) $data['leyka'],
            'aky' => (int) $data['akyra'],
            'egy' => (int) $data['egkyra'],
            'etcd' => $sign,
            'eggc' => $eggCnt
        );
        $sql = "UPDATE region SET 
                eggegramenoi=eggegramenoi+(:egg), 
                psifisan=psifisan+(:psi), 
                leyka=leyka+(:ley), 
                akyra=akyra+(:aky), 
                egkyra=egkyra+(:egy), 
                et_counted=et_counted+(:etcd), 
                egg_counted=egg_counted+(:eggc)
                WHERE id in (%s)
                ";
        // den einai kalo etsi...
        $sql = sprintf($sql, $etPIds);
        $this->em->getConnection()->executeQuery($sql, $params);
    }

}
