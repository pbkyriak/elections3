<?php
namespace Gnosis\ElectionsEntryBaseBundle\Listener;

use Gnosis\ElectionsEntryBaseBundle\Event\PsifoiChangeEvent;
use Slx\RabbitMqBundle\Rabbitmq\Connection as MqConnection;
use Doctrine\ORM\EntityManager;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;
/**
 * Description of PsifoiInsertListener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class PsifoiChangeListener {
    /** @var \Doctrine\ORM\EntityManager */
    protected $em;
    /** @var \Slx\RabbitMqBundle\Rabbitmq\Connection */
    protected $mq;
    
    public function __construct(MqConnection $mq, EntityManager $em) {
        $this->em = $em;
        $this->mq = $mq;
    }

    public function onInsert(PsifoiChangeEvent $event) {
        $messages = $this->createPsifoiMessages('i',$event->getEtId(),$event->getChangePData());
        $this->pushToMq(RabbitMqConstants::PSIFOI_TRACK_QUEUE,$messages);
        $etMessage= $this->createEtMessage('i', $event->getEtId(), $event->getChangeRData());
        $this->pushToMq(RabbitMqConstants::ET_LANE_QUEUE,$etMessage);
    }
    
    public function onUpdate(PsifoiChangeEvent $event) {
        $messages = $this->createPsifoiMessages('u',$event->getEtId(),$event->getChangePData());
        $this->pushToMq(RabbitMqConstants::PSIFOI_TRACK_QUEUE,$messages);
        $etMessage= $this->createEtMessage('u', $event->getEtId(), $event->getChangeRData());
        $this->pushToMq(RabbitMqConstants::ET_LANE_QUEUE,$etMessage);
    }

    public function onDelete(PsifoiChangeEvent $event) {
        $messages = $this->createPsifoiMessages('d',$event->getEtId(),$event->getChangePData());
        $this->pushToMq(RabbitMqConstants::PSIFOI_TRACK_QUEUE,$messages);
        $etMessage= $this->createEtMessage('d', $event->getEtId(), $event->getChangeRData());
        $this->pushToMq(RabbitMqConstants::ET_LANE_QUEUE,$etMessage);
    }

    private function createPsifoiMessages($action, $etId, $psifoiChangeData) {
        $out = array();
        foreach($psifoiChangeData as $sId => $amount) {
            $msg = array(
                'a'  => $action,
                'et' => $etId,
                'sy' => $sId,
                'am' => $amount,
            );
            $out[] = json_encode($msg);
        }
        return $out;
    }
    
    private function createEtMessage($action, $etId, $etChangeData) {
        $msg = array(
            'a' => $action,
            'et' => $etId,
        );
        return json_encode(array_merge($msg, $etChangeData));
    }
    
    private function pushToMq($queue, $messages) {
        if( !is_array($messages) ) {
            $this->mq->publish($messages, $queue);
        }
        else {
            foreach($messages as $msg) {
                $this->mq->publish($msg, $queue);
            }
        }
    }
}
