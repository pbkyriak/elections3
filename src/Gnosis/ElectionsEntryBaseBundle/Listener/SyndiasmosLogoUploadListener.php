<?php

namespace Gnosis\ElectionsEntryBaseBundle\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Gnosis\ElectionsBaseBundle\Entity\Syndiasmos;
use Gnosis\ElectionsEntryBaseBundle\Uploaders\LogoUploader;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Description of SyndiasmosLogoUploadListener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Dec 10, 2016
 */
class SyndiasmosLogoUploadListener {

    /** @var Gnosis\ElectionsEntryBaseBundle\Uploaders\LogoUploader */
    protected $uploader;

    public function __construct(LogoUploader $uploader) {
        $this->uploader = $uploader;
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args) {
        $entity = $args->getEntity();

        if (!$entity instanceof Syndiasmos) {
            return;
        }

        $this->uploadFile($entity);
        if( $entity->getLogo() instanceof File) {
            $file = $entity->getLogo();
            $entity->setLogo($file->getBasename());
        }
    }

    private function uploadFile($entity) {
        // upload only works for Syndiasmos entities
        if (!$entity instanceof Syndiasmos) {
            return;
        }

        $file = $entity->getLogo();
        $this->uploader->setElectionSlug($entity->getElection()->getId());
        // only upload new files
        if (!$file instanceof UploadedFile) {
            return;
        }

        $fileName = $this->uploader->upload($file);
        $entity->setLogo($fileName);
    }

    public function postLoad(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        if (!$entity instanceof Syndiasmos) {
            return;
        }
        
        $this->uploader->setElectionSlug($entity->getElection()->getId());
        $fileName = $entity->getLogo();
        $entity->setLogo(null);
        if( $fileName ) {
            if(file_exists($this->uploader->getUploadRootDir() . '/' . $fileName)) {
                $entity->setLogo(new File($this->uploader->getUploadRootDir() . '/' . $fileName));
                $entity->setLogoRelPath($this->uploader->getUploadRelDir().'/'.$fileName);
            }
        }
    }
    
    public function preRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        if( !$entity instanceof  Syndiasmos) {
            return;
        }
        
        $this->uploader->setElectionSlug($entity->getElection()->getId());
        $fileName = $entity->getLogo();
        if( $fileName ) {
            if(file_exists($fileName)) {
                @unlink($fileName);
            }
        }
        
    }

}
