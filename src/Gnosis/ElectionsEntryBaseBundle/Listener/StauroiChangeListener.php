<?php

namespace Gnosis\ElectionsEntryBaseBundle\Listener;

use Gnosis\ElectionsEntryBaseBundle\Event\StauroiChangeEvent;
use Slx\RabbitMqBundle\Rabbitmq\Connection as MqConnection;
use Doctrine\ORM\EntityManager;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;

/**
 * Description of StauroiChangeListener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class StauroiChangeListener {
    /** @var \Doctrine\ORM\EntityManager */
    protected $em;
    /** @var \Slx\RabbitMqBundle\Rabbitmq\Connection */
    protected $mq;
    
    public function __construct(MqConnection $mq, EntityManager $em) {
        $this->em = $em;
        $this->mq = $mq;
    }

    public function onInsert(StauroiChangeEvent $event) {
        $messages = $this->createRowMessages('i',$event->getEtId(),$event->getChangeData());
        $this->pushToMq(RabbitMqConstants::STAUROI_LANE_QUEUE,$messages);
    }
    
    public function onUpdate(StauroiChangeEvent $event) {
        $messages = $this->createRowMessages('u',$event->getEtId(),$event->getChangeData());
        $this->pushToMq(RabbitMqConstants::STAUROI_LANE_QUEUE,$messages);
    }

    public function onDelete(StauroiChangeEvent $event) {
        $messages = $this->createRowMessages('d',$event->getEtId(),$event->getChangeData());
        $this->pushToMq(RabbitMqConstants::STAUROI_LANE_QUEUE,$messages);
    }

    private function createRowMessages($action, $etId, $stauroiChangeData) {
        $out = array();
        foreach($stauroiChangeData as $sId => $amount) {
            $msg = array(
                'a'  => $action,
                'et' => $etId,
                'yp' => $sId,
                'am' => $amount,
            );
            $out[] = json_encode($msg);
        }
        return $out;
    }
    
    private function pushToMq($queue, $messages) {
        if( !is_array($messages) ) {
            $this->mq->publish($messages, $queue);
        }
        else {
            foreach($messages as $msg) {
                $this->mq->publish($msg, $queue);
            }
        }
    }
}
