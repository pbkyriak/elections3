<?php

namespace Gnosis\ElectionsEntryBaseBundle\Listener;

use Gnosis\ElectionsEntryBaseBundle\Listener\AbstractMqEventListener;
/**
 * Description of MqStauroiChangeListener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqStauroiChangeListener extends AbstractMqEventListener {
    
    protected function processEvent($event) {
        $data = $event->getData();
        printf("event for et=%s\n",$data['et']);
        $rIds = $this->getRegionsFromEtId($data['et']);
        if( $rIds ) {
            $this->persistEt($rIds, $data['yp'], $data['am']);
        }
    }
    
    private function getRegionsFromEtId($etId) {
        $out = null;
        $et = $this->em->getRepository("GnosisElectionsBaseBundle:RegionEklTmima")->find($etId);
        if( $et ) {
            $out = $et->getIdPath();
        }
        return $out;
    }
    
    private function persistEt($etPids, $ypoId, $amount) {
        $sql = "UPDATE region_stauroi SET amount=amount+(%s) WHERE region_id in (%s) and ypopsifios_id=%s";
        // den einai kalo etsi...
        $sql = sprintf($sql, $amount, $etPids, $ypoId);
        $this->em->getConnection()->query($sql);
    }
}
