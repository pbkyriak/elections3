<?php

namespace Gnosis\ElectionsEntryBaseBundle\Listener;

use Gnosis\ElectionsEntryBaseBundle\Listener\AbstractMqEventListener;

/**
 * Description of MqPsifoiChangeListener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqPsifoiChangeListener extends AbstractMqEventListener {
    
    protected function processEvent($event) {
        $data = $event->getData();
        printf("event for et=%s\n",$data['et']);
        $rIds = $this->getRegionsFromEtId($data['et']);
        if( $rIds ) {
            $this->persistEt($rIds, $data['sy'], $data['am']);
        }
    }
    
    private function getRegionsFromEtId($etId) {
        $out = null;
        $et = $this->em->getRepository("GnosisElectionsBaseBundle:RegionEklTmima")->find($etId);
        if( $et ) {
            $out = $et->getIdPath();
        }
        return $out;
    }
    
    private function persistEt($etPids, $syndId, $amount) {
        $sql = "UPDATE region_psifoi SET amount=amount+(%s) WHERE region_id in (%s) and syndiasmos_id=%s";
        // den einai kalo etsi...
        $sql = sprintf($sql, $amount, $etPids, $syndId);
        $this->em->getConnection()->query($sql);
    }

}
