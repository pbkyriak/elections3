<?php

namespace Gnosis\ElectionsEntryBaseBundle\Manager;

use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;
use Gnosis\ElectionsEntryBaseBundle\Entity\StauroiEntry;
use Gnosis\ElectionsEntryBaseBundle\Entity\StauroiEntryRow;
use Gnosis\ElectionsEntryBaseBundle\Event\StauroiChangeEvent;
use Gnosis\ElectionsEntryBaseBundle\Event\ElectionsEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Description of StauroiEntryManager
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class StauroiEntryManager {

    private $em;
    private $dispatcher;
    private $currData;
    private $message;

    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher) {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->currData = array();
    }

    /**
     * 
     * @param Ekloges $election
     * @param RegionEklTmima $et
     * @return StauroiEntry
     */
    public function createEntryForEt(Ekloges $election, RegionEklTmima $et) {
        $syndiasmoi = $this->em
                ->getRepository('GnosisElectionsBaseBundle:Syndiasmos')
                ->getSyndYpopsForStauroiEntry($election);
        $entry = new StauroiEntry();
        $entry->setElection($election);
        $entry->setRegion($et);
        $amount = 0;
        /** @var $syndiasmos \Gnosis\ElectionsBaseBundle\Entity\Syndiasmos */
        foreach ($syndiasmoi as $syndiasmos) {
            /** @var $ypopsifios Gnosis\ElectionsBaseBundle\Entity\Ypopsifios  */
            foreach ($syndiasmos->getYpopsifioi() as $ypopsifios) {
                $entryRow = new StauroiEntryRow();
                $entryRow->setStauroiEntry($entry);
                $entryRow->setSyndiasmos($syndiasmos);
                $entryRow->setYpopsifios($ypopsifios);
                $entryRow->setAmount($amount);
                $entry->addStauroiEntryRow($entryRow);
            }
        }
        $this->collectCurrData($entry);
        return $entry;
    }

    /**
     * 
     * @param Ekloges $election
     * @param RegionEklTmima $et
     * @return StauroiEntry
     */
    public function fetchEtEntry(Ekloges $election, RegionEklTmima $et) {
        try {
            $out = $this->em
                    ->getRepository('GnosisElectionsEntryBaseBundle:StauroiEntry')
                    ->getEntryForEt($election,$et);
            if ($out) {
                $this->collectCurrData($out);
            }
        } catch (\Exception $ex) {
            $this->message = $ex->getMessage();
            $out = null;
        }
        return $out;
    }

    /**
     * Persists record to database. If succesful operation fires an event 
     * for background updates.
     * @param StauroiEntry $entry
     * @return boolean
     */
    public function updateEtEntry(StauroiEntry $entry) {
        $out = true;
        try {
            $isNewEntry = ($entry->getId() == 0);
            $entry->setUpdatedAt(new \DateTime());
            $this->collectDiffData($entry);
            $this->em->persist($entry);
            $this->em->flush();
            // success full db operation, dispatch event for background updates
            $event = new StauroiChangeEvent($entry->getRegion()->getId(), $this->currData);
            $this->dispatcher->dispatch($isNewEntry ? ElectionsEvents::STAUROI_INSERT : ElectionsEvents::STAUROI_UPDATE, $event);
        } catch (\Exception $ex) {
            $this->message = $ex->getMessage();
            $out = false;
        }
        return $out;
    }

    /**
     * Removes entry from database. If successful fires an event for background updates.
     * 
     * @param Ekloges $election
     * @param RegionEklTmima $et
     * @return boolean
     */
    public function deleteEtEntry(Ekloges $election, RegionEklTmima $et) {
        $out = true;
        $entry = $this->fetchEtEntry($election, $et);
        if ($entry) {
            try {
                $etId = $entry->getRegion()->getId();
                $this->em->remove($entry);
                $this->em->flush();
                $this->negateCurrData();
                // success full db operation, dispatch event for background updates
                $event = new StauroiChangeEvent($etId, $this->currData);
                $this->dispatcher->dispatch(ElectionsEvents::STAUROI_DELETE, $event);
            } catch (\Exception $ex) {
                $this->message = $ex->getMessage();
                $out = false;
            }
        }
        return $out;
    }

    /**
     * Collect initial values of data
     * 
     * @param StauroiEntry $entry
     */
    private function collectCurrData(StauroiEntry $entry) {
        $this->currData = array();
        foreach ($entry->getStauroiEntryRows() as $row) {
            $this->currData[$row->getYpopsifios()->getId()] = $row->getAmount();
        }
    }

    /**
     * Collect new-old value diffs in order to use for updating regions
     * 
     * @param type $entry
     */
    private function collectDiffData($entry) {
        $keys = array_keys($this->currData);
        foreach ($entry->getStauroiEntryRows() as $row) {
            $sId = $row->getYpopsifios()->getId();
            if (in_array($sId, $keys)) {
                $this->currData[$sId] = $row->getAmount() - $this->currData[$sId];
            }
        }
    }

    /**
     * Negete values, used when deleting entry
     */
    private function negateCurrData() {
        foreach ($this->currData as $k => $v) {
            $this->currData[$k] = -$v;
        }
    }

    public function dumpCurrData() {
        print_R($this->currData);
    }

    public function getMessage() {
        return $this->message;
    }

    public function hasEtStauroiEntry($etId) {
        $out = false;
        $stmt = $this->em->getConnection()->executeQuery("SELECT count(*) FROM stauroi_entry WHERE region_id=:etid", array('etid'=>$etId));
        $cnt = $stmt->fetchColumn(0);
        if( $cnt ) {
            if($cnt>0) {
                $out = true;
            }
        }
        return $out;
    }

}
