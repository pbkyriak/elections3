<?php
namespace Gnosis\ElectionsEntryBaseBundle\Manager;

use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;
use Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntry;
use Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntryRow;
use Gnosis\ElectionsEntryBaseBundle\Event\PsifoiChangeEvent;
use Gnosis\ElectionsEntryBaseBundle\Event\ElectionsEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
/**
 * Description of PsifoiEntryManager
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class PsifoiEntryManager {

    private $em;
    private $dispatcher;
    
    private $currData;
    private $currEtData;

    private $message;
    
    public function __construct(EntityManager $em, EventDispatcherInterface $dispatcher) {
        $this->em = $em;
        $this->dispatcher = $dispatcher;
        $this->currData = array();
        $this->currEtData = array();
    }
    
    
    /**
     * 
     * @param Ekloges $election
     * @param RegionEklTmima $et
     * @return PsifoiEntry
     */
    public function createEntryForEt(Ekloges $election, RegionEklTmima $et) {
       $syndiasmoi = $this->em
               ->getRepository('GnosisElectionsBaseBundle:Syndiasmos')
               ->findBy(array('election'=>$election));
       
       $entry = new PsifoiEntry(); 
       $entry->setElection($election);
       $entry->setRegion($et);
       $entry->setAkyra(0);
       $entry->setEggegramenoi($et->getEggegramenoi());
       $entry->setEgkyra(0);
       $entry->setLeyka(0);
       $entry->setPsifisan(0);
       $amount = 0;
       foreach( $syndiasmoi as $syndiasmos) {
           $entryRow = new PsifoiEntryRow();
           $entryRow->setPsifoiEntry($entry);
           $entryRow->setSyndiasmos($syndiasmos);
           $entryRow->setAmount($amount);
           $entry->addPsifoiEntryRow($entryRow);
       }
       $this->collectCurrData($entry);
       return $entry;
    }

    /**
     * 
     * @param Ekloges $election
     * @param RegionEklTmima $et
     * @return PsifoiEntry
     */
    public function fetchEtEntry(Ekloges $election, RegionEklTmima $et) {
        $out = null;
        try {
            $out = $this->em
                    ->getRepository('GnosisElectionsEntryBaseBundle:PsifoiEntry')
                    ->getEntryForEt($election,$et);
            if($out) {
                $this->collectCurrData($out);
            }
        }
        catch(\Exception $ex) {
            $this->message = $ex->getMessage();
            $out = null;
        }
        return $out;
    }
    
    /**
     * Persists record to database. If succesful operation fires an event 
     * for background updates.
     * @param PsifoiEntry $entry
     * @return boolean
     */
    public function updateEtEntry(PsifoiEntry $entry) {
        $out = true;
        try {
            $entry->setUpdatedAt(new \DateTime());
            $isNewEntry = ($entry->getId()==0);            
            $this->collectDiffData($entry);
            $this->em->persist($entry);
            $this->em->flush();
            // success full db operation, dispatch event for background updates
            $event = new PsifoiChangeEvent($entry->getRegion()->getId(), $this->currEtData, $this->currData);
            $this->dispatcher->dispatch($isNewEntry ? ElectionsEvents::PSIFOI_INSERT : ElectionsEvents::PSIFOI_UPDATE, $event);
        }
        catch(\Exception $ex) {
            $this->message = $ex->getMessage();
            $out = false;
        }
        return $out;
    }
    
    /**
     * Removes entry from database. If successful fires an event for background updates.
     * 
     * @param Ekloges $election
     * @param RegionEklTmima $et
     * @return boolean
     */
    public function deleteEtEntry(Ekloges $election, RegionEklTmima $et) {
        $out = true;
        $entry = $this->fetchEtEntry($election, $et);
        if( $entry ) {
            try {
                $etId = $entry->getRegion()->getId();
                $this->em->remove($entry);
                $this->em->flush();
                $this->negateCurrData();
                // success full db operation, dispatch event for background updates
                $event = new PsifoiChangeEvent($etId, $this->currEtData, $this->currData);
                $this->dispatcher->dispatch(ElectionsEvents::PSIFOI_DELETE, $event);
            }
            catch(\Exception $ex) {
                $this->message = $ex->getMessage();
                $out = false;
            }
        }
        return $out;
    }
    
    /**
     * Collect initial values of data
     * 
     * @param PsifoiEntry $entry
     */
    private function collectCurrData(PsifoiEntry $entry) {
        $this->currData = array();
        foreach($entry->getPsifoiEntryRows() as $row) {
            $this->currData[$row->getSyndiasmos()->getId()] = $row->getAmount();
        }
        $this->currEtData['akyra'] = $entry->getAkyra();
        $this->currEtData['eggegramenoi'] = $entry->getEggegramenoi();
        $this->currEtData['egkyra'] = $entry->getEgkyra();
        $this->currEtData['leyka'] = $entry->getLeyka();
        $this->currEtData['psifisan'] = $entry->getPsifisan();
    }
    
    /**
     * Collect new-old value diffs in order to use for updating regions
     * 
     * @param type $entry
     */
    private function collectDiffData($entry) {
        $keys = array_keys($this->currData);
        foreach($entry->getPsifoiEntryRows() as $row) {
            $sId = $row->getSyndiasmos()->getId();
            if( in_array($sId, $keys) ) {
                $this->currData[$sId] = $row->getAmount()-$this->currData[$sId];
            }
        }        
        $this->currEtData['akyra'] = $entry->getAkyra()-$this->currEtData['akyra'];
        $this->currEtData['eggegramenoi'] = $entry->getEggegramenoi()-$this->currEtData['eggegramenoi'];
        $this->currEtData['egkyra'] = $entry->getEgkyra()-$this->currEtData['egkyra'];
        $this->currEtData['leyka'] = $entry->getLeyka()-$this->currEtData['leyka'];
        $this->currEtData['psifisan'] = $entry->getPsifisan()-$this->currEtData['psifisan'];
    }

    /**
     * Negete values, used when deleting entry
     */
    private function negateCurrData() {
        foreach($this->currData as $k => $v) {
            $this->currData[$k] = -$v;
        }
        $this->currEtData['akyra'] = -$this->currEtData['akyra'];
        $this->currEtData['eggegramenoi'] = -$this->currEtData['eggegramenoi'];
        $this->currEtData['egkyra'] = -$this->currEtData['egkyra'];
        $this->currEtData['leyka'] = -$this->currEtData['leyka'];
        $this->currEtData['psifisan'] = -$this->currEtData['psifisan'];
    }
    
    public function dumpCurrData() {
        print_R($this->currEtData);
        print_R($this->currData);
    }
    
    public function getMessage() {
        return $this->message;
    }    
}
