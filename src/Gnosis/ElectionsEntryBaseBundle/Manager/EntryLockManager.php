<?php

namespace Gnosis\ElectionsEntryBaseBundle\Manager;

use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsEntryBaseBundle\Entity\EntryLock;
use Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima;
use Slx\UserBundle\Entity\User;

/**
 * Manages locks for psifoi and stauroi entries
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class EntryLockManager {

    const LOCK_TYPE_P = 'psifoi';
    const LOCK_TYPE_S = 'stauroi';
    const LOCK_TYPE_Τ = 'praktika';
    const LOCK_TYPE_Y = 'phone';

    private $em;
    private $message;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    /**
     * Try to lock ET for psifoi. 
     * 
     * @param RegionEklTmima $eklTmima
     * @param integer $userId
     * @return boolean
     */
    public function lockForPsifoi(RegionEklTmima $eklTmima, $userId) {
        $lock = $this->createLock($eklTmima, self::LOCK_TYPE_P, $userId);
        return $this->lock($lock);
    }

    /**
     * Try to lock ET for stauroi. 
     * 
     * @param RegionEklTmima $eklTmima
     * @param integer $userId
     * @return boolean
     */
    public function lockForStauroi(RegionEklTmima $eklTmima, $userId) {
        $lock = $this->createLock($eklTmima, self::LOCK_TYPE_S, $userId);
        return $this->lock($lock);
    }

    /**
     * Try to lock ET for praktika. 
     * 
     * @param RegionEklTmima $eklTmima
     * @param integer $userId
     * @return boolean
     */
    public function lockForPraktika(RegionEklTmima $eklTmima, $userId) {
        $lock = $this->createLock($eklTmima, self::LOCK_TYPE_Τ, $userId);
        return $this->lock($lock);
    }

    /**
     * Try to lock ET for ypes phone.
     *
     * @param RegionEklTmima $eklTmima
     * @param integer $userId
     * @return boolean
     */
    public function lockForPhone(RegionEklTmima $eklTmima, $userId) {
        $lock = $this->createLock($eklTmima, self::LOCK_TYPE_Y, $userId);
        return $this->lock($lock);
    }
    
    /**
     * Create EntryLock entity
     * 
     * @param RegionEklTmima $eklTmima
     * @param string $type
     * @param integer $userId
     * @return EntryLock
     */
    private function createLock($eklTmima, $type, $userId) {
        $lock = new EntryLock();
        $lock->setEntryType($type)
                ->setLockTimestamp(time())
                ->setUser($this->getUser($userId))
                ->setRegion($eklTmima);
        return $lock;
    }

    /**
     * Tries to persist EntryLock entity. If duplicate error occurs means
     * that ET is already locked for that type
     * 
     * @param EntryLock $lock
     * @return boolean
     */
    private function lock(EntryLock $lock) {
        $out = false;
        $this->em->persist($lock);
        try {
            $this->em->flush();
            $out = true;
        } catch (\Doctrine\DBAL\DBALException $ex) {
            if ($ex->getPrevious()) {
                if ($ex->getPrevious()->getCode() == '23000') {
                    $this->message = 'duplicate';
                }
            }
            else {
                $this->message = $ex->getMessage();
            }
        } catch (\Exception $ex) {
            $out = false;
            $this->message = $ex->getMessage();
        }
        return $out;
    }

    /**
     * Tries to remove lock for psifoi for ET by user $userId.
     * 
     * @param RegionEklTmima $eklTmima
     * @param integer $userId
     * @return boolean
     */
    public function unlockForPsifoi(RegionEklTmima $eklTmima, $userId) {
        return $this->removeLock($eklTmima, self::LOCK_TYPE_P, $userId);
    }

    public function unlockForPsifoiByEtId($eklTmimaId, $userId) {
        $eklTmima = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->find($eklTmimaId);
        return $this->removeLock($eklTmima, self::LOCK_TYPE_P, $userId);
    }

    /**
     * Tries to remove lock for stauroi for ET by user $userId.
     * 
     * @param RegionEklTmima $eklTmima
     * @param integer $userId
     * @return boolean
     */
    public function unlockForStauroi(RegionEklTmima $eklTmima, $userId) {
        return $this->removeLock($eklTmima, self::LOCK_TYPE_S, $userId);
    }

    public function unlockForStauroiByEtId($eklTmimaId, $userId) {
        $eklTmima = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->find($eklTmimaId);
        return $this->removeLock($eklTmima, self::LOCK_TYPE_S, $userId);
    }

    /**
     * Tries to remove lock for praktika for ET by user $userId.
     * 
     * @param RegionEklTmima $eklTmima
     * @param integer $userId
     * @return boolean
     */
    public function unlockForPraktika(RegionEklTmima $eklTmima, $userId) {
        return $this->removeLock($eklTmima, self::LOCK_TYPE_Τ, $userId);
    }

    public function unlockForPraktikaByEtId($eklTmimaId, $userId) {
        $eklTmima = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->find($eklTmimaId);
        return $this->removeLock($eklTmima, self::LOCK_TYPE_Τ, $userId);
    }

    public function unlockForPhone(RegionEklTmima $eklTmima, $userId) {
        return $this->removeLock($eklTmima, self::LOCK_TYPE_Y, $userId);
    }

    public function unlockForPhoneByEtId($eklTmimaId, $userId) {
        $eklTmima = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->find($eklTmimaId);
        return $this->removeLock($eklTmima, self::LOCK_TYPE_Y, $userId);
    }

    /**
     * If there is a lock from user $userId for ET for lock_type $type
     * removes it.
     * 
     * @param RegionEklTmima $eklTmima
     * @param string $type
     * @param integer $userId
     * @return boolean
     */
    private function removeLock($eklTmima, $type, $userId) {
        $out = false;
        $criteria = array(
            'region' => $eklTmima, 
            'entry_type' => $type,
            'user' => $this->getUser($userId)
            );
        $lock = $this->em
                ->getRepository('GnosisElectionsEntryBaseBundle:EntryLock')
                ->findOneBy($criteria);
        if ($lock) {
            $this->em->remove($lock);
            try {
                $this->em->flush();
                $out = true;
            } catch (\Doctrine\DBAL\DBALException $ex) {
                $this->message = $ex->getMessage();
            }
        }
        return $out;
    }

    /**
     * Fetches from DB user entry
     * 
     * @param integer $userId
     * @return User
     */
    private function getUser($userId) {
        $out = $this->em->getRepository('SlxUserBundle:User')->find($userId);
        return $out;
    }

}
