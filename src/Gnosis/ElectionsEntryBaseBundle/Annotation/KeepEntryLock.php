<?php

namespace Gnosis\ElectionsEntryBaseBundle\Annotation;

/**
 * Description of CurrentMenuItem
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @Annotation
 */
class KeepEntryLock
{
    
    private $type;
    
    public function __construct($data)
    {
        if (isset($data['value'])) {
            $this->type = $data['value'];
            unset($data['value']);
        }        
    }
    
    public function getType() {
        return $this->type;
    }
    
}
