<?php

namespace Gnosis\ElectionsEntryBaseBundle\Entity;

/**
 * Description of LogUserActionsTrait
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
trait LogUserActionsTrait
{
    public function isLogUserActionEnabled() {
        return true;
    }
}

