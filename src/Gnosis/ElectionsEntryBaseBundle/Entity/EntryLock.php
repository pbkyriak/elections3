<?php

namespace Gnosis\ElectionsEntryBaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EntryLock
 */
class EntryLock
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $lock_timestamp;

    /**
     * @var string
     */
    private $entry_type;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima
     */
    private $region;

    /**
     * @var \Slx\UserBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lock_timestamp
     *
     * @param integer $lockTimestamp
     * @return EntryLock
     */
    public function setLockTimestamp($lockTimestamp)
    {
        $this->lock_timestamp = $lockTimestamp;

        return $this;
    }

    /**
     * Get lock_timestamp
     *
     * @return integer 
     */
    public function getLockTimestamp()
    {
        return $this->lock_timestamp;
    }

    /**
     * Set entry_type
     *
     * @param string $entryType
     * @return EntryLock
     */
    public function setEntryType($entryType)
    {
        $this->entry_type = $entryType;

        return $this;
    }

    /**
     * Get entry_type
     *
     * @return string 
     */
    public function getEntryType()
    {
        return $this->entry_type;
    }

    /**
     * Set region
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima $region
     * @return EntryLock
     */
    public function setRegion(\Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set user
     *
     * @param \Slx\UserBundle\Entity\User $user
     * @return EntryLock
     */
    public function setUser(\Slx\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Slx\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
