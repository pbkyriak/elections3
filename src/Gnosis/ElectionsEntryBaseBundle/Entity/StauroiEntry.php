<?php

namespace Gnosis\ElectionsEntryBaseBundle\Entity;

/**
 * StauroiEntry
 */
class StauroiEntry
{
    use LogUserActionsTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $stauroiEntryRows;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima
     */
    private $region;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\Ekloges
     */
    private $election;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->stauroiEntryRows = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add stauroiEntryRow
     *
     * @param \Gnosis\ElectionsEntryBaseBundle\Entity\StauroiEntryRow $stauroiEntryRow
     *
     * @return StauroiEntry
     */
    public function addStauroiEntryRow(\Gnosis\ElectionsEntryBaseBundle\Entity\StauroiEntryRow $stauroiEntryRow)
    {
        $this->stauroiEntryRows[] = $stauroiEntryRow;

        return $this;
    }

    /**
     * Remove stauroiEntryRow
     *
     * @param \Gnosis\ElectionsEntryBaseBundle\Entity\StauroiEntryRow $stauroiEntryRow
     */
    public function removeStauroiEntryRow(\Gnosis\ElectionsEntryBaseBundle\Entity\StauroiEntryRow $stauroiEntryRow)
    {
        $this->stauroiEntryRows->removeElement($stauroiEntryRow);
    }

    /**
     * Get stauroiEntryRows
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStauroiEntryRows()
    {
        return $this->stauroiEntryRows;
    }

    /**
     * Set region
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima $region
     *
     * @return StauroiEntry
     */
    public function setRegion(\Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set election
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Ekloges $election
     *
     * @return StauroiEntry
     */
    public function setElection(\Gnosis\ElectionsBaseBundle\Entity\Ekloges $election = null)
    {
        $this->election = $election;

        return $this;
    }

    /**
     * Get election
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Ekloges
     */
    public function getElection()
    {
        return $this->election;
    }
        
    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return ColorCode
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function __toString() {
        return sprintf("Σταυροί ΕΤ %s", $this->getRegion()->getTitle());
    }

}

