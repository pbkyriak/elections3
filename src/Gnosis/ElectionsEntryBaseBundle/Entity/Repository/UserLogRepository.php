<?php

namespace Gnosis\ElectionsEntryBaseBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
/**
 * Description of UserLogRepository
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Apr 5, 2017
 */
class UserLogRepository extends EntityRepository {
    
    public function getObjectUsers($objectClass, $objectId) {
        $logs = $this->createQueryBuilder('l')
                ->leftJoin('l.user', 'u')
                ->where('l.class=:oc')
                ->andWhere('l.object_id=:oi')
                ->setParameter('oc', $objectClass)
                ->setParameter('oi', $objectId)
                ->getQuery()
                ->getResult();
        $out = [];
        if($logs) {
            foreach($logs as $log) {
                $out[] = [
                    'user_id' => $log->getUser()->getId(),
                    'username' => $log->getUser()->getName(),
                    'action' => $log->getAction(),
                    'timestamp' => $log->getCreatedAt(),
                ];
            }
        }
        return $out;
    }

    public function getDistinctLoggedClasses() {
        $out = $this->createQueryBuilder('l')
                ->select('l.class')
                ->distinct()
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
        return $out;
    }
}
