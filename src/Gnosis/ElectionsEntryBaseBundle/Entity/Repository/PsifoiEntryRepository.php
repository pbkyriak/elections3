<?php
namespace Gnosis\ElectionsEntryBaseBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;

/**
 * Description of PsifoiEntryRepository
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class PsifoiEntryRepository extends EntityRepository {

    public function getEntryForEt(Ekloges $election, RegionEklTmima $et) {
        $qb = $this->createQueryBuilder('e')
                ->select()
                ->leftJoin('e.psifoiEntryRows', 'er')
                ->leftJoin('er.syndiasmos', 'sy')
                ->addSelect('er')
                ->addSelect('sy')
                ->where('e.region=:et and e.election=:el')
                ->setParameter('et', $et)
                ->setParameter('el', $election);
        return $qb->getQuery()
                ->getOneOrNullResult();
    }
    
}
