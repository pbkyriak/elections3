<?php

namespace Gnosis\ElectionsEntryBaseBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;
/**
 * Description of StauroiEntryRepository
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class StauroiEntryRepository extends EntityRepository {
    
    public function getEntryForEt(Ekloges $election, RegionEklTmima $et) {
        $qb = $this->createQueryBuilder('e')
                ->select()
                ->leftJoin('e.stauroiEntryRows', 'er')
                ->leftJoin('er.syndiasmos', 'sy')
                ->leftJoin('er.ypopsifios', 'yp')
                ->addSelect('er')
                ->addSelect('sy')
                ->addSelect('yp')
                ->where('e.region=:et and e.election=:el')
                ->setParameter('et', $et)
                ->setParameter('el', $election);
        return $qb->getQuery()
                ->getOneOrNullResult();
    }
}
