<?php

namespace Gnosis\ElectionsEntryBaseBundle\Entity;

/**
 * Sequence
 */
class Sequence
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $sqkey;

    /**
     * @var integer
     */
    private $sq;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set key
     *
     * @param string $key
     *
     * @return Sequence
     */
    public function setSqkey($key)
    {
        $this->sqkey = $key;

        return $this;
    }

    /**
     * Get key
     *
     * @return string
     */
    public function getSqkey()
    {
        return $this->sqkey;
    }

    /**
     * Set sq
     *
     * @param integer $sq
     *
     * @return Sequence
     */
    public function setSq($sq)
    {
        $this->sq = $sq;

        return $this;
    }

    /**
     * Get sq
     *
     * @return integer
     */
    public function getSq()
    {
        return $this->sq;
    }
}

