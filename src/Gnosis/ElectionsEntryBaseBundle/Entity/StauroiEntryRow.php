<?php

namespace Gnosis\ElectionsEntryBaseBundle\Entity;

/**
 * StauroiEntryRow
 */
class StauroiEntryRow
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $amount;

    /**
     * @var \Gnosis\ElectionsEntryBaseBundle\Entity\StauroiEntry
     */
    private $stauroi_entry;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\Syndiasmos
     */
    private $syndiasmos;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\Ypopsifios
     */
    private $ypopsifios;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     *
     * @return StauroiEntryRow
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set stauroiEntry
     *
     * @param \Gnosis\ElectionsEntryBaseBundle\Entity\StauroiEntry $stauroiEntry
     *
     * @return StauroiEntryRow
     */
    public function setStauroiEntry(\Gnosis\ElectionsEntryBaseBundle\Entity\StauroiEntry $stauroiEntry = null)
    {
        $this->stauroi_entry = $stauroiEntry;

        return $this;
    }

    /**
     * Get stauroiEntry
     *
     * @return \Gnosis\ElectionsEntryBaseBundle\Entity\StauroiEntry
     */
    public function getStauroiEntry()
    {
        return $this->stauroi_entry;
    }

    /**
     * Set syndiasmos
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Syndiasmos $syndiasmos
     *
     * @return StauroiEntryRow
     */
    public function setSyndiasmos(\Gnosis\ElectionsBaseBundle\Entity\Syndiasmos $syndiasmos = null)
    {
        $this->syndiasmos = $syndiasmos;

        return $this;
    }

    /**
     * Get syndiasmos
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Syndiasmos
     */
    public function getSyndiasmos()
    {
        return $this->syndiasmos;
    }

    /**
     * Set ypopsifios
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Ypopsifios $ypopsifios
     *
     * @return StauroiEntryRow
     */
    public function setYpopsifios(\Gnosis\ElectionsBaseBundle\Entity\Ypopsifios $ypopsifios = null)
    {
        $this->ypopsifios = $ypopsifios;

        return $this;
    }

    /**
     * Get ypopsifios
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Ypopsifios
     */
    public function getYpopsifios()
    {
        return $this->ypopsifios;
    }
}

