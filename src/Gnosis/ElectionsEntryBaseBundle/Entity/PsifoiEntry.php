<?php

namespace Gnosis\ElectionsEntryBaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsifoiEntry
 */
class PsifoiEntry
{
    use LogUserActionsTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $eggegramenoi;

    /**
     * @var integer
     */
    private $psifisan;

    /**
     * @var integer
     */
    private $leyka;

    /**
     * @var integer
     */
    private $akyra;

    /**
     * @var integer
     */
    private $egkyra;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $psifoiEntryRows;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima
     */
    private $region;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\Ekloges
     */
    private $election;

    /**
     * @var \DateTime
     */
    private $updated_at;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->psifoiEntryRows = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eggegramenoi
     *
     * @param integer $eggegramenoi
     * @return PsifoiEntry
     */
    public function setEggegramenoi($eggegramenoi)
    {
        $this->eggegramenoi = $eggegramenoi;

        return $this;
    }

    /**
     * Get eggegramenoi
     *
     * @return integer 
     */
    public function getEggegramenoi()
    {
        return $this->eggegramenoi;
    }

    /**
     * Set psifisan
     *
     * @param integer $psifisan
     * @return PsifoiEntry
     */
    public function setPsifisan($psifisan)
    {
        $this->psifisan = $psifisan;

        return $this;
    }

    /**
     * Get psifisan
     *
     * @return integer 
     */
    public function getPsifisan()
    {
        return $this->psifisan;
    }

    /**
     * Set leyka
     *
     * @param integer $leyka
     * @return PsifoiEntry
     */
    public function setLeyka($leyka)
    {
        $this->leyka = $leyka;

        return $this;
    }

    /**
     * Get leyka
     *
     * @return integer 
     */
    public function getLeyka()
    {
        return $this->leyka;
    }

    /**
     * Set akyra
     *
     * @param integer $akyra
     * @return PsifoiEntry
     */
    public function setAkyra($akyra)
    {
        $this->akyra = $akyra;

        return $this;
    }

    /**
     * Get akyra
     *
     * @return integer 
     */
    public function getAkyra()
    {
        return $this->akyra;
    }

    /**
     * Set egkyra
     *
     * @param integer $egkyra
     * @return PsifoiEntry
     */
    public function setEgkyra($egkyra)
    {
        $this->egkyra = $egkyra;

        return $this;
    }

    /**
     * Get egkyra
     *
     * @return integer 
     */
    public function getEgkyra()
    {
        return $this->egkyra;
    }

    /**
     * Add psifoiEntryRows
     *
     * @param \Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntryRow $psifoiEntryRows
     * @return PsifoiEntry
     */
    public function addPsifoiEntryRow(\Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntryRow $psifoiEntryRows)
    {
        $this->psifoiEntryRows[] = $psifoiEntryRows;

        return $this;
    }

    /**
     * Remove psifoiEntryRows
     *
     * @param \Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntryRow $psifoiEntryRows
     */
    public function removePsifoiEntryRow(\Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntryRow $psifoiEntryRows)
    {
        $this->psifoiEntryRows->removeElement($psifoiEntryRows);
    }

    /**
     * Get psifoiEntryRows
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPsifoiEntryRows()
    {
        return $this->psifoiEntryRows;
    }

    /**
     * Set region
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima $region
     * @return PsifoiEntry
     */
    public function setRegion(\Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set election
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Ekloges $election
     * @return PsifoiEntry
     */
    public function setElection(\Gnosis\ElectionsBaseBundle\Entity\Ekloges $election = null)
    {
        $this->election = $election;

        return $this;
    }

    /**
     * Get election
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Ekloges 
     */
    public function getElection()
    {
        return $this->election;
    }
    
    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;
    
        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    public function __toString() {
        return sprintf("Ψήφοι ΕΤ %s", $this->getRegion()->getTitle());
    }
    
}
