<?php

namespace Gnosis\ElectionsEntryBaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsifoiEntryRow
 */
class PsifoiEntryRow
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $amount;

    /**
     * @var \Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntry
     */
    private $psifoi_entry;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\Syndiasmos
     */
    private $syndiasmos;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return PsifoiEntryRow
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set psifoi_entry
     *
     * @param \Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntry $psifoiEntry
     * @return PsifoiEntryRow
     */
    public function setPsifoiEntry(\Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntry $psifoiEntry = null)
    {
        $this->psifoi_entry = $psifoiEntry;

        return $this;
    }

    /**
     * Get psifoi_entry
     *
     * @return \Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntry 
     */
    public function getPsifoiEntry()
    {
        return $this->psifoi_entry;
    }

    /**
     * Set syndiasmos
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Syndiasmos $syndiasmos
     * @return PsifoiEntryRow
     */
    public function setSyndiasmos(\Gnosis\ElectionsBaseBundle\Entity\Syndiasmos $syndiasmos = null)
    {
        $this->syndiasmos = $syndiasmos;

        return $this;
    }

    /**
     * Get syndiasmos
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Syndiasmos 
     */
    public function getSyndiasmos()
    {
        return $this->syndiasmos;
    }
}
