<?php

namespace Gnosis\ElectionsEntryBaseBundle\Uploaders;
use Symfony\Component\HttpFoundation\File\UploadedFile;
/**
 * Description of LogoUploader
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Dec 10, 2016
 */
class LogoUploader {

    private $relativeDir;
    private $uploadRootDir;
    private $webDir;
    
    public function __construct($webDir) {
        $this->webDir = $webDir;
    }
    public function setElectionSlug($electionSlug) {
        $webDir = $this->webDir;
        $this->relativeDir = sprintf(
                'uploads/elections/images/%s/syndiasmos_logos', $electionSlug
        );
        if( strrpos($webDir, '/')!==strlen($webDir)-1 ) {
            $webDir .= '/';
        }
        $this->uploadRootDir = $webDir . $this->relativeDir;
        $this->checkMediaFolder();
    }

    protected function checkMediaFolder() {
        $folder = $this->getUploadRootDir();
        if (!file_exists($folder)) {
            $oldmask = umask(0);
            mkdir($folder, 0777, true);
            umask($oldmask);
        }
    }
    
    public function upload(UploadedFile $file) {
        $fileName = md5(uniqid()) . '.' . $file->guessExtension();
        $file->move($this->uploadRootDir, $fileName);

        return $fileName;
    }

    public function getUploadRootDir() {
        return $this->uploadRootDir;
    }
    public function getUploadRelDir() {
        return $this->relativeDir;
    }
}
