<?php

namespace Gnosis\ElectionsEntryBaseBundle\Tests\Update;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntry;
use Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntryRow;

/**
 * Description of UpdateStauroiEntityTest
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class UpdateStauroiEntityTest  extends WebTestCase {

    private $em;
    private $container;

    public function setUp() {
        $client = static::createClient();
        $this->container = $client->getContainer();
        $this->em = $client->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * tests manager createEntry method
     */
    public function test02CreateEnrtyObject() {
        $election = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBySlug('ethnikes_2015');
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('slug' => 'fthiotida', 'election' => $election));
        $region = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($election, $nomos, '338');

        $ypopsifioi = $this->em->getRepository('GnosisElectionsBaseBundle:Ypopsifios')->findBy(array('election' => $election));
        $ypCount = count($ypopsifioi);

        $mgr = $this->container->get('gnosis_elections_entry_base.stauroi_manager');
        $entry = $mgr->createEntryForEt($election, $region);

        $this->assertTrue(count($entry->getStauroiEntryRows()) == $ypCount);
    }

    /**
     * tests manager method for creating new entry and saving to db
     */
    public function test03NewEntry() {
        // delete record so we can create the record 
        $this->em->getConnection()->delete('stauroi_entry', array('region_id' => 31490));

        $election = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBySlug('ethnikes_2015');
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('slug' => 'fthiotida', 'election' => $election));
        $region = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($election, $nomos, '338');

        $mgr = $this->container->get('gnosis_elections_entry_base.stauroi_manager');
        $entry = $mgr->createEntryForEt($election, $region);

        $amount = 0;
        foreach ($entry->getStauroiEntryRows() as $row) {
            $amount += 1;
            $row->setAmount($amount);
        }
        $out = $mgr->updateEtEntry($entry);
        $this->assertTrue($out);
    }

    /**
     * tests manager method that fetches an entry from db
     */
    public function test04FetchEntry() {
        $election = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBySlug('ethnikes_2015');
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('slug' => 'fthiotida', 'election' => $election));
        $region = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($election, $nomos, '338');

        $mgr = $this->container->get('gnosis_elections_entry_base.stauroi_manager');
        $entry = $mgr->fetchEtEntry($election, $region);
        $this->assertTrue($entry != null);
    }

    /**
     * tests manager method that updates an entry to db
     */
    public function test05UpdateEntry() {
        $election = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBySlug('ethnikes_2015');
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('slug' => 'fthiotida', 'election' => $election));
        $region = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($election, $nomos, '338');

        $mgr = $this->container->get('gnosis_elections_entry_base.stauroi_manager');
        $entry = $mgr->fetchEtEntry($election, $region);
        $amount = 0;
        foreach ($entry->getStauroiEntryRows() as $row) {
            $amount = 2;
            $row->setAmount($amount);
        }
        $out = $mgr->updateEtEntry($entry);
        $this->assertTrue($out);
    }

    /**
     * tests manager method that deletes an entry from db
     */
    public function test06DeleteEntry() {
        $election = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBySlug('ethnikes_2015');
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('slug' => 'fthiotida', 'election' => $election));
        $region = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($election, $nomos, '338');

        $mgr = $this->container->get('gnosis_elections_entry_base.stauroi_manager');
        $out = $mgr->deleteEtEntry($election, $region);
        $this->assertTrue($out);
    }

}
