<?php

namespace Gnosis\ElectionsEntryBaseBundle\Tests\Update;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntry;
use Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntryRow;

/**
 * PsifoiEntry validation tests
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ValidatePsifoiEntryTest extends WebTestCase {

    private $em;
    private $container;

    public function setUp() {
        $client = static::createClient();
        $this->container = $client->getContainer();
        $this->em = $client->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * 
     * @return PsifoiEntry
     */
    private function createNew() {
        $election = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBySlug('ethnikes_2015');
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('slug' => 'fthiotida', 'election' => $election));
        $region = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($election, $nomos, '338');

        $mgr = $this->container->get('gnosis_elections_entry_base.psifoi_manager');
        $entry = $mgr->createEntryForEt($election, $region);
        return $entry;
    }
    
    /**
     * all zero -> no errors
     */
    public function test01() {
        $entry = $this->createNew();
        $validator = $this->container->get('validator');
        $violationList =$validator->validate($entry);
        $this->assertEquals(0, $violationList->count());
    }

    /**
     * egkyra != psifisan-leyka-akyra
     * synolo psifon != egkyra
     */
    public function test02() {
        $entry = $this->createNew();
        $entry->setPsifisan(20);
        $entry->setAkyra(3);
        $entry->setLeyka(2);
        $entry->setEgkyra(16);
        $validator = $this->container->get('validator');
        $violationList =$validator->validate($entry);
        $this->assertEquals('elections.validation.total_egg', $violationList[0] ? $violationList[0]->getMessage(): '');
        $this->assertEquals('elections.validation.psifoi_egkyra', $violationList[1] ? $violationList[1]->getMessage(): '');
    }

    /**
     * psifisan > eggegramenoi
     * egkyra != psifisan-leyka-akyra
     * synolo psifon != egkyra
     */
    public function test03() {
        $entry = $this->createNew();
        $entry->setPsifisan(26);
        $entry->setAkyra(3);
        $entry->setLeyka(2);
        $entry->setEgkyra(16);
        $validator = $this->container->get('validator');
        $violationList =$validator->validate($entry);
        $this->assertEquals('elections.validation.egg_psif', $violationList[0] ? $violationList[0]->getMessage(): '');
        $this->assertEquals('elections.validation.total_egg', $violationList[1] ? $violationList[1]->getMessage(): '');
        $this->assertEquals('elections.validation.psifoi_egkyra', $violationList[2] ? $violationList[2]->getMessage(): '');
    }

    /**
     * synolo psifon != egkyra
     */
    public function test04() {
        $entry = $this->createNew();
        $entry->setPsifisan(20);
        $entry->setAkyra(3);
        $entry->setLeyka(2);
        $entry->setEgkyra(15);
        $rows = $entry->getPsifoiEntryRows();
        $rows[0]->setAmount(10);
        $rows[1]->setAmount(6);
        $validator = $this->container->get('validator');
        $violationList =$validator->validate($entry);
        $this->assertEquals('elections.validation.psifoi_egkyra', $violationList[0] ? $violationList[0]->getMessage() : '');
    }

    /**
     * all good
     */
    public function test05() {
        $entry = $this->createNew();
        $entry->setPsifisan(20);
        $entry->setAkyra(3);
        $entry->setLeyka(2);
        $entry->setEgkyra(15);
        $rows = $entry->getPsifoiEntryRows();
        $rows[0]->setAmount(10);
        $rows[1]->setAmount(5);
        $validator = $this->container->get('validator');
        $violationList =$validator->validate($entry);
        $this->assertEquals(0, $violationList->count());
    }

    /**
     * in EntryRow amount is string
     */
    public function test06() {
        $entry = $this->createNew();
        $entry->setPsifisan(20);
        $entry->setAkyra(3);
        $entry->setLeyka(2);
        $entry->setEgkyra(15);
        $rows = $entry->getPsifoiEntryRows();
        $rows[0]->setAmount(15);
        $rows[1]->setAmount('c');
        $validator = $this->container->get('validator');
        $violationList =$validator->validate($entry);
        $this->assertEquals('elections.validation.must_integer', $violationList[0] ? $violationList[0]->getMessage(): '');
        $this->assertEquals('psifoiEntryRows[1].amount', $violationList[0] ? $violationList[0]->getPropertyPath(): '');
    }
}

/*
 * notes for tests:
 
        $validator = $kernel->getContainer()->get('validator');
        $violationList = $validator->validate(new RequestModel);
        $this->assertEquals(1, $violationList->count());
        // or any other like:
        $this->assertEquals('client not valid', $violationList[0]->getMessage());
        foreach($violationList as $violation) {
            printf("%s\n", $violation->getMessage());
        }
        foreach($violationList as $violation) {
            printf("%s\n", $violation->getMessage());
            printf("code=%s\n",$violation->getCode());
            printf("invalidevalue=%s\n",$violation->getInvalidValue());
            printf("property path=%s\n",$violation->getPropertyPath());
        }
 */