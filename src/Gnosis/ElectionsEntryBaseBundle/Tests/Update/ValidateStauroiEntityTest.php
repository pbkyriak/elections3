<?php

namespace Gnosis\ElectionsEntryBaseBundle\Tests\Update;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Description of ValidateStauroiEntityTest
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ValidateStauroiEntityTest extends WebTestCase {

    private $em;
    private $container;

    public function setUp() {
        $client = static::createClient();
        $this->container = $client->getContainer();
        $this->em = $client->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * 
     * @return PsifoiEntry
     */
    private function createNewPsifoi() {
        $this->em->getConnection()->delete('psifoi_entry', array('region_id' => 31490));
        $election = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBySlug('ethnikes_2015');
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('slug' => 'fthiotida', 'election' => $election));
        $region = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($election, $nomos, '338');

        $mgr = $this->container->get('gnosis_elections_entry_base.psifoi_manager');
        $entry = $mgr->createEntryForEt($election, $region);
        // oloi oi syndiasmoi apo 4 psifous
        foreach($entry->getPsifoiEntryRows() as $row) {
            $row->setAmount(4);
        }
        $mgr->updateEtEntry($entry);
    }

    private function createNewStauroi() {
        //$this->em->getConnection()->delete('psifoi_entry', array('region_id' => 31490));
        $election = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBySlug('ethnikes_2015');
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('slug' => 'fthiotida', 'election' => $election));
        $region = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($election, $nomos, '338');

        $mgr = $this->container->get('gnosis_elections_entry_base.stauroi_manager');
        $entry = $mgr->createEntryForEt($election, $region);
        return $entry;
    }
    /**
     * all zero -> no errors
     */
    public function atest01() {
        $this->createNewPsifoi();
        $entry = $this->createNewStauroi();
        $validator = $this->container->get('validator');
        $violationList =$validator->validate($entry);
        $this->assertEquals(0, $violationList->count());
    }

    /**
     * stauroi > psifoi* SPB
     */
    public function atest02() {
        $this->createNewPsifoi();
        $entry = $this->createNewStauroi();
        $rows = $entry->getStauroiEntryRows();
        
        // o protos syndiasmos 20 staurous se ka8e ypopsifio
        $s = null;
        foreach($rows as $k => $row) {
            if(!$s) {
                $s = $row->getSyndiasmos()->getId();
            }
            $row->setAmount(20);
            if( $s != $row->getSyndiasmos()->getId() ) {
                break;
            }
        }
        $validator = $this->container->get('validator');
        $violationList =$validator->validate($entry);
        $this->assertEquals(1, count($violationList));
        if( count($violationList)) {
            $this->assertEquals('elections.validation.total_psifoi_stauroi_not_match', $violationList[0] ? $violationList[0]->getMessage(): '');
        }
    }


    /**
     * stauroi = psifoi* SPB
     */
    public function test03() {
        $this->createNewPsifoi();
        $entry = $this->createNewStauroi();
        $rows = $entry->getStauroiEntryRows();
        
        // bale ston 1o 16 staurous poy mporei na parei o syndiasmos
        $s = null;
        foreach($rows as $k => $row) {
            if( $s != $row->getSyndiasmos()->getId() ) {
                $row->setAmount(16);
                $s = $row->getSyndiasmos()->getId();
            }
        }
        $validator = $this->container->get('validator');
        $violationList =$validator->validate($entry);
        $this->assertEquals(0, count($violationList));
    }
}
