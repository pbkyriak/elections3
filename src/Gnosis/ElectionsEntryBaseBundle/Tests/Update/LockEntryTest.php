<?php
namespace Gnosis\Tests\Update;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Gnosis\ElectionsEntryBaseBundle\Entity\EntryLock;
use Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima;

/**
 * Tests for EntryLocManager.
 * Causion: Deletes all lock records!
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class LockEntryTest  extends WebTestCase {
    
    private $em;
    private $container;
    
    public function setUp() {
        $client = static::createClient();
        $this->container = $client->getContainer();
        $this->em = $client->getContainer()->get('doctrine.orm.entity_manager');

    }
    
    private function getET() {
       $election = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBySlug('ethnikes_2015');
       $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('slug'=>'fthiotida', 'election'=>$election));
       $region = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($election, $nomos, '338');

       return $region;
    }
    
    /**
     * Not realy a test, removes all lock records.
     */
    public function test00() {
        $this->em->getConnection()->query('DELETE FROM entry_lock');
        $this->assertTrue(true);
    }
    
    /**
     * create lock for psifoi for et=338 from user=1
     */
    public function test01() {
        $mgr = $this->container->get('gnosis_elections_entry_base.lock_manager');
        $out = $mgr->lockForPsifoi($this->getET(),1);
        $this->assertTrue($out);
    }
    
    /**
     * try to lock again psifoi for et=338 from user=1
     */
    public function ktest02() {
        $mgr = $this->container->get('gnosis_elections_entry_base.lock_manager');
        $out = $mgr->lockForPsifoi($this->getET(),1);
        $this->assertTrue(!$out);
    }
    
    /**
     * unlock  psifoi for et=338 wrong user
     */
    public function ktest03() {
        $mgr = $this->container->get('gnosis_elections_entry_base.lock_manager');
        $out = $mgr->unlockForPsifoi($this->getET(), 2);
        $this->assertTrue(!$out);
    }

    /**
     * unlock  psifoi for et=338 correct user
     */
    public function ktest04() {
        $mgr = $this->container->get('gnosis_elections_entry_base.lock_manager');
        $out = $mgr->unlockForPsifoi($this->getET(), 1);
        $this->assertTrue($out);
    }
}
