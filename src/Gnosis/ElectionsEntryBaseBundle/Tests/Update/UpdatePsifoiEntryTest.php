<?php

namespace Gnosis\ElectionsEntryBaseBundle\Tests\Update;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntry;
use Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntryRow;

/**
 * Description of UpdatePsifoiEntry
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class UpdatePsifoiEntryTest extends WebTestCase {

    private $em;
    private $container;

    public function setUp() {
        $client = static::createClient();
        $this->container = $client->getContainer();
        $this->em = $client->getContainer()->get('doctrine.orm.entity_manager');
    }

    /**
     * test repository method that returns ET by it's number
     */
    public function test01GetEtByNum() {
        $election = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBySlug('ethnikes_2015');
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('slug' => 'fthiotida', 'election' => $election));
        $region = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($election, $nomos, '338');
        $this->assertTrue($region->getId() == 31490);
    }

    /**
     * tests manager createEntry method
     */
    public function test02CreateEnrtyObject() {
        $election = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBySlug('ethnikes_2015');
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('slug' => 'fthiotida', 'election' => $election));
        $region = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($election, $nomos, '338');

        $syndiasmoi = $this->em->getRepository('GnosisElectionsBaseBundle:Syndiasmos')->findBy(array('election' => $election));
        $syndCount = count($syndiasmoi);

        $mgr = $this->container->get('gnosis_elections_entry_base.psifoi_manager');
        $entry = $mgr->createEntryForEt($election, $region);

        $this->assertTrue(count($entry->getPsifoiEntryRows()) == $syndCount);
    }

    /**
     * tests manager method for creating new entry and saving to db
     */
    public function test03NewEntry() {
        // delete record so we can create the record 
        $this->em->getConnection()->delete('psifoi_entry', array('region_id' => 31490));

        $election = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBySlug('ethnikes_2015');
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('slug' => 'fthiotida', 'election' => $election));
        $region = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($election, $nomos, '338');

        $mgr = $this->container->get('gnosis_elections_entry_base.psifoi_manager');
        $entry = $mgr->createEntryForEt($election, $region);
        $entry->setEggegramenoi(25);
        $entry->setPsifisan(20);
        $entry->setAkyra(3);
        $entry->setLeyka(2);
        $entry->setEgkyra(16);

        $amount = 0;
        foreach ($entry->getPsifoiEntryRows() as $row) {
            $amount += 5;
            $row->setAmount($amount);
        }
        $out = $mgr->updateEtEntry($entry);
        $this->assertTrue($out);
    }

    /**
     * tests manager method that fetches an entry from db
     */
    public function test04FetchEntry() {
        $election = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBySlug('ethnikes_2015');
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('slug' => 'fthiotida', 'election' => $election));
        $region = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($election, $nomos, '338');

        $mgr = $this->container->get('gnosis_elections_entry_base.psifoi_manager');
        $entry = $mgr->fetchEtEntry($election, $region);
        $this->assertTrue($entry != null);

    }

    /**
     * tests manager method that updates an entry to db
     */
    public function test05UpdateEntry() {
        $election = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBySlug('ethnikes_2015');
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('slug' => 'fthiotida', 'election' => $election));
        $region = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($election, $nomos, '338');

        $mgr = $this->container->get('gnosis_elections_entry_base.psifoi_manager');
        $entry = $mgr->fetchEtEntry($election, $region);
        $amount = 0;
        foreach ($entry->getPsifoiEntryRows() as $row) {
            $amount += 6;
            $row->setAmount($amount);
        }
        $out = $mgr->updateEtEntry($entry);
        $this->assertTrue($out);
    }

    /**
     * tests manager method that deletes an entry from db
     */
    public function test06DeleteEntry() {
        $election = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBySlug('ethnikes_2015');
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('slug' => 'fthiotida', 'election' => $election));
        $region = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($election, $nomos, '338');

        $mgr = $this->container->get('gnosis_elections_entry_base.psifoi_manager');
        $out = $mgr->deleteEtEntry($election, $region);
        $this->assertTrue($out);
    }

}
