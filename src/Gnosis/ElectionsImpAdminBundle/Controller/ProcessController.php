<?php

namespace Gnosis\ElectionsImpAdminBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;
use Gnosis\ElectionsImpAdminBundle\Entity\Job;
use Gnosis\ElectionsImpAdminBundle\Event\JobInsertEvent;
use Gnosis\ElectionsEntryBaseBundle\Event\ElectionsEvents;
use Gnosis\ElectionsImpAdminBundle\Util\InitJobCommandsBuilder;

/**
 * ImportBatch and Actions controller. Forms to select them and process form.
 * 
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Οργάνωση")
 * @Breadcrumb("Αρχικοποίηση", route="gnosis_elections_imp_admin_homepage")
 * @CurrentMenuItem("gnosis_elections_imp_admin_homepage")
 * @Security("has_role('ROLE_ADMIN')")
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Sep 9, 2016
 */
class ProcessController extends Controller {
    private $eklogesId;
    private $nomosId;
    private $election;
    private $nomos;
    
    private function loadRequired() {
        $em = $this->getDoctrine()->getManager();
        $this->eklogesId = $this->get('session')->get('election_id');
        $this->nomosId = $this->get('session')->get('nomos_id');
        $this->election = $em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($this->eklogesId);
        $this->nomos = $em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->find($this->nomosId);
    }
    

    /**
     * @Breadcrumb("elections.impui.import_file")
     */
    public function importMediaAction(Request $request) {
        $xlsname = $request->get('fname');
        $form = $this->getImportForm($xlsname);
        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                $parameters = $form->getData();
                $jobId = $this->createBatchJob($parameters);
                if( $jobId ) {
                    $this->get('session')->getFlashBag()->add('info', 'elections.impui.job_create_success');
                    return $this->redirect($this->generateUrl('gnosis_elections_imp_admin_import_process', ['jobId'=>$jobId]));
                }
                else {
                    $this->get('session')->getFlashBag()->add('notice', 'elections.impui.job_create_failed');
                }
            }
        }
        return $this->render(
                'GnosisElectionsImpAdminBundle:ImpUi:import.html.twig',
                array(
                    'form' => $form->createView(),
                    'xlsfile' => $xlsname,
                    'choices' => InitJobCommandsBuilder::getBatchNames(),
                    )
                );
    }
    
    private function getImportForm($xlsname) {
        $filterData = array('xlsname'=>$xlsname);
        $actionChoices = InitJobCommandsBuilder::getBatchNames();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData, array('method' => 'POST', 'csrf_protection' => false))
            ->add(
                    'actiontype', 
                    'choice', 
                    array(
                        'required' => true, 
                        'label' => 'elections.impui.actiontype', 
                        'choices' => $actionChoices,
                        'choices_as_values' => false,
                        'help' => 'elections.batches_descriptions.cleanImport'
                        )
                    )
            ->add(
                    'xlsname',
                    'hidden'
                    )
            ->add(
                    'save', 
                    'submit',
                    array(
                        'label' => 'gsprod.general.run', 
                        'attr' => array('class' => 'btn blue')
                        )
                    )
            ->getForm();
        return $form;
    }

    private function createBatchJob($parameters) {
        $out = null;
        $repo = $this->get('gnosis_elections_imp_admin.initfilesrepo');
        $finfo = $repo->getMediaInfoFromFilename($parameters['xlsname']);
        if( $finfo ) {
            $parameters['xlsname'] = $finfo['realname'];
            $out = $this->createJob($parameters);
        }
        return $out;
    }
     
    private function createJob($parameters) {
        $out = null;

        $cmdBuilder = $this->get('gnosis_elections_imp_admin.initcmdbuilder');
        $commands = $cmdBuilder->buildJobCommands($parameters);
        if( $commands ) {
            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository('SlxUserBundle:User')->find($this->getUser()->getId());
            $job = new Job();
            $job->setUser($user);
            $job->setCommands(serialize($commands));
            $job->setErrors('');
            $job->setOutput('');
            $job->setState(0);

            $em->persist($job);
            $em->flush();
            $out = $job->getId();
            $event = new JobInsertEvent($job->getId());
            $this->get('event_dispatcher')->dispatch(ElectionsEvents::JOB_INSERT, $event);

        }
        return $out;
    }
    
    /**
     * @Breadcrumb("elections.impui.process")
     */
    public function importProcessAction(Request $request, $jobId) {
        $em = $this->getDoctrine()->getManager();
        $job = $em->getRepository('GnosisElectionsImpAdminBundle:Job')->find($jobId);
        if( !$job ) {
            $this->get('session')->getFlashBag()->add('notice', 'elections.impui.job_not_found');
            return $this->redirect($this->generateUrl('gnosis_elections_imp_admin_homepage'));            
        }
        return $this->render(
                'GnosisElectionsImpAdminBundle:ImpUi:import_process.html.twig',
                array(
                    'jobId' => $jobId,
                    'job' => $job,
                    )
                );
        
    }
    
    
    public function jobStateEsAction(Request $request, $jobId) {
        $em = $this->getDoctrine()->getManager();
        $job = $em->getRepository('GnosisElectionsImpAdminBundle:Job')->find($jobId);
        $message = "data: done";
        if( $job ) {
            if( $job->getState()!=1 ) {
                $message = "data: running";
            }
        }
        $response = new Response($message."\n\n");
        $response->headers->set('Content-Type', 'text/event-stream');
        $response->headers->set('Cache-Control', 'no-cache');
        return $response;

    }
    
    public function jobStateLogsAction(Request $request, $jobId) {
        $em = $this->getDoctrine()->getManager();
        $job = $em->getRepository('GnosisElectionsImpAdminBundle:Job')->find($jobId);
        $data = array('output'=>'', 'errors'=>'');
        if( $job ) {
            $data['output'] = nl2br($job->getOutput());
            $data['errors'] = nl2br($job->getErrors());
        }
        $response = new Response(json_encode($data));
        $response->headers->set('Content-Type', 'text/json');
        $response->headers->set('Cache-Control', 'no-cache');
        return $response;

    }
    
    /**
     * @Breadcrumb("elections.impui.action")
     */
    public function actionsAction(Request $request) {
        $form = $this->getActionsForm();
        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                $parameters = $form->getData();
                $jobId = $this->createJob($parameters);
                if( $jobId ) {
                    $this->get('session')->getFlashBag()->add('info', 'elections.impui.job_create_success');
                    return $this->redirect($this->generateUrl('gnosis_elections_imp_admin_import_process', ['jobId'=>$jobId]));
                }
                else {
                    $this->get('session')->getFlashBag()->add('notice', 'elections.impui.job_create_failed');
                }
            }
        }
        return $this->render(
                'GnosisElectionsImpAdminBundle:ImpUi:action.html.twig',
                array(
                    'form' => $form->createView(),
                    'choices' => InitJobCommandsBuilder::getActionNames(),
                    )
                );
        
    }
    
    private function getActionsForm() {
        $filterData = array();
        $actionChoices = InitJobCommandsBuilder::getActionNames();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData, array('method' => 'POST', 'csrf_protection' => false))
            ->add(
                    'actiontype', 
                    'choice', 
                    array(
                        'required' => true, 
                        'label' => 'elections.impui.actiontype', 
                        'choices' => $actionChoices,
                        'choices_as_values' => false,
                        'help' => 'elections.action_descriptions.DropNomos'
                        )
                    )
            ->add(
                    'save', 
                    'submit',
                    array(
                        'label' => 'elections.impui.run', 
                        'attr' => array('class' => 'btn blue')
                        )
                    )
            ->getForm();
        return $form;
    }
        
}
