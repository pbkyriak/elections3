<?php

namespace Gnosis\ElectionsImpAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;

use Gnosis\ElectionsBaseBundle\Entity\Ekloges;
use Gnosis\ElectionsImpAdminBundle\Util\InitJobCommandsBuilder;

/**
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Οργάνωση")
 * @Breadcrumb("Αρχικοποίηση", route="gnosis_elections_imp_admin_homepage")
 * @CurrentMenuItem("gnosis_elections_imp_admin_homepage")
 * @Security("has_role('ROLE_ADMIN')")
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Sep 9, 2016
 */
class CronShController  extends Controller {
    private $eklogesId;
    private $nomosId;
    private $election;
    private $nomos;
    
    private function loadRequired() {
        $em = $this->getDoctrine()->getManager();
        $this->eklogesId = $this->get('session')->get('election_id');
        $this->nomosId = $this->get('session')->get('nomos_id');
        $this->election = $em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($this->eklogesId);
        $this->nomos = $em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->find($this->nomosId);
    }
   
   /**
     * @Breadcrumb("elections.impui.cron")
     */
    public function cronAction(Request $request) {
        $form = $this->getCronForm();
        $csh = $this->get('gnosis_elections_imp_admin.cronscripthandler');
        $cscontent = $csh->getContent();
        $csfilename = $csh->getFilename();
        
        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                $parameters = $form->getData();
                $cmdBuilder = $this->get('gnosis_elections_imp_admin.initcmdbuilder');
                $commands = $cmdBuilder->buildJobCommands($parameters);
                switch( $parameters['mode'] ) {
                    case 0:
                        $csh->append($commands);
                        break;
                    case 1:
                        $csh->replace($commands);
                        break;
                    case 2:
                        $csh->clear();
                        break;                    
                }
                $this->get('session')->getFlashBag()->add('info', 'elections.impui.cronscript_updated');
                return $this->redirect($this->generateUrl('gnosis_elections_imp_admin_cron'));
            }
        }
        return $this->render(
                'GnosisElectionsImpAdminBundle:ImpUi:cron.html.twig',
                array(
                    'form' => $form->createView(),
                    'cscontent' => $cscontent,
                    'csfilename' => $csfilename,
                    'choices' => InitJobCommandsBuilder::getActionNames(),
                    )
                );
        
    }
    
    private function getCronForm() {
        $filterData = array();
        $actionChoices = InitJobCommandsBuilder::getActionNames();
        $modeChoices = [0=>'elections.impui.append', 1=>'elections.impui.replace', 2=>'elections.impui.clearall'];
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData, array('method' => 'POST', 'csrf_protection' => false))
            ->add(
                    'actiontype', 
                    'choice', 
                    array(
                        'required' => true, 
                        'label' => 'elections.impui.actiontype', 
                        'choices' => $actionChoices,
                        'choices_as_values' => false,
                        'help' => 'elections.action_descriptions.DropNomos'
                        )
                    )
            ->add(
                    'mode', 
                    'choice', 
                    array(
                        'required' => true, 
                        'label' => 'elections.impui.mode', 
                        'choices' => $modeChoices,
                        'choices_as_values' => false,
                        )
                    )
            ->add(
                    'save', 
                    'submit',
                    array(
                        'label' => 'elections.impui.run', 
                        'attr' => array('class' => 'btn blue')
                        )
                    )
            ->getForm();
        return $form;
    }

}
