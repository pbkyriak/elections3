<?php

namespace Gnosis\ElectionsImpAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;
use Gnosis\ElectionsImpAdminBundle\Util\ConvertXlsToCsv;

/**
 * The UI controller for initialization imports. Offers a page to upload/delete import files and 
 * a page to execute remote import commands using the uploaded files. Operations
 * are made for the currently select elections/nomos from the top right selectors
 * (meaning the ones in user's session).
 *
 * The user uploads the import file. Import files are placed in web/uploads/[elections_slug]/initfiles/[nomos_slug]
 * A list with uploaded files for nomos is shown and the user and delete the file or perform an import command batch using the selected file.
 * 
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Οργάνωση")
 * @Breadcrumb("Αρχικοποίηση", route="gnosis_elections_imp_admin_homepage")
 * @CurrentMenuItem("gnosis_elections_imp_admin_homepage")
 * @Security("has_role('ROLE_ADMIN')")
 * 
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Sep 8, 2016
 * 
 */
class ImpUiController extends Controller {
    private $eklogesId;
    private $nomosId;
    private $election;
    private $nomos;

    public function indexAction() {
        $this->loadRequired();
        $repo = $this->get('gnosis_elections_imp_admin.initfilesrepo');
        $files = $repo->getMediaList();
        return $this->render('GnosisElectionsImpAdminBundle:ImpUi:index.html.twig', array('files'=>$files));
    }

    /**
     * @Breadcrumb("elections.impui.upload")
     * @param Request $request
     * @return type
     */
    public function uploadMediaAction(Request $request) {
        $form = $this->getUploadForm();
        $msg = null;
        $stats = null;
        $rej = null;
        if ($request->isMethod('POST')) {
            $form->bind($request);
            if ($form->isValid()) {
                $img = $form['mediafile']->getData();
                if ($img instanceof UploadedFile) {
                    $media = $this->get('gnosis_elections_imp_admin.initfile');
                    if ($media->isMediaValid($img)) {
                        $media->moveToUploadedMedia($img);
                        $conv = new ConvertXlsToCsv();
                        $conv->convert($media->getFullname());
                        $msg = 'success';
                    } else {
                        $msg = $media->getError();
                    }
                } else {
                    $msg = 'elections.impui.noFileUploaded';
                }
            }
            if ($msg == 'success') {
                $this->get('session')->getFlashBag()->add('info', 'elections.impui.upload_success');
                return $this->redirect($this->generateUrl('gnosis_elections_imp_admin_homepage'));
            } else {
                $this->get('session')->getFlashBag()->add('notice', $msg);
            }
        }
        return $this->render('GnosisElectionsImpAdminBundle:ImpUi:upload.html.twig',
                array(
                'form' => $form->createView(),
                'msg' => $msg,
        ));
    }
    
    private function getUploadForm()
    {
        $filterData = array();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData,
                array('method' => 'GET', 'csrf_protection' => false))
            ->add(
                'mediafile', 'file',
                array('required' => true, 'label' => 'File', 'attr' => array('class' => 'default'))
            )
            ->add('save', 'submit',
                array('label' => 'gsprod.general.save', 'attr' => array('class' => 'btn blue')))
            ->getForm();
        return $form;
    }
    
    public function deleteMediaAction($fname) {
        $repo = $this->get('gnosis_elections_imp_admin.initfilesrepo');
        if( $repo->deleteMedia($fname) ) {
            $this->get('session')->getFlashBag()->add('info', 'elections.impui.deleted');
        }
        else {
            $this->get('session')->getFlashBag()->add('info', 'elections.impui.notdeleted');
        }
        return $this->redirect($this->generateUrl('gnosis_elections_imp_admin_homepage'));
    }

    private function loadRequired() {
        $em = $this->getDoctrine()->getManager();
        $this->eklogesId = $this->get('session')->get('election_id');
        $this->nomosId = $this->get('session')->get('nomos_id');
        $this->election = $em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($this->eklogesId);
        $this->nomos = $em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->find($this->nomosId);
    }

    public function resetYpesAction() {
        $this->loadRequired();
        $em = $this->getDoctrine()->getManager();
        $sql = sprintf("UPDATE ypes_to_export SET TIMESTAMP=0, sent_timestamp=0 WHERE ekloges_id=%s", $this->eklogesId);
        $em->getConnection()->exec($sql); 
        return $this->redirect($this->generateUrl('gnosis_elections_imp_admin_homepage'));
    }
    
    public function cleanYpesAction() {
        $this->loadRequired();
        $em = $this->getDoctrine()->getManager();
        $sql = sprintf("delete * from ypes_to_export where ekloges_id=%s", $this->eklogesId);
        $this->em->getConnection()->exec($sql); 
        return $this->redirect($this->generateUrl('gnosis_elections_imp_admin_homepage'));
    }
}
