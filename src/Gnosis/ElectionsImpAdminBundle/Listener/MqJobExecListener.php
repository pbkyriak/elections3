<?php

namespace Gnosis\ElectionsImpAdminBundle\Listener;

use Gnosis\ElectionsEntryBaseBundle\Listener\AbstractMqEventListener;
use Slx\RabbitMqBundle\Event\MqJobEvent;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;
use Slx\RabbitMqBundle\Entity\MqJobTask;
use Symfony\Component\Process\Process;
use Slx\RabbitMqBundle\Rabbitmq\RpcCall;

/**
 * Description of MqJobExecListener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqJobExecListener extends AbstractMqEventListener {
    private $process;
    /** @var \Slx\RabbitMqBundle\Entity\MqJob */
    private $job;
    private $rpc=null;
    private $jobStartTime=0;
    private $taskStartTime=0;
    
    /**
     * The event lister. Listens to JobExec event
     * 
     * @param MqJobEvent $event
     */
    protected function processEvent(MqJobEvent $event) {
        $jobId = $event->getData();
        $this->job = $this->getEM()->getRepository('SlxRabbitMqBundle:MqJob')->find($jobId);
        if( $this->job ) {
            $this->getEM()->refresh($this->job);
            $this->process = $this->job->getMqProcess();
            //if( $this->checkProcessStatus()!=RabbitMqConstants::PROC_STATUS_RUNNING ) { // execute only if not canceled and not failed process
                $this->execJob();
            //}
            //else {
            //    printf("JOB FAILED\n");
            //}
            $this->sendJobResponse();
        }
        return 'ok';
    }
    
    /**
     * Executes all tasks related to the job
     * 
     */
    private function execJob() {
        printf("\tRunning job: %s\n", $this->job->getId());
        $this->jobStartTime = time();
        $this->sendProcessRunning();
        $this->setJobStatus(RabbitMqConstants::PROC_STATUS_RUNNING);
        try {
            $newStatus = RabbitMqConstants::PROC_STATUS_DONE;
            foreach($this->job->getTasks() as $task) {
                if( $task->getStatus()!=RabbitMqConstants::PROC_STATUS_DONE ) { // skip tasks that are done (will occur in case of process resume)
                    $pStatus = $this->checkProcessStatus();
                    if( $pStatus==RabbitMqConstants::PROC_STATUS_RUNNING ) { 
                        $newStatus = $this->execTask($task);
                        if( $newStatus != RabbitMqConstants::PROC_STATUS_DONE ) {    // something broke!
                            break;
                        }
                    }
                    else {
                        $newStatus = RabbitMqConstants::nextJobStatus($pStatus, $newStatus);
                        break;
                    }
                }
            }
            $this->setJobStatus($newStatus);
        }
        catch(\Exception $ex) {
            $this->setJobStatus(RabbitMqConstants::PROC_STATUS_FAILED);
        }
    }

    /**
     * Executes a Task
     * 
     * @param MqJobTask $task
     * @return integer
     */
    private function execTask(MqJobTask $task) {
        $this->setTaskStatus($task, RabbitMqConstants::PROC_STATUS_RUNNING);
        printf("\trunning task %s\n", $task->getId());
        $this->taskStartTime = time();
        
        $cmd = sprintf("app/console waev:exec:analysisTask %s %s", $task->getClass(), implode(" ", $task->getParamsArray()));
        try {
            $proc = new Process($cmd);
            $proc->setTimeout(null);
            $proc->run();
            $status = $proc->isSuccessful() ? RabbitMqConstants::PROC_STATUS_DONE : RabbitMqConstants::PROC_STATUS_FAILED;
            printf("Exit code =%s \n", $proc->getExitCode());
            $output = $proc->getOutput();
            $errorOutput = $proc->getErrorOutput();
        }
        catch(\Exception $ex) {
            $status = RabbitMqConstants::PROC_STATUS_FAILED;
            $errorOutput .= $ex->getMessage();
        }
        
        $this->setTaskStatus(
                $task, 
                $status, 
                $output, 
                $errorOutput
                );        
        return $status;
    }
        
    /**
     * publish in Fast lane jobDone or ProcessDone message
     * 
     */
    private function sendJobResponse() {
        $messageData = sprintf(RabbitMqConstants::MSG_MASK_JOB_DONE, $this->job->getId());
        if( $messageData ) {
            $this->getMQ()->publish($messageData, RabbitMqConstants::FAST_TRACK_QUEUE);
        }
    }
    
    /**
     * @todo Make this RPC so function will block till process status is set to Running
     * from fast lane client. Because will need process.status=Running so the job can execute.
     * 
     */
    private function sendProcessRunning() {
        if( !$this->rpc ) {
            $this->rpc = new RpcCall($this->getMQ());
        }
        $messageData = sprintf(RabbitMqConstants::MSG_MASK_PROCESS_RUNNING, $this->process->getId());
        $this->rpc->call(RabbitMqConstants::FAST_TRACK_QUEUE, $messageData); // waits till response comes back
        //$this->getMQ()->publish($messageData, RabbitMqConstants::FAST_TRACK_QUEUE);
    }
    
    /**
     * Set task status
     * 
     * @param MqJobTask $task
     * @param integer $status
     */
    private function setTaskStatus($task, $status, $output=null, $errorOutput=null) {
        $task->setStatus($status);
        if( $output ) {
            $task->setOutput($output);
        }
        if( $errorOutput ) {
            $task->setErrorOutput($errorOutput);
        }
        if( $status==RabbitMqConstants::PROC_STATUS_RUNNING ) {
            $duration = 0;
        }
        else {
            $duration = time()-$this->taskStartTime;
        }
        $task->setDuration($duration);
        $this->getEM()->persist($task);
        $this->getEM()->flush();
    }

    private function setJobStatus($status) {
        $duration = time()-$this->jobStartTime;
        $this->job->setDuration($duration);
        $this->job->setStatus($status);
        $this->getEM()->persist($this->job);
        $this->getEM()->flush();
    }

    /**
     * Checks if process and therefor the job is canceled
     * If process is canceled sets job status to Canceled and returns false
     * and no more tasks are executed
     * 
     * @return boolean
     */
    private function checkProcessStatus() {
        $out = RabbitMqConstants::PROC_STATUS_RUNNING;
        if( $this->job->getStatus()!=RabbitMqConstants::PROC_STATUS_CANCELED ) {    // isnt aware yet of a Cancel signal
            $this->getEM()->refresh($this->process);
            if( $this->process->getStatus()==RabbitMqConstants::PROC_STATUS_CANCELING) {    // if process current status is Canceling
                $out = RabbitMqConstants::PROC_STATUS_CANCELED;   // job cant continue executing
            }
            if( $this->process->getStatus()==RabbitMqConstants::PROC_STATUS_FAILED) {    // if process current status is Failed
                $out = RabbitMqConstants::PROC_STATUS_FAILED;   // job cant continue executing
            }
        }
        else {  // it is aware of cancelation signal, job cant continue executing
            $out = RabbitMqConstants::PROC_STATUS_CANCELED;
        }
        return $out;
    }
}
