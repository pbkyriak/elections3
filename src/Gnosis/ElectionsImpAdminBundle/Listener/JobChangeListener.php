<?php

namespace Gnosis\ElectionsImpAdminBundle\Listener;

use Gnosis\ElectionsImpAdminBundle\Event\JobInsertEvent;
use Slx\RabbitMqBundle\Rabbitmq\Connection as MqConnection;
use Doctrine\ORM\EntityManager;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;

/**
 * Description of JobChangeListener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Sep 8, 2016
 */
class JobChangeListener {
    /** @var \Doctrine\ORM\EntityManager */
    protected $em;
    /** @var \Slx\RabbitMqBundle\Rabbitmq\Connection */
    protected $mq;
    
    public function __construct(MqConnection $mq, EntityManager $em) {
        $this->em = $em;
        $this->mq = $mq;
    }

    public function onInsert(JobInsertEvent $event) {
        $messages = $this->createRowMessages('i',$event->getJobId());
        $this->pushToMq(RabbitMqConstants::JOBS_QUEUE,$messages);
    }
    
    private function createRowMessages($action, $jobId) {
        $msg = array(
            'a'  => $action,
            'jobId' => $jobId,
        );
        $out = json_encode($msg);
        return $out;
    }
    
    private function pushToMq($queue, $messages) {
        if( !is_array($messages) ) {
            $this->mq->publish($messages, $queue);
        }
        else {
            foreach($messages as $msg) {
                $this->mq->publish($msg, $queue);
            }
        }
    }
}