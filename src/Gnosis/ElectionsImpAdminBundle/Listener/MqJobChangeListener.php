<?php

namespace Gnosis\ElectionsImpAdminBundle\Listener;

use Gnosis\ElectionsEntryBaseBundle\Listener\AbstractMqEventListener;
use Symfony\Component\Process\Process;
/**
 * Description of MqJobChangeListener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Sep 8, 2016
 */
class MqJobChangeListener extends AbstractMqEventListener {
    
    protected function processEvent($event) {
        $data = $event->getData();
        $job = $this->em->getRepository('GnosisElectionsImpAdminBundle:Job')->find($data['jobId']);
        if( $job ) {
            $commands = $job->getCommandsAr();
            foreach($commands as $command) {
                if( !$this->execCmd($job, $command) ) {
                    break;
                }
            }
            $job->setState(1);
            $this->em->persist($job);
            $this->em->flush();
        }
    }
    
    private function execCmd(&$job, $cmd) {
        $status = false;
        try {
            $job->appendOutput(sprintf("Executing command: %s \n", $cmd));
            $job->appendErrors(sprintf("Executing command: %s \n", $cmd));
            $proc = new Process($cmd);
            $proc->setTimeout(null);
            $proc->run();
            $status = $proc->isSuccessful();
            printf("Exit code =%s \n", $proc->getExitCode());
            $job->appendOutput($proc->getOutput());
            $job->appendErrors($proc->getErrorOutput());
        }
        catch(\Exception $ex) {
            $status = false;
            $job->appendErrors($ex->getMessage());
        }
     
        return $status;
    }

}

