#Imports admin bundle

##Notes during development

The following are just notes made during development, not bundle documentation.

This bundle is a UI in order to let administrators use import tool from the web interface.

They can upload csv file with nomos elections data (regions, candidates etc) and post a job
to rabbitmq to run the appropriate commands. 
In order to initialize elections data you need to run from cmd (at the moment i am not sure which ones, maybe some cache:clear is also needed)

elections:drop:nonos <elections_slug> <nomos_slug>
elections:import <elections_slug> <nomos_slug> <filename>

Those will be executed in the server from a rabbitmq job event with the parameters set in the web interface.
The user will submit their task and go away (the web app will not wait for the job to finish).
The question now is for a simple way to inform the user that the job is done. Some simple async notification system.




0774