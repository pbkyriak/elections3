<?php

namespace Gnosis\ElectionsImpAdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;

/**
 * Description of ExportInitFileCommand
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ExcelInitFileToCsvCommand extends ContainerAwareCommand {

    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    private $handle;
    private $ets;
    private $synd;
    private $ypops;
    
    protected function configure() {
        $this
                ->setName('elections:init:excel-to-csv')
                ->setDescription("Convert initialization excel file to csv")
                ->addArgument('eklogesSlug', InputArgument::REQUIRED, 'ekloges to export?')
                ->addArgument('nomosSlug', InputArgument::REQUIRED, 'nomos to export?')
                ->addArgument('xlsFile', InputArgument::REQUIRED, 'excel file to convert?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $t1 = time();
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        
        $eklogesSlug = $input->getArgument('eklogesSlug');
        $nomosSlug = $input->getArgument('nomosSlug');
        $xlsFile = $input->getArgument('xlsFile');
        
        $output->writeln("<info>Ekloges slug=</info>".$eklogesSlug);
        $output->writeln("<info>Nomos slug  =</info>".$nomosSlug);
        $output->writeln("<info>Excel file  =</info>".$xlsFile);
        /*
        $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug'=>$eklogesSlug));
        if( !$ekloges ) {
            $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlug));
            return;
        }
        
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('election'=>$ekloges, 'slug'=>$nomosSlug));
        if( !$nomos ) {
            $output->writeln(sprintf("<error>No nomos found with slug %s</error>", $nomosSlug));
            return;
        }
        $repo = $this->getContainer()->get('gnosis_elections_imp_admin.initfilesrepo');
        $repo->force($ekloges->getId(), $nomos->getId());
        if(!file_exists($xlsFile) ) {
            $output->writeln(sprintf("<error>File %s not found!</error>", $xlsFile));
            return;            
        }
         * 
         */
$c = new \Gnosis\ElectionsImpAdminBundle\Util\ConvertXlsToCsv();
        $output->writeln($c->convert($xlsFile));
        
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
    }
    
}
