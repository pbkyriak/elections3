<?php

namespace Gnosis\ElectionsImpAdminBundle\Entity;

/**
 * Job
 */
class Job
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $state = 0;

    /**
     * @var string
     */
    private $commands;

    /**
     * @var string
     */
    private $output;

    /**
     * @var string
     */
    private $errors;

    /**
     * @var \Slx\UserBundle\Entity\User
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set state
     *
     * @param integer $state
     *
     * @return Job
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set commands
     *
     * @param string $commands
     *
     * @return Job
     */
    public function setCommands($commands)
    {
        $this->commands = $commands;

        return $this;
    }

    /**
     * Get commands
     *
     * @return string
     */
    public function getCommands()
    {
        return $this->commands;
    }

    public function getCommandsAr() {
        return unserialize($this->commands);
    }
    
    /**
     * Set output
     *
     * @param string $output
     *
     * @return Job
     */
    public function setOutput($output)
    {
        $this->output = $output;

        return $this;
    }

    public function appendOutput($aoutput) {
        $this->output = sprintf(
                "%s%s%s",
                $this->output,
                (!empty($this->output) ? "\n" : "") ,
                $aoutput);
        return $this;
    }
    
    /**
     * Get output
     *
     * @return string
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * Set errors
     *
     * @param string $errors
     *
     * @return Job
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;

        return $this;
    }

    public function appendErrors($aoutput) {
        $this->errors = sprintf(
                "%s%s%s",
                $this->errors,
                !empty($this->errors) ? "\n" : "" ,
                $aoutput);
        return $this;
    }

    /**
     * Get errors
     *
     * @return string
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Set user
     *
     * @param \Slx\UserBundle\Entity\User $user
     *
     * @return Job
     */
    public function setUser(\Slx\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Slx\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}

