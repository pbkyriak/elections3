<?php
namespace Gnosis\ElectionsImpAdminBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Description of JobInsertEvent
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Sep 8, 2016
 */
class JobInsertEvent extends Event {
    private $jobId;
    
    public function __construct($jobId) {
        $this->jobId = $jobId;
    }
    
    public function getJobId() {
        return $this->jobId;
    }
    
}
