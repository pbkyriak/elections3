<?php

namespace Gnosis\ElectionsImpAdminBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Description of MqJobInsertEvent
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Sep 8, 2016
 */
class MqJobInsertEvent extends Event {
    private $data;
    
    public function __construct($data) {
        $this->data = $data;
    }
    
    public function getData() {
        return $this->data;
    }
    
    public function isRPC() {
        return false;
    }

}
