<?php

namespace Gnosis\ElectionsImpAdminBundle\Event\Transformer;

use PhpAmqpLib\Message\AMQPMessage;
use Gnosis\ElectionsEntryBaseBundle\Event\Transformer\MqMessageToChangeEventInterface;
use Gnosis\ElectionsEntryBaseBundle\Event\ElectionsEvents;
use Gnosis\ElectionsImpAdminBundle\Event\MqJobInsertEvent;

/**
 * Description of MqMessageToJobInsertEvent
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Sep 8, 2016
 */
class MqMessageToJobInsertEvent implements MqMessageToChangeEventInterface {
    private $eventName;
    private $event;
    
    public function transform(AMQPMessage $msg) {
        $this->eventName = ElectionsEvents::MQ_JOB_INSERT;
        $eventData = json_decode($msg->body,true);
        $this->event = new MqJobInsertEvent($eventData);
    }
        
    public function getEventName() {
        return $this->eventName;
    }
    
    public function getEvent() {
        return $this->event;
    }
}
