<?php

namespace Gnosis\ElectionsImpAdminBundle\Util;

use Symfony\Component\Finder\Finder;
use Gnosis\ElectionsImpAdminBundle\Util\UserElectionContext;

/**
 * Description of ImportExcelFileRepository
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ImportExcelFileRepository {

    private $uploadRootDir;
    private $relativeDir = 'uploads';

    public function __construct(UserElectionContext $userElectionContext) {
        $this->relativeDir = sprintf(
                'uploads/elections/initial/%s/%s', $userElectionContext->getElection()->getSlug(), $userElectionContext->getNomos()->getSlug()
        );
        $webDir = $userElectionContext->getWebDir();
        if( strpos($webDir, DIRECTORY_SEPARATOR)!==strlen($webDir)-1 ) {
            $webDir .= DIRECTORY_SEPARATOR;
        }
        $this->uploadRootDir = $userElectionContext->getWebDir() . $this->relativeDir;
        $this->checkMediaFolder();
    }

    public function getUploadRootDir() {
        return $this->uploadRootDir;
    }

    protected function checkMediaFolder() {
        $folder = $this->getUploadRootDir();
        if (!file_exists($folder)) {
            $oldmask = umask(0);
            mkdir($folder, 0777, true);
            umask($oldmask);
        }
    }

    public function getMediaList() {
        $finder = new Finder();
        $finder->in($this->getUploadRootDir());
        $files = array();
        foreach ($finder as $file) {
            $files[] = array(
                'name' => str_replace($this->getUploadRootDir() . DIRECTORY_SEPARATOR, '', $file->getRealpath()),
                'asset' => $this->relativeDir . DIRECTORY_SEPARATOR . str_replace($this->getUploadRootDir(), '', $file->getRealpath()),
                'size' => $file->getSize(),
                'extension' => $file->getExtension(),
            );
        }
        return $files;
    }

    public function deleteMedia($name) {
        $out = false;
        $fpName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $name;
        if (file_exists($fpName)) {
            if (@unlink($fpName)) {
                $out = true;
            }
        }
        return $out;
    }

    public function getMediaInfoFromFilename($fname) {
        $out = null;
        $fn = sprintf("%s/%s",$this->getUploadRootDir(),$fname);
        if(file_exists($fn) ) {
            $file = new \SplFileInfo($fn);
            $out = array(
                'name' => str_replace($this->getUploadRootDir() . DIRECTORY_SEPARATOR, '', $file->getRealpath()),
                'asset' => $this->relativeDir . DIRECTORY_SEPARATOR . str_replace($this->getUploadRootDir(), '', $file->getRealpath()),
                'size' => $file->getSize(),
                'realname' => $file->getRealPath(),
                'extension' => $file->getExtension(),
            );
            
        }
        return $out;
    }
}
