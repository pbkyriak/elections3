<?php

namespace Gnosis\ElectionsImpAdminBundle\Util;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Gnosis\ElectionsImpAdminBundle\Util\ImportExcelFileRepository;
use Slx\MetronicBundle\Lib\GreekText;

/**
 * InitImportFile is uploaded excel file for import
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class InitImportFile {

    protected $validExtensions = array('xls');
    private $errorMsg = '';
    private $maxImageSize = 9000000;
    private $uploadRootDir = '';
    private $fullname = '';
    private $repo;

    public function __construct($repo) {
        $this->repo = $repo;
        $this->uploadRootDir = $repo->getUploadRootDir();
    }

    public function getUploadRootDir() {
        return $this->uploadRootDir;
    }

    public function getError() {
        return $this->errorMsg;
    }

    public function isMediaValid(UploadedFile $image) {
        if ($this->hasUploadSuccess($image)) {
            if ($this->hasUploadValidFileType($image)) {
                if ($this->hasUploadValidSize($image)) {
                    return true;
                }
            }
        }
        return false;
    }

    private function hasUploadSuccess(UploadedFile $image) {
        if ($image->getError()!=UPLOAD_ERR_OK) {
            $this->errorMsg = 'media.edit.uploadError';
            return false;
        }
        return true;
    }

    private function hasUploadValidFileType(UploadedFile $image) {
        $ext = $image->getClientOriginalExtension();
        if (!in_array(strtolower($ext), $this->validExtensions)) {
            $this->errorMsg = 'media.edit.notSupportedFileType ' . $ext;
            return false;
        }
        return true;
    }

    private function hasUploadValidSize(UploadedFile $image) {
        if ($image->getSize() > $this->maxImageSize) {
            $this->errorMsg = 'media.edit.fileTooBig';
            return false;
        }
        return true;
    }

    /**
     * @todo Αν υπάρχει αρχείο με το ιδιο όνομα να το κάνει ονομα-1 , ονομα-2 κλπ όπως στο paste sta windows. 
     * 
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function moveToUploadedMedia(UploadedFile $image) {
        $originalname = \Slx\MetronicBundle\Lib\GreekText::toGreeklish($image->getClientOriginalName());
        $info = pathinfo($originalname);
        $fname = GreekText::slugify($info['filename']).'.'.$info['extension'];
        $image->move($this->getUploadRootDir(), $fname);
        $this->fullname = sprintf("%s/%s", $this->getUploadRootDir(), $fname);
    }

    public function getFullname() {
        return $this->fullname;
    }

}
