<?php

namespace Gnosis\ElectionsImpAdminBundle\Util;

/**
 * Description of CronScriptHandler
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Sep 9, 2016
 */
class CronScriptHandler {

    private $rootDir;
    private $scriptName;
    
    public function __construct($rootDir) {
        $this->rootDir = realpath($rootDir.'/..');
        $this->scriptName = sprintf('%s/bin/cron.sh', $this->rootDir);
        if( !file_exists($this->scriptName) ) {
            file_put_contents($this->scriptName,"#!/bin/bash\ncd ..\n");
            chmod($this->scriptName, 0774);
        }
    }
    
    public function getContent() {
        
        $content = file_get_contents($this->scriptName);
        return $content;
    }
    
    public function append($commands) {
        if(is_array($commands) ) {
            foreach($commands as $command) {
                 file_put_contents($this->scriptName,"\n".$command,FILE_APPEND);
            }
        } 
        else {
            file_put_contents($this->scriptName,"\n".$commands,FILE_APPEND);
        }
    }
    
    public function replace($commands) {
        file_put_contents($this->scriptName,"#!/bin/bash\ncd ..\n");
        if(is_array($commands) ) {
            foreach($commands as $command) {
                 file_put_contents($this->scriptName,"\n".$command,FILE_APPEND);
            }
        } 
        else {
            file_put_contents($this->scriptName,"\n".$commands,FILE_APPEND);
        }
    }
    
    public function clear() {
        file_put_contents($this->scriptName,"#!/bin/bash\n");
    }
    
    public function getFilename() {
        return $this->scriptName;
    }
}
