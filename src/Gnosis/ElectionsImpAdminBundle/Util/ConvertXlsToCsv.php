<?php

namespace Gnosis\ElectionsImpAdminBundle\Util;
use PHPExcel;
/**
 * Description of ConvertXlsToCsv
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Sep 8, 2016
 */
class ConvertXlsToCsv {
    private $skipTopRows=0;
    private $handle = null;
    
    public function convert($xlsFilename) {
        $out = true;
        $csvFilename = $this->xlsToCsvFilename($xlsFilename);
        printf("%s\n", $csvFilename);
        $this->handle = fopen($csvFilename, "w+");
        $excel = \PHPExcel_IOFactory::load($xlsFilename);
        foreach( $excel->getAllSheets() as $sheet ) {
            $this->importSheet($sheet);
        }
        fclose($this->handle);
        return $out;
    }
    
    private function importSheet(\PHPExcel_Worksheet $sheet) {
        foreach($sheet->getRowIterator($this->skipTopRows+1) as $row) {
            $orow = array();
            foreach($row->getCellIterator() as $cell) {
                $orow[] = $cell->getValue();
            }
            fputcsv($this->handle, $orow, "\t");
        }
        
    }
    
    private function xlsToCsvFilename($xlsFilename) {
        $path_parts = pathinfo($xlsFilename);
        print_R($path_parts);
        $out = $path_parts['dirname'] . DIRECTORY_SEPARATOR .
                $path_parts['filename'] .
                '.csv';
        return $out;
    }
    
}
