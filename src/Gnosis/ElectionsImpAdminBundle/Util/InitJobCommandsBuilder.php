<?php

namespace Gnosis\ElectionsImpAdminBundle\Util;
use Gnosis\ElectionsImpAdminBundle\Util\UserElectionContext;

/**
 * InitJobBuilder is the middleware to make cli commands available to the UI.
 * 
 * In order to add a new action (i.e. for ExportYpesCommand)
 *  - add method actionExportYpesCommandCommands
 *  - add to getActionNames an element 'ExportYpesCommand' => 'elections.actions.ExportYpesCommand'; 
 *  - go to messages.el.yml and add title text under actions key ExportYpesCommand: this is a command
 *  - go to messages.el.yml and add title text under action_descriptions key ExportYpesCommand: this is a command

 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Sep 8, 2016
 */
class InitJobCommandsBuilder {

    private $election;
    private $nomos;

    /**
     * Returns the supported batch command names. Batch is a file processing procedure.
     * 
     * The keys of the array is the first part of the corresponding method. The second part is 'Commands'.
     * For example cleanImport is for cleanImportCommands method.
     * 
     * @return array
     */
    public static function getBatchNames() {
        return [
            'cleanImport' => 'elections.batches.cleanImport',
            'updateImport' => 'elections.batches.updateImport',
            'importLawyers' => 'elections.batches.importLawyers',
        ];
    }
    
    /**
     * Returns the supported action names. Actions are cli commands of the system.
     * 
     * Naming conversion. Methods returning the cli command for the actions is in the form action[action_array_key]Commands. 
     * For example DropNomos -> actionDropNomosCommands. So if you add a new one, add a line to this array and a method building the command.
     *
     * @return array
     */
    public static function getActionNames() {
        return [
            'DropNomos' => 'elections.actions.DropNomos',
            'ClearCache' => 'elections.actions.ClearCache',
            'WarmCache' => 'elections.actions.WarmCache',
            'MatchPrev' => 'elections.actions.MatchPrev',
            'MatchPrevReset' => 'elections.actions.MatchPrevReset',
            'CleanMatches' => 'elections.actions.CleanMatches',
            'FixEggCounted' => 'elections.actions.FixEggCounted',
            'DropElectionEntries' => 'elections.actions.DropElectionEntries',
            'DropElectionResults' => 'elections.actions.DropElectionResults',
            'SumEntries' => 'elections.actions.SumEntries',
            'DropNomosPraktika' => 'elections.actions.DropNomosPraktika',
            'DropNomosProtokolo' => 'elections.actions.DropNomosProtokolo',
            'ColorResultsMap' => 'elections.actions.ColorResultsMap',
            'ExportTotalFileMME' => 'elections.actions.ExportTotalFileMME',
            'ExportYpes' => 'elections.actions.ExportYpes',
        ];
    }
    
    public function __construct(UserElectionContext $userElectionContext) {
        $this->election = $userElectionContext->getElection();
        $this->nomos = $userElectionContext->getNomos();
        $this->user = null;
    }
    
    public function buildJobCommands($parameters) {
        $placeholders = array();
        $placeholders[] = '%eslug%';
        $placeholders[] = '%nslug%';
        
        $cmdParameters = array();
        $cmdParameters[] = $this->election->getSlug();
        $cmdParameters[] = $this->nomos->getSlug();
        
        if( isset($parameters['xlsname'])) {
            $placeholders[] = '%csvname%';
            $cmdParameters[] = $parameters['xlsname'];
        }

        $commands = array();
        // check if actiontype is batch method
        $methodName = sprintf("%sCommands", $parameters['actiontype']);
        if(method_exists($this, $methodName)) {
            $commands = $this->$methodName();
        }
        else {
            // try for action
            $methodName = sprintf("action%sCommands", $parameters['actiontype']);
            if(method_exists($this, $methodName)) {
                $commands = $this->$methodName($commands);
            }
        }
        foreach($commands as $idx => $command) {
            $commands[$idx] = str_replace($placeholders, $cmdParameters, $command);
        }
        return $commands;
    }
    
    // ----------- IMPORT BATCHES ----------------------------------------------
    private function cleanImportCommands() {
        $commands = array();
        $commands[] = "app/console elections:drop:nomos %eslug% %nslug%";
        $commands[] = "app/console elections:cache:clear %eslug% %nslug%";
        $commands[] = "app/console elections:import --utf8-encoding %eslug% %nslug% %csvname%";
        return $commands;
    }
    
    private function updateImportCommands() {
        $commands = array();
        $commands[] = "app/console elections:cache:warm %eslug% %nslug%";
        $commands[] = "app/console elections:import --utf8-encoding %eslug% %nslug% %csvname%";
        return $commands;        
    }
    
    private function importLawyersCommands() {
        $commands = array();
        $commands[] = "app/console elections:import:lawyers --utf8-encoding %eslug% %nslug% %csvname%";
        return $commands;        
    }
    
    // --------------- ACTIONS -------------------------------------------------
    private function actionDropNomosCommands($commands) {
        $commands[] = "app/console elections:drop:nomos %eslug% %nslug%";        
        return $commands;        
    }
    
    private function actionClearCacheCommands() {
        $commands[] = "app/console elections:cache:clear %eslug% %nslug%";
        return $commands;                
    }
    
    private function actionWarmCacheCommands($commands) {
        $commands[] = "app/console elections:cache:warm %eslug% %nslug%";
        return $commands;        
    }
 
    private function actionMatchPrevCommands($commands) {
        $commands[] = "app/console elections:match:prev %eslug%";
        return $commands;        
    }
    
    private function actionMatchPrevResetCommands($commands) {
        $commands[] = "app/console elections:match:prev:reset %eslug%";
        return $commands;        
    }
    
    private function actionCleanMatchesCommands($commands) {
        $commands[] = "app/console elections:import:clean-matches %eslug%";
        return $commands;        
    }
    
    private function actionFixEggCountedCommands($commands) {
        $commands[] = "app/console elections:import:fix-egg-counted %eslug%";
        return $commands;        
    }
    
    private function actionDropElectionEntriesCommands($commands) {
        $commands[] = "app/console elections:drop:entries %eslug%";
        return $commands;        
    }
    
    private function actionDropElectionResultsCommands($commands) {
        $commands[] = "app/console elections:drop:results %eslug%";
        return $commands;        
    }
    
    private function actionSumEntriesCommands($commands) {
        $commands[] = "app/console elections:sum:entries %eslug% %nslug%";
        return $commands;        
    }
    
    private function actionDropNomosPraktikaCommands() {
        $commands[] = "app/console elections:drop:praktika %eslug% %nslug%";
        return $commands;        
    }
    
    private function actionDropNomosProtokoloCommands($commands) {
        $commands[] = "app/console elections:drop:protokolo %eslug% %nslug%";
        return $commands;        
    }
    
    private function actionColorResultsMapCommands($commands) {
        $commands[] = "app/console elections:map:color-results %eslug% steraia-ellada2";
        return $commands;        
    }

    private function actionExportTotalFileMMECommands() {
        $commands[] = "app/console elections:export:mme:totals %eslug% %nslug%";
        return $commands;        
    }

    private function actionExportYpesCommands() {
        $commands[] = "app/console elections:export:mme:totals %eslug% %nslug%";
        return $commands;        
    }

}
