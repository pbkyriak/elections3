<?php

namespace Gnosis\ElectionsImpAdminBundle\Util;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;
use Gnosis\ElectionsBaseBundle\Entity\RegionNomos;

/**
 * Description of UserElectionContext
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Sep 8, 2016
 */
class UserElectionContext {
    
    private $eklogesId;
    private $nomosId;
    private $election=null;
    private $nomos=null;
    private $webDir;
    private $em;
    
    public function __construct($em, $session, $webDir) {
        $this->em = $em;
        $this->eklogesId = $session->get('election_id');
        $this->nomosId = $session->get('nomos_id');
        $this->loadEntities();
        $this->webDir = $webDir;
    }
    
    public function force($eklogesId, $nomosId) {
        $this->eklogesId = $eklogesId;
        $this->nomosId = $nomosId;
        $this->loadEntities();
    }
    
    private function loadEntities() {
        if( $this->eklogesId ) {
            $this->election = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($this->eklogesId);
        }
        if( $this->nomosId ) {
            $this->nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->find($this->nomosId);
        }        
    }
    
    /**
     * 
     * @return Ekloges
     */
    public function getElection() {
        return $this->election;
    }
    
    /**
     * 
     * @return RegionNomos
     */
    public function getNomos() {
        return $this->nomos;
    }
    
    public function getWebDir() {
        return $this->webDir;
    }
    
    public function getEm() {
        return $this->em;
    }
}
