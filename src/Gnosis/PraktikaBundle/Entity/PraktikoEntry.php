<?php

namespace Gnosis\PraktikaBundle\Entity;

/**
 * PraktikoEntry
 */
class PraktikoEntry
{
    use \Gnosis\ElectionsEntryBaseBundle\Entity\LogUserActionsTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $entry_id;

    /**
     * @var string
     */
    private $remarks;

    /**
     * @var \Gnosis\PraktikaBundle\Entity\Lawyer
     */
    private $lawyer;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima
     */
    private $region;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\Ekloges
     */
    private $election;

    /**
     * @var \DateTime
     */
    private $updated_at;
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entryId
     *
     * @param integer $entryId
     *
     * @return PraktikoEntry
     */
    public function setEntryId($entryId)
    {
        $this->entry_id = $entryId;

        return $this;
    }

    /**
     * Get entryId
     *
     * @return integer
     */
    public function getEntryId()
    {
        return $this->entry_id;
    }

    /**
     * Set remarks
     *
     * @param string $remarks
     *
     * @return PraktikoEntry
     */
    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;

        return $this;
    }

    /**
     * Get remarks
     *
     * @return string
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * Set lawyer
     *
     * @param \Gnosis\PraktikaBundle\Entity\Lawyer $lawyer
     *
     * @return PraktikoEntry
     */
    public function setLawyer(\Gnosis\PraktikaBundle\Entity\Lawyer $lawyer = null)
    {
        $this->lawyer = $lawyer;

        return $this;
    }

    /**
     * Get lawyer
     *
     * @return \Gnosis\PraktikaBundle\Entity\Lawyer
     */
    public function getLawyer()
    {
        return $this->lawyer;
    }

    /**
     * Set region
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima $region
     *
     * @return PraktikoEntry
     */
    public function setRegion(\Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set election
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Ekloges $election
     *
     * @return PraktikoEntry
     */
    public function setElection(\Gnosis\ElectionsBaseBundle\Entity\Ekloges $election = null)
    {
        $this->election = $election;

        return $this;
    }

    /**
     * Get election
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Ekloges
     */
    public function getElection()
    {
        return $this->election;
    }
    
    /**
     * 
     * @return \DateTime 
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }
    
    public function setUpdatedAt($v) {
        $this->updated_at = $v;
        return $this;
    }
    
    public function __toString() {
        return sprintf("Πρακτικό %s %s", $this->getElection()->getTitle(), $this->getEntryId());
    }
}

