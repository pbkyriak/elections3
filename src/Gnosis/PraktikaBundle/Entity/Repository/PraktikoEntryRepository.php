<?php
namespace Gnosis\PraktikaBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
/**
 * Description of PraktikoEntryRepository
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class PraktikoEntryRepository extends EntityRepository {
    
    public function hasEtPraktiko($etId) {
        $out = false;
        $stmt = $this->getEntityManager()->getConnection()->executeQuery("SELECT count(*) FROM praktiko_entry WHERE region_id=:etid", array('etid'=>$etId));
        $cnt = $stmt->fetchColumn(0);
        if( $cnt ) {
            if($cnt>0) {
                $out = true;
            }
        }
        return $out;
    }
    
    public function getNomosEntries($nomos) {
        $qb = $this->createQueryBuilder('pr')
                ->select('pr.entry_id, pr.updated_at, r.title, pr.remarks')
                ->leftJoin('pr.region', 'r')
                ->where('r.id_path like :idp')
                ->orderBy('pr.entry_id')
                ->setParameter('idp', sprintf("%s,%%", $nomos->getIdPath()))
                ;
        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }
}
