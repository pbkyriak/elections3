<?php

namespace Gnosis\PraktikaBundle\Entity;

/**
 * Lawyer
 */
class Lawyer
{
    use \Gnosis\ElectionsEntryBaseBundle\Entity\LogUserActionsTrait;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $fullname;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima
     */
    private $region;

    /**
     * @var \Gnosis\ElectionsBaseBundle\Entity\Ekloges
     */
    private $election;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     *
     * @return Lawyer
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Set region
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima $region
     *
     * @return Lawyer
     */
    public function setRegion(\Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set election
     *
     * @param \Gnosis\ElectionsBaseBundle\Entity\Ekloges $election
     *
     * @return Lawyer
     */
    public function setElection(\Gnosis\ElectionsBaseBundle\Entity\Ekloges $election = null)
    {
        $this->election = $election;

        return $this;
    }

    /**
     * Get election
     *
     * @return \Gnosis\ElectionsBaseBundle\Entity\Ekloges
     */
    public function getElection()
    {
        return $this->election;
    }
    
    public function __toString() {
        return sprintf("Δικαστικός: %s", $this->getFullname());
    }
}

