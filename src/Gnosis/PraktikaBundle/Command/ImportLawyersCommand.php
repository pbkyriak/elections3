<?php
namespace Gnosis\PraktikaBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Gnosis\PraktikaBundle\Importers\Importer;

/**
 * Description of ImportMapPointsCommand
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 4 Μαϊ 2014
 */
class ImportLawyersCommand extends ContainerAwareCommand
{
    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    
    protected function configure()
    {
        $this
            ->setName('elections:import:lawyers')
            ->setDescription("Imports lawyers for ETs")
            ->addArgument('eklogesSlug',
                InputArgument::REQUIRED,
                'ekloges to import?')
            ->addArgument('nomosSlug',
                InputArgument::REQUIRED,
                'nomos to import?')
            ->addArgument('fn',
                InputArgument::REQUIRED,
                'file to import?')
            ->addOption(
               'utf8-encoding',
               null,
               InputOption::VALUE_NONE,
               'If it set means that is in utf8 encoding otherwise texts will be converted from iso greek to utf8'
            )

        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $t1 = time();
    
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $filename = $input->getArgument('fn');
        $eklogesSlug = $input->getArgument('eklogesSlug');
        $nomosSlug = $input->getArgument('nomosSlug');
        $utf8Encoding = $input->getOption('utf8-encoding') ? true : false;
        
        $output->writeln("<info>Ekloges slug=</info>".$eklogesSlug);
        $output->writeln("<info>Nomos slug=</info>".$nomosSlug);
        $output->writeln("<info>File name=</info>".$filename);
        
        $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug'=>$eklogesSlug));
        if( !$ekloges ) {
            $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlug));
            return;
        }
        
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('election'=>$ekloges, 'slug'=>$nomosSlug));
        if( !$nomos ) {
            $output->writeln(sprintf("<error>No nomos found with slug %s</error>", $nomosSlug));
            return;
        }
        
        if( !file_exists($filename) ) {
            $output->writeln(sprintf("<error>File '%s' not found!</error>", $filename));
            return;
        }

        $importer = $this->getContainer()->get('gnosis_praktika_importers.lawyers_importer');
        $importer->setup($nomos, $filename, $utf8Encoding);
        $importer->import();
        
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
    }   
    
}
