<?php

namespace Gnosis\PraktikaBundle\Importers;

use Doctrine\ORM\EntityManager;
use Slx\MetronicBundle\Lib\GreekText;
use Gnosis\PraktikaBundle\Entity\Lawyer;
/**
 * Description of Impoerter
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class Importer {

    const DELIMETER = "\t";
    
    /** @var EntityManager */
    protected $em;
    /** @var RegionNomos */
    protected $nomos;
    /** @var RegionImportCache */
    protected $cacheLdr;
    
    private $eklogesId;
    private $fn;
    private $utf8Encoding;
    private $handle;
        
    public function __construct(EntityManager $manager)
    {
        $this->em=$manager;
    }
    
    /**
     * Set (inject) cache handler
     * @param \Gnosis\ElectionsImportersBundle\Cache\RegionImportCache $cc
     */
    public function setCacheLdr(\Gnosis\ElectionsImportersBundle\Cache\RegionImportCache $cc) {
        $this->cacheLdr = $cc;
    }

    /**
     * Setups components (cache, processors and cleaners) for nomos
     * @param \Gnosis\ElectionsBaseBundle\Entities\RegionNomos $nomos
     * @param string $fn    file to import
     */
    public function setup($nomos, $fn, $utf8Encoding) {
        $this->cacheLdr->setup($nomos);
        $this->cacheLdr->readCacheRegionIndex();
        $this->nomos = $nomos;
        $this->eklogesId = $nomos->getElection()->getId();
        $this->fn = $fn;
        $this->utf8Encoding = $utf8Encoding;
        return $this;
    }

    public function import() {
        if( $this->openFile($this->fn) ) {
            $this->cleanup();
            $lineNum = 0;
            while($row = $this->getDataLine()) {
                if( count($row)>=2 ) {
                    $lineNum = $this->processLine($lineNum,$row);
                }
            }
            printf("%s lines read\n", $lineNum);
            $this->closeFile();
        }
    }
    
    /**
     * opens input file
     * 
     * @param string $filename
     * @return boolean
     */
    private function openFile($filename) {
        $out = false;
        if(file_exists($filename)) {
            if( ($this->handle = fopen($filename,"r")) ) {
                $out = true;
            }
        }
        return $out;
    }
    
    /**
     * Reads line from csv file 
     * 
     * @return array
     */
    private function getDataLine() {
        $data = null;
        if( $this->handle ) {
            $data = fgetcsv($this->handle,0,self::DELIMETER);
        }
        return $data;
    }
    
    /**
     * closes input file
     * 
     */
    private function closeFile() {
        if( $this->handle ) {
            fclose($this->handle);
        }
    }

    protected function fixEncoding($text) {
        if( !$this->utf8Encoding ) {
            $text = iconv("ISO-8859-7", "UTF-8",$text);
            $text = str_replace('’', 'A', $text);
        }
        return $text;
    }

    private function processLine($lineNum,$row) {
        $etSlug = GreekText::slugify($this->fixEncoding($row[0]));
        $lawyer = $this->fixEncoding($row[1]);
        $etId = $this->getEtId($etSlug);
        $lineNum++;
        //printf("%s etSlug=%s etId=%s lawyer=%s\n", $lineNum, $etSlug, $etId, $lawyer);
        $this->em->getConnection()->insert('lawyer', array(
            'region_id' => $etId,
            'ekloges_id' => $this->eklogesId,
            'fullname' => $lawyer
        ));
        return $lineNum;
    }
    
    private function getEtId($slug) {
        $out = null;
        $skey = $this->cacheLdr->toRegionKey('et', $slug);
        if( $this->cacheLdr->hasKey($skey)) {
            $out = $this->cacheLdr->getKey($skey)['id'];
        }
        return $out;
    }
    
    private function cleanup() {
        $this->em->getConnection()->delete('lawyer', array('ekloges_id'=>$this->eklogesId));
    }

}
