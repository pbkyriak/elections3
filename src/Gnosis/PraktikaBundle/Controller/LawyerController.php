<?php

namespace Gnosis\PraktikaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Gnosis\PraktikaBundle\Entity\Lawyer;
use Gnosis\PraktikaBundle\Form\LawyerType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Οργάνωση")
 * @Breadcrumb("Δικαστικοί", route="elections_admin_lawyers")
 * @Security("has_role('ROLE_RADMIN')")
 * @CurrentMenuItem("elections_admin_lawyers")
 */
class LawyerController extends Controller {

    
    public function indexAction()
    {
        $form = $this->getFilterForm();
        $query = $this->makeListQuery($form);
        $request = $this->get('request');
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1) /* page number */,
            $this->get('request')->query->get('limit', 15)
        );
        return $this->render('GnosisPraktikaBundle:Lawyer:index.html.twig',
                array(
                'pagination' => $pagination,
                'filterForm' => $form->createView()
        ));
    }

    /**
     * Create list query
     */
    private function makeListQuery($form)
    {
        $em = $this->getDoctrine()->getManager();
        $nomos = $em->getRepository("GnosisElectionsBaseBundle:RegionNomos")->find($this->get('session')->get('nomos_id'));
        $dql = "SELECT a,r FROM GnosisPraktikaBundle:Lawyer a LEFT JOIN a.region r";
        $form->handleRequest($this->get('request'));
        $filterQryParams = array();
        $where = array();
        $where[] = " a.election=:ekl ";
        $filterQryParams['ekl'] = $this->get('session')->get('election_id');
        $where[] = " r.id_path  like :nom ";
        $filterQryParams['nom'] = sprintf("%s,%%",$nomos->getIdPath());
        $this->get('request')->setLocale('el');
        if ($form->isValid()) {
            $filterData = $form->getData();
            if ($filterData['fullname']) {
                $where[] = " a.fullname like :fullname ";
                $filterQryParams['fullname'] = str_replace('*', '%',$filterData['fullname']);
            }
        }
        if ($where) {
            $dql .= ' where ' . implode(' and ', $where);
        }

        $query = $em->createQuery($dql);
        $query->setParameters($filterQryParams);
        return $query;
    }

    /**
     * Creates filter form
     *
     */
    private function getFilterForm()
    {
        $filterData = array();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData,
                array('method' => 'GET', 'csrf_protection' => false))
            ->add(
                'fullname', 'text', array('required' => false, 'label' => 'Όνομα')
            )
            ->getForm();
        return $form;
    }

    /**
     * Creates a new Project entity.
     * @Breadcrumb("Προβολή")
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GnosisPraktikaBundle:Lawyer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lawyer entity.');
        }

        return $this->render('GnosisPraktikaBundle:Lawyer:show.html.twig',
                array(
                'entity' => $entity,
                )
            );

    }
    
    /**
     * Creates a new Project entity.
     * @Breadcrumb("Προσθήκη")
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $election = $em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($this->get('session')->get('election_id'));
        $entity = new Lawyer();
        $entity->setElection($election);
        $form = $this->createForm(new LawyerType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info', 'gsprod.general.record_saved');
            return $this->redirect($this->generateUrl('elections_admin_lawyers_show',
                        array('id' => $entity->getId())));
        } else {
            $this->get('session')->getFlashBag()->add('notice', 'gsprod.general.record_errors');
        }

        return $this->render('GnosisPraktikaBundle:Lawyer:new.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Project entity.
     * @Breadcrumb("Προσθήκη")
     */
    public function newAction()
    {
        $entity = new Lawyer();
        $form = $this->createForm(new LawyerType(), $entity);

        return $this->render('GnosisPraktikaBundle:Lawyer:new.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Project entity.
     * @Breadcrumb("Μεταβολή")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GnosisPraktikaBundle:Lawyer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lawyer entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('GnosisPraktikaBundle:Lawyer:edit.html.twig',
                array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Project entity.
     *
     * @param Ekloges $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Lawyer $entity)
    {
        $form = $this->createForm(new LawyerType(), $entity,
            array(
            'action' => $this->generateUrl('elections_admin_lawyers_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));


        return $form;
    }

    /**
     * Edits an existing Project entity.
     * @Breadcrumb("Μεταβολή")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('GnosisPraktikaBundle:Lawyer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lawyer entity.');
        }

        $editForm = $this->createForm(new LawyerType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info', 'gsprod.general.record_saved');
            return $this->redirect($this->generateUrl('elections_admin_lawyers_show', array('id' => $id)));
        } else {
            $this->get('session')->getFlashBag()->add('notice', 'gsprod.general.record_errors');
        }


        return $this->render('GnosisPraktikaBundle:Lawyer:edit.html.twig',
                array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a Project entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('GnosisPraktikaBundle:Lawyer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lawyer entity.');
        }
        $em->remove($entity);
        $em->flush();
        $this->get('session')->getFlashBag()->add('info', 'gsprod.general.record_deleted');

        return $this->redirect($this->generateUrl('elections_admin_lawyers'));
    }

}
