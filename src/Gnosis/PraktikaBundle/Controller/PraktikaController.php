<?php

namespace Gnosis\PraktikaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Gnosis\ElectionsEntryBundle\Form\EtSelectType;
use Gnosis\PraktikaBundle\Form\PraktikoEntryType;
use Gnosis\PraktikaBundle\Entity\PraktikoEntry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
/**
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Καταχώρηση")
 * @Breadcrumb("Πρακτικά", route="election_admin_praktika_etsearch")
 * @Security("has_role('ROLE_PRAKT')")
 * @CurrentMenuItem("election_admin_praktika_etsearch")
 */
class PraktikaController extends Controller {
    private $eklogesId;
    private $nomosId;
    private $election;
    private $nomos;
    private $region;
    private $lawyer;
    private $entryExists = false;
    
    public function etSelectAction(Request $request) {
        $form = $this->createForm(new EtSelectType());

        $viewParams = array(
            'form' => $form->createView(),
            'printEt' => false,
        );
        if( $request->get('print', null) ) {
            $viewParams['printEt'] = $request->get('print');
        }
        return $this->render('GnosisPraktikaBundle:Praktika:etSelect.html.twig', $viewParams);
    }
    
    /**
     * an yparxei to tmima redirect sto enterResults allios redirect piso sto etSelect
     * KeepEntryLock("praktika")
     */
    public function etSearchAction(Request $request) {
        $form = $this->createForm(new EtSelectType());
        $form->handleRequest($this->get('request'));
        $data = $form->getData();
        $et = $data['et_number'];
        $this->entryExists = false;
        if( !$this->canUpdateEt($et) ) {
            if($this->entryExists) {
                return $this->redirect($this->generateUrl('gnosis_elections_praktika_show', array('etId'=>$this->region->getId())));
            }
            else {
                return $this->redirect($this->generateUrl('election_admin_praktika_etsearch'));
            }
        }
        else {
            $key = uniqid();
            $this->get('session')->set('entry_key', $key);
            return $this->forward('GnosisPraktikaBundle:Praktika:enter', array('etId'=>$this->region->getId(), 'key'=>$key));
        }
    }

    /**
     * KeepEntryLock("praktika")
     */
    public function enterAction($etId, $key, Request $request) {
        if( !$this->isEntryKeyValid($key) ) {
            return $this->redirect($this->generateUrl('election_admin_praktika_etsearch'));            
        }
        $em = $this->getDoctrine()->getManager();
        $this->loadRequired(null, $etId);
        
        $repo = $this->container->get('gnosis_elections_entry_base.psifoi_manager');
        $entity = $this->getEtRecord($this->election, $this->region);
        
        $form = $this->createForm(new PraktikoEntryType(), $entity);
        return $this->render('GnosisPraktikaBundle:Praktika:entry.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }
    
    public function updateAction($etId, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $this->loadRequired(null, $etId);
        $entity = $this->getEtRecord($this->election, $this->region);
        $form = $this->createForm(new PraktikoEntryType(), $entity);
        $form->submit($request);

        if ($form->isValid()) {
            if( $this->updateEtEntry($entity) ) {
                $this->get('session')->getFlashBag()->add('info','Αποθηκεύτηκε πρακτικό του τμήματος '.$this->region->getTitle());
                return $this->redirect($this->generateUrl('election_admin_praktika_etsearch', array('print'=> $this->region->getId())));
            }
        }
        else {
            $this->get('session')->getFlashBag()->add('notice','gsprod.general.record_errors');
        }
        return $this->render('GnosisPraktikaBundle:Praktika:entry.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));

    }
    
    public function printAction($etId) {
        $em = $this->getDoctrine()->getManager();
        $this->loadRequired(null, $etId);
        $entity = $this->getEtRecord($this->election, $this->region);
        if ($this->container->has('profiler')) {
            $this->container->get('profiler')->disable();
        }

        return $this->render('GnosisPraktikaBundle:Praktika:print.html.twig',
                array(
                'nomos' => $this->nomos,
                'entity' => $entity,
        ));
    }
    
    public function showAction($etId) {
        $this->loadRequired(null, $etId);
        $entity = $this->getEtRecord($this->election, $this->region);
        return $this->render('GnosisPraktikaBundle:Praktika:show.html.twig',
                array(
                'nomos' => $this->nomos,
                'entity' => $entity,
        ));
        
    }
    
    public function deleteAction($etId) {
        $this->loadRequired(null, $etId);
        $entry = $this->getDoctrine()
                ->getRepository('GnosisPraktikaBundle:PraktikoEntry')
                ->findOneBy(array('region'=>$this->region));
        if( $entry ) {
            $this->getDoctrine()->getManager()->remove($entry);
            $this->getDoctrine()->getManager()->flush();
            $this->get('session')->getFlashBag()->add('info', $this->get('translator')->trans('elections.praktika.record_deleted'));
        }
        else {
            $this->get('session')->getFlashBag()->add('notice', $this->get('translator')->trans('elections.praktika.record_not_found', array('%et%' => $etId)));
        }
        return $this->redirect($this->generateUrl('election_admin_praktika_etsearch'));
    }
    
    public function printAllAction() {
        $this->loadRequired();
        $rows = $this->getDoctrine()->getRepository('GnosisPraktikaBundle:PraktikoEntry')->getNomosEntries($this->nomos);
        return $this->render('GnosisPraktikaBundle:Praktika:printall.html.twig',
                array(
                'nomos' => $this->nomos,
                'ekloges' => $this->election,
                'rows' => $rows,
        ));        
    }
    
    private function loadRequired($et=null, $etId=null) {
        $em = $this->getDoctrine()->getManager();
        $this->eklogesId = $this->get('session')->get('election_id');
        $this->nomosId = $this->get('session')->get('nomos_id');
        $this->election = $em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($this->eklogesId);
        $this->nomos = $em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->find($this->nomosId);
        $this->region = null;
        if($et) {
            $this->region = $em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($this->election, $this->nomos, $et);
        }
        elseif($etId) {
            $this->region = $em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->find($etId);
        }
        if( $this->region ) {
        $this->lawyer = $this->getDoctrine()->getRepository('GnosisPraktikaBundle:Lawyer')->findOneBy(array('region'=>$this->region));
        }
        else {
            $this->lawyer = null;
        }
    }
    
    private function canUpdateEt($et=null, $etId=null) {
        $this->loadRequired($et, $etId);
        if( !$this->election->getActive() ) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('elections.psifoi.elections_not_active'));
            return false;
        }
        if(!$this->region) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('elections.psifoi.et_not_found', array('%et%' => ($et? $et : $etId))));
            return false;
        }
        if(!$this->lawyer) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('elections.praktika.no_lawyer_set', array('%et%' => ($et? $et : $etId))));
            return false;
        }
        if( $this->getDoctrine()->getRepository('GnosisPraktikaBundle:PraktikoEntry')->hasEtPraktiko($this->region->getId()) ) {
            $this->get('session')->getFlashBag()->add('notice', $this->get('translator')->trans('elections.praktika.has_entry'));
            $this->entryExists = true;
            return false;
        }
        
        $lockmgr = $this->container->get('gnosis_elections_entry_base.lock_manager');
        $out = $lockmgr->lockForPraktika($this->region,$this->getUser()->getId());
        if($out) {
            $this->get('session')->set('et_lock', array('type'=>'praktika', 'et'=>$this->region->getId(), 'user_id'=>$this->getUser()->getId()));
        }
        else {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('elections.praktika.et_is_locked'));
            return false;
        }
        
        return true;
    }
    
    private function isEntryKeyValid($entryKey) {
        $out = false;
        if($this->get('session')->has('entry_key')) {
            $sessKey = $this->get('session')->get('entry_key');
            if(!empty($sessKey) && !empty($entryKey) && $sessKey==$entryKey) {
                $out = true;
            }
            $this->get('session')->remove('entry_key');
        }
        return $out;
    }

    private function getEtRecord($election, $region) {
        $entry = $this->getDoctrine()
                ->getRepository('GnosisPraktikaBundle:PraktikoEntry')
                ->findOneBy(array('region'=>$region));
        if(!$entry) {
            $entry = new PraktikoEntry();
            $entry->setElection($election)
                    ->setRegion($region)
                    ->setLawyer($this->lawyer)
                    ->setRemarks('');
                    
        }
        return $entry;
    }
    
    private function updateEtEntry(PraktikoEntry $entity) {
        if( !$entity->getId() ) {   // new record! get PaktikoId from service
            $rpc = $this->get('slx_rabbit_mq.rpccall');
            $data = array('key'=>sprintf("praktika_%s", $this->nomosId));
            $prId = $rpc->call(json_encode($data));
            $entity->setEntryId($prId);
            $entity->setUpdatedAt(new \DateTime());
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        return true;
    }
}
