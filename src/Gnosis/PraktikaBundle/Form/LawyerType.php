<?php

namespace Gnosis\PraktikaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LawyerType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add(
                    'fullname', 
                    null, 
                    array(
                        'label' => 'elections.lawyers.fullname',
                        )
                    )
            ->add(
                'region', 'region_selector',
                array(
                'required' => true,
                'label' => 'elections.lawyers.region',
                'list_route' => 'elections_admin_regions_json_list',
                'list_route_params' => array('level' => 6),
                'item_route' => 'elections_admin_regions_json_getItem',
                'entity' => 'region',
                'attr' => array(
                    'class' => 'select2obj col-md-12 select2',
                    'data-inited' => 0,
                    ),
                )
            )
            ->add('save', 'submit', array('label' => 'Αποθήκευση', 'attr' => array('class' => 'btn blue')))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Gnosis\PraktikaBundle\Entity\Lawyer'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'gnosis_praktikabundle_lawyer';
    }

}
