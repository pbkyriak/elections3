<?php
namespace Gnosis\ElectionsPhoneBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
/**
 * Description of YpesToPhoneRepository
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Apr 24, 2017
 */
class YpesToPhoneRepository extends EntityRepository {

    public function getEntriesList($nomos, $listType='all') {
        $qb = $this->createQueryBuilder('p')
                ->select("et.id et_id,et.ekltm_code,p.status, p.timestamp, et.title, GROUP_CONCAT(CONCAT(ye.status,':',ye.timestamp)) yestatus")
                ->leftJoin('p.region', 'et')
                ->leftJoin('GnosisElectionsExportsBundle:YpesToExport', 'ye','WITH', 'ye.et_id=p.region')
                ->where('et.id_path like :idp')
                ->groupBy('p.region')
                ->orderBy('et.id')
                ->setParameter('idp', sprintf("%s,%%", $nomos->getIdPath()))
                ;
        if( $listType=='new' || $listType=='sent' ) {
            $qb->andWhere('p.status=:st')
                    ->setParameter('st', $listType);
        }
        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);

    }

    public function getNomosLog($nomos) {
        $qb = $this->createQueryBuilder('r')
                ->select("et.title, r.timestamp")
                ->leftJoin('r.region', 'et')
                ->where('r.election=:ekl')
                ->andWhere('et.id_path like :idp')
                ->orderBy('et.id')
                ->setParameter('idp', sprintf("%s,%%", $nomos->getIdPath()))
                ->setParameter('ekl', $nomos->getElection());
        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }
}
