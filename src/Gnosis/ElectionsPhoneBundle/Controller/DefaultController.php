<?php

namespace Gnosis\ElectionsPhoneBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Gnosis\ElectionsEntryBundle\Form\PsifoiEntryType;
use Gnosis\ElectionsEntryBaseBundle\Annotation\KeepEntryLock;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Gnosis\ElectionsPhoneBundle\Form\PhoneSendType;

/**
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Καταχώρηση", route="gnosis_elections_entry_psifoi")
 * @Breadcrumb("Τηλεφωνική αποστολή", route="gnosis_elections_phone_index")
 * @CurrentMenuItem("gnosis_elections_phone_index")
 * @Security("has_role('ROLE_PHONE')")
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class DefaultController extends Controller {

    private $eklogesId;
    private $nomosId;
    private $election;
    private $nomos;
    private $region;

    private function loadRequired($et=null, $etId=null) {
        $em = $this->getDoctrine()->getManager();
        $this->eklogesId = $this->get('session')->get('election_id');
        $this->nomosId = $this->get('session')->get('nomos_id');
        $this->election = $em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($this->eklogesId);
        $this->nomos = $em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->find($this->nomosId);
        $this->region = null;
        if($et) {
            $this->region = $em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($this->election, $this->nomos, $et);
        }
        elseif($etId) {
            $this->region = $em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->find($etId);
        }
    }

    public function indexAction(Request $request) {
        $this->loadRequired();
        $trans = ['new'=> 'Νέο', 'update'=>'Ενημέρωση', 'sent'=>'Αποστάλθηκε'];
        $em = $this->getDoctrine()->getManager();
        $listType = $request->get('list_type', 'all');
        $list = $em->getRepository('GnosisElectionsPhoneBundle:YpesToPhone')->getEntriesList($this->nomos, $listType);
        if($list) {
            foreach($list as $idx => $item) {
                $yestatus = [];
                if($item['yestatus']) {
                    $sts = explode(',', $item['yestatus']);
                    foreach($sts as $st) {
                        $ss = explode(':', $st);
                        $yestatus[] = ['status'=> str_replace(['new', 'update'], ['Νέο', 'Ενημέρωση'],$ss[0]), 'timestamp'=>$ss[1]];
                    }
                }
                $list[$idx]['yestatus'] = $yestatus;

                $list[$idx]['trstatus'] = isset($trans[$list[$idx]['status']]) ? $trans[$list[$idx]['status']] : 'N/A';
            }
        }
        return $this->render('GnosisElectionsPhoneBundle:Default:index.html.twig', array('list' => $list, 'list_type'=>$listType));
    }

    /**
     *
     * @param Request $request
     * @KeepEntryLock("phone")
     */
    public function readResultsAction(Request $request, $etId) {
        if(!$this->canUpdateEt(null, $etId) ) {
            return $this->redirect($this->generateUrl('gnosis_elections_phone_index'));
        }
        $em = $this->getDoctrine()->getManager();
        $this->loadRequired(null, $etId);
        $mgr = $this->container->get('gnosis_elections_entry_base.psifoi_manager');
        $entity = $this->getEtRecord($mgr, $this->election, $this->region);
        if(!$entity) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('elections.phone.no_psifoi_for_et'));
            return $this->redirect($this->generateUrl('gnosis_elections_phone_index'));
        }
        $form = $this->createForm(new PsifoiEntryType(), $entity, ['readonly'=>true]);
        $eform =  $this->createForm(new PhoneSendType(), ['et_id'=>$etId]);
        return $this->render('GnosisElectionsPhoneBundle:Default:etPsifoi.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
                'eform' => $eform->createView(),
        ));

    }

    public function updateAction(Request $request, $etId) {
        $em = $this->getDoctrine()->getManager();
        $this->loadRequired(null, $etId);
        if($this->region) { // et found
            $entity = $em->getRepository('GnosisElectionsPhoneBundle:YpesToPhone')->findOneBy(['region'=>$this->region]);
            if($entity) {
                $entity->setStatus('sent')
                        ->setTimestamp(time());
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('info', $this->get('translator')->trans('elections.phone.record_updated', ['%et%'=>$this->region->getTitle()]));
            }
        }
        return $this->redirect($this->generateUrl('gnosis_elections_phone_index'));
    }
    private function canUpdateEt($et=null, $etId=null) {
        $this->loadRequired($et, $etId);
        $em = $this->getDoctrine()->getManager();
        if( !$this->election->getActive() ) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('elections.psifoi.elections_not_active'));
            return false;
        }
        if(!$this->region) {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('elections.psifoi.et_not_found', array('%et%' => ($et? $et : $etId))));
            return false;
        }
        $lockmgr = $this->container->get('gnosis_elections_entry_base.lock_manager');
        $out = $lockmgr->lockForPhone($this->region,$this->getUser()->getId());
        if($out) {
            $this->get('session')->set('et_lock', array('type'=>'phone', 'et'=>$this->region->getId(), 'user_id'=>$this->getUser()->getId()));
        }
        else {
            $this->get('session')->getFlashBag()->add('error', $this->get('translator')->trans('elections.psifoi.et_is_locked'));
            return false;
        }
        return true;
    }

    private function getEtRecord($mgr, $election, $region, $mute=false) {
        $entity = $mgr->fetchEtEntry($election, $region);
        return $entity;
    }
}
