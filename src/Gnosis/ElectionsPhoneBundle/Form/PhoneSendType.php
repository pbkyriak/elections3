<?php
namespace Gnosis\ElectionsPhoneBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Description of PhoneSendType
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Apr 24, 2017
 */
class PhoneSendType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('et_id', 'hidden', [])
                ->add('save', 'submit', array('label'=>'Αποθήκευση','attr'=>array('class'=>'btn blue')))
                ;
    }

    public function getName() {
        return 'phone_send_form';
    }
}
