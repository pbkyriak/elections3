<?php
namespace Gnosis\ElectionsPhoneBundle\Listener;

use Gnosis\ElectionsEntryBaseBundle\Event\PsifoiChangeEvent;
use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsPhoneBundle\Entity\YpesToPhone;

/**
 * Description of PsifoiEntryListener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class PsifoiEntryListener {
    /** @var \Doctrine\ORM\EntityManager */
    protected $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    public function onInsert(PsifoiChangeEvent $event) {
        $et = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->find($event->getEtId());
        $this->newRecord($et);
    }
    
    public function onUpdate(PsifoiChangeEvent $event) {
        $et = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->find($event->getEtId());
        $p = $this->em->getRepository('GnosisElectionsPhoneBundle:YpesToPhone')->findOneBy(['region'=>$et]);
        if($p) {
            $p->setStatus('update');
            $p->setTimestamp(0);
            $this->em->persist($p);
            $this->em->flush($p);
        }
        else {
            $this->newRecord($et);
        }
    }

    public function onDelete(PsifoiChangeEvent $event) {
        $this->em->getConnection()->delete('ypes_to_phone', array('et_id'=>$event->getEtId()));
    }

    private function newRecord($et) {
        $p = new YpesToPhone();
        $p->setElection($et->getElection());
        $p->setRegion($et);
        $p->setStatus('new');
        $p->setTimestamp(0);
        $this->em->persist($p);
        $this->em->flush($p);
    }
}
