<?php
namespace Gnosis\ElectionsExportsBundle\Listener;

use Gnosis\ElectionsEntryBaseBundle\Event\PsifoiChangeEvent;
use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsExportsBundle\Entity\YpesToExport;

/**
 * Description of PsifoiEntryListener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class PsifoiEntryListener {
    /** @var \Doctrine\ORM\EntityManager */
    protected $em;
    
    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    public function onInsert(PsifoiChangeEvent $event) {
        $this->updateEt($event->getEtId(), 'new');
    }
    
    public function onUpdate(PsifoiChangeEvent $event) {
        $this->updateEt($event->getEtId(), 'update');
    }

    public function onDelete(PsifoiChangeEvent $event) {
        $this->em->getConnection()->delete('ypes_to_export', array('et_id'=>$event->getEtId()));
    }

    private function updateEt($etId, $status) {
        $et = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->find($etId);
        $eklogesId = $et->getElection()->getId();
        $this->em->getConnection()->insert('ypes_to_export', array('et_id'=>$etId, 'status'=>$status, 'ekloges_id'=>$eklogesId, 'timestamp'=>0));
    }
}
