<?php

namespace Gnosis\ElectionsExportsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsExportsBundle\Exporter\ExporterTotalPsifoi;
use Gnosis\ElectionsExportsBundle\Exporter\ExporterTotalStauroi;
/**
 * Description of ExportTotalFileZeroCommand
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ExportTotalFileMMECommand extends ContainerAwareCommand {

    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;

    protected function configure() {
        $this
                ->setName('elections:export:mme:totals')
                ->setDescription("Export totals for MME")
                ->addArgument('eklogesSlug', InputArgument::REQUIRED, 'ekloges to export?')
                ->addArgument('nomosSlug', InputArgument::REQUIRED, 'nomos to export?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $t1 = time();
    
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $eklogesSlug = $input->getArgument('eklogesSlug');
        $nomosSlug = $input->getArgument('nomosSlug');
        
        $output->writeln("<info>Ekloges slug=</info>".$eklogesSlug);
        $output->writeln("<info>Nomos slug=</info>".$nomosSlug);
        
        $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug'=>$eklogesSlug));
        if( !$ekloges ) {
            $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlug));
            return;
        }
        
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('election'=>$ekloges, 'slug'=>$nomosSlug));
        if( !$nomos ) {
            $output->writeln(sprintf("<error>No nomos found with slug %s</error>", $nomosSlug));
            return;
        }

        $exporter = new ExporterTotalPsifoi();
        $exporter->export($this->em, $output, $ekloges, $nomos, $nomosSlug);
        
        if( $ekloges->getHasStaurous()) {
            $exporter = new ExporterTotalStauroi();
            $exporter->export($this->em, $output, $ekloges, $nomos, $nomosSlug);            
        }
        
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
    }

}
