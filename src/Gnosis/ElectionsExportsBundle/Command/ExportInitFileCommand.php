<?php

namespace Gnosis\ElectionsExportsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;

/**
 * Description of ExportInitFileCommand
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ExportInitFileCommand extends ContainerAwareCommand {

    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    private $handle;
    private $ets;
    private $synd;
    private $ypops;
    
    protected function configure() {
        $this
                ->setName('elections:export:init:default')
                ->setDescription("Export init file")
                ->addArgument('eklogesSlug', InputArgument::REQUIRED, 'ekloges to export?')
                ->addArgument('nomosSlug', InputArgument::REQUIRED, 'nomos to export?')
                ->addOption("with-results",null, InputOption::VALUE_NONE, 'If it will export results lines also')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $t1 = time();
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $eklogesSlug = $input->getArgument('eklogesSlug');
        $nomosSlug = $input->getArgument('nomosSlug');
        $exportResults = $input->getOption('with-results') ? true : false;
        
        $output->writeln("<info>Ekloges slug=</info>".$eklogesSlug);
        $output->writeln("<info>Nomos slug=</info>".$nomosSlug);
        
        $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug'=>$eklogesSlug));
        if( !$ekloges ) {
            $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlug));
            return;
        }
        
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('election'=>$ekloges, 'slug'=>$nomosSlug));
        if( !$nomos ) {
            $output->writeln(sprintf("<error>No nomos found with slug %s</error>", $nomosSlug));
            return;
        }

        $handle = fopen('init-'.$nomosSlug.'.txt', "w+");
        if($handle ) {
            $this->handle = $handle;
            $this->exportEt($ekloges,$nomos);
            $this->exportEgg($ekloges,$nomos);
            $this->exportSynd($ekloges,$nomos);
            $this->exportYpops($ekloges,$nomos);
            if( $exportResults ) {
                $this->exportPsifoi($ekloges,$nomos);
                if( $ekloges->getHasStaurous() ) {
                    $this->exportStauroi($ekloges,$nomos);
                }
            }
            fclose($handle);
        }
        
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
    }
    
    private function exportEt($ekloges, $nomos) {
        $sql = "select 'et',lv3.title t1, lv4.title t2, lv5.title t3, et.title t4
                from region et
                left join region lv5 on (et.parent_id=lv5.id and lv5.level=5)
                left join region lv4 on (lv5.parent_id=lv4.id and lv4.level=4)
                left join region lv3 on (lv4.parent_id=lv3.id and lv3.level=3)
                where et.id_path like :idp and et.region='eklTmima'";
        $params = array(
            'idp' => sprintf("%s,%%", $nomos->getIdPath()),
        );
        $stmt = $this->em->getConnection()->executeQuery($sql, $params);
        $ets = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $this->ets = array();
        foreach($ets as $et) {
            fputcsv($this->handle, $et, "\t");
            $this->ets[] = $et['t4'];
        }
    }
    
    private function exportEgg($ekloges, $nomos) {
        $sql = "select 'eg',title, eggegramenoi, 0 psifisan, 0 leyka, 0 akyra, 0 egkyra from region et
                where et.id_path like :idp and et.region='eklTmima'";
        $params = array(
            'idp' => sprintf("%s,%%", $nomos->getIdPath()),
        );
        $stmt = $this->em->getConnection()->executeQuery($sql, $params);
        $ets = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach($ets as $et) {
            fputcsv($this->handle, $et, "\t");
            
        }
    }
    
    private function exportSynd($ekloges, $nomos) {
        $sql = "select 'sy',title from syndiasmos
                where ekloges_id=:ekl";
        $params = array(
            'ekl' => $ekloges->getId(),
        );
        $stmt = $this->em->getConnection()->executeQuery($sql, $params);
        $ets = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $this->synd = array();
        foreach($ets as $et) {
            fputcsv($this->handle, $et, "\t");
            $this->synd[] = $et['title'];
        }
    }
    private function exportYpops($ekloges, $nomos) {
        $sql = "select 'yp', s.title stitle, y.title ytitle from ypopsifios y left join syndiasmos s on (s.id=y.komma_id) where y.ekloges_id=:ekl";
        $params = array(
            'ekl' => $ekloges->getId(),
        );
        $stmt = $this->em->getConnection()->executeQuery($sql, $params);
        $ets = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $this->ypops = array();
        foreach($ets as $et) {
            fputcsv($this->handle, $et, "\t");
            $this->ypops[] = array($et['stitle'],$et['ytitle']);
        }
    }
    
    private function exportPsifoi($ekloges, $nomos) {
        foreach($this->ets as $et) {
            foreach($this->synd as $sy) {
                $r = array('ps', $et, $sy, 0);
                fputcsv($this->handle, $r, "\t");
            }
        }
    }
    
    private function exportStauroi($ekloges, $nomos) {
        foreach($this->ets as $et) {
            foreach($this->ypops as $sy) {
                $r = array('yp', $et, $sy[0], $sy[1], 0);
                fputcsv($this->handle, $r, "\t");
            }
        }
    }
}
