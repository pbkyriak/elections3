<?php

namespace Gnosis\ElectionsExportsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsExportsBundle\Encoder\Array2XML;

/**
 * Description of ExportYpesCommand
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ExportYpesCommand extends ContainerAwareCommand {

    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;

    protected function configure() {
        $this
                ->setName('elections:export:ypes')
                ->setDescription("Export ypes xml file")
                ->addArgument('eklogesSlug', InputArgument::REQUIRED, 'ekloges to export?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $t1 = time();
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $eklogesSlug = $input->getArgument('eklogesSlug');

        $output->writeln("<info>Ekloges slug=</info>" . $eklogesSlug);

        $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug' => $eklogesSlug));
        if (!$ekloges) {
            $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlug));
            return;
        }

        for($partIdx=1; $partIdx<=123; $partIdx++) {
            $fn = $ekloges->getUploadsDir() . "/ypes-".sprintf("%03d",$partIdx).".xml";
            //$exporter = $this->getContainer()->get('gnosis_elections_exports.ypes_psifoi');
            $exporter = $this->getContainer()->get('gnosis_elections_exports.ypes_psifoi_from_et');
            $exporter->setup($ekloges);
            $cnt = $exporter->export($partIdx);
            if ($cnt) {
                $this->write($exporter->getExportedData(), $fn);
            }
            $output->writeln(sprintf("<info>Ets exported:</info> %s", $cnt));
            $output->writeln(sprintf("<info>Output file:</info> %s", $fn));
        }
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time() - $t1));
    }

    private function write($data, $fn) {
        try {
            $xml = Array2XML::createXML('wsmodel', $data);
            $handle = fopen($fn, "w+");
            if ($handle) {
                fwrite($handle, $xml->saveXML());
                fclose($handle);
            }
        } catch (\Exception $ex) {
            $this->output->writeln(sprintf("<error>Export failed:</error> %s", $ex->getMessage()));
        }
    }

}
