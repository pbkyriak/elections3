<?php
namespace Gnosis\ElectionsExportsBundle\Converter;
/**
 * Description of RawToCrosstabTotals
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class RawToCrosstabTotals {
    
    private $raw;
    private $headerRow;
    private $report;
    
    public function buildCrossTab($ets, $syndiasmoi) {
        $this->headerRow =$this->buildHeader($ets, $syndiasmoi);
        $this->report[0] = $this->headerRow;
        foreach($ets as $row) {
            if(!array_key_exists($row['etId'], $this->report)) {    // new ET
                $this->addEtRow($row);
            }
        }
    }

    /**
     * Adds new row to the report with ET data only (no psifoi or stauroi)
     * @param array $row
     */
    private function addEtRow($row) {
        $etId = $row['etId'];
        $this->report[$etId] = $this->headerRow;
        foreach($this->report[$etId] as $k => $v) {
            if(array_key_exists($k, $row)) {
                $this->report[$etId][$k] = $row[$k];
            }
            else {
                $this->report[$etId][$k] = 0;
            }
        }
        $etNum = $this->getEtNumber($row['etTitle']);
        $this->report[$etId]['et']= $etNum;
        $this->report[$etId]['etTitle'] = sprintf("(%s) %s",$etNum, $this->report[$etId]['etTitle']);
    }

    /**
     * ftaixnei ti header row. Εχει στην αρχη τις σταθερες στήλες για τα δεδομενα του ΕΤ
     * και μετά έχει μια στήλη για κάθε συνδυασμό με key sΣΥΝΔΥΑΣΜΟΣ_ID πχ s831
     * και τιμή το όνομα του συνδυασμού.
     * 
     * @return array
     */
    private function buildHeader($ets, $syndiasmoi) {
        $out = $this->getDefaultHeaderColumns();
        // loop gia na paroume tous syndiasmous
        $synd = array();
        foreach($syndiasmoi as $syndiasmos) {
            $sId = sprintf('s%s',$syndiasmos['sId']);
            if( !array_key_exists($sId, $synd) ) {
                $synd[$sId] = $syndiasmos['titleS'];
            }
            if( array_key_exists('yId',$syndiasmos) ) { // υπαρχουν και δεδομένα υποψηφίων
                $sId = sprintf('y%s',$syndiasmos['yId']);
                if( !array_key_exists($sId, $synd) ) {
                    $synd[$sId] = $syndiasmos['titleY'];
                }                
            }
        }
        return array_merge($out, $synd);
    }

    private function getDefaultHeaderColumns() {
        return array(
            'title3'=>'Δήμος',
            'title4'=>'Εκλογ.Περιφέρεια',
            'title5'=>'Δημ/Τοπ. Κοινότητα',
            'et'=>'Ε.Τ.',
            'etTitle'=>'Περιγραφή Εκλ.Τμήματος',
            'eggegramenoi'=>'Εγγεγραμμένοι',
            'psifisan'=>'Ψήφισαν',
            'leyka'=>'Λευκά',
            'akyra'=>'Άκυρα',
            'egkyra'=>'Έγκυρα',            
        );
        
    }
    
    /**
     * Adds psifoi and stauroi data
     * @param array $row
     */
    public function setData($row) {
        $etId = $row['etId'];
        if(array_key_exists('sId', $row)) { // psifoi data
            $this->addPsifoiData($etId, $row);
        }
        if(array_key_exists('yId', $row)) { // stauroi data
            $this->addStauroiData($etId, $row);
        }

    }
    /**
     * Adds syndiasmos psifoi
     * 
     * @param int $etId
     * @param array $row
     */
    private function addPsifoiData($etId, $row) {
        $sKey = 's'.$row['sId'];
        if(array_key_exists($sKey, $this->report[$etId])) {
            $this->report[$etId][$sKey] = $row['amountP'];
        }
    }

    private function addStauroiData($etId, $row) {
        $sKey = 's'.$row['yId'];
        if(array_key_exists($sKey, $this->report[$etId])) {
            $this->report[$etId][$sKey] = $row['amountS'];
        }
    }

    /**
     * Extracts et number from title
     * 
     * @param string $etTitle
     * @return string
     */
    private function getEtNumber($etTitle) {
        $out = '';
        if( preg_match("/^[0-9]{1,3}/", $etTitle, $matches) ) {
            $out = $matches[0];
        }
        return $out;
    }

    public function getReport() {
        return $this->report;
    }
}
