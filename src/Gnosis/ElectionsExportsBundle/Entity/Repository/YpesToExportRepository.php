<?php
namespace Gnosis\ElectionsExportsBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;
use Gnosis\ElectionsBaseBundle\Entity\RegionNomos;
use Doctrine\ORM\Query;

/**
 * Description of YpesToExportRepository
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Mar 31, 2017
 */
class YpesToExportRepository extends EntityRepository {
    
    public function getNomosLog($nomos) {
        $qb = $this->createQueryBuilder('r')
                ->select("et.title, r.status, r.timestamp, r.sent_timestamp")
                ->leftJoin('GnosisElectionsBaseBundle:RegionEklTmima', 'et','WITH', 'r.et_id=et.id')
                ->where('r.ekloges_id=:eklid')
                ->andWhere('et.id_path like :idp')
                ->orderBy('et.id')->addOrderBy('r.status')
                ->setParameter('idp', sprintf("%s,%%", $nomos->getIdPath()))
                ->setParameter('eklid', $nomos->getElection()->getId());
        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }
}
