<?php
namespace Gnosis\ElectionsExportsBundle\Entity;

/**
 * Description of YpesToExport
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class YpesToExport {
    private $id;
    private $et_id;
    private $status;
    private $timestamp;
    private $ekloges_id;
    private $sent_timestamp=0;
    
    function getId() {
        return $this->id;
    }

    function getEtId() {
        return $this->et_id;
    }

    function getStatus() {
        return $this->status;
    }

    function setId($id) {
        $this->id = $id;
        return $this;
    }

    function setEtId($et_id) {
        $this->et_id = $et_id;
        return $this;
    }

    function setStatus($status) {
        $this->status = $status;
        return $this;
    }

    function getTimestamp() {
        return $this->timestamp;
    }
    
    function setTimestamp($v) {
        $this->timestamp = $v;
        return $this;
    }
    function getEklogesId() {
        return $this->ekloges_id;
    }
    
    function setEklogesId($v) {
        $this->ekloges_id = $v;
        return $this;
    }

    function getSentTimestamp() {
        return $this->sent_timestamp;
    }
    
    function setSentTimestamp($v) {
        $this->sent_timestamp = $v;
        return $this;
    }
}
