<?php

namespace Gnosis\ElectionsExportsBundle\Exporter;

use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;
use Gnosis\ElectionsBaseBundle\Entity\RegionPeriferia;
use Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntry;

/**
 * Description of ExportYpesPsifoi
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ExportYpesPsifoi {
    /**
     *
     * @var EntityManager
     */
    protected $em;
    /** @var Ekloges */
    private $election;
    private $dimoi;
    private $outData;
    private $nomosDepts;
    
    public function __construct($em) {
        $this->em = $em;
    }
    
    public function setup($election) {
        $this->election = $election;
        $this->loadDimoi();
        return $this;
    }
    
    private function loadDimoi() {
        $dimoi = $this->em->getRepository('GnosisElectionsBaseBundle:RegionDimos')->findBy(array('election'=>$this->election));
        $this->dimoi = array();
        foreach($dimoi as $dimos) {
            $this->dimoi[$dimos->getId()] = array(
                'dtitle'=>$dimos->getTitle(), 
                'dotaid'=>$dimos->getYpesOtaId(),
                'ntitle'=>$dimos->getParent()->getTitle(),
                'notaid'=>$dimos->getParent()->getYpesOtaId(),
            );
        }
    }
    
    public function export() {
        $this->outData = array();
        $this->outData['electionType'] = $this->election->getYpesType();
        $this->outData['electoralPeripheries'] = array();

        $ets = $this->getEtToExport();
        $this->nomosDepts = array();
        
        foreach($ets as $etId => $et) {
            $regionEt = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->find($etId);
            if($regionEt) {
                $entry = $this->em
                        ->getRepository('GnosisElectionsEntryBaseBundle:PsifoiEntry')
                        ->getEntryForEt($this->election,$regionEt);
                if( $entry ) {
                    $this->exportEt($entry, $et['status']);
                }
            }
        }
        // now we have foreach periphery records
        $this->addToOutDepts();
        $this->markEts($ets);
        return count($ets);
    }
    
    public function getExportedData() {
        return $this->outData;
    }
    
    private function getEtToExport() {
        $out = array();
        $sql = "select id, et_id, status from ypes_to_export where ekloges_id=:ekl and timestamp=0";
        $stmt = $this->em->getConnection()->executeQuery($sql, array('ekl'=>$this->election->getId()));
        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        foreach($rows as $row) {
            if(array_key_exists($row['et_id'], $out)) {
                if( $out[$row['et_id']]['status']=='update' && $row['status']=='new' ) {
                    $out[$row['et_id']]['status']='new';
                }
                $out[$row['et_id']]['ids'][] = $row['id'];
            }
            else {
                $out[$row['et_id']] = array('status'=>$row['status'], 'ids'=>array($row['id']));
            }
        }
        return $out;
    }
    
    private function exportEt(PsifoiEntry $entry, $status) {
        $rIds = explode(',', $entry->getRegion()->getIdPath());
        $dimos = $this->dimoi[$rIds[2]];
        $out = array(
            'otaId'         => $dimos['dotaid'],
            'otaName'       => $dimos['dtitle'],
            'id'            => $entry->getRegion()->getEtNumber(),
            'registered'    => $entry->getEggegramenoi(),
            'voted'         => $entry->getPsifisan(),
            'valid'         => $entry->getEgkyra(),
            'invalid'       => $entry->getAkyra(),
            'blanc'         => $entry->getLeyka(),
            'update'        => $status=='new' ? 'false' : 'true',
            'eterodim'      => 'false',
            'votes'         => array(),
        );
        $cnt = 1;
        foreach($entry->getPsifoiEntryRows() as $pRow) {
            $out['votes'][] = array(
                    'partyId'   => $cnt,
                    'partyName' => $pRow->getSyndiasmos()->getTitle(),
                    'votesCount'=> $pRow->getAmount(),
            );
            $cnt++;
        }
        if( !array_key_exists($dimos['notaid'], $this->nomosDepts)) {
            $this->nomosDepts[$dimos['notaid']] = array(
                'ntitle' => $dimos['ntitle'],
                'data' => array(),
            );
        }
        $this->nomosDepts[$dimos['notaid']]['data'][] = $out;
    }

    private function addToOutDepts() {
        foreach($this->nomosDepts as $notaId => $nota) {
            $perif = array();
            $perif['peripheryID'] = $notaId;
            $perif['name'] = $nota['ntitle'];
            $perif['departments'] = $nota['data'];
            $this->outData['electoralPeripheries'][] = $perif;
        }
    }
    
    private function markEts($ets) {
        if(count($ets)==0) {
            return;
        }
        $etIds = array();
        foreach($ets as $row) {
            $etIds = array_merge($etIds, $row['ids']);
        }
        $sql = sprintf("UPDATE ypes_to_export SET timestamp=:t, sent_timestamp=0 WHERE id in (%s)", implode(',', $etIds));
        $this->em->getConnection()->executeUpdate($sql, array('t'=>time()));
    }
}
