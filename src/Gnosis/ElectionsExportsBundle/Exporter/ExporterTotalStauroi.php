<?php

namespace Gnosis\ElectionsExportsBundle\Exporter;
use Gnosis\ElectionsExportsBundle\Exporter\ExporterTotalPsifoi;
/**
 * Description of ExporterTotalStauroi
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ExporterTotalStauroi extends ExporterTotalPsifoi {
    public function __construct() {
        $this->fileMaskZ = "%s_stauroi_sygkentrotiko_0.txt";
        $this->fileMaskNZ = "%s_stauroi_sygkentrotiko.txt";
    }

    protected function processData($output, $ekloges, $nomos, $conv) {
        $output->writeln('Processing psifous...');
        $sql = $this->getQueryPsifoi();
        $stmt = $this->getRawDataStmt($sql,$ekloges, $nomos);
        $this->processResults($output, $stmt, $ekloges, $nomos, $conv);    

        $output->writeln('Processing staurous...');
        $sql = $this->getQueryStaurous();
        $stmt = $this->getRawDataStmt($sql,$ekloges, $nomos);
        $this->processResults($output, $stmt, $ekloges, $nomos, $conv);    

    }

    protected  function getSyndiasmoiQuery() {
        return "select s.id sId, s.title titleS, y.id yId, y.title titleY
                from syndiasmos s
                left join ypopsifios y on (s.id=y.komma_id)
                where s.ekloges_id=:ekid";

    }

    protected function getQueryStaurous() {
        return "select et.id etId, 
                rs.ypopsifios_id yId, rs.amount amountS
                from region et
                left join region_stauroi rs on (et.id=rs.region_id)
                where et.id_path like :idp and et.region='eklTmima'";
    }

}
