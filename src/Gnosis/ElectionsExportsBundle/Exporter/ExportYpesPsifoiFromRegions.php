<?php

namespace Gnosis\ElectionsExportsBundle\Exporter;

use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;
use Gnosis\ElectionsBaseBundle\Entity\RegionPeriferia;
use Gnosis\ElectionsEntryBaseBundle\Entity\PsifoiEntry;

/**
 * Description of ExportYpesPsifoiFromRegions
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Nov 22, 2016
 */
class ExportYpesPsifoiFromRegions {
    /**
     *
     * @var EntityManager
     */
    protected $em;
    /** @var Ekloges */
    private $election;
    private $dimoi;
    private $outData;
    private $nomosDepts;
    
    public function __construct($em) {
        $this->em = $em;
    }
    
    public function setup($election) {
        $this->election = $election;
        $this->loadDimoi();
        return $this;
    }
    
    private function loadDimoi() {
        $dimoi = $this->em->getRepository('GnosisElectionsBaseBundle:RegionDimos')->findBy(array('election'=>$this->election));
        $this->dimoi = array();
        foreach($dimoi as $dimos) {
            $this->dimoi[$dimos->getId()] = array(
                'dtitle'=>$dimos->getTitle(), 
                'dotaid'=>$dimos->getYpesOtaId(),
                'ntitle'=>$dimos->getParent()->getTitle(),
                'notaid'=>$dimos->getParent()->getYpesOtaId(),
            );
        }
    }
    
    public function export($partIdx) {
        $this->outData = array();
        $this->outData['electionType'] = $this->election->getYpesType();
        $this->outData['electoralPeripheries'] = array();

        $ets = $this->getEtToExport($partIdx);
        $this->nomosDepts = array();
        
        foreach($ets as $etId => $et) {
            $regionEt = $this->em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->find($et['et_id']);
            $regionPsifoiEntries = $this->em
                    ->getRepository('GnosisElectionsBaseBundle:RegionPsifoi')
                    ->findBy(['region'=>$regionEt]);
            if( $regionPsifoiEntries ) {
                $this->exportEt($regionEt, $regionPsifoiEntries);
            }
            else {
                
            }
        }
        // now we have foreach periphery records
        $this->addToOutDepts();
        return count($ets);
    }
    
    public function getExportedData() {
        return $this->outData;
    }
    
    private function getEtToExport($partIdx) {
        $lim = ($partIdx-1)*10;
        $sql = "SELECT id as et_id FROM region WHERE ekloges_id=:ekl AND region='ekltmima' AND psifisan!=0 limit $lim,10";
        $stmt = $this->em->getConnection()->executeQuery(
                $sql, 
                array(
                    'ekl'=>$this->election->getId(),
                )
                );
        $out = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $out;
    }
    
    private function exportEt(\Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima $regionEt,  $regionPsifoiEntries) {
        var_dump($regionEt->getIdPath());
        $status = 'new';
        $rIds = explode(',', $regionEt->getIdPath());
        $dimos = $this->dimoi[$rIds[2]];
        $out = array(
            'otaId'         => $dimos['dotaid'],
            'otaName'       => $dimos['dtitle'],
            'id'            => $regionEt->getEtNumber(),
            'registered'    => $regionEt->getEggegramenoi(),
            'voted'         => $regionEt->getPsifisan(),
            'valid'         => $regionEt->getEgkyra(),
            'invalid'       => $regionEt->getAkyra(),
            'blanc'         => $regionEt->getLeyka(),
            'update'        => $status=='new' ? 'false' : 'true',
            'eterodim'      => $regionEt->getIsEterodim(),
            'votes'         => array(),
        );
        $cnt = 1;
        foreach($regionPsifoiEntries as $pRow) {
            $out['votes'][] = array(
                    'partyId'   => $cnt,
                    'partyName' => $pRow->getSyndiasmos()->getTitle(),
                    'votesCount'=> $pRow->getAmount(),
            );
            $cnt++;
        }
        if( !array_key_exists($dimos['notaid'], $this->nomosDepts)) {
            $this->nomosDepts[$dimos['notaid']] = array(
                'ntitle' => $dimos['ntitle'],
                'data' => array(),
            );
        }
        $this->nomosDepts[$dimos['notaid']]['data'][] = $out;
    }

    private function addToOutDepts() {
        foreach($this->nomosDepts as $notaId => $nota) {
            $perif = array();
            $perif['peripheryID'] = $notaId;
            $perif['name'] = $nota['ntitle'];
            $perif['departments'] = $nota['data'];
            $this->outData['electoralPeripheries'][] = $perif;
        }
    }
    
}
