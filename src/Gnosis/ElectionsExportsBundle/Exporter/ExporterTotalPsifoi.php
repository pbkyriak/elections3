<?php

namespace Gnosis\ElectionsExportsBundle\Exporter;

use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsExportsBundle\Converter\RawToCrosstabTotals;

/**
 * Description of ExporterTotalPsifoi
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ExporterTotalPsifoi {

    protected $fileMaskZ;
    protected $fileMaskNZ;
    
    public function __construct() {
        $this->fileMaskZ = "%s_psifoi_sygkentrotiko_0.txt";
        $this->fileMaskNZ = "%s_psifoi_sygkentrotiko.txt";
    }
    
    /**
     *
     * @var EntityManager
     */
    protected $em;
    
    public function export($em, $output, $ekloges, $nomos, $nomosSlug) {
        $this->em = $em;
        
        $outPath = $ekloges->getUploadsDir() . DIRECTORY_SEPARATOR . 'downloads' . DIRECTORY_SEPARATOR . $nomosSlug;
        if(!file_exists($outPath)) {
            mkdir($outPath, 0777, true);
        }
        
        $ets = $this->getEts($ekloges, $nomos);
        $syndiasmoi = $this->getSyndiasmoi($ekloges);
        $conv = new RawToCrosstabTotals();
        $conv->buildCrossTab($ets, $syndiasmoi);
        $this->processData($output, $ekloges, $nomos, $conv);
        $report = $conv->getReport();
        $conv = null;
        $fn = $outPath .DIRECTORY_SEPARATOR. sprintf($this->fileMaskZ, $ekloges->getSlug());
        $this->writeFile($fn, $report, $output);
        $fn = $outPath .DIRECTORY_SEPARATOR. sprintf($this->fileMaskNZ, $ekloges->getSlug());
        $this->writeFileNZ($fn, $report, $output);

    }

    protected function processData($output, $ekloges, $nomos, $conv) {
        $output->writeln('Processing psifous...');
        $sql = $this->getQueryPsifoi();
        $stmt = $this->getRawDataStmt($sql,$ekloges, $nomos);
        $this->processResults($output, $stmt, $ekloges, $nomos, $conv);    
    }
    
    protected function getEts($ekloges, $nomos) {
        $sql = "select et.id etId, lv3.title title3, lv4.title title4, lv5.title title5, et.title etTitle, et.eggegramenoi, et.psifisan, et.leyka, et.akyra, et.egkyra
                from region et
                left join region lv5 on (et.parent_id=lv5.id and lv5.level=5)
                left join region lv4 on (lv5.parent_id=lv4.id and lv4.level=4)
                left join region lv3 on (lv4.parent_id=lv3.id and lv3.level=3)
                where et.id_path like :idp and et.region='eklTmima';
                ";        
        $params = array(
            'idp' => sprintf("%s,%%", $nomos->getIdPath()),
        );
        $stmt = $this->em->getConnection()->executeQuery($sql, $params);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    protected function getSyndiasmoi($ekloges) {
        $sql =  $this->getSyndiasmoiQuery();
        $params = array(
            'ekid' => $ekloges->getId(),
        );
        $stmt = $this->em->getConnection()->executeQuery($sql, $params);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);

    }
    
    protected  function getSyndiasmoiQuery() {
        return "select s.id sId, s.title titleS
                from syndiasmos s
                where s.ekloges_id=:ekid";                    
    }


    protected function getRawDataStmt($sql, $ekloges, $nomos) {
        $params = array(
            'idp' => sprintf("%s,%%", $nomos->getIdPath()),
        );
        $this->em->getConnection()->getWrappedConnection()->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false);
        $stmt = $this->em->getConnection()->prepare($sql, array(\PDO::ATTR_CURSOR => \PDO::CURSOR_SCROLL));
        $stmt->execute($params);
        return $stmt;
    }
 
    protected function getQueryPsifoi() {
        return "select et.id etId, rp.syndiasmos_id sId, rp.amount amountP
                from region et 
                left join region_psifoi rp on (et.id=rp.region_id)
                where et.id_path like :idp and et.region='eklTmima';
                ";
    }
    
    protected function processResults($output, $stmt, $ekloges, $nomos, $conv) {
        $cnt =0;
        while($row=$stmt->fetch(\PDO::FETCH_ASSOC, \PDO::FETCH_ORI_NEXT)) {
            $conv->setData($row);
            $cnt++;
            if($cnt % 10000==0) {
                $output->write('.');
            }
        }
        $output->writeln($cnt);
    }
    
    protected function writeFile($fn, $report, $output) {
        $output->writeln(sprintf("<info>writing to file:</info> %s", $fn));
        $handle = fopen($fn, "w+");
        if($handle) {
            foreach($report as $row) {
                fwrite($handle, $this->fixEncoding(implode("\t", $row))."\n");
            }
            fclose($handle);
            $zip = new \ZipArchive();
            $filename = $fn.".zip";
            if ($zip->open($filename, \ZipArchive::CREATE)==TRUE) {
                $zip->addFile($fn, basename($fn));
                $zip->close();
                unlink($fn);
            }

        }
        else {
            $output->writeln(sprintf("<error>Cannot create file:</error> %s", $fn));
        }
    }
    
    protected function writeFileNZ($fn, $report, $output) {
        $output->writeln(sprintf("<info>writing to file:</info> %s", $fn));
        $handle = fopen($fn, "w+");
        if($handle) {
            foreach($report as $row) {
                $skip = false;
                if(is_numeric($row['psifisan'])) {
                    if( $row['psifisan']==0 ) {
                        $skip = true;
                    }
                }
                if(!$skip) {
                    fwrite($handle, $this->fixEncoding(implode("\t", $row))."\n");
                }
            }
            fclose($handle);
            $zip = new \ZipArchive();
            $filename = $fn.".zip";
            if ($zip->open($filename, \ZipArchive::CREATE)==TRUE) {
                $zip->addFile($fn, basename($fn));
                $zip->close();
                unlink($fn);
            }
        }
        else {
            $output->writeln(sprintf("<error>Cannot create file:</error> %s", $fn));
        }
    }
    
    protected function fixEncoding($text) {
        $text = iconv("UTF-8", "ISO-8859-7", $text);
        return $text;
    }

}
