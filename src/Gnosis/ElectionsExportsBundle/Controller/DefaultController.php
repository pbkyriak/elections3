<?php

namespace Gnosis\ElectionsExportsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('GnosisElectionsExportsBundle:Default:index.html.twig', array('name' => $name));
    }
}
