<?php

namespace Gnosis\ElectionsBundle\Exception;

/**
 * Description of EklogesNotFoundException
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 26 Απρ 2014
 */
class EklogesNotFoundException extends \RuntimeException
{
    public function __construct($message = null, \Exception $previous = null, $code = 0)
    {
        parent::__construct($message, $code, $previous);
    }
}
