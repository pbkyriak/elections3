<?php

namespace Gnosis\ElectionsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Gnosis\ElectionsBaseBundle\Entity\Region;
use Gnosis\ElectionsBaseBundle\Entity\RegionNomos;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;
use Gnosis\ElectionsBundle\Exception\EklogesNotFoundException;
use Gnosis\ElectionsBundle\Exception\RegionNotFoundException;
use Gnosis\ElectionsBaseBundle\Util\EklogesConsts;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Finder\Finder;

/**
 * Description of SidebarController
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class SidebarController extends Controller {

    public function lastEntriesAction($eklSlug, $currRegion, $nomos, Request $request) {
        $ekloges = $this->getEklogesFromRequest($eklSlug);
        
        $results = null;
        if( $ekloges ) {
            if( $ekloges->getActive() ) {
                $sql = "select a.id,a.region_id, r.title, r.slug, a.updated_at, a.type, n.slug as nslug
                        from (
                            SELECT id,region_id, updated_at, 'psifoi' as type FROM psifoi_entry where ekloges_id=:eklId
                            union (SELECT id,region_id, updated_at, 'stauroi' as type FROM stauroi_entry where ekloges_id=:eklId)
                            order by updated_at desc limit 0,5
                        ) a
                        left join region as r on (a.region_id=r.id)
                        left join region as n on (n.id= substring(r.id_path,locate(',',r.id_path)+1,locate(',',r.id_path,locate(',',r.id_path)+1)-locate(',',r.id_path)-1) )
                        ";
                $stmt = $this->getDoctrine()->getConnection()->executeQuery($sql,array('eklId'=>$ekloges->getId()));
                $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);
            }
        }

        return $this->render(
                'GnosisElectionsBundle:Sidebar:lastEntries.html.twig',
                array(
                    'ekloges' => $ekloges,
                    'results' => $results,
                    'currRegion' => $currRegion,
                    'nomos' => $nomos,
                ));

    }
    
    public function topStauroiAction($eklSlug, $currRegion, $nomos, Request $request) {
        $ekloges = $this->getEklogesFromRequest($eklSlug);
        
        $results = null;
        if( $ekloges ) {
            $sql = "select yp.title as yptitle, s.title as stitle, rs.amount 
                    from region_stauroi rs
                    left join region r on (r.id=rs.region_id and r.region='periferia' )
                    left join ypopsifios yp on (rs.ypopsifios_id=yp.id)
                    left join syndiasmos s on (yp.komma_id=s.id)
                    where r.ekloges_id=:eklId and rs.amount!=0
                    order by rs.amount desc limit 0,5";
            $stmt = $this->getDoctrine()->getConnection()->executeQuery($sql,array('eklId'=>$ekloges->getId()));
            $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        }

        return $this->render(
                'GnosisElectionsBundle:Sidebar:topVotes.html.twig',
                array(
                    'ekloges' => $ekloges,
                    'results' => $results,
                    'currRegion' => $currRegion,
                    'nomos' => $nomos,
                ));
    }
    
    public function downloadsAction($eklSlug, $currRegionSlug, $nomosSlug, Request $request) {
        $ekloges = $this->getEklogesFromRequest($eklSlug);
        $outPath = $ekloges->getUploadsDir() . DIRECTORY_SEPARATOR . 'downloads' . DIRECTORY_SEPARATOR;
        $nomos= null;
        $regPath = '';
        if( $nomosSlug ) {
            $nomos = $this->getDoctrine()->getManager()->getRepository('GnosisElectionsBaseBundle:Region')->findOneBy(array('slug'=>$nomosSlug, 'election'=>$ekloges));
        }
        if($nomos) {
            $regPath = $nomosSlug;
        }
        $currRegion = $this->getDoctrine()->getManager()->getRepository('GnosisElectionsBaseBundle:Region')->findOneBy(array('slug'=>$currRegionSlug, 'election'=>$ekloges));
        if( $currRegion->getLevel()==2) {
            $regPath = $currRegion->getSlug();
        }
        $basePath = substr($outPath, strpos($outPath,'web/')+4);
        $basePath = $basePath.$regPath.DIRECTORY_SEPARATOR;
        $results = array();
        if($regPath ) {
            $finder = new Finder();
            try {
            $finder->in($outPath.$regPath)->files()->name('*.zip');
            foreach ($finder as $file) {
                $results[] = array(
                    'name' => $file->getFilename(),
                    'path' => $basePath.$file->getFilename(),
                    'size' => $file->getSize(),
                    'date' => date("d/m/Y H:i",$file->getCTime()),
                );
            }
            }
            catch(\Exception $ex) {
                $results = array();
            }
        }
        return $this->render(
                'GnosisElectionsBundle:Sidebar:downloads.html.twig',
                array(
                    'ekloges' => $ekloges,
                    'results' => $results,
                    'currRegion' => $currRegion,
                    'nomos' => $nomos,
                ));
    }

    /**
     * 
     * @param string $eklSlug
     * @return Ekloges
     * @throws EklogesNotFoundException
     */
    private function getEklogesFromRequest($eklSlug) {
        $ekloges = $this->getDoctrine()->getManager()->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug' => $eklSlug));
        if( !$ekloges ) {
            throw new EklogesNotFoundException('Δεν βρέθηκαν οι εκλογές που ζητάτε ('.$eklSlug.')');
        }
        return $ekloges;
    }

}
