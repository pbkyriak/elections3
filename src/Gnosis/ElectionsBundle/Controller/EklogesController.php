<?php

namespace Gnosis\ElectionsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Gnosis\ElectionsBaseBundle\Entity\Region;
use Gnosis\ElectionsBaseBundle\Entity\RegionNomos;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;
use Gnosis\ElectionsBundle\Exception\EklogesNotFoundException;
use Gnosis\ElectionsBundle\Exception\RegionNotFoundException;
use Gnosis\ElectionsBaseBundle\Util\EklogesConsts;
use Symfony\Component\HttpFoundation\Request;
/**
 * Description of EklogesController
 *
 * @Breadcrumb("Αρχική", route="metronic_public_homepage")
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 24 Απρ 2014
 */
class EklogesController extends Controller
{
         
    /*
     * @Breadcrumb("Εκλογές")
     */
    public function indexAction()
    {
        $ekloges = $this->getEklogesFromRequest();
        if (!$ekloges) {
            $this->get('session')->getFlashBag()->add('notice',
                'Δεν βρέθηκαν οι εκλογές που επιλέξατε');
            return $this->redirect($this->generateUrl('metronic_public_homepage'));
        }
        $this->get("apy_breadcrumb_trail")->add($ekloges->getTitle());
        return $this->render('GnosisElectionsBundle:Ekloges:index.html.twig',
                array('ekloges' => $ekloges));
    }
    
    /**
     * action gia psifous periferias
     * 
     * 
     */
    public function lvPeriferiaAction() {
        try {
            $ekloges=$this->getEklogesFromRequest();
        }
        catch( EklogesNotFoundException $ex ) {
            return $this->notFoundFromSlug($ex->getMessage());
        }
        if( $ekloges->getEtype()==EklogesConsts::ELECTION_TYPE_DIMO ) {
            return $this->showDimoi();
        }
        else {
            return $this->showPsifoi();
        }
    }
    
    /**
     * action gia psifous nomou
     * 
     */
    public function lvNomosAction() {
        try {
            $ekloges=$this->getEklogesFromRequest();
        }
        catch( EklogesNotFoundException $ex ) {
            return $this->notFoundFromSlug($ex->getMessage());
        }
        if( $ekloges->getEtype()==EklogesConsts::ELECTION_TYPE_DIMO ) {
            return $this->redirect($this->generateUrl('elections_ekloges_psifoi', array('eklSlug'=>$ekloges->getSlug())));
        }
        else {
            return $this->showPsifoi();
        }
    }
    
    /**
     * action gia psifous kato apo nomo
     * 
     */
    public function lvOtherAction() {
        return $this->showPsifoi();
    }
    
    /**
     * action gia staurous periferias
     * 
     */
    public function lvPeriferiaStauroiAction() {
        try {
            $ekloges=$this->getEklogesFromRequest();
        }
        catch( EklogesNotFoundException $ex ) {
            return $this->notFoundFromSlug($ex->getMessage());
        }
        if( $ekloges->getEtype()==EklogesConsts::ELECTION_TYPE_DIMO ) {
            return $this->showDimoi();
        }
        else {
            return $this->showStauroi();
        }
    }
    
    /**
     * action gia staurous nomou
     * 
     */
    public function lvNomosStauroiAction() {
        try {
            $ekloges=$this->getEklogesFromRequest();
        }
        catch( EklogesNotFoundException $ex ) {
            return $this->notFoundFromSlug($ex->getMessage());
        }
        if( $ekloges->getEtype()==EklogesConsts::ELECTION_TYPE_DIMO ) {
            return $this->redirect($this->generateUrl('elections_ekloges_psifoi', array('eklSlug'=>$ekloges->getSlug())));
        }
        else {
            return $this->showStauroi();
        }
    }

    /**
     * action gia staurous kato apo nomo
     * 
     */
    public function lvOtherStauroiAction() {
        return $this->showStauroi();
    }

    /**
     * Δείχνει τους νομους και τους δημους τους για Δημοτικές εκλογές
     * 
     */
    private function showDimoi() {
        $ekloges=$this->getEklogesFromRequest();
        $periferia = $this->getPeriferiaFromEkloges($ekloges);
        $this->addRegionBreadCrubs($ekloges,$periferia, null, null);
        return $this->render('GnosisElectionsBundle:Ekloges:dimoi.html.twig',
                array(
                    'ekloges' => $ekloges,
                    'periferia' => $periferia,
            ));
    }
    /**
     * Δείχνει τους ψήφους για το επιλεγμένο Region
     * 
     */
    private function showPsifoi() {
        try {
            $ekloges=$this->getEklogesFromRequest();
            $prEkloges = $this->getPrevEkloges($ekloges);
            $periferia = $this->getPeriferiaFromEkloges($ekloges);
            $idPath = $periferia->getIdPath();
            $nomos = $this->getRegionFromRequest($ekloges, 'nomos');
            if( $nomos ) {
                $idPath = $nomos->getIdPath();
            }
            $region = $this->getRegionFromRequest($ekloges, 'slug', $idPath);
            if(!$region) {
                $region = $periferia;
            }
        }
        catch( EklogesNotFoundException $ex ) {
            return $this->notFoundFromSlug($ex->getMessage());
        }
        catch( RegionNotFoundException $ex ) {
            return $this->notFoundFromSlug($ex->getMessage());
        }

        $currRegion = $this->getCurrentRegion($periferia, $nomos, $region);
        $regions = $this->getSubRegions($ekloges,$periferia, $nomos, $region);
        $this->addRegionBreadCrubs($ekloges,$periferia, $nomos, $region);
        
        if( $currRegion->getPrevId() && $prEkloges ) {
            $psifoi = $this->getDoctrine()->getRepository('GnosisElectionsBaseBundle:RegionPsifoi')->getRegionPsifoiAndPrev($currRegion);
            $this->fixPsifoiSyndiasmosLogoPath($ekloges,$psifoi);
            $showPrev = true;
        }
        else {
            $psifoi = $this->getDoctrine()->getRepository('GnosisElectionsBaseBundle:RegionPsifoi')->getRegionPsifoi($currRegion);
            $this->fixPsifoiSyndiasmosLogoPath($ekloges,$psifoi);
            $showPrev = false;
        }
        $psifoiSum = 0;
        foreach($psifoi as $psi) {
            $psifoiSum+=$psi['amount'];
        }
        $pieColorStr = '';
        $chartReport1 = $this->formatPsifoiForChart($psifoi);
        if( $ekloges->getEType()==EklogesConsts::ELECTION_TYPE_PERI || $ekloges->getEType()==EklogesConsts::ELECTION_TYPE_EURO || $ekloges->getEType()==EklogesConsts::ELECTION_TYPE_BOYL ) {
            $pieColors = $this->getSyndColorsForChart($chartReport1,$ekloges);
            if( $pieColors ) {
                $pieColorStr = sprintf('"%s"',implode('","', $pieColors));
            }
        }
        return $this->render('GnosisElectionsBundle:Ekloges:index.html.twig',
                array(
                    'ekloges' => $ekloges,
                    'nomos'=>$nomos,
                    'regions'=>$regions,
                    'currRegion' => $currRegion,
                    'psifoi' => $psifoi,
                    'showPrev' => $showPrev,
                    'prEkloges' => $prEkloges,
                    'rep1' => $chartReport1,
                    'psifoiSum' => $psifoiSum,
                    'pieColors' => $pieColorStr,
            ));
        
    }
      
    /**
     * Δείχνει τους σταυρους για το επιλεγμένο Region
     * 
     */
    private function showStauroi() {

        try {
            $ekloges=$this->getEklogesFromRequest();
            $prEkloges = $this->getPrevEkloges($ekloges);
            $periferia = $this->getPeriferiaFromEkloges($ekloges);
            $idPath = $periferia->getIdPath();
            $nomos = $this->getRegionFromRequest($ekloges, 'nomos');
            if( $nomos ) {
                $idPath = $nomos->getIdPath();
            }
            $region = $this->getRegionFromRequest($ekloges, 'slug', $idPath);
            if(!$region) {
                $region = $periferia;
            }
            $syndiasmos = $this->getSyndiasmosFromRequest($ekloges, $region);
        }
        catch( EklogesNotFoundException $ex ) {
            return $this->notFoundFromSlug($ex->getMessage());
        }
        catch( RegionNotFoundException $ex ) {
            return $this->notFoundFromSlug($ex->getMessage());
        }

        $currRegion = $this->getCurrentRegion($periferia, $nomos, $region);
        $regions = $this->getSubRegions($ekloges,$periferia, $nomos, $region);
        $this->addRegionBreadCrubs($ekloges,$periferia, $nomos, $region, true);
        if($syndiasmos) {
            $stauroi = $this->getDoctrine()->getRepository('GnosisElectionsBaseBundle:RegionStauroi')->getRegionSyndiasmosStauroi($currRegion, $syndiasmos);
        }
        else {
            $stauroi = $this->getDoctrine()->getRepository('GnosisElectionsBaseBundle:RegionStauroi')->getRegionAllStauroi($currRegion, $syndiasmos);
        }
        $showPrev = false;
        $tpl = 'GnosisElectionsBundle:Ekloges:stauroi.html.twig';
        if( !$syndiasmos ) {
            $tpl = 'GnosisElectionsBundle:Ekloges:stauroiForAll.html.twig';
        }
        return $this->render($tpl,
                array(
                    'ekloges' => $ekloges,
                    'nomos'=>$nomos,
                    'regions'=>$regions,
                    'currRegion' => $currRegion,
                    'stauroi' => $stauroi,
                    'showPrev' => $showPrev,
                    'prEkloges' => $prEkloges,
                    'syndiasmos' => $syndiasmos,
            ));

    }
    
    /**
     * helper exceptions
     * 
     */
    private function notFoundFromSlug($msg) {
        $this->get('session')->getFlashBag()->add('notice',$msg);
        return $this->redirect($this->generateUrl('metronic_public_homepage'));
    }
    
    private function getEklogesFromRequest() {
        $request = $this->get('request');
        $eklSlug = $request->get('eklSlug');
        $ekloges = $this->getDoctrine()->getManager()->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug' => $eklSlug));
        if( !$ekloges ) {
            throw new EklogesNotFoundException('Δεν βρέθηκαν οι εκλογές που ζητάτε');
        }
        return $ekloges;
    }
    
    private function getPeriferiaFromEkloges($ekloges) {
        return $this->getDoctrine()
            ->getRepository('GnosisElectionsBaseBundle:RegionPeriferia')
            ->findOneBy(array('election'=>$ekloges));
    }
    
    private function getRegionFromRequest($ekloges, $parameter, $parentPath=null) {
        $region = null;
        $request = $this->get('request');
        $slug = $request->get($parameter);
        if( $slug ) {
            $region = $this->getDoctrine()
                ->getManager()
                ->getRepository('GnosisElectionsBaseBundle:Region')
                ->getSubRegion($ekloges, $slug, $parentPath);
            if(!$region) {
                throw new RegionNotFoundException('Δεν βρέθηκε η περιοχή που ζητάτε');
            }
        }
        return $region;
    }

    private function getCurrentIdPath($periferia, $nomos, $region) {
        $parentPath = '';
        $r = $this->getCurrentRegion($periferia, $nomos, $region);
        if( $r ) {
            $parentPath = $r->getIdPath();
        }
        return $parentPath;
    }

    private function getCurrentRegion($periferia, $nomos, $region) {
        if( $region ) {
            return $region;
        }
        elseif( $nomos ) {
            return $nomos;
        }
        elseif($periferia) {
            return $periferia;
        }
        return null;
    }

    private function getSubRegions($ekloges, $periferia, $nomos, $region) {
        $parentPath = $this->getCurrentIdPath($periferia, $nomos, $region);
        $subRegions = $this->getDoctrine()
            ->getManager()
            ->getRepository('GnosisElectionsBaseBundle:Region')
            ->getSubRegions($ekloges, $parentPath);
        return $subRegions;
    }
    
    private function getSyndiasmosFromRequest($ekloges, $region) {
        $syndiasmos = null;
        $request = $this->get('request');
        $slug = $request->get('sSlug');
        if( $slug ) {
            $syndiasmos = $this->getDoctrine()
                ->getManager()
                ->getRepository('GnosisElectionsBaseBundle:Syndiasmos')
                ->getRegionSyndiasmos($ekloges, $region, $slug);
                //->findOneBy(array('election'=>$ekloges,'slug'=> $slug));
            if(!$syndiasmos) {
                if( $slug!=='forall' ) {
                    throw new RegionNotFoundException('Δεν βρέθηκε o συνδυασμός που ζητάτε');
                }
            }
        }
        return $syndiasmos;
    }
    
    private function addRegionBreadCrubs($ekloges, $periferia, $nomos, $region, $addStauroi=false) {
        // set active menu item
        $this->get('metronic_home.menu_builder')
            ->setCurrentMenuItemUri(
                $this->generateUrl(
                    'elections_ekloges_psifoi', 
                    array('eklSlug'=>$ekloges->getSlug())
                    )
                );
        
        $path = $this->getCurrentIdPath($periferia, $nomos, $region);
        $regs = $this->getDoctrine()
            ->getManager()
            ->getRepository('GnosisElectionsBaseBundle:Region')
            ->getRegionsFromPath($path);
        if( $regs ) {
            foreach($regs as $reg) {
                if( $reg instanceof \Gnosis\ElectionsBaseBundle\Entity\RegionPeriferia ) {
                    $this->get("apy_breadcrumb_trail")->add( 
                        $ekloges->getTitle(),
                        'elections_ekloges_psifoi', array('eklSlug'=>$ekloges->getSlug())
                        );
                }
                elseif( $reg instanceof \Gnosis\ElectionsBaseBundle\Entity\RegionNomos ) {
                    $this->get("apy_breadcrumb_trail")->add( 
                        $reg->getTitle(),
                        'elections_ekloges_psifoi_nomos', array('eklSlug'=>$ekloges->getSlug(), 'slug'=>$reg->getSlug())
                        );
                }
                else {
                    $this->get("apy_breadcrumb_trail")
                        ->add( 
                            $reg->getTitle(),
                            'elections_ekloges_psifoi_other', array('eklSlug'=>$ekloges->getSlug(), 'nomos'=>$nomos->getSlug(), 'slug'=>$reg->getSlug())
                            );
                }
            }
        }
        if( $addStauroi ) {
            $this->get("apy_breadcrumb_trail")->add('Σταυροί');
            
        }
    }
    
    private function getPrevEkloges($ekloges) {
        $out = null;
        if( $ekloges->getPrevId() ) {
            $out = $this->getDoctrine()->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($ekloges->getPrevId());
        }
        return $out;
    }
    
    private function formatPsifoiForChart($psifoi) {
        $numOfParties = 4;
        $out = array();
        for($i=0; $i<$numOfParties+1; $i++) {
            $out[] = array('label'=>'', 'value'=>0, 'slug'=>'', 'color'=>'#efefef');
        }
        $top = array_slice($psifoi, 0, $numOfParties);
        $egkyra = 0;
        $rest = 100;
        foreach($top as $k => $row) {
            $out[$k]['label']=$row['title'];
            $out[$k]['slug']=$row['slug'];
            $out[$k]['color']=$row['color'];
            if( $row['egkyra'] ) {
                $out[$k]['value']=round($row['amount']/$row['egkyra']*100,2);
            }
            else {
                $out[$k]['value']=0;
            }
            $rest = $rest - $out[$k]['value'];
            $egkyra = $row['egkyra'];
        }
        if( $rest!=0 ) {
            $out[$numOfParties]['label']='Λοιποί';
            $out[$numOfParties]['value']=$rest;
            $out[$numOfParties]['color']='#efefef';
        }
        return $out;
    }
    
    private function getSyndColorsForChart($data, $ekloges) {
        $colors = array();
        $em = $this->getDoctrine()->getManager();
        $syndColors = $em->getRepository('GnosisElectionsBaseBundle:SyndiasmosColor')->findBy(array('ekloges_id'=>$ekloges->getId()));
        foreach($data as $row) {
            if( $row['slug'] ) {
                $color = $this->matchSyndiasmos($row['slug'], $syndColors);
                if( $color ) {
                    $c = trim($color->getColor());
                    $c = '#'.(strlen($c)<6 ? '0' : '').$c;
                    $colors[] = $c;
                } 
                else {
                    $colors[] = '#ffffff';
                }
            }
        }
        if( $colors ) {
            $colors[] = '#efefef';
        }
        return $colors;
    }
    
    /**
     * 
     * @param string $rSlug
     * @param array $syndColors
     * @return \Gnosis\ElectionsBase\Bundle\SyndiasmosColor
     */
    private function matchSyndiasmos($rSlug, $syndColors) {
        $out = null;
        foreach($syndColors as $color) {
            $s1 = $color->getSlug();
            $d1 = levenshtein($rSlug, $s1);
            if( $d1<3 ) {
                $out = $color;
                break;
            }
        }
        return $out;
    }
 
    public function etReceivedAction($eklSlug, $nomosSlug, Request $request) {
         try {
            $ekloges=$this->getEklogesFromRequest();
        }
        catch( EklogesNotFoundException $ex ) {
            return $this->notFoundFromSlug($ex->getMessage());
        }
        return $this->showEtReceived();
    }
    
    private function showEtReceived() {
         try {
            $ekloges=$this->getEklogesFromRequest();
            $prEkloges = $this->getPrevEkloges($ekloges);
            $periferia = $this->getPeriferiaFromEkloges($ekloges);
            $idPath = $periferia->getIdPath();
            $nomos = $this->getRegionFromRequest($ekloges, 'nomosSlug');
            if( $nomos ) {
                $idPath = $nomos->getIdPath();
            }
            $region = $this->getRegionFromRequest($ekloges, 'nomosSlug');
            if(!$region) {
                $region = $periferia;
            }
        }
        catch( EklogesNotFoundException $ex ) {
            return $this->notFoundFromSlug($ex->getMessage());
        }
        catch( RegionNotFoundException $ex ) {
            return $this->notFoundFromSlug($ex->getMessage());
        }

        $currRegion = $this->getCurrentRegion($periferia, $nomos, $region);
        $regions = $this->getSubRegions($ekloges,$periferia, $nomos, $region);
        $this->addRegionBreadCrubs($ekloges,$periferia, $nomos, $region);
        
        $sql = "select r.id, r.title, r.slug, if(isnull(ps.id), 'n', 'y') as psifoi, if(isnull(st.id), 'n', 'y') as stauroi from region r 
                left join psifoi_entry ps on (r.id=ps.region_id)
                left join stauroi_entry st on (r.id=st.region_id)
                where r.ekloges_id=:eklId and r.region='eklTmima' and r.id_path like :idp
                having psifoi='y' or stauroi='y' order by r.id";
        $stmt = $this->getDoctrine()
                ->getConnection()
                ->executeQuery(
                        $sql,
                        array(
                            'eklId'=>$ekloges->getId(),
                            'idp' => sprintf("%s,%%", $nomos->getIdPath())
                        )
                );
        $results = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        //$psifoi = $this->getDoctrine()->getRepository('GnosisElectionsBaseBundle:RegionPsifoi')->getRegionPsifoi($currRegion);
         return $this->render('GnosisElectionsBundle:Ekloges:etReceived.html.twig',
                array(
                    'ekloges' => $ekloges,
                    'nomos'=>$nomos,
                    'regions'=>$regions,
                    'currRegion' => $currRegion,
                    'results' => $results,
            ));
       
    }
    
    private function fixPsifoiSyndiasmosLogoPath($ekloges, &$psifoi) {
        if( $psifoi ) {
            $uploader = $this->get('gnosis_elections_entry_base.syndiasmos_logo_uploader');
            $uploader->setElectionSlug($ekloges->getId());
            foreach($psifoi as $idx => $psi) {
                if( isset($psi['logo']) ) {
                    $psifoi[$idx]['logo'] = $uploader->getUploadRelDir().'/'.$psi['logo'];
                }
            }
        }
    }
}