<?php

namespace Gnosis\ElectionsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;

class DefaultController extends Controller
{
    /**
     * @CurrentMenuItem("metronic_public_homepage", menu="home")
     * @return type
     */
    public function indexAction()
    {
        $ekloges = $this->getDoctrine()->getRepository('GnosisElectionsBaseBundle:Ekloges')->getActiveEklogesMashup();
        return $this->render('GnosisElectionsBundle:Default:index.html.twig', array('ekloges'=>$ekloges));
    }
}
