<?php

// src/Acme/MainBundle/Menu/MenuBuilder.php

namespace Gnosis\ElectionsBundle\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityManager;
use Slx\MetronicBundle\Menu\AbstractMenuBuilder;

class MenuBuilder extends AbstractMenuBuilder
{

    /** @var ContainerInterface */
    private $container;

    /** @var Router */
    private $router;

    /**
     * @var SecurityContext      
     */
    private $securityContext;
    /** @var EntityManager */
    private $em;
    
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->router = $container->get('router');
        $this->securityContext = $container->get('security.context');
        $this->em=$container->get('doctrine');
        $this->currentItemRouteName = 'metronic_public_homepage';
    }

    public function createMainMenu(FactoryInterface $factory)
    {
        
        $menu = $factory->createItem('root');
        $menu->addChild('Αρχική', array('route' => 'metronic_public_homepage'))->setAttribute('icon',
            'icon-home');
        $this->addEklogesMenu($menu, 'dimotikes', 'Δημοτικές');
        $this->addEklogesMenu($menu, 'periferiakes', 'Περιφερειακές');
        $this->addEklogesMenu($menu, 'euro', 'Ευρωεκλογές');
        $this->addEklogesMenu($menu, 'bouleytikes', 'Εθνικές');
                
        if($this->getCurrentMenuItemUri()) {
            $this->setCurrentMenuItemFromUri($menu,$this->getCurrentMenuItemUri());
        }
        elseif($this->currentItemRouteName ) {
            $this->setCurrentMenuItem($menu, $this->currentItemRouteName);
        }
        return $menu;
    }

    private function addEklogesMenu($menu, $etype, $title) {
        //$dEkloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findBy(array('published'=>1, 'etype'=>$etype));
        $dEkloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->getPublishedEkloges($etype);
        if( $dEkloges ) {
            $eMenu = $menu->addChild($title);
            foreach($dEkloges as $eklogi) {
                $eMenu->addChild(
                    $eklogi->getTitle(), 
                    array(
                        'route' => 'elections_ekloges_psifoi',
                        'routeParameters' => array('eklSlug' => $eklogi->getSlug())
                        )
                    );
            }
        }
    }
}