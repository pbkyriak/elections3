<?php

namespace Gnosis\ElectionsImportersBundle\Cache;

use Slx\MetronicBundle\Lib\GreekText;
/**
 * Description of RegionImportCache
 * 
 * cache records:
 *  for syndiasmos: id, regionId, hasRegionRows
 *  for ypopsifios: id, regionId, kommaId, hasRegionRows
 *  for region:     id, id_path, parent_slug, parent_id, pids
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 9 Απρ 2014
 */
class RegionImportCache extends AbstractCache
{

    private $lastTitle='';
    private $lastSlug ='';
    /**
     * reads from cache region index
     */
    public function readCacheRegionIndex($warmup=false) {
        $this->regIndex = array();
        try {
            $this->loadCache($warmup);
        }
        catch(\Exception $ex) {
            $this->regIndex = array();
        }
        return $this;
    }
    
    /**
     * Returns an array with cache keys that start with $like.
     * 
     * @param string $like
     * @return array
     */
    public function getKeysForKeyLike($like) {
        $out = array();
        $l = $like.'.';
        reset($this->regionIndex);
        while( list($key,$value) = each($this->regionIndex)){
            if(strpos($key, $l)===0) {
                $out[] = $key;
            }
        }
        return $out;
    }
    
    private function loadCache($warmup=false) {
        $this->regionIndex = array();
        if( $this->hasCache() ) {
            $this->regionIndex = unserialize(file_get_contents($this->getCacheFilename()));
        }
        $this->loadGlobalData();
    }
    
    private function loadGlobalData() {
        // load global syndiasmoi
        $this->loadGlobalSyndiasmoi();
        // load global ypopsifioi
        $this->loadGlobalYpopsifioi();
    }
    
    private function loadGlobalSyndiasmoi() {
        $synd = $this->em->getRepository('GnosisElectionsBaseBundle:Syndiasmos')->getRegionSyndiasmoi($this->nomos->getParent()->getId());
        if( $synd ) {
            $ar = array();
            $first = true;
            $hasRegionRows = false;
            while( list($k, $v) = each($synd) ) {
                if( $first ) {
                    $hasRegionRows = $this->em->getRepository('GnosisElectionsBaseBundle:RegionPsifoi')->hasSyndiasmosRegionRows($v['id'],$this->nomosId);
                    $first = false;
                }
                $ar[$this->toSyndKey($v['slug'])] = array('id'=>$v['id'], 'regionId'=>$v['rid'], 'hasRegionRows'=>$hasRegionRows);
            }
            $this->regionIndex = array_merge($this->regionIndex, $ar);
        }        
    }
    
    private function loadGlobalYpopsifioi() {
        $synd = $this->em->getRepository('GnosisElectionsBaseBundle:Ypopsifios')->getRegionYpopsifioi($this->nomos->getParent()->getId());
        if( $synd ) {
            $ar = array();
            $first = true;
            $hasRegionRows = false;
            while( list($k, $v) = each($synd) ) {
                if( $first ) {
                    $hasRegionRows = $this->em->getRepository('GnosisElectionsBaseBundle:RegionStauroi')->hasYpopsifiosRegionRows($v['id'],$this->nomosId);
                    $first = false;
                }
                $ar[$this->toYpoKey($v['kslug'], $v['slug'])] = array('id'=>$v['id'], 'kommaId'=>$v['kid'], 'regionId'=>$v['rid'], 'hasRegionRows'=>$hasRegionRows);
            }
            $this->regionIndex = array_merge($this->regionIndex, $ar);
        }
    }
    
    public function getSyndiasmosSlug($title) {
        $title = trim($title);
        /*
        if($this->nomosId!=12) {
            if( $title=='Η ΣΤΕΡΕΑ ΣΕ ΝΕΑ ΤΡΟΧΙΑ - ΑΠΟΣΤΟΛΟΥ ΒΑΓΓΕΛΗΣ - ΣΤΟΥΠΗΣ ΝΙΚΟΛΑΟΣ' )  $title = 'Η Στερεά σε νέα τροχιά - Βαγγέλης Αποστόλου';
            if( $title=='ΑΛΛΑΖΟΥΜΕ ΣΤΗΝ ΚΑΡΔΙΑ ΤΗΣ ΕΛΛΑΔΑΣ - ΜΠΑΚΟΓΙΑΝΝΗΣ ΚΩΝΣΤΑΝΤΙΝΟΣ - ΣΠΑΝΟΣ ΦΑΝΗΣ' ) $title = 'αλλάζουμε Στην καρδιά της Ελλάδας';
            if( $title=='ΑΡΙΣΤΕΡΗ ΠΑΡΕΜΒΑΣΗ' ) $title = 'ΑΡΙΣΤΕΡΗ ΠΑΡΕΜΒΑΣΗ ΣΤΗ ΣΤΕΡΕΑ ΕΛΛΑΔΑ';
            if( $title=='ΑΛΛΑΖΟΥΜΕ' ) $title = 'αλλάζουμε Στην καρδιά της Ελλάδας';
            if( $title=='ΕΛΛΗΝΙΚΗ ΑΥΓΗ' ) $title = 'ΕΛΛΗΝΙΚΗ ΑΥΓΗ ΓΙΑ ΤΗΝ ΣΤΕΡΕΑ ΕΛΛΑΔΑ';
            if( $title=='ΕΝΕΡΓΟΙ ΠΟΛΙΤΕΣ' ) $title = 'ΕΝΕΡΓΟΙ ΠΟΛΙΤΕΣ ΣΤΕΡΕΑΣ';
            if( $title=='ΛΑΪΚΗ ΣΥΣΠΕΙΡΩΣΗ' ) $title = 'ΛΑΪΚΗ ΣΥΣΠΕΙΡΩΣΗ ΣΤΕΡΕΑΣ ΕΛΛΑΔΑΣ';
            if( $title=='ΝΕΑ ΤΡΟΧΙΑ' ) $title = 'Η Στερεά σε νέα τροχιά - Βαγγέλης Αποστόλου';
            if( $title=='ΣΥΜΦΩΝΙΑ' ) $title = 'ΘΑΝΑΣΗΣ ΓΙΑΝΝΟΠΟΥΛΟΣ ΣΥΜΦΩΝΙΑ ΓΙΑ ΤΗ ΣΤΕΡΕΑ';
        }
         * 
         */
        if( $this->lastTitle==$title) {
            return $this->lastSlug;
        }
        $slug = GreekText::slugify($title);
        $synds = $this->getKeysForKeyLike('s');
        // remove s.
        foreach($synds as $k => $v) {
            $synds[$k] = str_replace('s.', '', $v);
        }
        $bCheckSimilar = true;
        foreach($synds as $sslug) {
            if( $slug==$sslug ) {
                $bCheckSimilar = false;
                break;
            }
        }
        if( $bCheckSimilar && $this->nomos->getElection()->getEtype()!='dimotikes' ) {
            foreach($synds as $sslug) {
                if(levenshtein($slug, $sslug)<=1) {
                    $slug = $sslug;
                    break;
                }
            }
        }
        $this->lastTitle = $title;
        $this->lastSlug = $slug;
        return $slug;
    }
}
