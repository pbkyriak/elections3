<?php

namespace Gnosis\ElectionsImportersBundle\Cache;

use Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface;
use Doctrine\ORM\EntityManager;
/**
 * Description of RegionCacheWarmer
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class RegionCacheWarmer implements CacheWarmerInterface {
    
    /** @var EntityManager */
    private $em;
    private $warmer;
    
    public function __construct($em, $warmer) {
        $this->em = $em;
        $this->warmer = $warmer;
    }
    
    public function isOptional() {
        return false;
    }

    public function warmUp($cacheDir) {
        $activeEkloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findBy(array('active'=>true));
        foreach($activeEkloges as $ekloges) {
            $nomoi = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findBy(array('election'=>$ekloges));
            foreach($nomoi as $nomos) {
                $this->warmer->setup($nomos);
                $this->warmer->warm();
            }
        }
    }

    
    
}
