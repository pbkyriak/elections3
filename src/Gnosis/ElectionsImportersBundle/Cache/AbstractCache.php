<?php

namespace Gnosis\ElectionsImportersBundle\Cache;

use Doctrine\ORM\EntityManager;
/**
 * Description of AbstractCache
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 23 Απρ 2014
 */
class AbstractCache
{
    
    protected $cacheDir;
    /** @var EntityManager */    
    protected $em;
    /** @var \Gnosis\ElectionsBaseBundle\Entity\RegionNomos */
    protected $nomos;
    protected $nomosId;
    protected $regionIndex;
    
    public function __construct($manager, $cacheDir) {
        $this->em = $manager;
        $this->cacheDir=$cacheDir.'/elections';
        if( !file_exists($this->cacheDir) ) {
            mkdir($this->cacheDir);
        }
    }

    public function setup($nomos) {
        $this->nomos = $nomos;
        $this->nomosId=$nomos->getId();
        return $this;
    }

    protected function getCacheFilename() {
        return sprintf("%s/%s-reg", $this->cacheDir, $this->nomosId);
    }

    /**
     * caches region index to flat text file
     */
    public function cacheRegionIndex() {
        file_put_contents($this->getCacheFilename(), serialize($this->regionIndex));
    }

    /**
     * Returns region cache key
     * @param string $reg   region type, valid values d (dimos),ep (eklogiki periferia),de (dimotiki enotita),et (eklogiko tmima)
     * @param string $slug
     * @return string
     */
    public function toRegionKey($reg, $slug) {
        return sprintf("r.%s.%s", $reg, $slug);
    }
    
    /**
     * Returns syndiasmos cache key
     * @param string $slug  syndiasmos slug
     * @return string
     */
    public function toSyndKey($slug) {
        return sprintf("s.%s", $slug);
    }
    
    /**
     * Returns cache key for ypopsifios
     * @param string $kSlug syndiasmos slug
     * @param string $ySlug ypopsifios slug
     * @return string
     */
    public function toYpoKey($kSlug, $ySlug) {
        return sprintf("y.%s.%s", $kSlug, $ySlug);
    }
    
    public function hasCache() {
        $out = false;
        if( file_exists($this->getCacheFilename()) ) {
            $out=true;
        }
        return $out;
    }
    
    public function hasKey($key) {
        if( isset($this->regionIndex[$key]) ) {
            return true;
        }
        return false;
    }
    
    public function getKey($key) {
        $out = null;
        if( $this->hasKey( $key )) {
            $out = $this->regionIndex[$key];
        }
        return $out;
    }
    
    public function setKey($key, $v) {
        $this->regionIndex[$key] = $v;
        return $this;
    }
    
    public function setKeySubValue($key, $idx, $v) {
        if( $this->hasKey($key) ) {
            if( is_array($this->regionIndex[$key])) {
                $this->regionIndex[$key][$idx]=$v;
            }
        }
        return $this;
    }

}
