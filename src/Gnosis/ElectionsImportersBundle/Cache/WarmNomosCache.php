<?php
namespace Gnosis\ElectionsImportersBundle\Cache;

use Gnosis\ElectionsBaseBundle\Util\RegionUtil;
use \Slx\MetronicBundle\Lib\GreekText;
/**
 * Description of WarmNomosCache
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 23 Απρ 2014
 */
class WarmNomosCache extends AbstractCache
{

    public function warm() {
        $this->loadRegions();
        $this->loadSyndiasmoi();
        $this->loadYpopsifioi();
        $this->cacheRegionIndex();
    }
    
    private function loadRegions() {
        $like = sprintf("'%s,%%'", $this->nomos->getIdPath());
        $res = $this->em->getConnection()->fetchAll("SELECT r.id, r.slug, r.id_path, r.parent_id, r.region, pr.slug as p_title FROM region r left join region pr on (r.parent_id=pr.id) WHERE r.id_path like ".$like);
        foreach($res as $re) {
            $slug = $re['slug']; //GreekText::slugify($re['title']);
            $va = array(
                'id' => $re['id'],
                'id_path' => $re['id_path'],
                'pids' => explode(',',$re['id_path']),
                'parent_id' => $re['parent_id'],
                'parent_slug' => $re['p_title'], //GreekText::slugify($re['p_title']),
            );
            $reg = RegionUtil::discriminatorToRegKey($re['region']);
            $rKey = $this->toRegionKey($reg, $slug);
            $this->setKey($rKey, $va);
        }
    }

    /**
     * φερνει όλους τους συνδυασμόυς που έχουν region=tin periferia ή αυτούς που έχουν region παιδί του νομού εργασίας
     * 
     */
    private function loadSyndiasmoi() {
        $like = sprintf("'%s,%%'", $this->nomos->getIdPath());
        $sql = "SELECT s.id, s.slug, s.region_id , r.slug as rslug
            FROM syndiasmos s 
            LEFT JOIN region r on (s.region_id=r.id)
            WHERE s.region_id in (select id from region where id_path like %s)
            or s.region_id=%s ";
        $sql = sprintf($sql, $like, $this->nomos->getParent()->getId());
        $res = $this->em->getConnection()->fetchAll($sql);
        foreach($res as $re) {
            $slug = $re['slug'];
            $hasRegionRows = $this->em->getRepository('GnosisElectionsBaseBundle:RegionPsifoi')->hasSyndiasmosRegionRows($re['id'],$this->nomosId);
            $va = array(
                'id' => $re['id'],
                'regionId' => $re['region_id'],
                'hasRegionRows' => $hasRegionRows,
                'regionSlug' => $re['rslug'],
            );
            $rKey = $this->toSyndKey($slug);
            $this->setKey($rKey, $va);
        }        
    }
    
    /**
     * φερνει όλους τους συνδυασμόυς που έχουν region=tin periferia ή αυτούς που έχουν region παιδί του νομού εργασίας
     * 
     */
    private function loadYpopsifioi() {
        $like = sprintf("'%s%%'", $this->nomos->getIdPath());
        $sql = "SELECT r.id, r.slug, r.region_id, r.komma_id, s.slug kslug
            FROM ypopsifios r 
            left join syndiasmos as s on (r.komma_id=s.id)
            WHERE r.region_id in (select id from region where id_path like %s)
            or r.region_id=%s ";
        $sql = sprintf($sql, $like, $this->nomos->getParent()->getId());
        $res = $this->em->getConnection()->fetchAll($sql);
        foreach($res as $re) {
            $hasRegionRows = $this->em->getRepository('GnosisElectionsBaseBundle:RegionStauroi')->hasYpopsifiosRegionRows($re['id'],$this->nomosId);
            $va = array(
                'id' => $re['id'],
                'region_id' => $re['region_id'],
                'hasRegionRows' => $hasRegionRows,
            );
            $rKey = $this->toYpoKey($re['kslug'], $re['slug']);
            $this->setKey($rKey, $va);
        }        
    }

}
