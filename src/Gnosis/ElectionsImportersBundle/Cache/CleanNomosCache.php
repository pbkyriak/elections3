<?php

namespace Gnosis\ElectionsImportersBundle\Cache;

/**
 * Description of CleanCache
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 23 Απρ 2014
 */
class CleanNomosCache extends AbstractCache
{
    public function clean() {
        if( file_exists($this->getCacheFilename()) ) {
            unlink($this->getCacheFilename());
        }
    }
}
