<?php

namespace Gnosis\ElectionsImportersBundle\Cleaners;

/**
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 16 Απρ 2014
 * 
 */
interface CleanerInterface
{
    public function __construct($em);
    public function setRegionId($regionId);
    public function clean();
}
