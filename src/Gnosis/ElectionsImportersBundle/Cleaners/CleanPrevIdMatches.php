<?php

namespace Gnosis\ElectionsImportersBundle\Cleaners;

/**
 * Description of CleanPrevIdMatches
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 24 Απρ 2014
 */
class CleanPrevIdMatches implements CleanerInterface
{
    /** @var EntityManager */
    protected $em;
    protected $eklogesId;
    
    public function __construct($manager)
    {
        $this->em = $manager;
        $this->eklogesId=0;

    }
    
    /**
     * Bad naming, pass elections Id 
     * @param integer $eklogesId
     * @return \Gnosis\ElectionsImportersBundle\Cleaners\CleanPrevIdMatches
     */
    public function setRegionId($eklogesId) {
        $this->eklogesId = $eklogesId;
        return $this;
    }
    
    public function clean() {
        $this->em->getConnection()->update('region', array('prev_id'=>0), array('ekloges_id'=>$this->eklogesId));
        $this->em->getConnection()->update('syndiasmos', array('prev_id'=>0), array('ekloges_id'=>$this->eklogesId));
    }
}
