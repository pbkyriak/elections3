<?php

namespace Gnosis\ElectionsImportersBundle\Cleaners;

use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsBaseBundle\Entity\RegionNomos;

/**
 * Description of CleanNomosResults
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class CleanNomosResults extends AbstractCleanerNomos
{
    
    public function clean() {
        $this->substractPsifoiFromPeriferia();
        $this->cleanRegionPsifoi();
        $this->substractStauroiFromPeriferia();
        $this->cleanRegionStauroi();
    }
    
    private function substractPsifoiFromPeriferia() {
        $sql = "update 
                    region as r1
                    left join region_psifoi as rp1 on (rp1.region_id=r1.id)
                    left join region_psifoi as rp2 on (r1.parent_id=rp2.region_id and rp1.syndiasmos_id=rp2.syndiasmos_id)
                set rp2.amount = rp2.amount-rp1.amount
                where r1.region='nomos' and r1.id=:nid";
        $this->em->getConnection()->executeQuery($sql, array('nid'=>$this->nomos->getId()));
    }
    
    private function cleanRegionPsifoi() {
        $sql = "update region_psifoi as rp 
                set amount=0
                where rp.region_id in (select id from region where id_path  like :idp)";
        $this->em->getConnection()->executeQuery($sql, array('idp'=>$this->nomos->getIdPath().'%'));
    }
    
    private function substractStauroiFromPeriferia() {
        $sql = "update 
                    region as r1
                    left join region_stauroi as rp1 on (rp1.region_id=r1.id)
                    left join region_stauroi as rp2 on (r1.parent_id=rp2.region_id and rp1.ypopsifios_id=rp2.ypopsifios_id)
                set rp2.amount = rp2.amount-rp1.amount
                where r1.region='nomos' and r1.id=:nid";
        $this->em->getConnection()->executeQuery($sql, array('nid'=>$this->nomos->getId()));
    }
    
    private function cleanRegionStauroi() {
        $sql = "update region_stauroi as rp 
                set amount=0
                where rp.region_id in (select id from region where id_path  like :idp)";
        $this->em->getConnection()->executeQuery($sql, array('idp'=>$this->nomos->getIdPath().'%'));
    }
}
