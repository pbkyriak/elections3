<?php

namespace Gnosis\ElectionsImportersBundle\Cleaners;

use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsBaseBundle\Entity\RegionNomos;

/**
 * Description of DropNomosData
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 9 Απρ 2014
 */
class DropNomosData extends AbstractCleanerNomos
{

    public function clean()
    {
        $this->substractFromPeriferia();
        $this->setNomosToZero();
        $this->dropNomosRegions();
    }

    private function substractFromPeriferia()
    {
        $perif = $this->nomos->getParent();
        $this->em->refresh($perif);
        $perif->setEggegramenoi($perif->getEggegramenoi() - $this->nomos->getEggegramenoi());
        $perif->setPsifisan($perif->getPsifisan() - $this->nomos->getPsifisan());
        $perif->setLeyka($perif->getLeyka() - $this->nomos->getLeyka());
        $perif->setAkyra($perif->getAkyra() - $this->nomos->getAkyra());
        $perif->setEgkyra($perif->getEgkyra() - $this->nomos->getEgkyra());
        $perif->setEtCount($perif->getEgkyra()-$this->nomos->getEtCount());
        $perif->setEtCounted($perif->getEgkyra()-$this->nomos->getEtCounted());
        $perif->setEggCounted($perif->getEggCounted()-$this->nomos->getEggCounted());
        $this->em->persist($perif);
        $this->em->flush();
    }

    private function setNomosToZero()
    {
        $this->nomos->setEggegramenoi(0);
        $this->nomos->setPsifisan(0);
        $this->nomos->setLeyka(0);
        $this->nomos->setAkyra(0);
        $this->nomos->setEgkyra(0);
        $this->nomos->setEtCount(0);
        $this->nomos->setEtCounted(0);
        $this->em->persist($this->nomos);
        $this->em->flush();
    }

    private function dropNomosRegions()
    {
        $this->em
            ->createQuery("DELETE FROM GnosisElectionsBaseBundle:Region r WHERE r.id_path LIKE :idp")
            ->execute(array('idp' => $this->nomos->getIdPath() . ',%'));
    }

}
