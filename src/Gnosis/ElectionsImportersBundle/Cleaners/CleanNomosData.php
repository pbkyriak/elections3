<?php

namespace Gnosis\ElectionsImportersBundle\Cleaners;

use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsBaseBundle\Entity\RegionNomos;

/**
 * CleanNomosData cleans Region data (eggegramenoi/psifisan klp).
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class CleanNomosData extends AbstractCleanerNomos
{
        
    public function clean() {
        $this->substractFromPeriferia();
        $this->setNomosToZero();
    }
    
    private function substractFromPeriferia() {
        $perif = $this->nomos->getParent();
        $this->em->refresh($perif);
        $perif->setEggegramenoi($perif->getEggegramenoi()-$this->nomos->getEggegramenoi());
        $perif->setPsifisan($perif->getPsifisan()-$this->nomos->getPsifisan());
        $perif->setLeyka($perif->getLeyka()-$this->nomos->getLeyka());
        $perif->setAkyra($perif->getAkyra()-$this->nomos->getAkyra());
        $perif->setEgkyra($perif->getEgkyra()-$this->nomos->getEgkyra());
        $perif->setEtCount($perif->getEtCount()-$this->nomos->getEtCount());
        $perif->setEtCounted($perif->getEtCounted()-$this->nomos->getEtCounted());
        $perif->setEggCounted($perif->getEggCounted()-$this->nomos->getEggCounted());
        $this->em->persist($perif);
        $this->em->flush();
    }
    
    private function setNomosToZero() {
        $this->em
            ->createQuery("UPDATE GnosisElectionsBaseBundle:Region r SET r.eggegramenoi=0, r.psifisan=0, r.leyka=0, r.akyra=0, r.egkyra=0, r.et_count=0, r.et_counted=0, r.egg_counted=0 WHERE r.id_path LIKE :idp")
            ->execute(array('idp'=>$this->nomos->getIdPath().'%'));
    }
}
