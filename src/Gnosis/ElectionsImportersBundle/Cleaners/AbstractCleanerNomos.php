<?php

namespace Gnosis\ElectionsImportersBundle\Cleaners;

use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsBaseBundle\Entity\RegionNomos;

/**
 * Description of AbstractCleanerNomos
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 16 Απρ 2014
 */
abstract class AbstractCleanerNomos implements CleanerInterface
{
    /** @var EntityManager */
    protected $em;
    /** @var RegionNomos */
    protected $nomos;
    protected $nomosId;
    
    public function __construct($manager)
    {
        $this->em = $manager;
        $this->nomosId=0;
        $this->nomos = null;
    }
    
    public function setRegionId($v) {
        $this->nomosId = $v;
        $this->nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->find($this->nomosId);
        $this->em->refresh($this->nomos);
        return $this;
    }
    
    abstract function clean();
}
