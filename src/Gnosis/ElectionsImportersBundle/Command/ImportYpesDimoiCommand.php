<?php

namespace Gnosis\ElectionsImportersBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Gnosis\ElectionsBaseBundle\Entity\YpesDimos;
use Doctrine\ORM\EntityManager;

/**
 * Description of ImportYpesDimoiCommand
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ImportYpesDimoiCommand extends ContainerAwareCommand
{
    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    
    protected function configure()
    {
        $this
            ->setName('elections:import:ypesdimoi')
            ->setDescription("Imports ypes dimoi list")
            ->addArgument('fn',
                InputArgument::REQUIRED,
                'file to import?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $t1 = time();
    
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $filename = $input->getArgument('fn');
        if( !file_exists($filename) ) {
            $output->writeln(sprintf("<error>File '%s' not found!</error>", $filename));
            return;
        }
        try {
        $this->em->getConnection()->query("SET autocommit=0;"); 
        $this->importDimoi($filename);
        $this->em->getConnection()->query("COMMIT;");
        }
        catch(\Exception $ex) {
            $this->em->getConnection()->query("ROLLBACK;");
            $output->writeln($ex->getMessage());
        }
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
    }   
    
    private function importDimoi($fn) {
        $handle = fopen($fn,"r");
        if( $handle ) {
            $cnt = 0;
            $this->em->getConnection()->query("delete from ypes_dimos");
            while($data = fgetcsv($handle, 2000, "\t")) {
                $cnt++;
                $title = preg_replace('/\s-\s/', '-', $data[2]);
                $rmp = new YpesDimos();
                $rmp->setOtaCode($data[0])
                        ->setOtaTitle($title)
                        ->setPeCode($data[3])
                        ->setPeTitle($data[4]);
                        $this->em->persist($rmp);
                $this->em->flush();
            }
            fclose($handle);
            
            $this->output->writeln(sprintf("Imported %s lines",$cnt));
        }

    }
    
}
