<?php
namespace Gnosis\ElectionsImportersBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Doctrine\ORM\EntityManager;
/**
 * Imports result files
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ImportCommand extends ContainerAwareCommand
{
    /** @var EntityManager */
    private $em;
    private $output;
    
    protected function configure()
    {
        $this
            ->setName('elections:import')
            ->setDescription("Imports data file")
            ->addArgument('eklogesSlug',
                InputArgument::REQUIRED,
                'ekloges to import?')
            ->addArgument('nomosSlug',
                InputArgument::REQUIRED,
                'nomos to import?')
            ->addArgument('fn',
                InputArgument::REQUIRED,
                'file to import?')
            ->addOption(
               'show-bar',
               null,
               InputOption::VALUE_NONE,
               'If it set shows progress bar'
            )
            ->addOption(
               'utf8-encoding',
               null,
               InputOption::VALUE_NONE,
               'If it set means that is in utf8 encoding otherwise texts will be converted from iso greek to utf8'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $t1 = time();
    
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        
        $eklogesSlug = $input->getArgument('eklogesSlug');
        $nomosSlug = $input->getArgument('nomosSlug');
        $filename = $input->getArgument('fn');
        $showProgressBar = $input->getOption('show-bar') ? true : false;
        $utf8Encoding = $input->getOption('utf8-encoding') ? true : false;
        
        $output->writeln("<info>Ekloges slug=</info>".$eklogesSlug);
        $output->writeln("<info>Nomos slug=</info>".$nomosSlug);
        $output->writeln("<info>File name=</info>".$filename);
        
        $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug'=>$eklogesSlug));
        if( !$ekloges ) {
            $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlug));
            return;
        }
        
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('election'=>$ekloges, 'slug'=>$nomosSlug));
        if( !$nomos ) {
            $output->writeln(sprintf("<error>No nomos found with slug %s</error>", $nomosSlug));
            return;
        }
        
        if( !file_exists($filename) ) {
            $output->writeln(sprintf("<error>File '%s' not found!</error>", $filename));
            return;
        }
        
        /* import jobs here */
        $output->writeln("<info>Importing...</info>");
        $this->insertData($nomos, $ekloges->getEtype(), $filename, $showProgressBar, $utf8Encoding);
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
    }
        
    /**
     * 
     * @param \Gnosis\ElectionsBaseBundle\Entity\RegionNomos $nomos
     * @param string $eklogesTypeSlug
     * @param string $filename
     * @param boolean $verbose
     */
    private function insertData($nomos, $eklogesTypeSlug, $filename, $verbose=false, $utf8Encoding=false) {
        $servId = 'gnosis_elections_importers.'.$eklogesTypeSlug.'_importer';
        if( $this->getContainer()->has($servId) ) {
            $this->output->writeln(sprintf("<info>importer service id:</info> %s", $servId));
            /* @var $imp \Gnosis\ElectionsImportersBundle\Importers\Importer */
            $imp = $this->getContainer()->get($servId);
            $imp->setup($nomos,$utf8Encoding);
            $progressBar = $this->setupProgressBar($filename,$verbose);
            $imp->process($filename, $progressBar); 
            if( $verbose ) {
                $progressBar->finish();
            }
            $this->printStats($imp->getExecTimes());
        }
        else {
            $this->output->writeln(sprintf("<error>No importer service defined for '%s' </error>", $eklogesTypeSlug));
        }
    }
    
    private function setupProgressBar($filename,$verbose) {
        $progress = null;
        if( $verbose ) {
            $lnCnt = $this->getFileLineCount($filename);
            $progress = $this->getHelperSet()->get('progress');
            $progress->start($this->output, $lnCnt);
            $progress->setRedrawFrequency(100);
            $progress->advance();
        }
        return $progress;
    }
    
    private function getFileLineCount($filename) {
        $results = shell_exec('wc -l '.$filename);
        $res = trim(str_replace($filename, '', $results));
        $out = (int)$res;
        return $out;
    }
    
    private function printStats($stats) {
        foreach($stats as $stat) {
            $this->output->writeln($stat);
        }
    }
    
    // TEST METHODS ************************************************************
    

    

}
