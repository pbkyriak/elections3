<?php
namespace Gnosis\ElectionsImportersBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;

/**
 * Description of CacheWarmupCommand
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 23 Απρ 2014
 */
class MatchPrevResetCommand extends ContainerAwareCommand
{
    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    
    protected function configure()
    {
        $this
            ->setName('elections:match:prev:reset')
            ->setDescription("Reset Match prev regions and syndyasmous")
            ->addArgument('eklogesSlug',
                InputArgument::REQUIRED,
                'current ekloges slug?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $t1 = time();
    
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        
        $eklogesSlug = $input->getArgument('eklogesSlug');
   
        $output->writeln("<info>Current Ekloges slug=</info>".$eklogesSlug);
        
        $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug'=>$eklogesSlug));
        if( !$ekloges ) {
            $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlug));
            return;
        }
                        
        /* import jobs here */
        $output->writeln("<info>Matching...</info>");
        $this->doMatch($ekloges);
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
    }
    
    /**
     * 
     * @param \Gnosis\ElectionsBaseBundle\Entities\Ekloges $cEkloges
     */
    private function doMatch(Ekloges $cEkloges) {        

        $this->em->getConnection()->update('region', array('prev_id'=>0), array('ekloges_id'=>$cEkloges->getId()));
        $this->em->getConnection()->update('syndiasmos', array('prev_id'=>0), array('ekloges_id'=>$cEkloges->getId()));
        $cEkloges->setPrevId(0);
        $this->em->persist($cEkloges);
        $this->em->flush();
    }
}