<?php
namespace Gnosis\ElectionsImportersBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Doctrine\ORM\EntityManager;

/**
 * Description of ImportMapPointsCommand
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 4 Μαϊ 2014
 */
class ImportSyndiasmosColorsCommand extends ContainerAwareCommand
{
    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    
    protected function configure()
    {
        $this
            ->setName('elections:import:syndiasmos-colors')
            ->setDescription("Imports colors for syndiasmous")
            ->addArgument('eklogesSlug',
                InputArgument::REQUIRED,
                'ekloges to import?')
            ->addArgument('fn',
                InputArgument::REQUIRED,
                'file to import?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $t1 = time();
    
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $filename = $input->getArgument('fn');
        $eklogesSlug = $input->getArgument('eklogesSlug');
        if( !file_exists($filename) ) {
            $output->writeln(sprintf("<error>File '%s' not found!</error>", $filename));
            return;
        }
        $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug'=>$eklogesSlug));
        if( !$ekloges ) {
            $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlug));
            return;
        }
        try {
        $this->em->getConnection()->query("SET autocommit=0;"); 
        $this->importSyndiasmosColors($filename, $ekloges->getId());
        $this->em->getConnection()->query("COMMIT;");
        }
        catch(\Exception $ex) {
            $this->em->getConnection()->query("ROLLBACK;");
            $output->writeln($ex->getMessage());
        }
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
    }   
    
    private function importSyndiasmosColors($filename, $eklogesId) {
        $handle = fopen($filename,"r");
        if( $handle ) {
            $cnt = 0;
            $this->em->getConnection()->delete('syndiasmos_color', array('ekloges_id'=>$eklogesId));
            while($data = fgetcsv($handle, 2000, ';')) {
                $cnt++;
                $rmp = new \Gnosis\ElectionsBaseBundle\Entity\SyndiasmosColor();
                $rmp->setTitle($data[0])
                    ->setColor($data[1])
                    ->setEklogesId($eklogesId)
                    ;
                $this->em->persist($rmp);
                $this->em->flush();
            }
            fclose($handle);
            
            $this->output->writeln(sprintf("Imported %s lines",$cnt));
        }
    }    
}
