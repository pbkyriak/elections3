<?php
namespace Gnosis\ElectionsImportersBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Doctrine\ORM\EntityManager;

/**
 * Description of ImportMapPointsCommand
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 4 Μαϊ 2014
 */
class FixEggCountedCommand extends ContainerAwareCommand
{
    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    
    protected function configure()
    {
        $this
            ->setName('elections:import:fix-egg-counted')
            ->setDescription("Resum eggegramenoi")
            ->addArgument('eklogesSlug',
                InputArgument::REQUIRED,
                'ekloges to import?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $t1 = time();
    
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $eklogesSlug = $input->getArgument('eklogesSlug');
        $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug'=>$eklogesSlug));
        if( !$ekloges ) {
            $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlug));
            return;
        }
        try {
        $this->em->getConnection()->query("SET autocommit=0;"); 
        $sums = $this->em->createQuery(
                "SELECT sum(r.eggegramenoi) egg, sum(r.egg_counted) eggc, sum(r.et_count) as etc, 
                    sum(r.et_counted) as etced, sum(r.psifisan) as psifisan, 
                    sum(r.leyka) as leyka, sum(r.akyra) as akyra, sum(r.egkyra) as egkyra
                    FROM GnosisElectionsBaseBundle:Region r WHERE r.level=6 and r.election = :ekl")
                ->setParameter('ekl', $ekloges)
                ->getScalarResult();
        $sums = $sums[0];
        $region = $this->em->getRepository('GnosisElectionsBaseBundle:RegionPeriferia')->findOneBy(array('election'=>$ekloges));
        print_R($sums);
        
        if( $region ) {
            $this->em->getConnection()->update(
                'region', 
                array(
                    'eggegramenoi'=>$sums['egg'], 
                    'egg_counted'=>$sums['eggc'],
                    'et_count'=>$sums['etc'],
                    'et_counted'=>$sums['etced'],
                    'psifisan'=> $sums['psifisan'],
                    'leyka'=> $sums['leyka'],
                    'akyra' => $sums['akyra'],
                    'egkyra' => $sums['egkyra'],
                    ), 
                array('id'=>$region->getId()));
        }
        $this->em->getConnection()->query("COMMIT;");
        }
        catch(\Exception $ex) {
            $this->em->getConnection()->query("ROLLBACK;");
            $output->writeln($ex->getMessage());
        }
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
    }       
}
