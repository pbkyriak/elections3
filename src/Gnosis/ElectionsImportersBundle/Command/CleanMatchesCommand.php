<?php
namespace Gnosis\ElectionsImportersBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Doctrine\ORM\EntityManager;

/**
 * Cleans prev ids from region and syndiasmos
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 24 Απρ 2014
 */
class CleanMatchesCommand extends ContainerAwareCommand
{
    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    
    protected function configure()
    {
        $this
            ->setName('elections:import:clean-matches')
            ->setDescription("Cleans prev ids from region and syndiasmos")
            ->addArgument('eklogesSlug',
                InputArgument::REQUIRED,
                'ekloges to use?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $t1 = time();
    
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
                
        $eklogesSlug = $input->getArgument('eklogesSlug');
        
        $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug'=>$eklogesSlug));
        if( !$ekloges ) {
            $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlug));
            return;
        }

        $cln = $this->getContainer()->get('gnosis_elections_importers.clean_prev_ids');
        $cln->setRegionId($ekloges->getId());
        $cln->clean();
        
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
    }   
    
}
