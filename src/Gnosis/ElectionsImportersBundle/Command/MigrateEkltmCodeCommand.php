<?php

namespace Gnosis\ElectionsImportersBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;
use Gnosis\ElectionsImportersBundle\Matchers\SyndiasmosApplyUserMatches;

/**
 * Description of MigrateEkltmCodeCommand
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Dec 16, 2016
 */
class MigrateEkltmCodeCommand extends ContainerAwareCommand {

    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;

    protected function configure() {
        $this
                ->setName('elections:migrate:ekltmcode')
                ->setDescription("Migrate: fill missing ekltm_code to ETs 16 dec 2016")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $t1 = time();

        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);

        $ets = $this->em->getConnection()->fetchAll("SELECT id, title FROM region WHERE region='eklTmima' AND ekltm_code=0");
        if (count($ets)) {
            $output->writeln(sprintf("<info>%s et need fix:</info>", count($ets)));
            try {
                $this->em->getConnection()->query("SET autocommit=0;");
                foreach ($ets as $et) {
                    $etCode = $this->extractEklTmCodeFromTitle($et['title']);
                    $this->em->getConnection()->update(
                            'region', array(
                        'ekltm_code' => $etCode,
                            ), array('id' => $et['id']));
                }
                $this->em->getConnection()->query("COMMIT;");
            } catch (\Exception $ex) {
                $this->em->getConnection()->query("ROLLBACK;");
                $output->writeln($ex->getMessage());
            }
        }
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time() - $t1));
    }

    private function extractEklTmCodeFromTitle($title) {
        $t = trim($title);
        $code = substr($t, 0, strpos($t, ' '));
        return (int)$code;
    }

}
