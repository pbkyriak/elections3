<?php
namespace Gnosis\ElectionsImportersBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

use Doctrine\ORM\EntityManager;

/**
 * Description of DropNomosDataCommand
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 23 Απρ 2014
 */
class DropNomosDataCommand extends ContainerAwareCommand
{
    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    
    protected function configure()
    {
        $this
            ->setName('elections:drop:nomos')
            ->setDescription("Drops nomos data (not just result)")
            ->addArgument('eklogesSlug',
                InputArgument::REQUIRED,
                'ekloges to use?')
            ->addArgument('nomosSlug',
                InputArgument::REQUIRED,
                'nomos to drop data?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $t1 = time();
    
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        
        $eklogesSlug = $input->getArgument('eklogesSlug');
        $nomosSlug = $input->getArgument('nomosSlug');

        $output->writeln("<info>Ekloges slug=</info>".$eklogesSlug);
        $output->writeln("<info>Nomos slug=</info>".$nomosSlug);
        
        $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug'=>$eklogesSlug));
        if( !$ekloges ) {
            $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlug));
            return;
        }
        
        $nomos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')->findOneBy(array('election'=>$ekloges, 'slug'=>$nomosSlug));
        if( !$nomos ) {
            $output->writeln(sprintf("<error>No nomos found with slug %s</error>", $nomosSlug));
            return;
        }
        /*
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Continue with this action? (y/n)', false);
        if (!$helper->ask($input, $output, $question)) {
            $output->writeln('Operation rejected.');
            return;
        }
         * 
         */
        /* import jobs here */
        $output->writeln("<info>Droping nomos results...</info>");
        $cl2 = $this->getContainer()->get('gnosis_elections_importers.clean_nomos_results');
        $cl2->setRegionId($nomos->getId());
        $cl2->clean(); 
        // drop nomos data
        $output->writeln("<info>Droping nomos data...</info>");
        $dr = $this->getContainer()->get('gnosis_elections_importers.drop_nomos_data');
        $dr->setRegionId($nomos->getId());
        $dr->clean();       
        $cmd = "delete from region_psifoi where region_id in ( select id from region where region.id_path like :idp);";
        $this->em->getConnection()->executeQuery($cmd, array('idp'=> $nomos->getIdPath().'%'));
        $cmd = "delete from ypopsifios where region_id= :idp;";
        $this->em->getConnection()->executeQuery($cmd, array('idp'=> $nomos->getId()));
        $cmd = "delete from region_stauroi where region_id in ( select id from region where region.id_path like :idp);";
        $this->em->getConnection()->executeQuery($cmd, array('idp'=> $nomos->getIdPath().'%'));
        // drop cache file
        $output->writeln("<info>Droping nomos cache file...</info>");
        $cacheCln = $this->getContainer()->get('gnosis_elections_importers.cache_nomos_clean');
        $cacheCln->setup($nomos);
        $cacheCln->clean();
        $output->writeln("<info>Done.</info>");
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
        
    }
}