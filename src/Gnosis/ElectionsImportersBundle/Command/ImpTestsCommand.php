<?php

namespace Gnosis\ElectionsImportersBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Slx\MetronicBundle\Lib\GreekText;

/**
 * Description of ImpTestsCommand
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 24 Απρ 2014
 */
class ImpTestsCommand extends ContainerAwareCommand {

    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;

    protected function configure() {
        $this
                ->setName('elections:import:test')
                ->setDescription("Playground command")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $t1 = time();

        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);

        //$this->testMatch();  
        $this->testFindOtaCode();

        $output->writeln(sprintf("<info>execution time:</info> %s sec", time() - $t1));
    }

    private function testMatch() {

        $cEkloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug' => 'perifereiakes_2014_v'));
        $pEkloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug' => 'perifereiakes_2014'));
        $m = new \Gnosis\ElectionsImportersBundle\Matchers\RegionMatcher($this->em);
        $m->setup($cEkloges, $pEkloges);
        $m->match();

        $m = new \Gnosis\ElectionsImportersBundle\Matchers\SyndiasmosMatcher($this->em);
        $m->setup($cEkloges, $pEkloges);
        $m->match();
    }

    private function testFindOtaCode() {
        $stmt = $this->em->getConnection()->executeQuery("select d.title dimos, n.title nomos from region d left join region n on (n.id=d.parent_id) where d.level =3");
        $dimoi = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $matcher = $this->getContainer()->get('gnosis_elections_matchers.ypes_ota_code_matcher');

        foreach ($dimoi as $dimos) {
            
            $res = $matcher->match($dimos['dimos'],$dimos['nomos']);
            if (!$res) {
                printf("d=%s n=%s ---------\n", $dimos['dimos'],$dimos['nomos']);
            }
        }
/*
        foreach ($dimoi as $dimos) {
            $dTitle = trim(preg_replace('/\s-\s/', '-', str_replace('Δήμος', '', $dimos['dimos'])));
            $dTitle = GreekText::toUpper($dTitle);
            $nTitle = GreekText::toUpper($dimos['nomos']);
            $found = false;

            $dTitle = strtoupper($dTitle);
            foreach ($ypesDimoi as $result) {
                if(levenshtein($nTitle,$result['pe_title'])<3) {
                    $dist = levenshtein($result['ota_title'], $dTitle);
                    //printf("(%s, %s)=%s\n", $dTitle, $result['ota_title'], $dist);
                    if ($dist < 3) {  // perfect
                        $found = true;
                        break;
                    }
                }
            }

            if (!$found) {
                printf("d=%s n=%s ---------\n", $dTitle, $nTitle);
            }
        }
 */       
        //ΜΑΝΤΟΥΔΙΟΥ-ΛΙΜΝΗΣ-ΑΓΙΑΣ ΑΝΝΑΣ
    }

}
