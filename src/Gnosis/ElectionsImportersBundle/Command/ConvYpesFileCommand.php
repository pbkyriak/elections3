<?php

namespace Gnosis\ElectionsImportersBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Gnosis\ElectionsBaseBundle\Entity\YpesDimos;
use Doctrine\ORM\EntityManager;
use Slx\MetronicBundle\Lib\GreekText;
/**
 * Description of ConvYpesFileCommand
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Nov 21, 2016
 */
class ConvYpesFileCommand extends ContainerAwareCommand
{
    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    const DELIMETER = "\t";
    
    protected function configure()
    {
        $this
            ->setName('elections:conv:ypesfile')
            ->setDescription("Imports ypes file to pste file")
            ->addArgument('fn',
                InputArgument::REQUIRED,
                'file to import?')
            ->addArgument('syfn',
                InputArgument::REQUIRED,
                'syndiasmoi file to import?')
            ->addArgument('outfn',
                InputArgument::REQUIRED,
                'file to export?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $t1 = time();
    
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $filename = $input->getArgument('fn');
        if( !file_exists($filename) ) {
            $output->writeln(sprintf("<error>File '%s' not found!</error>", $filename));
            return;
        }
        $syFilename = $input->getArgument('syfn');
        if( !file_exists($syFilename) ) {
            $output->writeln(sprintf("<error>File '%s' not found!</error>", $filename));
            return;
        }
        $outFilepath = $input->getArgument('outfn');
        
        $nomoi = $this->locateNomoiRows($filename);
        try {
            foreach($nomoi as $nomos => $rowLimits) {
                $outFilename = $outFilepath.'/'.$nomos.'.csv';
                printf("converting for nomos %s (%s)\n", $nomos,$outFilename);
                $this->convertFilePass1($filename, $syFilename, $outFilename,$rowLimits[0],$rowLimits[1]);
                $this->convertFilePass2($filename, $syFilename, $outFilename,$rowLimits[0],$rowLimits[1]);
                $this->convertFilePass3($filename, $syFilename, $outFilename,$rowLimits[0],$rowLimits[1]);
                $this->convertFilePass4($filename, $syFilename, $outFilename,$rowLimits[0],$rowLimits[1]);
            }
        }
        catch(\Exception $ex) {
            $output->writeln($ex->getMessage());
        }
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
    }   
    
    private function locateNomoiRows($fn) {
        $out = array();
        $inhandle = fopen($fn,"r");
        if( $inhandle ) {
            $lnCnt = 0;
            $currNomos = '';
            $stLine=1;
            while($data=fgetcsv($inhandle, 2000, ',')) {
                if( $lnCnt>0 ) {    // skip header line
                    if( $currNomos!=$data[0] ) {
                        if( $currNomos!='' ) {
                            $out[GreekText::slugify($currNomos)] = [$stLine, $lnCnt-1];
                            $stLine = $lnCnt;
                        }
                        $currNomos = $data[0];
                    }
                }
                $lnCnt++;
            }
            if( $stLine!=$lnCnt ) {
                $out[GreekText::slugify($currNomos)] = [$stLine, $lnCnt];
            }
            fclose($inhandle);
        }
        return $out;
    }

    private function formatEtTitle($data) {
        $out = $data[4];
        if( $data[5]=='Ε' || $data[5]=='Μ' || $data[5]=='Φ' ) {
            $out = $data[3].' '.$data[4];
        }
        return $out;
    }
    
    private function convertFilePass1($fn, $syfn, $outfn, $iRow, $fRow) {
        $syndiasmoi = $this->loadSyndiasmoi($syfn);
        $inhandle = fopen($fn,"r");
        if( $inhandle ) {
            $outhandle = fopen($outfn, "w+");
            if( $outhandle ) {
                $lnCnt=0;
                while($data = fgetcsv($inhandle, 2000, ',') ) {
                    if( $lnCnt==0 ) {
                        $lnCnt=1;
                        continue;
                    }
                    if( $lnCnt<$iRow) {
                        $lnCnt++;
                        continue;
                    }
                    $out=false;
                    if( $data[5]=='Κ' ) {
                        $out = array(
                            'type' => 'et',
                            'lv1' => $data[8],
                            'lv2' => $data[9],
                            'lv3' => $data[7],
                            'lv4' => $data[4],
                        );
                    }
                    elseif( $data[5]=='Ε' || $data[5]=='Μ' ) {
                        $out = array(
                            'type' => 'et',
                            'lv1' => 'Ετεροδημοτών',
                            'lv2' => 'Ετεροδημοτών',
                            'lv3' => 'Ετεροδημοτών',
                            'lv4' => $this->formatEtTitle($data),
                        );                        
                    }
                    elseif( $data[5]=='Φ' ) {
                        $out = array(
                            'type' => 'et',
                            'lv1' => 'Κρατουμένων',
                            'lv2' => 'Κρατουμένων',
                            'lv3' => 'Κρατουμένων',
                            'lv4' => $this->formatEtTitle($data),
                        );                                                
                    }
                    if($out) {
                        fputcsv($outhandle, $out, self::DELIMETER);
                    }
                    $lnCnt++;
                    if( $lnCnt>$fRow) {
                        break;
                    }
                }
                fclose($outhandle);
            }    
            fclose($inhandle);
        }

    }

    private function convertFilePass2($fn, $syfn, $outfn, $iRow, $fRow) {
        $inhandle = fopen($fn,"r");
        if( $inhandle ) {
            $outhandle = fopen($outfn, "a+");
            if( $outhandle ) {
                $lnCnt=0;
                while($data = fgetcsv($inhandle, 2000, ',') ) {
                    if( $lnCnt==0 ) {
                        $lnCnt=1;
                        continue;
                    }
                    if( $lnCnt<$iRow) {
                        $lnCnt++;
                        continue;
                    }

                    $out = array(
                        'type' => 'eg',
                        'et' => $this->formatEtTitle($data),
                        'eggegr' => $data[10],
                        'psifisan' => $data[11]+$data[12]+$data[13],
                        'leyka' => $data[13],
                        'akyra' => $data[12],
                        'egkyra' => $data[11],
                    );
                    fputcsv($outhandle, $out, self::DELIMETER);
                    $lnCnt++;
                    if( $lnCnt>$fRow) {
                        break;
                    }
                }
                fclose($outhandle);
            }    
            fclose($inhandle);
        }

    }
    
    private function convertFilePass3($fn, $syfn, $outfn, $iRow, $fRow) {
        $outhandle = fopen($outfn, "a+");
        if ($outhandle) {
            $syndiasmoi = $this->loadSyndiasmoi($syfn);
            foreach($syndiasmoi  as $sy) {
                $out = array(
                    'type' => 'sy',
                    'sy' => $sy,
                );
                fputcsv($outhandle, $out, self::DELIMETER);
            }
        }
    }
    
    private function convertFilePass4($fn, $syfn, $outfn, $iRow, $fRow) {
        $syndiasmoi = $this->loadSyndiasmoi($syfn);
        $syndiasmoiCount = count($syndiasmoi);
        $inhandle = fopen($fn,"r");
        if( $inhandle ) {
            $outhandle = fopen($outfn, "a+");
            if( $outhandle ) {
                $lnCnt=0;
                while($data = fgetcsv($inhandle, 2000, ',') ) {
                    if( $lnCnt==0 ) {
                        $lnCnt=1;
                        continue;
                    }
                    if( $lnCnt<$iRow) {
                        $lnCnt++;
                        continue;
                    }

                    for($i=0; $i<$syndiasmoiCount; $i++) {
                        $dataIdx = 14+$i;
                        if( isset($data[$dataIdx]) ) {
                            $out = array(
                                'type' => 'ps',
                                'et' => $this->formatEtTitle($data),
                                'sy' => $syndiasmoi[$i],
                                'ps' => $data[$dataIdx],
                            );
                            fputcsv($outhandle, $out, self::DELIMETER);
                        }
                    }
                    $lnCnt++;
                    if( $lnCnt>$fRow) {
                        break;
                    }
                }
                fclose($outhandle);
            }    
            fclose($inhandle);
        }
    }
    
    private function loadSyndiasmoi($syfn) {
        $syndiasmoi = array();
        $handle = fopen($syfn,"r");
        if( $handle ) {
            $lnCnt = 0;
            while($data=  fgetcsv($handle, 1000, ',') ) {
                if( $lnCnt>0 ) {
                $syndiasmoi[] = $data[1];
                }
                $lnCnt++;
            }
            fclose($handle);
        }
        return $syndiasmoi;
    }
    
}
