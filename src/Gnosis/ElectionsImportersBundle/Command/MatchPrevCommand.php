<?php
namespace Gnosis\ElectionsImportersBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsBaseBundle\Entity\Ekloges;
use Gnosis\ElectionsImportersBundle\Matchers\SyndiasmosApplyUserMatches;

/**
 * Description of CacheWarmupCommand
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 23 Απρ 2014
 */
class MatchPrevCommand extends ContainerAwareCommand
{
    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    
    protected function configure()
    {
        $this
            ->setName('elections:match:prev')
            ->setDescription("Match prev regions and syndyasmous")
            ->addArgument('eklogesSlug',
                InputArgument::REQUIRED,
                'current ekloges slug?')
/*
                ->addArgument('eklogesSlugPrev',
                InputArgument::REQUIRED,
                'prev ekloges slug?')
            ->addArgument('extraSyndMatches', InputArgument::OPTIONAL, 'a tab delimeted file with extra matches for syndiasmoys',null)
 *
 */
        ;
    }

    protected function executeFullExtra(InputInterface $input, OutputInterface $output)
    {
        $t1 = time();
    
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        
        $eklogesSlug = $input->getArgument('eklogesSlug');

        $eklogesSlugPrev = $input->getArgument('eklogesSlugPrev');
        $extraSyndMatchesFile = $input->getArgument('extraSyndMatches');
        
        $output->writeln("<info>Current Ekloges slug=</info>".$eklogesSlug);
        $output->writeln("<info>Previous Ekloges slug=</info>".$eklogesSlugPrev);
        $output->writeln("<info>Extra Synd Matches from=</info>".$extraSyndMatchesFile);
        
        $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug'=>$eklogesSlug));
        if( !$ekloges ) {
            $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlug));
            return;
        }
        
        $eklogesPrev = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug'=>$eklogesSlugPrev));
        if( !$ekloges ) {
            $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlugPrev));
            return;
        }
                
        /* import jobs here */
        $output->writeln("<info>Matching...</info>");
        $this->doMatch($ekloges, $eklogesPrev);
        $output->writeln("<info>Applying user matches...</info>");
        $extra = new SyndiasmosApplyUserMatches($this->em, $ekloges, $eklogesPrev);
        $extra->apply($extraSyndMatchesFile);
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $t1 = time();

        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);

        $eklogesSlug = $input->getArgument('eklogesSlug');
        $output->writeln("<info>Current Ekloges slug=</info>".$eklogesSlug);

        $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug'=>$eklogesSlug));
        if( !$ekloges ) {
            $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlug));
            return;
        }

        $eklogesPrev = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($ekloges->getPrevId());
        if( !$ekloges ) {
            $output->writeln(sprintf("<error>No ekloges found with id %s</error>", $ekloges->getPrevId()));
            return;
        }

        /* import jobs here */
        $output->writeln("<info>Matching...</info>");
        $this->doMatch($ekloges, $eklogesPrev);
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
    }

    /**
     * 
     * @param \Gnosis\ElectionsBaseBundle\Entities\Ekloges $cEkloges
     * @param \Gnosis\ElectionsBaseBundle\Entities\Ekloges $pEkloges
     */
    private function doMatch(Ekloges $cEkloges, Ekloges $pEkloges) {        
        $m = new \Gnosis\ElectionsImportersBundle\Matchers\RegionMatcher($this->em);
        $m->setup($cEkloges, $pEkloges);
        $m->match();
        
        $m = new \Gnosis\ElectionsImportersBundle\Matchers\SyndiasmosMatcher($this->em);
        $m->setup($cEkloges, $pEkloges);
        $m->match();
        
        $cEkloges->setPrevId($pEkloges->getId());
        $this->em->persist($cEkloges);
        $this->em->flush();
    }
}