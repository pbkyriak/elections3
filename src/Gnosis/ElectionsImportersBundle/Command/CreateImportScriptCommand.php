<?php

namespace Gnosis\ElectionsImportersBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Doctrine\ORM\EntityManager;

/**
 * Description of CreateImportScriptCommand
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 23 Απρ 2014
 */
class CreateImportScriptCommand extends ContainerAwareCommand
{
   /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    
    protected function configure()
    {
        $this
            ->setName('elections:generate:import-script')
            ->setDescription("Create import script. If no eklogesSlug defined it goes for all active ekloges.")
            ->addArgument('eklogesSlug',
                InputArgument::OPTIONAL,
                'ekloges to use?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        
        $eklogesSlug = $input->getArgument('eklogesSlug');
        $ekloges=null;
        $res=null;
        
        if( $eklogesSlug ) {
            $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug'=>$eklogesSlug));
            if( !$ekloges ) {
                $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlug));
                return;
            }
        }
        else {
            $res = $this->getActiveEkloges();
        }
        
        printf("#!/bin/bash\nstart_time=`date +%%s`\n");
        if( $res ) {
            foreach($res as $elect) {
                $this->createForEkloges($elect);
            }
        }
        if( $ekloges ) {
            $this->createForEkloges($ekloges);
        }
        printf("end_time=`date +%%s`\n");
        printf("echo Total execution time was `expr \$end_time - \$start_time` s.\n");        
    }    
        
    private function createForEkloges($ekloges) {
        printf("\n# ekloges: %s\n\n", $ekloges->getSlug());
        $nomoi= $this->getNomoi($ekloges);
        foreach($nomoi as $nomos) {
            if( !($nomos->getImportFile() && $nomos->getDownloadUrl()) ) {
                continue;
            }
            printf("\n# nomos: %s\n\n", $nomos->getSlug());
            $cmd = sprintf("rm %s/*",
                $nomos->getUploadsDir()
                );
            printf("%s\n", $cmd);
            if( strpos($nomos->getDownloadUrl(),'http')===0 ) {
                $cmd = sprintf("wget -O %s/a.zip %s",
                    $nomos->getUploadsDir(),
                    $nomos->getDownloadUrl()
                    );
                printf("%s\n", $cmd);
            }
            else {
                $cmd = sprintf("cp %s %s/a.zip",
                    $nomos->getDownloadUrl(),
                    $nomos->getUploadsDir()
                    );
                printf("%s\n", $cmd);                
            }
            $cmd = sprintf("unzip -o %s/a.zip -d %s/",
                $nomos->getUploadsDir(),
                $nomos->getUploadsDir()
                );
            printf("%s\n", $cmd);
            $cmd = sprintf("app/console elections:import %s %s %s/%s --show-bar",
                $ekloges->getSlug(),
                $nomos->getSlug(),
                $nomos->getUploadsDir(),
                $nomos->getImportFile()
                );
            printf("%s\n", $cmd);
        }
        $cmd = sprintf("app/console elections:map:color-results %s steraia-ellada2",
            $ekloges->getSlug()
            );
        printf("%s\n", $cmd);
    }
    
    private function getActiveEkloges() {
        $nomoi = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')
            ->findBy(array('active'=>true));
        return $nomoi;        
    }
    
    private function getNomoi($ekloges) {
        $nomoi = $this->em->getRepository('GnosisElectionsBaseBundle:RegionNomos')
            ->findBy(array('election'=>$ekloges));
        return $nomoi;
    }
}
