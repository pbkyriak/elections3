<?php
namespace Gnosis\ElectionsImportersBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Doctrine\ORM\EntityManager;

/**
 * Description of ImportMapPointsCommand
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 4 Μαϊ 2014
 */
class ImportMapPointsCommand extends ContainerAwareCommand
{
    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    
    protected function configure()
    {
        $this
            ->setName('elections:import:mappoints')
            ->setDescription("Imports map fill points for dimous")
            ->addArgument('fn',
                InputArgument::REQUIRED,
                'file to import?')
            ->addArgument('map',
                InputArgument::REQUIRED,
                'map name?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $t1 = time();
    
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $filename = $input->getArgument('fn');
        $map = $input->getArgument('map');
        if( !file_exists($filename) ) {
            $output->writeln(sprintf("<error>File '%s' not found!</error>", $filename));
            return;
        }

        $this->importRegionMapPoints($filename, $map);
        
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
    }   
    
    private function importRegionMapPoints($filename, $map) {
        $handle = fopen($filename,"r");
        if( $handle ) {
            $cnt = 0;
            while($data = fgetcsv($handle, 2000, ',')) {
                $cnt++;
                $rmp = new \Gnosis\ElectionsBaseBundle\Entity\RegionMapPoint();
                $rmp->setTitle($data[0])
                    ->setFillX($data[1])
                    ->setFillY($data[2])
                    ->setMapFile($map)
                    ;
                $this->em->persist($rmp);
                $this->em->flush();
            }
            fclose($handle);
            $this->output->writeln(sprintf("Imported %s lines",$cnt));
        }
        
    }    
}
