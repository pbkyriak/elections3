<?php

namespace Gnosis\ElectionsImportersBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsBaseBundle\Util\ColorMap;

/**
 * Description of ColorResultsMapCommand
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 9 Μαϊ 2014
 */
class ColorResultsMapCommand extends ContainerAwareCommand
{
    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    
    protected function configure()
    {
        $this
            ->setName('elections:map:color-results')
            ->setDescription("Colors results map for ekloges")
            ->addArgument('eklogesSlug',
                InputArgument::REQUIRED,
                'ekloges to import?')
            ->addArgument('map',
                InputArgument::REQUIRED,
                'map name?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $t1 = time();
    
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $eklogesSlug = $input->getArgument('eklogesSlug');
        $map = $input->getArgument('map');
        
        $ekloges = $this->em->getRepository('GnosisElectionsBaseBundle:Ekloges')->findOneBy(array('slug'=>$eklogesSlug));
        if( !$ekloges ) {
            $output->writeln(sprintf("<error>No ekloges found with slug %s</error>", $eklogesSlug));
            return;
        }

        $mapImage = sprintf("%s%s.png", $this->getContainer()->getParameter('mapFilesPath'), $map);
        if( !file_exists($mapImage) ) {
            $output->writeln(sprintf("<error>Map file %s does not exist</error>", $mapImage));
            return;            
        }
        $winners = $this->em->getRepository('GnosisElectionsBaseBundle:RegionPsifoi')->getWhosAheadPerDimoSlugs($ekloges->getId());
        $fillPoints = $this->em->getRepository('GnosisElectionsBaseBundle:RegionMapPoint')->findBy(array('mapfile'=>$map));
        $syndColors = $this->em->getRepository('GnosisElectionsBaseBundle:SyndiasmosColor')->findBy(array('ekloges_id'=>$ekloges->getId()));
        $winners = $this->matchRegionToFillPoints($winners, $fillPoints);
        //$winners = $this->matchSyndiasmosToColors($winners, $syndColors);
        $painter = new ColorMap($winners, $mapImage);
        $painter->saveImage(sprintf("%s/map.png", $ekloges->getUploadsDir()));
        
        $output->writeln(sprintf("<info>execution time:</info> %s sec", time()-$t1));
    }   

    /**
     * 
     * @param array $winners
     * @param array $fillPoints
     * @return array
     */
    private function matchRegionToFillPoints($winners, $fillPoints) {
        foreach($winners as $k => $v) {
            $fillPoint = $this->matchRegion($v['rslug'], $fillPoints);
            if( $fillPoint ) {
                $winners[$k]['fillX'] = $fillPoint->getFillX();
                $winners[$k]['fillY'] = $fillPoint->getFillY();
            }
            else {
                unset($winners[$k]);
            }
        }
        return $winners;
    }
    /**
     * 
     * @param string $rSlug
     * @param array $fillPoints
     * @return \Gnosis\ElectionsBase\Bundle\RegionMapPoint
     */
    private function matchRegion($rSlug, $fillPoints) {
        $out = null;
        foreach($fillPoints as $fillPoint) {
            $s1 = $fillPoint->getSlug();
            $d1 = levenshtein($rSlug, $s1);
            $s2 = 'dimos_'.$fillPoint->getSlug();
            $d2 = levenshtein($rSlug, $s2);
            if( $rSlug=='d_dimos_stylidas' ) {
                printf("slug=%s lev=%s %s lev2=%s %s\n", $rSlug, $d1, $s1, $d2, $s2);
            }
            if( $d1<3 || $d2<3 ) {
                $out = $fillPoint;
                break;
            }
        }
        return $out;
    }
    
    /**
     * 
     * @param array $winners
     * @param array $syndColors
     * @return array
     */
    private function matchSyndiasmosToColors($winners, $syndColors) {
        foreach($winners as $k => $v) {
            $color = $this->matchSyndiasmos($v['sslug'], $syndColors);
            if( $color ) {
                $winners[$k]['color'] = $color->getColor();
            }
            else {
                $winners[$k]['color'] = 'ffffff';
            }
        }
        return $winners;
    }

    /**
     * 
     * @param string $rSlug
     * @param array $syndColors
     * @return \Gnosis\ElectionsBase\Bundle\SyndiasmosColor
     */
    private function matchSyndiasmos($rSlug, $syndColors) {
        $out = null;
        foreach($syndColors as $color) {
            $s1 = $color->getSlug();
            $d1 = levenshtein($rSlug, $s1);
            if( $d1<3 ) {
                $out = $color;
                break;
            }
        }
        return $out;
    }
    
}
