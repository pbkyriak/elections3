<?php

namespace Gnosis\ElectionsImportersBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('GnosisElectionsImportersBundle:Default:index.html.twig', array('name' => $name));
    }
}
