<?php

namespace Gnosis\ElectionsImportersBundle\Matchers;
use Slx\MetronicBundle\Lib\GreekText;
use Doctrine\ORM\EntityManager;
/**
 * Description of OtaDimosMatcher
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class OtaDimosMatcher {

    /** @var Doctrine\ORM\EntityManager */
    protected $em;
    private $dimoi;
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $stmt = $this->em->getConnection()->executeQuery("select * from ypes_dimos");
        $this->dimoi = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
    
    public function match($dTitle, $nTitle) {
        $dTitle = trim(preg_replace('/\s-\s/', '-', str_replace('Δήμος', '', $dTitle)));
        $dTitle = GreekText::toUpper($dTitle);
        $nTitle = GreekText::toUpper($nTitle);
        $out = false;

        $dTitle = strtoupper($dTitle);
        foreach ($this->dimoi as $result) {
            if(levenshtein($nTitle,$result['pe_title'])<3) {
                $dist = levenshtein($result['ota_title'], $dTitle);
                if ($dist < 3) {  // perfect
                    $out = $result['ota_code'];
                    break;
                }
            }
        }

        return $out;
    }
}
