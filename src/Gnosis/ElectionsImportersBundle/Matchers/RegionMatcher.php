<?php

namespace Gnosis\ElectionsImportersBundle\Matchers;

use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsBaseBundle\Util\RegionUtil;
/**
 * Description of RegionMatcher
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 17 Απρ 2014
 */
class RegionMatcher extends AbstractMatcher
{

    public function match()
    {
        $this->matchLevel('Periferia', 0, 0);
    }

    private function matchLevel($regionLevel, $parentId1 = 0, $parentId2 = 0)
    {
        $r1 = $this->getRegions($this->cEkloges, $regionLevel, $parentId1);
        $r2 = $this->getRegions($this->pEkloges, $regionLevel, $parentId2);
        foreach ($r1 as $reg1) {
            $idx = $this->matchSlug($reg1['slug'], $r2);
            if ($idx !== false) {
                $this->updateRecord($regionLevel,$reg1['id'], $r2[$idx]['id']);
                $nextLevel = $this->getNextRegionLevel($regionLevel);
                if( $nextLevel ) {
                    $this->matchLevel($nextLevel, $reg1['id'], $r2[$idx]['id']);
                }
            }
        }
    }

    private function getNextRegionLevel($level)
    {
        $next= ucfirst(RegionUtil::getNextDiscriminator(lcfirst($level)));
        if( $next=='EklTmima') {
            $next = null;
        }
        return $next;
    }

    private function updateRecord($regionLevel,$cId, $pId) {
        $this->em->getConnection()->update('region', array('prev_id'=>$pId), array('id'=>$cId));
    }
    
    private function getRegions($election, $regionLevel, $parentId = 0)
    {
        $q = $this->em->createQueryBuilder()
            ->select("a.id, a.slug")
            ->from("GnosisElectionsBaseBundle:Region" . $regionLevel, "a")
            ->where("a.election=:e")
            ->setParameter('e', $election);
        if ($parentId) {
            $q->andWhere("a.parent=:pid")
                ->setParameter('pid', $parentId);
        }
        $r = $q->getQuery()->getArrayResult();
        return $r;
    }

}
