<?php

namespace Gnosis\ElectionsImportersBundle\Matchers;

/**
 * Description of SyndiasmosMatcher
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 17 Απρ 2014
 */
class SyndiasmosMatcher extends AbstractMatcher
{

    public function match()
    {
        $s1 = $this->getSyndiasmoi($this->cEkloges);
        $s2 = $this->getSyndiasmoi($this->pEkloges);
        foreach ($s1 as $syn1) {
            $idx = $this->matchSlug($syn1['slug'], $s2);
            if ($idx !== false) {
                $this->updateRecord($syn1['id'], $s2[$idx]['id']);
            }
        }
    }

    private function updateRecord($cId, $pId) {
        $this->em->getConnection()->update('syndiasmos', array('prev_id'=>$pId), array('id'=>$cId));
    }
    
    private function getSyndiasmoi($election)
    {
        $q = $this->em->createQueryBuilder()
            ->select("a.id, a.slug")
            ->from("GnosisElectionsBaseBundle:Syndiasmos", "a")
            ->where("a.election=:e")
            ->setParameter('e', $election);
        $r = $q->getQuery()->getArrayResult();
        return $r;
    }
}
