<?php
namespace Gnosis\ElectionsImportersBundle\Matchers;

use Doctrine\ORM\EntityManager;
/**
 * Description of AbstractMatcher
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 17 Απρ 2014
 */
abstract class AbstractMatcher
{

    /** @var Doctrine\ORM\EntityManager */
    protected $em;
    protected $cEkloges;
    protected $pEkloges;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function setup($cEkloges, $pEkloges)
    {
        $this->cEkloges = $cEkloges;
        $this->pEkloges = $pEkloges;
    }
    
    abstract function match();
    
    protected function matchSlug($slug, $choices)
    {
        $out = false;
        $minLev = strlen($slug);
        foreach ($choices as $idx => $ch) {
            $lev = levenshtein($slug, $ch['slug']);
            //printf("%s %s lev=%s minLev=%s\n", $slug,$ch['slug'], $lev, $minLev);
            if ($lev <= $minLev) {
                $out = $idx;
                $minLev = $lev;
            }
        }
        if ($minLev > 2) {   // an oi allages einai to poly 2 tote einai dekto apotelesma
            $out = false;
        }
        return $out;
    }
}
