<?php

namespace Gnosis\ElectionsImportersBundle\Matchers;

/**
 * Description of SyndiasmosApplyUserMatches
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class SyndiasmosApplyUserMatches {
    
    private $em;
    private $cEkloges;
    private $pEkloges;
    
    public function __construct($em, $cEkloges, $pEkloges) {
        $this->em  = $em;
        $this->cEkloges = $cEkloges;
        $this->pEkloges = $pEkloges;
    }
    
    public function apply($fn) {
        if(empty($fn)) {
            return;
        }
        $userMatches = $this->loadExtraSyndMatches($fn);
        if($userMatches) {
            foreach($userMatches as $cSyndSlug => $pSyndSlug) {
                $this->applyMatch($cSyndSlug, $pSyndSlug);
            }
        }
    }
    
    private function loadExtraSyndMatches($fn) {
        $out = array();
        $handle = fopen($fn, "r");
        if($handle) {
            while($data=fgetcsv($handle, 1000, "\t")) {
                $out[$data[0]]=$data[1];
            }
            fclose($handle);
        }
        return $out;
    }
    
    private function applyMatch($cSyndSlug, $pSyndSlug) {
        $cSynd = $this->em->getRepository("GnosisElectionsBaseBundle:Syndiasmos")->findOneBy(array('election'=>$this->cEkloges, 'slug'=>$cSyndSlug));
        $pSynd = $this->em->getRepository("GnosisElectionsBaseBundle:Syndiasmos")->findOneBy(array('election'=>$this->pEkloges, 'slug'=>$pSyndSlug));
        if($cSynd && $pSynd) {
            $this->em->getConnection()->update('syndiasmos', array('prev_id'=>$pSynd->getId()), array('id'=>$cSynd->getId()));
        }
    }
}
