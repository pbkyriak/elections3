<?php

namespace Gnosis\ElectionsImportersBundle\Importers;

use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsImportersBundle\Cache\RegionImportCache;
use Gnosis\ElectionsBaseBundle\Entity\RegionNomos;

/**
 * Description of AbstractLineProcessor
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 21 Απρ 2014
 */
abstract class AbstractLineProcessor implements LineProcessorInterface
{
    /** @var EntityManager */
    protected $em;
    /** @var RegionNomos */
    protected $nomos;
    /** @var RegionImportCache */
    protected $cacheLdr;
    
    protected $utf8Encoding = false;
    
    public function __construct(EntityManager $manager)
    {
        $this->em=$manager;
    }
    
    /**
     * 
     * @param RegionNomos $nomos
     * @param RegionImportCache $cacheLdr
     * @param boolean $utf8Encoding
     */
    public function setup(RegionNomos $nomos, RegionImportCache $cacheLdr, $utf8Encoding)
    {
        $this->nomos = $nomos;
        $this->cacheLdr = $cacheLdr;
        $this->utf8Encoding = $utf8Encoding;
    }
    
    abstract public function processLine($lineNum, $data);
    
    protected function fixEncoding($text) {
        if( !$this->utf8Encoding ) {
            $text = iconv("ISO-8859-7", "UTF-8",$text);
            $text = str_replace('’', 'A', $text);
        }
        return $text;
    }

}
