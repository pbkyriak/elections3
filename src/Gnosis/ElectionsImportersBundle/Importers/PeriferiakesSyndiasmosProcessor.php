<?php

namespace Gnosis\ElectionsImportersBundle\Importers;

/**
 * Description of PeriferiakesSyndiasmosProcessor
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 23 Απρ 2014
 */
class PeriferiakesSyndiasmosProcessor extends DefaultSyndiasmosProcessor
{
    
    /**
     * Sets if( $title=='ndiasmos regionId to perifereia
     * 
     * @param array $data
     * @return integer
     */
    protected function getSyndRegion($data) {
        return array($this->nomos->getParent()->getId(), $this->nomos->getParent()->getSlug());
    }
    
    protected function getSyndiasmosSlug($title)
    {
        $slug = $this->cacheLdr->getSyndiasmosSlug($title);
        return $slug;
    }
}
