<?php

namespace Gnosis\ElectionsImportersBundle\Importers;

use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsImportersBundle\Cache\RegionImportCache;
use Gnosis\ElectionsBaseBundle\Entity\RegionNomos;

/**
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 21 Απρ 2014
 * 
 */
interface LineProcessorInterface
{
    public function __construct(EntityManager $manager);
    public function setup(RegionNomos $nomos, RegionImportCache $cacheLdr, $doConvert);
    public function processLine($lineNum, $data);

}
