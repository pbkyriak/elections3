<?php

namespace Gnosis\ElectionsImportersBundle\Importers;

use Slx\MetronicBundle\Lib\GreekText;
/**
 * Description of DefaultPsifoiProcessor
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 22 Απρ 2014
 */
class DefaultPsifoiProcessor extends AbstractLineProcessor
{
    public function processLine($lineNum, $data)
    {
        $etSlug = GreekText::slugify($this->fixEncoding($data[0]));
        $syndSlug = $this->getSyndiasmosSlug($this->fixEncoding($data[1]));
        $psifoi = (int)$data[2];
        $pids = $this->getEtPIds($etSlug);
        if( $this->nomos->getId()==12 && $syndSlug=='laiki_syspeirosi' ) {
            $dimos = $this->em->getRepository('GnosisElectionsBaseBundle:RegionDimos')->find($pids[2]);
            $syndSlug = $syndSlug .'_'.$dimos->getSlug();
        }

        $syndId = $this->getSyndId($syndSlug);
        if( $pids && $syndId && $psifoi ) {
            $this->persistEt($pids, $syndId, $psifoi);
        }
    }
    
    protected function getSyndiasmosSlug($title) {
        $slug = $this->cacheLdr->getSyndiasmosSlug($title);
        return $slug;
    }
    
    private function getEtPIds($slug) {
        $out = null;
        $skey = $this->cacheLdr->toRegionKey('et', $slug);
        if( $this->cacheLdr->hasKey($skey)) {
            $out = $this->cacheLdr->getKey($skey)['pids'];
        }
        return $out;
    }
    
    private function getSyndId($slug) {
        $out = null;
        $skey = $this->cacheLdr->toSyndKey($slug);
        if( $this->cacheLdr->hasKey($skey)) {
            $out = $this->cacheLdr->getKey($skey)['id'];
        }
        return $out;
    }

    private function persistEt($etPids, $syndId, $amount) {
        $sql = "UPDATE region_psifoi SET amount=amount+%s WHERE region_id in (%s) and syndiasmos_id=%s";
        $pids = implode(',', $etPids);
        // den einai kalo etsi...
        $sql = sprintf($sql, $amount, $pids, $syndId);
        //fwrite($this->oHandle, sprintf("%s;\n",$sql));
        $this->em->getConnection()->query($sql);
    }
    
    private function persistEtNC($etPids, $syndId, $amount ) {
        $pids = implode(',', $etPids);
        $etId = end($etPids);
        reset($etPids);
        $sql = "set @am = ( select rp.amount from region_psifoi as rp left join region r on r.id=rp.region_id where r.id=%s and rp.syndiasmos_id=%s )";
        $sql = sprintf($sql, $etId, $syndId);
        
        $this->em->getConnection()->query($sql);        
        $sql = "UPDATE region_psifoi SET amount=amount-@am+%s WHERE region_id in (%s) and syndiasmos_id=%s";
        $sql = sprintf($sql, $amount, $pids, $syndId);
        $this->em->getConnection()->query($sql);        
    }
}
