<?php

namespace Gnosis\ElectionsImportersBundle\Importers;

/**
 * Description of EuroYpopsifiosProcessor
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 23 Απρ 2014
 */
class EuroYpopsifiosProcessor extends DefaultYpopsifiosProcessor
{
    
    /**
     * Sets ypopsifios region to periferia
     * 
     * @param array $data
     * @return integer
     */
    protected function getYpoRegionId($data)
    {
        return $this->nomos->getParent()->getId();
    }
}
