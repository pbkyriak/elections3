<?php

namespace Gnosis\ElectionsImportersBundle\Importers;

use Slx\MetronicBundle\Lib\GreekText;
/**
 * Description of DefaultEggegramenoiProcessor
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 22 Απρ 2014
 */
class DefaultEggegramenoiProcessor extends AbstractLineProcessor
{
    private $lineNum;
    
    public function processLine($lineNum, $data)
    {
        $this->lineNum = $lineNum;
        $etSlug = GreekText::slugify($this->fixEncoding($data[0]));
        $pids = $this->getEtPIds($etSlug);
        $this->updateRegion($pids, $data);
    }
    
    private function getEtPIds($slug) {
        $out = null;
        $skey = $this->cacheLdr->toRegionKey('et', $slug);
        if( $this->cacheLdr->hasKey($skey)) {
            $out = $this->cacheLdr->getKey($skey)['pids'];
        }
        return $out;
    }
    
    protected function updateRegion($etPIds, $data)
    {
        $params = array(
            'egg'=> (int)$data[1],
            'psi'=> (int)$data[2],
            'ley'=> (int)$data[3],
            'aky'=> (int)$data[4],
            'egy'=> (int)$data[5],
            'etc'=> 1,
            'etcd'=> (int)$data[2]!=0 ? 1 : 0,
            'eggc'=> (int)$data[2]!=0 ? (int)$data[1] : 0,
        );
        $sql = "UPDATE region SET eggegramenoi=eggegramenoi+:egg, psifisan=psifisan+:psi, leyka=leyka+:ley, akyra=akyra+:aky, egkyra=egkyra+:egy, et_count=et_count+:etc, et_counted=et_counted+:etcd, egg_counted=egg_counted+:eggc WHERE id in (%s)";
        try {
            $pids = implode(',', $etPIds);
        }
        catch(\Exception $ex ) {
            printf("linenum=%s\n", $this->lineNum);
            var_dump($etPIds);
        }
        // den einai kalo etsi...
        $sql = sprintf($sql, $pids);
        $this->em->getConnection()->executeQuery($sql, $params);
    }
}
