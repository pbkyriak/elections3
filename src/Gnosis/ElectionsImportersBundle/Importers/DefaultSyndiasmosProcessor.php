<?php

namespace Gnosis\ElectionsImportersBundle\Importers;

use Slx\MetronicBundle\Lib\GreekText;
use Gnosis\ElectionsBaseBundle\Util\RegionUtil;
/**
 * Description of DefaultSyndiasmosProcessor
 * ΠΡΟΣΟΧΗ! Εχουμε την υπόθεση ότι οι εγγραφές από τους πίνακες region_psifoi exoyn διαγραφεί!
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 21 Απρ 2014
 */
class DefaultSyndiasmosProcessor extends AbstractLineProcessor
{
    
    public function processLine($lineNum, $data)
    {
        $title = $this->fixEncoding($data[0]);
        $slug = $this->getSyndiasmosSlug($title);
        /* election 2014 hack
        if( $this->nomos->getSlug()=='voiotia' && $slug=='laiki_syspeirosi' ) {
            $slug = $slug .'_'.$regionSlug;
        }
        */
        list($regionId, $regionSlug) = $this->getSyndRegion($data);
        $sKey = $this->cacheLdr->toSyndKey($slug);
        if( !$this->cacheLdr->hasKey($sKey) ) {
            $this->persistSyndiasmos($title, $slug, $regionId, $regionSlug);
        }
        if( !$this->cacheLdr->getKey($sKey)['hasRegionRows'] ) {
            $this->buildRegionSyndResults($this->cacheLdr->getKey($sKey)['id']);
        }
    }

    protected function getSyndiasmosSlug($title) {
       $slug = $this->cacheLdr->getSyndiasmosSlug($title);
       return $slug;
    }
    
    protected function getSyndRegion($data) {
        $regionId = null;
        $regSlug = null;
        if( isset($data[1]) ) {
            $regSlug = 'd_'.GreekText::slugify($this->fixEncoding($data[1]));
            $rKey = $this->cacheLdr->toRegionKey('d', $regSlug);
            if( $this->cacheLdr->hasKey($rKey) ) {
                $regionId = $this->cacheLdr->getKey($rKey)['id'];
            }
        }
        return array($regionId, $regSlug);
    }
    
    protected function persistSyndiasmos($title, $slug, $regionId, $regionSlug) {
        $data = array(
            'title'=>$title,
            'slug'=>$slug,
            'region_id'=>$regionId,
            'ekloges_id'=>$this->nomos->getElection()->getId(),
        );
        $this->em->getConnection()->insert('syndiasmos', $data);
        $newId = $this->em->getConnection()->lastInsertId();
        $va = array('id'=>$newId, 'regionId'=>$regionId, 'hasRegionRows'=>false, 'regionSlug'=>$regionSlug);
        $this->cacheLdr->setKey($this->cacheLdr->toSyndKey($slug), $va);
        
    }
    
    protected function buildRegionSyndResults($syndId) {
        $this->insertPeriferiaRow($syndId);
        $this->insertRRRow($syndId, $this->nomos->getId());     // nomos
        $regKey = 'd';
        while( $regKey ) {
            foreach($this->cacheLdr->getKeysForKeyLike('r.'.$regKey) as $k ) {
                $this->insertRRRow($syndId, $this->cacheLdr->getKey($k)['id']);
            }
            $regKey = RegionUtil::getNextRegKey($regKey);
        }
    }
    
    protected function insertPeriferiaRow($syndId) {
        $syndiasmos = $this->em
            ->getRepository('GnosisElectionsBaseBundle:Syndiasmos')
            ->find($syndId);
        $row = $this->em
            ->getRepository('GnosisElectionsBaseBundle:RegionPsifoi')
            ->findOneBy(array('syndiasmos'=>$syndiasmos, 'region'=>$this->nomos->getParent()));
        if( !$row ) {
            $this->insertRRRow($syndId, $this->nomos->getParent()->getId());    // periferia
        }
    }
    
    protected function insertRRRow($syndId, $regId) {
        $data = array(
            'region_id' => $regId,
            'syndiasmos_id' => $syndId,
            'amount' => 0,
        );
        $this->em->getConnection()->insert('region_psifoi', $data);
    }
}
