<?php

namespace Gnosis\ElectionsImportersBundle\Importers;

/**
 * Κάνει την εισαγωγή του αρχείου. Του δίνουμε ston constructor τους κατάλληλους για την περίπτωση processors.
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class Importer
{

    const DELIMETER = "\t";
    /** @var HeaderProcessorInterface */
    protected $processors;
    protected $cleaners;
    /** @var \Doctrine\ORM\EntityManager */
    protected $em;
    /** @var \Gnosis\ElectionsImportersBundle\Cache\RegionImportCache */
    protected $cacheLdr;
    /** @var \Gnosis\ElectionsBaseBundle\Entity\RegionNomos */
    protected $nomos;
    protected $handle=null;
    private $times;
    private $progressBar;
    private $lineTypes;
    
    public function __construct($em)
    {
        $this->em=$em;
        $this->times = array('et'=>0, 'eg'=>0, 'yp'=>0, 'sy'=>0,'ps'=>0, 'st'=>0, 'cacheLoad'=>0); 
        $this->lineTypes = array('et', 'eg', 'yp', 'sy', 'ps', 'st');
    }
        
    /**
     * Set (inject) cache handler
     * @param \Gnosis\ElectionsImportersBundle\Cache\RegionImportCache $cc
     */
    public function setCacheLdr(\Gnosis\ElectionsImportersBundle\Cache\RegionImportCache $cc) {
        $this->cacheLdr = $cc;
    }
    
    /**
     * set (inject) processor for lineType. Valid line types are et, eg, yp, sy, ps, st
     * 
     * @param string $lineType
     * @param \Gnosis\ElectionsImportersBundle\Importers\LineProcessorInterface $processor
     */
    public function setProcessor($lineType, \Gnosis\ElectionsImportersBundle\Importers\LineProcessorInterface $processor) {
        if(in_array($lineType, $this->lineTypes)) {
            $this->processors[$lineType] = $processor;
        }
    }
    
    /**
     * Adds a clean to run before import
     * @param \Gnosis\ElectionsImportersBundle\Cleaners\CleanerInterface $cleaner
     */
    public function addCleaner(\Gnosis\ElectionsImportersBundle\Cleaners\CleanerInterface $cleaner) {
        $this->cleaners[] = $cleaner;
    }
    
    /**
     * Setups components (cache, processors and cleaners) for nomos
     * @param \Gnosis\ElectionsBaseBundle\Entities\RegionNomos $nomos
     * @param boolean $utf8Encoding
     */
    public function setup($nomos, $utf8Encoding) {
        $this->cacheLdr->setup($nomos);
        $this->nomos = $nomos;
        foreach($this->processors as $proc) {
            $proc->setup($nomos,$this->cacheLdr, $utf8Encoding);
        }        
        foreach($this->cleaners as $cln) {
            $cln->setRegionId($nomos->getId());
        }
    }

    /**
     * Process import file.
     * 
     * @param string $filename
     * @param type $progress
     */
    public function process($filename, $progress=null) {
        $this->progressBar = $progress;
        if( $this->openFile($filename) ) {
            $this->em->getConnection()->query("SET autocommit=0;"); 
            $this->runCleaners();
            $t1 = time();
            $this->cacheLdr->readCacheRegionIndex(true);
            $this->times['cacheLoad']= (time()-$t1);
            $lineNum=0;
            while($data=$this->getDataLine()) {
                $lineNum++;
                $lineType = array_shift($data);
                $this->processLine($lineNum, $lineType, $data);
                $this->updateUI();
            }
            $this->closeFile();
            $this->processors['st']->finalize();
            $this->cacheLdr->cacheRegionIndex();
            $this->em->getConnection()->query("COMMIT;");
            echo 'after commit';
        }
        else {
            printf("counld open file");
        }
    }

    /**
     * runs injected cleaners
     */
    private function runCleaners() {
        $t1 = time();
        foreach($this->cleaners as $cln) {
            $cln->clean();
        }
        $this->times['clean']= (time()-$t1);
    }
    
    /**
     * Actual line processing, using injected processors 
     * 
     * @param integer $lineNum  file line number 
     * @param string $lineType  line type (See valid line types)
     * @param array $data       line data
     */
    private function processLine($lineNum, $lineType,$data) {
        $t1 = time();
        if( isset($this->processors[$lineType]) ) {
            if(is_object($this->processors[$lineType])) {
                if( !($this->nomos->getId()==12 && in_array($lineType, array('st', 'yp')) )) {  // den pernaei sti biotia stis eklogesid=2 staurous kai psifous gia 2014
                    $this->processors[$lineType]->processLine($lineNum, $data);
                }
            }
        }        
        if( isset($this->times[$lineType]) ) {
            $this->times[$lineType]+= (time()-$t1);
        }
    }
    
    /**
     * opens input file
     * 
     * @param string $filename
     * @return boolean
     */
    private function openFile($filename) {
        $out = false;
        if(file_exists($filename)) {
            if( ($this->handle = fopen($filename,"r")) ) {
                $out = true;
            }
        }
        return $out;
    }
    
    /**
     * Reads line from csv file 
     * 
     * @return array
     */
    private function getDataLine() {
        $data = null;
        if( $this->handle ) {
            $data = fgetcsv($this->handle,0,self::DELIMETER);
        }
        return $data;
    }
    
    /**
     * closes input file
     * 
     */
    private function closeFile() {
        if( $this->handle ) {
            fclose($this->handle);
        }
    }
   
    /**
     * Returns an associative array with execution times
     * 
     * @return array
     */
    public function getExecTimes() {
        $out = array();
        foreach($this->times as $k => $time) {
            $out[] = sprintf("<info>imp %s = </info>%s sec", $k, $time);
        }
        return $out;
    }
    
    /**
     * updates progress bar (if any)
     */
    private function updateUI() {
        if( $this->progressBar ) {
            $this->progressBar->advance();
        }
    }
}
