<?php

namespace Gnosis\ElectionsImportersBundle\Importers;

use Slx\MetronicBundle\Lib\GreekText;
use Gnosis\ElectionsBaseBundle\Util\RegionUtil;
/**
 * Description of DefaultYpopsifiosProcessor
 * ΠΡΟΣΟΧΗ! Εχουμε την υπόθεση ότι οι εγγραφές από τους πίνακες region_stauroi exoyn διαγραφεί!
 * 
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 21 Απρ 2014
 */
class DefaultYpopsifiosProcessor extends AbstractLineProcessor
{

    public function processLine($lineNum, $data)
    {
        $syndSlug = $this->getSyndiasmosSlug($this->fixEncoding($data[0]));
        $title = $this->fixEncoding($data[1]);
        $slug = GreekText::slugify($title);
        $regionId = null;
        $kommaId = null;
        
        $ypKey = $this->cacheLdr->toYpoKey($syndSlug, $slug);
        if( !$this->cacheLdr->hasKey($ypKey) ) {    // den yparxei o ypopsifios
            $kommaId = $this->getKommaId($syndSlug);
            $regionId = $this->getYpoRegionId($data);
            if( $kommaId ) {
                $this->persistYpopsifios($title, $slug, $kommaId, $regionId, $ypKey);
            }
        }
        if( !$this->cacheLdr->getKey($ypKey)['hasRegionRows'] ) {
            $this->cacheLdr->setKeySubValue($ypKey, 'hasRegionRows', true);
            $this->buildRegionYpoResults($this->cacheLdr->getKey($ypKey)['id']);
        }
    }

    protected function getSyndiasmosSlug($title) {
        //return GreekText::slugify($title);
        $slug = $this->cacheLdr->getSyndiasmosSlug($title);
        return $slug;
    }
    protected function getKommaId($syndSlug) {
        $kommaId = null;
        $sKey = $this->cacheLdr->toSyndKey($syndSlug);
        if( $this->cacheLdr->hasKey($sKey) ) {
            $va = $this->cacheLdr->getKey($sKey);
            $kommaId = $va['id'];
        }
        return $kommaId;
    }
    
    protected function getKommaRegionId($data) {
        $syndSlug = $this->getSyndiasmosSlug($this->fixEncoding($data[0]));
        $regionId = null;
        $sKey = $this->cacheLdr->toSyndKey($syndSlug);
        if( $this->cacheLdr->hasKey($sKey) ) {
            $va = $this->cacheLdr->getKey($sKey);
            $regionId = $va['regionId'];
        }
        return $regionId;        
    }

    protected function getKommaRegionSlug($data) {
        $syndSlug = $this->getSyndiasmosSlug($this->fixEncoding($data[0]));
        $regionSlug = null;
        $sKey = $this->cacheLdr->toSyndKey($syndSlug);
        if( $this->cacheLdr->hasKey($sKey) ) {
            $va = $this->cacheLdr->getKey($sKey);
            $regionSlug = $va['regionSlug'];
        }
        return $regionSlug;        
    }

    protected function getYpoRegionId($data) {
        $out = null;
        $regSlug = null; 
        if( isset($data[2]) ) {
            $regSlug= $this->buildYpopsRegionSlug($data);
            $rtype = 'de';
        }
        if( $regSlug ) {
            $rKey = $this->cacheLdr->toRegionKey($rtype, $regSlug);
            if( $this->cacheLdr->hasKey($rKey) ) {
                $out = $this->cacheLdr->getKey($rKey)['id'];
            }
        }  
        if( !$out ) {
            $out = $this->getKommaRegionId($data);
        }
        return $out;
    }
    /**
     * Φτιαχνει το slug δ.ε. στην περίπτωση υποψηφίου που είναι σε τοπικο συμβούλιο.
     * Οταν υπάρχει στη γραμμή το data[2] είναι όταν ο υποψήφιος έχει locality διαφορετική του συνδυασμού του.
     * Αυτό συμβαίνει στην περίπτωση των τοπικών συμβούλων στις δημοτικές.
     * 
     * @param type $data
     * @return string|null
     */
    private function buildYpopsRegionSlug($data) {
        $de = GreekText::slugify($this->fixEncoding($data[2])); // de title slug, we need and dimos slug to make de slug
        $d = $this->getKommaRegionSlug($data); // dimos slug 
        $out = sprintf("%s_%s", $d, $de);   
        return $out;
    }
    
    private function persistYpopsifios($title, $slug, $kommaId, $regionId, $ypKey) {
        $data = array(
            'title'=>$title,
            'slug'=>$slug,
            'region_id'=>$regionId,
            'komma_id' => $kommaId,
            'ekloges_id'=>$this->nomos->getElection()->getId(),
        );
        $this->em->getConnection()->insert('ypopsifios', $data);
        $newId = $this->em->getConnection()->lastInsertId();
        $va = array('id'=>$newId, 'kommaId'=>$kommaId, 'regionId'=>$regionId, 'hasRegionRows'=>false);
        $this->cacheLdr->setKey($ypKey, $va);
    }
    
    protected function buildRegionYpoResults($ypoId) {
        $this->insertYpoPeriferiaRow($ypoId);
        $this->insertRYRRow($ypoId, $this->nomos->getId());     // nomos
        $regKey = 'd';
        while( $regKey ) {
            $regionKeys = $this->cacheLdr->getKeysForKeyLike('r.'.$regKey);
            foreach($regionKeys as $k ) {
                $this->insertRYRRow($ypoId, $this->cacheLdr->getKey($k)['id']);
            }
            $regKey = RegionUtil::getNextRegKey($regKey);
        }        
    }
    
    protected function insertYpoPeriferiaRow($ypoId) {
        //printf("a %s \n", $ypoId);
        $ypopsifios = $this->em
            ->getRepository('GnosisElectionsBaseBundle:Ypopsifios')
            ->find($ypoId);
        $row = $this->em
            ->getRepository('GnosisElectionsBaseBundle:RegionStauroi')
            ->findOneBy(array('ypopsifios'=>$ypopsifios, 'region'=>$this->nomos->getParent()));
        if( !$row ) {
            $this->insertRYRRow($ypoId, $this->nomos->getParent()->getId());    // periferia
        }
    }
    
    protected function insertRYRRow($ypoId, $regId) {
        $data = array(
            'region_id' => $regId,
            'ypopsifios_id' => $ypoId,
            'amount' => 0,
        );
        $this->em->getConnection()->insert('region_stauroi', $data);
    }
}
