<?php

namespace Gnosis\ElectionsImportersBundle\Importers;

use Slx\MetronicBundle\Lib\GreekText;
/**
 * Description of DefaultStauroiProcessor
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 22 Απρ 2014
 */
class DefaultStauroiProcessor extends AbstractLineProcessor
{
    private $queryList=array();
    
    public function processLine($lineNum, $data)
    {
        $etSlug = GreekText::slugify($this->fixEncoding($data[0]));
        $syndSlug = $this->getSyndiasmosSlug($this->fixEncoding($data[1]));
        $ypoSlug = GreekText::slugify($this->fixEncoding($data[2]));
        $amount = (int)$data[3];
        $pids = $this->getEtPIds($etSlug);
        $ypoId = $this->getYpoId($syndSlug, $ypoSlug);
        if( $pids && $ypoId && $amount ) {
            $this->persistEt($pids, $ypoId, $amount);
        }
    }
    
    protected function getSyndiasmosSlug($title) {
        $slug = $this->cacheLdr->getSyndiasmosSlug($title);
        return $slug;
    }
    
    private function getEtPIds($slug) {
        $out = null;
        $skey = $this->cacheLdr->toRegionKey('et', $slug);
        if( $this->cacheLdr->hasKey($skey)) {
            $out = $this->cacheLdr->getKey($skey)['pids'];
        }
        return $out;
    }
    
    private function getYpoId($syndSlug, $slug) {
        $out = null;
        $skey = $this->cacheLdr->toYpoKey($syndSlug, $slug);
        if( $this->cacheLdr->hasKey($skey)) {
            $out = $this->cacheLdr->getKey($skey)['id'];
        }
        return $out;
    }

    private function persistEt($etPids, $ypoId, $amount) {
        $pids = implode(',', $etPids);
        $sql = "UPDATE region_stauroi SET amount=amount+%s WHERE region_id in (%s) and ypopsifios_id=%s";
        $sql = sprintf($sql, $amount, $pids, $ypoId);
        $this->addQuery($sql);
    }
    
    private function addQuery($query) {
        $this->queryList[] = $query;
        if( count($this->queryList)>30 ) {
           $this->runQueryBatch();
        }
    }
    
    private function runQueryBatch() {
        $batch = implode(';', $this->queryList);
        $this->em->getConnection()->query($batch);
        $this->queryList = array();        
    }

    public function finalize() {
        if( count($this->queryList)>0 ) {
            $this->runQueryBatch();
        }
    }
}
