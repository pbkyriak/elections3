<?php

namespace Gnosis\ElectionsImportersBundle\Importers;

/**
 * Description of PeriferiakesYpopsifiosProcessor
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 23 Απρ 2014
 */
class PeriferiakesYpopsifiosProcessor extends DefaultYpopsifiosProcessor
{
    /**
     * Sets ypopsifios region to nomos
     * 
     * @param array $data
     * @return integer
     */
    protected function getYpoRegionId($data)
    {
        return $this->nomos->getId();
    }
    
    protected function getSyndiasmosSlug($title)
    {
        /*
        if( $title=='ΑΡΙΣΤΕΡΗ ΠΑΡΕΜΒΑΣΗ' ) $title = 'ΑΡΙΣΤΕΡΗ ΠΑΡΕΜΒΑΣΗ ΣΤΗ ΣΤΕΡΕΑ ΕΛΛΑΔΑ';
        if( $title=='ΑΛΛΑΖΟΥΜΕ' ) $title = 'αλλάζουμε Στην καρδιά της Ελλάδας';
        if( $title=='ΕΛΛΗΝΙΚΗ ΑΥΓΗ' ) $title = 'ΕΛΛΗΝΙΚΗ ΑΥΓΗ ΓΙΑ ΤΗΝ ΣΤΕΡΕΑ ΕΛΛΑΔΑ';
        if( $title=='ΕΝΕΡΓΟΙ ΠΟΛΙΤΕΣ' ) $title = 'ΕΝΕΡΓΟΙ ΠΟΛΙΤΕΣ ΣΤΕΡΕΑΣ';
        if( $title=='ΛΑΪΚΗ ΣΥΣΠΕΙΡΩΣΗ' ) $title = 'ΛΑΪΚΗ ΣΥΣΠΕΙΡΩΣΗ ΣΤΕΡΕΑΣ ΕΛΛΑΔΑΣ';
        if( $title=='ΝΕΑ ΤΡΟΧΙΑ' ) $title = 'Η Στερεά σε νέα τροχιά - Βαγγέλης Αποστόλου';
        if( $title=='ΣΥΜΦΩΝΙΑ' ) $title = 'ΘΑΝΑΣΗΣ ΓΙΑΝΝΟΠΟΥΛΟΣ ΣΥΜΦΩΝΙΑ ΓΙΑ ΤΗ ΣΤΕΡΕΑ';
         * 
         */
        $slug = parent::getSyndiasmosSlug($title);
        $synds = $this->cacheLdr->getKeysForKeyLike('s');
        return $slug;

    }
}
