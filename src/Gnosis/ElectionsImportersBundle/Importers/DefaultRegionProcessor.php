<?php

namespace Gnosis\ElectionsImportersBundle\Importers;

use Slx\MetronicBundle\Lib\GreekText;
use Gnosis\ElectionsBaseBundle\Util\RegionUtil;
use Gnosis\ElectionsBaseBundle\Entity\RegionNomos;
use Gnosis\ElectionsBaseBundle\Entity\RegionDimos;
use Gnosis\ElectionsBaseBundle\Entity\RegionEklPeriferia;
use Gnosis\ElectionsBaseBundle\Entity\RegionDimEnotita;
use Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima;
use Gnosis\ElectionsImportersBundle\Cache\RegionImportCache;
use Gnosis\ElectionsImportersBundle\Matchers\OtaDimosMatcher;
/**
 * Κάνει εισαγωγή γεωγραφικών περιοχών του νομού και ενημερώνει τους εγγεγραμμένους/ψηφισαντες κλπ μέχρι το επίπεδο της περιφέρειας.
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class DefaultRegionProcessor extends AbstractLineProcessor
{
    /** @var Gnosis\ElectionsImportersBundle\Matchers\OtaDimosMatcher */
    private $ypesMatcher;
    
    public function setup(RegionNomos $nomos, RegionImportCache $cacheLdr, $utf8Encoding)
    {
        parent::setup($nomos, $cacheLdr, $utf8Encoding);
        $this->rootPath = array( $this->nomos->getParent()->getId(),$this->nomos->getId());
        $this->seira = 1;
    }
    
    /**
     * Set OtaDimosMAtcheer, to use it to get YpEs dimoi codes
     * @param OtaDimosMatcher $matcher
     */
    public function setYpesMatcher($matcher) {
        $this->ypesMatcher = $matcher;
    }
    
    /**
     * Factory Method. Returns the entity matching $regKey
     * 
     * @param string $regKey
     * @return \Gnosis\ElectionsBaseBundle\Entity\RegionEklTmima|\Gnosis\ElectionsBaseBundle\Entity\RegionDimEnotita|\Gnosis\ElectionsBaseBundle\Entity\RegionEklPeriferia|\Gnosis\ElectionsBaseBundle\Entity\RegionDimos
     */
    protected function regionFactory($regKey) {
        switch ($regKey) {
            case 'd':
                return new RegionDimos();
            case 'ep':
                return new RegionEklPeriferia();
            case 'de':
                return new RegionDimEnotita();
            case 'et':
                return new RegionEklTmima();
        }
    }

    /*
     * Δήμος	Εκλογ.Περιφέρεια	Δημ/Τοπ. Κοινότητα	Ε.Τ.	Περιγραφή Εκλ.Τμήματος
     * 
     */
    public function processLine($lineNum, $data)
    {
       $dData = $this->cleanRawData($data);
       
       $this->checkRegion('d', $dData['d_slug'], $dData['d'], '', $data);
       $this->checkRegion('ep', $dData['ep_slug'], $dData['ep'], $dData['d_slug'], $data);
       $this->checkRegion('de', $dData['de_slug'], $dData['de'], $dData['ep_slug'], $data);
       $this->checkRegion('et', $dData['et_slug'], $dData['et'], $dData['de_slug'], $data);
       
    }

    
    /**
     * Helper function for processLine
     * @param array $data
     * @return array
     */
    private function cleanRawData($data) {
       $dData = array();
       $dData['d'] = $this->fixEncoding($data[0]);
       $dData['ep'] = $this->fixEncoding($data[1]);
       $dData['de'] = $this->fixEncoding($data[2]);
       $dData['et'] = $this->fixEncoding($data[3]); 
       
       $dData['d_slug'] = 'd_'.GreekText::slugify($dData['d']);
       $dData['ep_slug'] = 'ep_'.GreekText::slugify($dData['ep']);
       $dData['de_slug'] = sprintf("%s_%s",$dData['d_slug'],GreekText::slugify($dData['de']));
       $dData['et_slug'] = GreekText::slugify($dData['et']);

      return $dData;
    }
    
    /**
     * if region does not exist it adds it to the array 
     * 
     * @param type $reg
     * @param type $slug
     * @param type $title
     * @param type $egg
     * @param type $pslug
     */
    private function checkRegion($reg, $slug, $title, $pslug, $rawData) {
        $rKey = $this->cacheLdr->toRegionKey($reg,$slug);
       if( !$this->cacheLdr->hasKey($rKey)) {
           $ypesOtaId = '';
           if($reg=='d') {
               $ypesOtaId = $this->ypesMatcher->match($title, $this->nomos->getTitle());
           }
            $va = array(
               'title'=>$title, 
               'id'=>0, 
               'parent_slug'=>$pslug, 
               'parent_id'=>$this->getParentId($reg,$pslug),
               'pids'=>$this->getParentPath($reg, $pslug),
               'ypes_ota_id' => $ypesOtaId ? $ypesOtaId : '',
               'is_eterodim' => false,
               'ekltm_code' => 0,
               );
            if( $reg=='et' ) {
                $va['ekltm_code'] = $this->extractEklTmCodeFromTitle($title);
            }
            if( $reg=='et' && isset($rawData[4]) ) {
                $va['is_eterodim'] = $rawData[4]==1 ? true : false;
            }
            $this->cacheLdr->setKey($rKey, $va);
            $this->persistRegion($reg, $slug);
       } 
    }

    private function getParentId($regKey, $pslug) {
        if( $regKey=='d' ) {
            return $this->nomos->getId();
        }
        else {
            $rKey = $this->cacheLdr->toRegionKey(RegionUtil::getPrevRegKey($regKey),$pslug);
            return $this->cacheLdr->getKey($rKey)['id'];
        }        
    }
    
    private function getParentPath($regKey,$pslug) {
        if( $regKey=='d' ) {
            return $this->rootPath;
        }
        else {
            $rKey = $this->cacheLdr->toRegionKey(RegionUtil::getPrevRegKey($regKey),$pslug);
            return $this->cacheLdr->getKey($rKey)['pids'];
        }
    }
    
    private function persistRegion($regKey, $slug) {
        $rKey = $this->cacheLdr->toRegionKey($regKey, $slug);
        $v = $this->cacheLdr->getKey($rKey);
        $data = array(
            'region'=> RegionUtil::regKeyToDiscriminator($regKey),
            'title'=>$v['title'],
            'slug'=>$slug,
            'seira'=>$this->seira++,
            'id_path'=>implode(',',$v['pids']),
            'parent_id'=>$v['parent_id'],
            'ekloges_id'=>$this->nomos->getElection()->getId(),
            'eggegramenoi'=>0,
            'psifisan'=>0,
            'leyka'=>0,
            'akyra'=>0,
            'egkyra'=>0,
            'et_count'=>0,
            'et_counted'=>0,
            'ypes_ota_id' => $v['ypes_ota_id'],
            'is_eterodim' => $v['is_eterodim'],
            'ekltm_code' => $v['ekltm_code'],
        );
        $this->em->getConnection()->insert('region', $data);
        $newId = $this->em->getConnection()->lastInsertId();
        array_push($v['pids'], $newId);
        $this->em->getConnection()->update('region', 
            array(
                'id_path'=>implode(',',  $v['pids']),
                'level'=> count($v['pids']),
            ), 
            array('id'=>$newId)
            );    
        $v['id'] = $newId;
        $this->cacheLdr->setKey($rKey, $v);
    }
    
    public function persist() {
        
    }
        
    private function extractEklTmCodeFromTitle($title) {
        $t = trim($title);
        $code = substr($t, 0, strpos($t, ' '));
        return (int)$code;
    }
}
