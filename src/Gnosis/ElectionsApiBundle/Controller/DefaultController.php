<?php

namespace Gnosis\ElectionsApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('GnosisElectionsApiBundle:Default:index.html.twig', array('name' => $name));
    }
  
    public function ypopsSearchAction($ypName,$ypID) {
        
        if ( $ypID == 0)    //search by name
        {
            $sql="select yp.id as ypopsid,yp.title as ypopsname,ekl.title as eklogestitle,
                IF(SUBSTRING_INDEX(reg.slug,'_',1) = 'fthiotida','perifereiakes',IF(SUBSTRING_INDEX(reg.slug,'_',1) = 'top','topikes','dimotikes')) as eklogesslug,
                reg.title as regiontitle,syn.title as syndiasmos from ypopsifios as yp
                left join ekloges as ekl on ekl.id=yp.ekloges_id
                left join region as reg on reg.id=yp.region_id and reg.ekloges_id=ekl.id
                left join syndiasmos as syn on syn.id=yp.komma_id and syn.ekloges_id=ekl.id
                where yp.title LIKE :ypname and ekl.active=1
                group by ekl.title,yp.title
                order by ekl.title,yp.title;";
            $stmt = $this->getDoctrine()->getConnection()->executeQuery($sql,array('ypname'=>$ypName.'%'));
        }
        else    //search by id
        {
            $sql="select yp.id as ypopsid,yp.title as ypopsname,ekl.title as eklogestitle,
                IF(SUBSTRING_INDEX(reg.slug,'_',1) = 'fthiotida','perifereiakes',IF(SUBSTRING_INDEX(reg.slug,'_',1) = 'top','topikes','dimotikes')) as eklogesslug,
                reg.title as regiontitle,syn.title as syndiasmos from ypopsifios as yp
                left join ekloges as ekl on ekl.id=yp.ekloges_id
                left join region as reg on reg.id=yp.region_id and reg.ekloges_id=ekl.id
                left join syndiasmos as syn on syn.id=yp.komma_id and syn.ekloges_id=ekl.id
                where yp.id = :ypid and ekl.active=1
                group by ekl.title,yp.title
                order by ekl.title,yp.title;";
            $stmt = $this->getDoctrine()->getConnection()->executeQuery($sql,array('ypid'=>$ypID));
        }
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        //print_R($data);

        $results = array('error'=>'0', 'results'=>$data);
        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'text/json; charset=utf-8');
        return $response;
    }

        public function ypVoteDetPerifAction($ypID,$regType) {
        
        if ($regType == 0 ) //dimoi level
        {
            $sql="select reg.title as dimos,reg.id as regionID,st.amount as stauroi,reg.et_count as katamET, reg.et_counted as totET,round(100*reg.et_count/reg.et_counted,2) as ensomatosi, find_in_set(
                      st.amount, (
                          select group_concat(stm.amount order by stm.amount desc )
                                 from region_stauroi as stm
                                 left join region r on (r.id=stm.region_id) where r.region='dimos' and r.ekloges_id=3 and r.id=st.region_id
                )
                ) as rank,
                (
                     select sum(if(st22.amount!=0,1,0)) as getStauroiET
                     from region_stauroi st22
                     left join region r22 on (r22.id=st22.region_id)
                     where r22.ekloges_id=3 and r22.level=6  and st22.ypopsifios_id = :ypid and r22.id_path like concat(reg.id_path,'%')
                ) as getStauroiET
                from region_stauroi as st
                left join region as reg on reg.id=st.region_id where st.ypopsifios_id = :ypid and reg.region='dimos' and reg.ekloges_id=3;";
        }
        else    //nomos level
        {
            $sql="select reg.title as dimos,reg.id as regionID,st.amount as stauroi,reg.et_count as katamET, reg.et_counted as totET,round(100*reg.et_count/reg.et_counted,2) as ensomatosi, find_in_set(
                      st.amount, (
                          select group_concat(stm.amount order by stm.amount desc )
                                 from region_stauroi as stm
                                 left join region r on (r.id=stm.region_id) where r.region='nomos' and r.ekloges_id=3 and r.id=st.region_id
                )
                ) as rank,
                (
                     select sum(if(st22.amount!=0,1,0)) as getStauroiET
                     from region_stauroi st22
                     left join region r22 on (r22.id=st22.region_id)
                     where r22.ekloges_id=3 and r22.level=6  and st22.ypopsifios_id = :ypid and r22.id_path like concat(reg.id_path,'%')
                ) as getStauroiET
                from region_stauroi as st
                left join region as reg on reg.id=st.region_id where st.ypopsifios_id = :ypid and reg.region='nomos' and reg.ekloges_id=3;";
        }
        $stmt = $this->getDoctrine()->getConnection()->executeQuery($sql,array('ypid'=>$ypID));
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $results = array('error'=>'0', 'results'=>$data);
        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'text/json; charset=utf-8');
        return $response;
    }

    
    public function ypVoteDetDimSymbAction($ypID,$regType) {
        
        if ($regType == 0 ) //dim.enotita level
        {
            $sql="select reg.title as dimos,reg.id as regionID,st.amount as stauroi,reg.et_count as katamET, reg.et_counted as totET,round(100*reg.et_count/reg.et_counted,2) as ensomatosi, find_in_set(
                      st.amount, (
                          select group_concat(stm.amount order by stm.amount desc )
                                 from region_stauroi as stm
                                 left join region r on (r.id=stm.region_id) where r.region='eklperiferia' and r.ekloges_id=4 and r.id=st.region_id and st.amount<>0
                )
                ) as rank,
                (
                     select sum(if(st22.amount!=0,1,0)) as getStauroiET
                     from region_stauroi st22
                     left join region r22 on (r22.id=st22.region_id)
                     where r22.ekloges_id=4 and r22.level=6  and st22.ypopsifios_id = :ypid and r22.id_path like concat(reg.id_path,'%')
                ) as getStauroiET
                from region_stauroi as st
                left join region as reg on reg.id=st.region_id where st.ypopsifios_id = :ypid and reg.region='eklperiferia' and reg.ekloges_id=4 and st.amount<>0;";
            }
        else    //dimos level
        {
            $sql="select reg.title as dimos,reg.id as regionID,st.amount as stauroi,reg.et_count as katamET, reg.et_counted as totET,round(100*reg.et_count/reg.et_counted,2) as ensomatosi, find_in_set(
                      st.amount, (
                          select group_concat(stm.amount order by stm.amount desc )
                                 from region_stauroi as stm
                                 left join region r on (r.id=stm.region_id) where r.region='dimos' and r.ekloges_id=4 and r.id=st.region_id and st.amount<>0
                )
                ) as rank,
                (
                     select sum(if(st22.amount!=0,1,0)) as getStauroiET
                     from region_stauroi st22
                     left join region r22 on (r22.id=st22.region_id)
                     where r22.ekloges_id=4 and r22.level=6  and st22.ypopsifios_id = :ypid and r22.id_path like concat(reg.id_path,'%')
                ) as getStauroiET
                from region_stauroi as st
                left join region as reg on reg.id=st.region_id where st.ypopsifios_id = :ypid and reg.region='dimos' and reg.ekloges_id=4 and st.amount<>0;";
        }
        $stmt = $this->getDoctrine()->getConnection()->executeQuery($sql,array('ypid'=>$ypID));
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $results = array('error'=>'0', 'results'=>$data);
        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'text/json; charset=utf-8');
        return $response;
    }

        
    public function ypVoteDetTopSymbAction($ypID,$regType) {
        
        if ($regType == 0 ) //ekl.tmima level
        {
            $sql="select reg.title as dimos,st.amount as stauroi,reg.et_count as katamET, reg.et_counted as totET,round(100*reg.et_count/reg.et_counted,2) as ensomatosi, find_in_set(
                      st.amount, (
                          select group_concat(stm.amount order by stm.amount desc )
                                 from region_stauroi as stm
                                 left join region r on (r.id=stm.region_id) where r.region='ekltmima' and r.ekloges_id=4 and r.id=st.region_id and st.amount<>0
                )
                ) as rank,
                (
                     select sum(if(st22.amount!=0,1,0)) as getStauroiET
                     from region_stauroi st22
                     left join region r22 on (r22.id=st22.region_id)
                     where r22.ekloges_id=4 and r22.level=6  and st22.ypopsifios_id = :ypid and r22.id_path like concat(reg.id_path,'%')
                ) as getStauroiET
                from region_stauroi as st
                left join region as reg on reg.id=st.region_id where st.ypopsifios_id = :ypid and reg.region='ekltmima' and reg.ekloges_id=4 and st.amount<>0;";
            }
        else    //dim.enotita level
        {
            $sql="select reg.title as dimos,st.amount as stauroi,reg.et_count as katamET, reg.et_counted as totET,round(100*reg.et_count/reg.et_counted,2) as ensomatosi, find_in_set(
                      st.amount, (
                          select group_concat(stm.amount order by stm.amount desc )
                                 from region_stauroi as stm
                                 left join region r on (r.id=stm.region_id) where r.region='dimenotita' and r.ekloges_id=4 and r.id=st.region_id and st.amount<>0
                )
                ) as rank,
                (
                     select sum(if(st22.amount!=0,1,0)) as getStauroiET
                     from region_stauroi st22
                     left join region r22 on (r22.id=st22.region_id)
                     where r22.ekloges_id=4 and r22.level=6  and st22.ypopsifios_id = :ypid and r22.id_path like concat(reg.id_path,'%')
                ) as getStauroiET
                from region_stauroi as st
                left join region as reg on reg.id=st.region_id where st.ypopsifios_id = :ypid and reg.region='dimenotita' and reg.ekloges_id=4 and st.amount<>0;";
        }
        $stmt = $this->getDoctrine()->getConnection()->executeQuery($sql,array('ypid'=>$ypID));
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $results = array('error'=>'0', 'results'=>$data);
        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'text/json; charset=utf-8');
        return $response;
    }
    
    public function ypVoteAnalytPerifAction($ypID,$regionID) {
        
        $sql="select reg.title as dimos,reg.id as regionID,st.amount as stauroi,reg.et_count as katamET, reg.et_counted as totET,round(100*reg.et_count/reg.et_counted,2) as ensomatosi, find_in_set(
                  st.amount, (
                      select group_concat(stm.amount order by stm.amount desc )
                             from region_stauroi as stm
                             left join region r on (r.id=stm.region_id) where r.region='ekltmima' and r.ekloges_id=3 and r.id=st.region_id
            )
            ) as rank,
            (
                 select sum(if(st22.amount!=0,1,0)) as getStauroiET
                 from region_stauroi st22
                 left join region r22 on (r22.id=st22.region_id)
                 where r22.ekloges_id=3 and r22.level=6  and st22.ypopsifios_id = :ypid and r22.id_path like concat(reg.id_path,'%')
            ) as getStauroiET
            from region_stauroi as st
            left join region as reg on reg.id=st.region_id where st.ypopsifios_id = :ypid and reg.region='ekltmima' and reg.ekloges_id=3
            and reg.id_path like (select concat(id_path,'%') from region where id = :regionid);";
        $stmt = $this->getDoctrine()->getConnection()->executeQuery($sql,array('ypid'=>$ypID,'regionid'=>$regionID));
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $results = array('error'=>'0', 'results'=>$data);
        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'text/json; charset=utf-8');
        return $response;
    }

    public function ypVoteAnalytDimSymbAction($ypID,$regionID) {
        
        $sql="select reg.title as dimos,reg.id as regionID,st.amount as stauroi,reg.et_count as katamET, reg.et_counted as totET,round(100*reg.et_count/reg.et_counted,2) as ensomatosi, find_in_set(
                  st.amount, (
                      select group_concat(stm.amount order by stm.amount desc )
                             from region_stauroi as stm
                             left join region r on (r.id=stm.region_id) where r.region='ekltmima' and r.ekloges_id=4 and r.id=st.region_id and st.amount<>0
            )
            ) as rank,
            (
                 select sum(if(st22.amount!=0,1,0)) as getStauroiET
                 from region_stauroi st22
                 left join region r22 on (r22.id=st22.region_id)
                 where r22.ekloges_id=4 and r22.level=6  and st22.ypopsifios_id = :ypid and r22.id_path like concat(reg.id_path,'%')
            ) as getStauroiET
            from region_stauroi as st
            left join region as reg on reg.id=st.region_id where st.ypopsifios_id = :ypid and reg.region='ekltmima' and reg.ekloges_id=4 and st.amount<>0
            and reg.id_path like (select concat(id_path,'%') from region where id = :regionid);";
        $stmt = $this->getDoctrine()->getConnection()->executeQuery($sql,array('ypid'=>$ypID,'regionid'=>$regionID));
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $results = array('error'=>'0', 'results'=>$data);
        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'text/json; charset=utf-8');
        return $response;
    }

    public function ypVoteAnalytDimSymbAndroid1Action($ypID,$regionID) {
        
        $sql="select reg.title as dimos,reg.id as regionID,st.amount as stauroi,reg.et_count as katamET, reg.et_counted as totET,round(100*reg.et_count/reg.et_counted,2) as ensomatosi, find_in_set(
                  st.amount, (
                      select group_concat(stm.amount order by stm.amount desc )
                             from region_stauroi as stm
                             left join region r on (r.id=stm.region_id) where r.region='dimenotita' and r.ekloges_id=4 and r.id=st.region_id and st.amount<>0
            )
            ) as rank,
            (
                 select sum(if(st22.amount!=0,1,0)) as getStauroiET
                 from region_stauroi st22
                 left join region r22 on (r22.id=st22.region_id)
                 where r22.ekloges_id=4 and r22.level=5 and st22.ypopsifios_id = :ypid and r22.id_path like concat(reg.id_path,'%')
            ) as getStauroiET
            from region_stauroi as st
            left join region as reg on reg.id=st.region_id where st.ypopsifios_id = :ypid and reg.region='dimenotita' and reg.ekloges_id=4 and st.amount<>0
            and reg.id_path like (select concat(id_path,'%') from region where id = :regionid);";
        $stmt = $this->getDoctrine()->getConnection()->executeQuery($sql,array('ypid'=>$ypID,'regionid'=>$regionID));
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $results = array('error'=>'0', 'results'=>$data);
        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'text/json; charset=utf-8');
        return $response;
    }
    
    public function ypVoteAnalytDimSymbAndroid2Action($ypID,$regionID) {
        
        $sql="select reg.title as dimos,reg.id as regionID,st.amount as stauroi,reg.et_count as katamET, reg.et_counted as totET,round(100*reg.et_count/reg.et_counted,2) as ensomatosi, find_in_set(
                  st.amount, (
                      select group_concat(stm.amount order by stm.amount desc )
                             from region_stauroi as stm
                             left join region r on (r.id=stm.region_id) where r.region='ekltmima' and r.ekloges_id=4 and r.id=st.region_id and st.amount<>0
            )
            ) as rank,
            (
                 select sum(if(st22.amount!=0,1,0)) as getStauroiET
                 from region_stauroi st22
                 left join region r22 on (r22.id=st22.region_id)
                 where r22.ekloges_id=4 and r22.level=6 and st22.ypopsifios_id = :ypid and r22.id_path like concat(reg.id_path,'%')
            ) as getStauroiET
            from region_stauroi as st
            left join region as reg on reg.id=st.region_id where st.ypopsifios_id = :ypid and reg.region='ekltmima' and reg.ekloges_id=4 and st.amount<>0
            and reg.id_path like (select concat(id_path,'%') from region where id = :regionid);";
        $stmt = $this->getDoctrine()->getConnection()->executeQuery($sql,array('ypid'=>$ypID,'regionid'=>$regionID));
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $results = array('error'=>'0', 'results'=>$data);
        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'text/json; charset=utf-8');
        return $response;
    }
    public function ypVoteAnalytPerifAndroid1Action($ypID,$regionID) {
        
        $sql="select reg.title as dimos,reg.id as regionID,st.amount as stauroi,reg.et_count as katamET, reg.et_counted as totET,round(100*reg.et_count/reg.et_counted,2) as ensomatosi, find_in_set(
                  st.amount, (
                      select group_concat(stm.amount order by stm.amount desc )
                             from region_stauroi as stm
                             left join region r on (r.id=stm.region_id) where r.region='eklperiferia' and r.ekloges_id=3 and r.id=st.region_id and st.amount<>0
            )
            ) as rank,
            (
                 select sum(if(st22.amount!=0,1,0)) as getStauroiET
                 from region_stauroi st22
                 left join region r22 on (r22.id=st22.region_id)
                 where r22.ekloges_id=3 and r22.level=4 and st22.ypopsifios_id = :ypid and r22.id_path like concat(reg.id_path,'%')
            ) as getStauroiET
            from region_stauroi as st
            left join region as reg on reg.id=st.region_id where st.ypopsifios_id = :ypid and reg.region='eklperiferia' and reg.ekloges_id=3 and st.amount<>0
            and reg.id_path like (select concat(id_path,'%') from region where id = :regionid);";
        $stmt = $this->getDoctrine()->getConnection()->executeQuery($sql,array('ypid'=>$ypID,'regionid'=>$regionID));
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $results = array('error'=>'0', 'results'=>$data);
        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'text/json; charset=utf-8');
        return $response;
    }

    public function ypVoteAnalytPerifAndroid2Action($ypID,$regionID) {
        
        $sql="select reg.title as dimos,reg.id as regionID,st.amount as stauroi,reg.et_count as katamET, reg.et_counted as totET,round(100*reg.et_count/reg.et_counted,2) as ensomatosi, find_in_set(
                  st.amount, (
                      select group_concat(stm.amount order by stm.amount desc )
                             from region_stauroi as stm
                             left join region r on (r.id=stm.region_id) where r.region='dimenotita' and r.ekloges_id=3 and r.id=st.region_id and st.amount<>0
            )
            ) as rank,
            (
                 select sum(if(st22.amount!=0,1,0)) as getStauroiET
                 from region_stauroi st22
                 left join region r22 on (r22.id=st22.region_id)
                 where r22.ekloges_id=3 and r22.level=5 and st22.ypopsifios_id = :ypid and r22.id_path like concat(reg.id_path,'%')
            ) as getStauroiET
            from region_stauroi as st
            left join region as reg on reg.id=st.region_id where st.ypopsifios_id = :ypid and reg.region='dimenotita' and reg.ekloges_id=3 and st.amount<>0
            and reg.id_path like (select concat(id_path,'%') from region where id = :regionid);";
        $stmt = $this->getDoctrine()->getConnection()->executeQuery($sql,array('ypid'=>$ypID,'regionid'=>$regionID));
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $results = array('error'=>'0', 'results'=>$data);
        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'text/json; charset=utf-8');
        return $response;
    }
    
}
