<?php

namespace Gnosis\ElectionsApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use Gnosis\ElectionsExportsBundle\Encoder\Array2XML;
/**
 * Description of YpesController
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class YpesController extends Controller {

    public function ypesExportAction($eklogesId, $format) {
        $em = $this->getDoctrine()->getManager();
        $ekloges = $em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($eklogesId);
        if (!$ekloges) {
            $results = array('error'=>'101', 'message'=>'no ekloges');
        }
        else {
            $exporter = $this->get('gnosis_elections_exports.ypes_psifoi');
            $exporter->setup($ekloges);
            $cnt = $exporter->export();
            if ($cnt) {
                if($format=='xml') {
                    $results = $this->getFormatedResults($exporter);
                }
                else {
                    $results = $this->getFormatedResultsJson($exporter);
                }
            }
            else {
                $results = array('error'=>'102', 'message'=>'no results');
            }
        }
        if ($this->container->has('profiler')) {
            $this->container->get('profiler')->disable();
        }

        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'text/json; charset=utf-8');
        return $response;
    }
    
    private function getFormatedResults($exporter) {
        $out = array('error'=>'103', 'results'=>'Error in generating results');
        $data = $exporter->getExportedData();
        try {
            $xml = Array2XML::createXML('wsmodel', $data);
            $out = array('error'=>'0', 'results'=>$xml->saveXML());
        }
        catch(\Exception $ex) {
            $out = array('error'=>'103', 'message'=>'Error in generating results');
            
        }
        return $out;
    }
    
    private function getFormatedResultsJson($exporter) {
        $out = array('error'=>'103', 'results'=>'Error in generating results');
        $data = $exporter->getExportedData();
        try {
            $out = array('error'=>'0', 'results'=>$data);
        }
        catch(\Exception $ex) {
            $out = array('error'=>'103', 'message'=>'Error in generating results');
            
        }
        return $out;        
    }
    
    public function ypesEtSentAction($eklogesId, $otaId, $etNumber) {
        $em = $this->getDoctrine()->getManager();
        $ekloges = $em->getRepository('GnosisElectionsBaseBundle:Ekloges')->find($eklogesId);
        if (!$ekloges) {
            $results = array('error'=>'101', 'message'=>'no ekloges');
        }
        else {
            $dimos = $em->getRepository('GnosisElectionsBaseBundle:RegionDimos')->findOneBy(['election'=>$ekloges, 'ypes_ota_id'=>$otaId]);
            if(!$dimos) {
                $results = array('error'=>'102', 'message'=>'no ota');
            }
            else {
                $et = $em->getRepository('GnosisElectionsBaseBundle:RegionEklTmima')->getEklTmimaByNum($ekloges, $dimos->getParent(), $etNumber);
                if(!$et) {
                    $results = array('error'=>'103', 'message'=>'no et '. $dimos->getTitle().' '. $dimos->getParent()->getTitle(). ' ' .$etNumber);
                }
                else {
                    
                    $em->getConnection()->executeUpdate(
                            "UPDATE ypes_to_export SET sent_timestamp=:t WHERE et_id=:etid and sent_timestamp=0", 
                            array('t'=>time(), 'etid'=>$et->getId())
                            );
                    $results = array('error'=>'0', 'message'=>'updated '.$et->getId());
                }
            }
        }
        if ($this->container->has('profiler')) {
            $this->container->get('profiler')->disable();
        }

        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'text/json; charset=utf-8');
        return $response;
    }
}
