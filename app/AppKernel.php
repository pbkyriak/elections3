<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new APY\BreadcrumbTrailBundle\APYBreadcrumbTrailBundle(),
            new Gnosis\ElectionsBaseBundle\GnosisElectionsBaseBundle(),
            new Slx\UserBundle\SlxUserBundle(),
            new Slx\MetronicBundle\SlxMetronicBundle(),
            new Gnosis\ElectionsBundle\GnosisElectionsBundle(),
            new Gnosis\ElectionsAdminBundle\GnosisElectionsAdminBundle(),
            new Gnosis\ElectionsImportersBundle\GnosisElectionsImportersBundle(),
            new Gnosis\ElectionsApiBundle\GnosisElectionsApiBundle(),
            new Gnosis\ElectionsEntryBaseBundle\GnosisElectionsEntryBaseBundle(),
            new Slx\RabbitMqBundle\SlxRabbitMqBundle(),
            new Gnosis\ElectionsEntryBundle\GnosisElectionsEntryBundle(),
            new Craue\ConfigBundle\CraueConfigBundle(),
            new Gnosis\PraktikaBundle\GnosisPraktikaBundle(),
            new Gnosis\ProtokoloBundle\GnosisProtokoloBundle(),
            new Gnosis\ElectionsExportsBundle\GnosisElectionsExportsBundle(),
            new Gnosis\ElectionsImpAdminBundle\GnosisElectionsImpAdminBundle(),
            new Slx\MediaBundle\SlxMediaBundle(),
            new Gnosis\ElectionsPhoneBundle\GnosisElectionsPhoneBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
